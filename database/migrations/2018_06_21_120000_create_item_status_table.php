<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_status', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->integer('status_prev')->unsigned()->nullable();
            $table->tinyInteger('active')->default(1);
            $table->integer('user_id')->unsigned();
            $table->foreign('item_id')->references('id')->on('items');
            $table->foreign('status_id')->references('id')->on('status');
            $table->foreign('status_prev')->references('id')->on('status');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     
        Schema::dropIfExists('item_status');
    }
}
