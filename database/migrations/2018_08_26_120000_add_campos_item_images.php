<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposItemImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_images', function ($table) {
            $table->text('commentary')->nullable();
            $table->tinyInteger('rejected')->default(0);
            $table->integer('commentary_by')->unsigned()->nullable();
            $table->foreign('commentary_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_images', function ($table) {
            $table->dropForeign(['commentary_by']);
            $table->dropColumn(['commentary','commentary_by','rejected']);
        });
    }
}
