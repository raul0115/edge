<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->string('code',25);
            $table->text('description')->nullable();
            $table->integer('material_id')->unsigned();
            $table->integer('mounting_id')->unsigned();
            $table->integer('feature_id')->unsigned();
            $table->integer('size_id')->unsigned();
            $table->integer('group_id')->unsigned()->nullable();
            $table->integer('character_id')->unsigned();
            $table->integer('customer_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned();
            $table->string('sequence',20);
            $table->tinyInteger('active')->default(1);
            $table->foreign('material_id')->references('id')->on('materials');
            $table->foreign('mounting_id')->references('id')->on('mountings');
            $table->foreign('feature_id')->references('id')->on('features');
            $table->foreign('size_id')->references('id')->on('sizes');
            $table->foreign('group_id')->references('id')->on('groups');
            $table->foreign('character_id')->references('id')->on('characteres');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
        Schema::dropIfExists('items');
    }
}
