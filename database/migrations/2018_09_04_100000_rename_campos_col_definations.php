<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameCamposColDefinations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('col_definations', function ($table) {
            $table->renameColumn('license_coordinator', 'License_coordinator');
            $table->renameColumn('license', 'License');
            $table->renameColumn('designer', 'Designer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('col_definations', function ($table) {
            $table->renameColumn('License_coordinator', 'license_coordinator');
            $table->renameColumn('License', 'license');
            $table->renameColumn('Designer', 'designer');
        });
    }
}
