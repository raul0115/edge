<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharacteresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('characteres', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',2)->unique();
            $table->string('name');
            $table->tinyInteger('active')->default(1);
            $table->integer('license_id')->unsigned();
            $table->foreign('license_id')->references('id')->on('licenses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('characteres');
    }
}
