<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposItemStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_status', function ($table) {
            $table->dropForeign(['status_prev']);
            $table->text('commentary')->nullable();
            $table->foreign('status_prev')->references('id')->on('item_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_status', function ($table) {
            $table->dropForeign(['status_prev']);
            $table->foreign('status_prev')->references('id')->on('status');
            $table->dropColumn(['commentary']);
        });
    }
}
