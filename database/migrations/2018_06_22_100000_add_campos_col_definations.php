<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposColDefinations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('col_definations', function ($table) {
            $table->tinyInteger('license_coordinator')->default(1);
            $table->tinyInteger('license')->default(1);
            $table->tinyInteger('designer')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('col_definations', function ($table) {
            $table->dropColumn('license_coordinator');
            $table->dropColumn('license');
            $table->dropColumn('designer');
        });
    }
}
