<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {return view('welcome');});//for home page

Auth::routes();

//Dashboards
Route::get('/', 'HomeController@welcome')->name('welcome');
Route::get('/home', 'HomeController@welcome')->name('home');

Route::get('/getOrderDashboardData', 'HomeController@getOrderDashboardData');

//IMPORTANT THESE ROUTE SHOULD BE REMOVED AFTER DEVELOPMENT, AS IT CAN GIVE ADMIN RIGHTS TO USERS
//Route::get('/initialized', 'Initialized@index')->name('home');
//Route::get('usermanagement/createUser', 'UserController@createUser2');//REMOVE
//Route::get('test/{id?}', 'TriggerEventsController@triggerLicenseConditions');
//MailController
//Route::get('mail', 'MailController@send_mail');


//UserController
Route::get('usermanagement/users/{id?}', 'UserController@users');
Route::post('usermanagement/users/{id?}', 'UserController@users');
Route::post('usermanagement/fieldRightsSubmit', 'UserController@userFieldPermissionSubmit');
Route::post('usermanagement/assignFactorySubmit', 'UserController@assignFactorySubmit');
Route::post('usermanagement/assignSalesmenSubmit', 'UserController@assignSalesmenSubmit');

Route::get('usermanagement/userSettings/{id?}', 'UserController@userSettings');

Route::get('/edit-profile', 'UserController@editprofile');
Route::post('/edit-profile', 'UserController@saveeditprofile');

Route::get('/edit-password', 'UserController@resetPassword');
Route::post('/edit-password', 'UserController@submitNewPassword');

Route::post('usermanagement/deleteUsers', 'UserController@checkDeleteRequest');//delete user
Route::post('usermanagement/activateUsers', 'UserController@activateUsers');//delete user
Route::post('usermanagement/updateUsers', 'UserController@updateUsersRole');//update user role
Route::get('usermanagement/newUpdateUsers/{id?}', 'UserController@newUpdateUsers');

Route::get('usermanagement/group', 'UserController@groupManagemenet');
Route::post('usermanagement/groupSubmit', 'UserController@postGroupManagemenet');

Route::get('usermanagement/profile', 'UserController@profileManagemenet');

Route::get('usermanagement/display', 'UserController@displayManagemenet');
Route::post('usermanagement/displayRightsSubmit', 'UserController@userDisplayPermissionSubmit');

Route::post('usermanagement/createUser', 'UserController@createUser');

//obtener lista de factories
Route::get('usermanagement/factories', 'UserController@factories')->name('usermanagement.factories');

//TableController
Route::get('table/factories', 'FactoryController@index')->name('table.factory.index');
Route::post('table/factories', 'FactoryController@index')->name('table.factory.post');
Route::get('table/export_cos', 'ExportCoController@index')->name('table.exportco.index');
Route::post('table/export_cos', 'ExportCoController@index')->name('table.exportco.post');
Route::get('table/customers', 'CustomerController@index')->name('table.customer.index');
Route::post('table/customers', 'CustomerController@index')->name('table.customer.post');
Route::get('table/ports', 'PortController@index')->name('table.port.index');
Route::post('table/ports', 'PortController@index')->name('table.port.post');
Route::get('table/portsto', 'PortToController@index')->name('table.portto.index');
Route::post('table/portsto', 'PortToController@index')->name('table.portto.post');
Route::get('table/licenses', 'LicenseController@index')->name('table.license.index');
Route::post('table/licenses', 'LicenseController@index')->name('table.license.post');
Route::get('table/product_styles', 'ProductStyleController@index')->name('table.product_style.index');
Route::post('table/product_styles', 'ProductStyleController@index')->name('table.product_style.post');
Route::get('table/product_types', 'ProductTypeController@index')->name('table.product_type.index');
Route::post('table/product_types', 'ProductTypeController@index')->name('table.product_type.post');
Route::get('table/freight_forwarders', 'FreightForwarderController@index')->name('table.freight_forwarder.index');
Route::post('table/freight_forwarders', 'FreightForwarderController@index')->name('table.freight_forwarder.post');
Route::get('table/salesmen', 'SalesmenController@index')->name('table.salesmen.index');
Route::post('table/salesmen', 'SalesmenController@index')->name('table.salesmen.post');
Route::get('table/materials', 'MaterialController@index')->name('table.material.index');
Route::post('table/materials', 'MaterialController@index')->name('table.material.post');
Route::get('table/sizes', 'SizeController@index')->name('table.size.index');
Route::post('table/sizes', 'SizeController@index')->name('table.size.post');
Route::get('table/features', 'FeatureController@index')->name('table.feature.index');
Route::post('table/features', 'FeatureController@index')->name('table.feature.post');
Route::get('table/characteres', 'CharacterController@index')->name('table.character.index');
Route::post('table/characteres', 'CharacterController@index')->name('table.character.post');
Route::get('table/mounting', 'MountingController@index')->name('table.mounting.index');
Route::post('table/mounting', 'MountingController@index')->name('table.mounting.post');

//TableController
Route::get('tables', 'TableController@index');
Route::post('tables', 'TableController@index');
        
//OrderController
Route::get('orders/closed', 'OrderController@showClosedOrders');
Route::get('orders/open', 'OrderController@showOpenOrders');
Route::post('orders/open', 'OrderController@showOpenOrders');
Route::post('orders/movetoclosed/{id}', 'OrderController@move_to_closed');
Route::post('orders/movetoopen/{id}', 'OrderController@move_to_open');
Route::post('orders/delete/{id}', 'OrderController@delete_order');
Route::get('orders/settings', 'OrderController@showOrdersSettings');
Route::post('orders/settings', 'OrderController@showOrdersSettings');
Route::post('ordermanagement/startNewOrder', 'OrderController@startNewOrder');
Route::post('ordermanagement/submitDaysSafetyTestOn', 'OrderController@submitDaysSafetyTestOn');
Route::post('ordermanagement/submitOrderDisplayLimits', 'OrderController@submitOrderDisplayLimits');
Route::get('orders/search', 'SearchController@searchOrders');
Route::post('orders/search', 'SearchController@searchOrders');


Route::post('orders/submit_text_fields', 'OrderController@submit_text_fields');
Route::post('orders/submit_textarea_fields', 'OrderController@submit_textarea_fields');
Route::post('orders/submit_date_fields', 'OrderController@submit_date_fields');
Route::post('orders/submit_yes_no_fields', 'OrderController@submit_yes_no_fields');
Route::post('orders/submit_pass_fail_fields', 'OrderController@submit_pass_fail_fields');
Route::post('orders/submit_numeric_fields', 'OrderController@submit_numeric_fields');
Route::post('orders/submit_cbm_field', 'OrderController@submit_cbm_field');
Route::post('orders/submit_fty_produc_field', 'OrderController@submit_fty_produc_field');
Route::post('orders/submit_export_cos_field', 'OrderController@submit_export_cos_field');
Route::post('orders/submit_product_styles_field', 'OrderController@submit_product_styles_field');
Route::post('orders/submit_product_types_field', 'OrderController@submit_product_types_field');
Route::post('orders/submit_licenses_field', 'OrderController@submit_licenses_field');
Route::post('orders/submit_customers_field', 'OrderController@submit_customers_field');
Route::post('orders/submit_tickets_ordered_field', 'OrderController@submit_tickets_ordered_field');
Route::post('orders/submit_portsto_field', 'OrderController@submit_portsto_field');
Route::post('orders/submit_ports_field', 'OrderController@submit_ports_field');
Route::post('orders/submit_svn_field', 'OrderController@submit_svn_field');
Route::post('orders/submit_freight_forwarders_field', 'OrderController@submit_freight_forwarders_field');
Route::post('orders/submit_salesmen_field', 'OrderController@submit_salesmen_field');


Route::get('orders/obtener_fama/{id}', 'OrderController@obtener_fama');
Route::get('orders/obtener_arrival', 'OrderController@obtener_arrival');
Route::get('orders/validar_customer_cancel', 'OrderController@validar_customer_cancel');
Route::get('orders/obtener_test_on', 'OrderController@obtener_test_on');
Route::post('orders/validarLicencias', 'OrderController@validarLicencias');
Route::post('orders/validarCBM', 'OrderController@validarCBM');
Route::post('orders/validarETD', 'OrderController@validarETD');
Route::post('orders/validarCustomers', 'OrderController@validarCustomers');
Route::get('orders/create', 'OrderController@createOrder');
Route::post('orders/store', 'OrderController@storeOrder');
Route::get('orders/{id}', 'OrderController@editOrders');
Route::post('orders/{id}', 'OrderController@updateOrders');

//NotificationsController
Route::get('notifications', 'NotificationsController@notificationsPage');
Route::post('notifications/delete_notifications', 'NotificationsController@delete_notifications');
Route::get('notifications/unreadNotificationsTopMenu', 'NotificationsController@unreadNotificationsTopMenu');
Route::get('notifications/notification_schedule', 'NotificationsController@checkNotificationSchedule');

//Permission Controller

Route::get('checkUserEditPermissions', 'PermissionController@checkUserEditPermissions');
Route::get('checkUserDispalyPermissions', 'PermissionController@checkUserDispalyPermissions');

// item
Route::get('items', 'ItemController@index')->name('item.index');

Route::get('items/create', 'ItemController@create')->name('item.create');
Route::post('items/getGroups', 'ItemController@get_groups')->name('item.get_groups');
Route::post('items/store', 'ItemController@store')->name('item.store');
Route::get('items/obtener_character/{id?}', 'ItemController@obtener_character');
Route::get('items/basic-data','ItemController@getBasicObjectData')->name('item.basic-data');
Route::get('items/edit/{id}', 'ItemController@edit')->name('item.edit');
Route::post('items/submit_edit/{id}', 'ItemController@submit_edit')->name('item.submit_edit');
Route::post('items/loadImagen/{id}', 'ItemController@load_imagen')->name('item.load_imagen');
Route::post('items/{id}/imgDescription', 'ItemController@img_description')->name('item.img_description');
Route::get('items/{id}/img', 'ItemController@view_img')->name('item.view_img');
Route::get('items/{id}/sendLicense', 'ItemController@send_license')->name('item.send_license');
Route::get('items/{id}/sendForSampling', 'ItemController@send_for_sampling')->name('item.send_for_sampling');
Route::post('items/{id}/sendDesigner', 'ItemController@send_designer')->name('item.send_designer');
Route::post('items/{id}/changeStatusCommentary', 'ItemController@change_status_commentary')->name('item.change_status_commentary');

//group

Route::get('groups', 'GroupController@index')->name('group.index');
Route::get('groups/create', 'GroupController@create')->name('group.create');
Route::post('groups/store', 'GroupController@store')->name('group.store');
Route::post('groups/getItems', 'GroupController@get_items')->name('group.get_items');
Route::get('groups/basic-data','GroupController@getBasicObjectData')->name('group.basic-data');
Route::get('groups/details-data/{id}','GroupController@getDetailsData')->name('group.details-data');
Route::post('groups/{id}/item/{id2}', 'GroupController@delete_item')->name('group.delete_item');
Route::get('groups/{id}/groupItems','GroupController@get_group_items')->name('group.group_items');
Route::post('groups/{id}', 'GroupController@update')->name('group.update');
//UploadController
Route::get('/upload', 'UploadController@uploadForm');
Route::post('/upload', 'UploadController@uploadSubmit');
Route::get('/download/{dir}/{file_name?}', 'UploadController@downlaodFile');
Route::get('/delete_file/{dir}/{file_name?}', 'UploadController@deleteFile');

Route::post('/view_files', 'OrderController@viewFiles');

//remove registration page
if (!env('ALLOW_REGISTRATION', false)) {
    Route::any('/register', function() {
        return null;
    });
}