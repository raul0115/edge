// 7-02-2018
ALTER TABLE `edge`.`export_cos` 
ADD COLUMN `fama_exp_date` DATE NULL AFTER `active`;

// 01-03-2018

ALTER TABLE `edge`.`import_po` 
ADD COLUMN `active` tinyint(1) unsigned NOT NULL DEFAULT '1';

// 07-03-2018

ALTER TABLE `edge`.`licenses` 
ADD COLUMN `fama_require` tinyint(1);



// 12-03-2018

( eliminar registros sobrantes en tabla ports, son los primeros 3 registros).


-- ----------------------------
-- Table structure for ports_to
-- ----------------------------
DROP TABLE IF EXISTS `ports_to`;
CREATE TABLE `ports_to` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `port_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `days_est_arrival` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of ports_to
-- ----------------------------
INSERT INTO `ports_to` VALUES ('1', 'LA', '24');
INSERT INTO `ports_to` VALUES ('2', 'NJ', '35');
INSERT INTO `ports_to` VALUES ('3', 'FOB', '5');

// 26-03-2018
ALTER TABLE `edge`.`import_po` 
ADD COLUMN `date_closed` date;


// Si se ejecuto el script del 26-03-2018 eliminar ese script, eliminar la columna

//28/03/2018

ALTER TABLE `edge`.`import_po_dates` 
ADD COLUMN `closed` date;

//30/03/2018

ALTER TABLE `edge`.`import_po_dates` 
ADD COLUMN `factory_delivery` date;

//12/04/2018

ALTER TABLE `edge`.`col_definations` 
ADD COLUMN `List` tinyint(1) unsigned NOT NULL DEFAULT '1';


ALTER TABLE `edge`.`col_definations` 
ADD COLUMN `Order` int(10) unsigned NOT NULL DEFAULT '1';

//13/04/2018
ALTER TABLE `edge`.`import_po_dates` 
DROP COLUMN `factory_delivery`;

//

Eliminar el valor que tenga la columna svn_no
y luego realizar el siguiente query:

ALTER TABLE `edge`.`import_po` 
ADD COLUMN `svn_increment` int(10) unsigned;

UPDATE `import_po` SET `svn_no`=null
