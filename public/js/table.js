function display_addnew_div(tableID){
    $(tableID).toggle();
    }
    function display_edit_div(tableID, editID, editName){
    $(tableID).toggle();
    $(tableID+ "_name").val(editName);
    $(tableID+ "_id").val(editID);
    }
    function display_edit_code_div(tableID, editID, editName,editCode){
    $(tableID).toggle();
    $(tableID+ "_name").val(editName);
    $(tableID+ "_code").val(editCode);
    $(tableID+ "_id").val(editID);
    }
    function display_edit_character_div(tableID, editID, editName,editCode,editLicenseId){
    $(tableID).toggle();
    $(tableID+ "_name").val(editName);
    $(tableID+ "_code").val(editCode);
    $(tableID+ "_license_id").val(editLicenseId);
    $(tableID+ "_id").val(editID);
    }
    
    function display_edit_license_div(tableID, editID, editName,fama,code){
    $(tableID).toggle();
    $(tableID+ "_name").val(editName);
    $(tableID+ "_fama_require").val(fama).change();
    $(tableID+ "_code").val(code);
    $(tableID+ "_id").val(editID);
    }
    function display_edit_portsto_div(tableID, editID, editPortName,days_est_arrival){
    $(tableID).toggle();
    $(tableID+ "_port_name").val(editPortName);
    $(tableID+ "_days_est_arrival").val(days_est_arrival).change();
    $(tableID+ "_id").val(editID);
    }
    
    function display_edit_factory_div(tableID, editID, editName, vendor_id,full_name,address1,address2,city,province,zip,country,phone,bank_name,bank_address,bank_swift,usd_bank_acct) {
    $(tableID).toggle();
    $(tableID+ "_name").val(editName);
    $(tableID+ "_id").val(editID);
    $(tableID+ "_vendor_id").val(vendor_id);
    $(tableID+ "_full_name").val(full_name); 
    $(tableID+ "_address1").val(address1); 
    $(tableID+ "_address2").val(address2); 
    $(tableID+ "_city").val(city); 
    $(tableID+ "_province").val(province); 
    $(tableID+ "_zip").val(zip); 
    $(tableID+ "_country").val(country); 
    $(tableID+ "_phone").val(phone); 
    $(tableID+ "_bank_name").val(bank_name); 
    $(tableID+ "_bank_address").val(bank_address);
    $(tableID+ "_bank_swift").val(bank_swift);
    $(tableID+ "_usd_bank_acct").val(usd_bank_acct);   
    }
    function display_edit_ecportco_div(tableID, editID, editName, fama,fama_exp_date) {
    $(tableID).toggle();
    $(tableID+ "_name").val(editName);
    $(tableID+ "_id").val(editID);
    $(tableID+ "_fama_exp_date").val(fama_exp_date);
    $(tableID+ "_fama_exp_date").datepicker({
       keyboardNavigation: false,
       forceParse: false,
       autoclose: true,
       format: 'yyyy-mm-dd',
    });
    $(tableID+ "_fama").val(fama).change();

}