@extends('loggedin.layout')

@section('content')

@if (session('message'))

<div class="alert alert-success">
    <strong>Successful Action!</strong><br>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row">
<div class="col-lg-12">
       
        
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Materials Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_materials_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{route('table.material.post') }}">
                            <input type="hidden" name="table" value="materials">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_materials">
                            <input type="hidden"  id="table_materials_edit_id" name="edit_id" value="">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>code</label>
                                    <input id="table_materials_edit_code" name='code' type="text" value="" class="form-control" maxlength="1">
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_materials_edit_name" name='name' type="text" value="" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                             
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_materials_addnew' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{route('table.material.post') }}">
                            <input type="hidden" name="table" value="materials">{{ csrf_field() }}<input type="hidden" name="action" value="add_materials">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Code</label>
                                    <input name='code' type="text" placeholder="Enter  " class="form-control" maxlength="1">
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter  Name" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                           
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New Material</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_materials_addnew');">Add New Material</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group pull-right"><form class="form-horizontal" role="form" method="get" action="{{route('table.material.index') }}">
                                <input type="hidden" name="table" value='materials'>
                                <input type="search" name="search_materials" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($materials as $material)
                            <tr>
                                <td>{{ $material->id }}</td>
                                <td>{{ $material->code }}</td>
                                <td>{{ $material->name }}</td>
                                
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_code_div('#table_materials_edit', {{ $material->id }} , '{{ $material->name }}','{{$material->code}}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($material->active == 1)
                                    <form id="delete_material{{ $material->id }}" method="POST" action="{{route('table.material.post') }}">
                                        <input type="hidden" name="delete_material_id" value="{{ $material->id }}"><input type="hidden" name="table" value="materials"><input type="hidden" name="action" value="delete_materials">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($material->active == 0)
                                    <form id="activate_material{{ $material->id }}" method="POST" action="{{route('table.material.post') }}">
                                        <input type="hidden" name="activate_material_id" value="{{ $material->id }}"><input type="hidden" name="table" value="materials"><input type="hidden" name="action" value="activate_materials">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $materials->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/table.js') }}"></script>
@endpush