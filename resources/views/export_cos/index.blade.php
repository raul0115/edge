@extends('loggedin.layout')

@section('content')

@if (session('message'))

<div class="alert alert-success">
    <strong>Successful Action!</strong><br>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row">
<div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Export Co Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_export_cos_edit' class="col-sm-12" style="display:none;" >
                        <form role="form" method="POST" action="{{  route('table.exportco.post') }}">
                            {{csrf_field()}}
                            <input type="hidden" name="table" value="export_cos">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_export_cos">
                            <input type="hidden"  id="table_export_cos_edit_id" name="edit_id" value="">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_export_cos_edit_name" name='name' type="text" value="" class="form-control" >
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Fama</label>
                                    <select id='table_export_cos_edit_fama'  name='fama' class="form-control" style="height: 34px;">
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Fama Exp Date</label>
                                        <div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" name="fama_exp_date" id="table_export_cos_edit_fama_exp_date" class="form-control" value="">
                                        </div>
                                    </div>
                               
                            </div>
            
                            <button class="btn btn-primary pull-right" type="submit" style="margin-top:22px;"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_export_cos_addnew' class="col-sm-12" style="display:none;" >
                        <form role="form" method="POST" action="{{  route('table.exportco.post') }}">
                            <input type="hidden" name="table" value="export_cos">
                            {{ csrf_field() }}
                            <input type="hidden" name="action" value="add_export_cos">
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input name='name' type="text" placeholder="Enter Export Name" class="form-control" >
                                    </div>
                               
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Fama</label>
                                        <select  name='fama' class="form-control" style="height: 34px;">
                                                <option value="1">Yes</option><option value="0">No</option>
                                            </select>
                                    </div>
                               
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Fama Exp Date</label>
                                        <div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" name="fama_exp_date" id="fama_exp_date" class="form-control" value="{{\Carbon\Carbon::now()->format('M d Y')}}">
                                        </div>
                                    </div>
                               
                            </div>
                            
                            <button class="btn btn-primary pull-right" type="submit"><strong>Add New Export Co</strong></button>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_export_cos_addnew');">Add New Export Co</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group pull-right"><form class="form-horizontal" role="form" method="get" action="{{  route('table.exportco.index')}}">
                                <input type="hidden" name="table" value='export_cos'> 
                                <input type="search" name="search_export_cos" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>FAMA</th>
                                <th>Fama Exp Date</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($export_cos as $export_co)
                            <tr>
                                <td>{{ $export_co->id }}</td>
                                <td>{{ $export_co->name }}</td>
                                <td>@if ($export_co->fama == 1) Yes @elseif ($export_co->fama == 0) No @endif </td>
                                <td>{{ ($export_co->fama_exp_date)?\Carbon\Carbon::parse($export_co->fama_exp_date)->format('M d Y'):null }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_ecportco_div('#table_export_cos_edit', {{ $export_co->id }} , '{{ $export_co->name }}', {{ $export_co->fama }},'{{ ($export_co->fama_exp_date)?\Carbon\Carbon::parse($export_co->fama_exp_date)->format('M d Y'):null }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($export_co->active == 1)
                                    <form id="delete_export_co{{ $export_co->id }}" method="POST" action="{{  route('table.exportco.post') }}">
                                        <input type="hidden" name="delete_export_co_id" value="{{ $export_co->id }}"><input type="hidden" name="table" value="export_cos"><input type="hidden" name="action" value="delete_export_cos">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($export_co->active == 0)
                                    <form id="activate_export_co{{ $export_co->id }}" method="POST" action="{{  route('table.exportco.post') }}">
                                        <input type="hidden" name="activate_export_co_id" value="{{ $export_co->id }}"><input type="hidden" name="table" value="export_cos"><input type="hidden" name="action" value="activate_export_cos">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $export_cos->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>     
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/table.js') }}"></script>
@endpush