@extends('loggedin.layout')

@section('content')

@if (session('message'))

<div class="alert alert-success">
    <strong>Successful Action!</strong><br>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Ports To Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_portsto_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{  route('table.portto.post') }}">
                            <input type="hidden" name="table" value="portsto">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_portsto">
                            <input type="hidden"  id="table_portsto_edit_id" name="edit_id" value="">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Port Name</label>
                                    <input id="table_portsto_edit_port_name" name='port_name' type="text" value="" class="form-control" >
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Days Est Arrival</label>
                                    <input id="table_portsto_edit_days_est_arrival" name='days_est_arrival' type="text" value="" class="form-control" >
                                </div>
                           
                            </div>
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_portsto_addnew' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{  route('table.portto.post') }}">
                            <input type="hidden" name="table" value="portsto">
                            {{ csrf_field() }}
                            <input type="hidden" name="action" value="add_portsto">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Port Name</label>
                                    <input name='port_name' type="text" placeholder="Enter Port Name" class="form-control" >
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label>Days Est Arrival</label>
                                        <input name='days_est_arrival' type="text" placeholder="Enter Days Est Arrival" class="form-control" >
                                    </div>
                                </div>
                           
                            </div>
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New Port To</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_portsto_addnew');">Add New Port To</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group pull-right"><form class="form-horizontal" role="form" method="get" action="{{  route('table.portto.index') }}">
                                <input type="hidden" name="table" value='ports_to'> 
                                <input type="search" name="search_portsto" class="form-control input-sm" placeholder="Search Port Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Port Name</th>
                                <th>Days Est Arrival</th>
                                <th>Edit</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($portsto as $ports_to)
                            <tr>
                                <td>{{ $ports_to->id }}</td>
                                <td>{{ $ports_to->port_name }}</td>
                                <td>{{ $ports_to->days_est_arrival }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_portsto_div('#table_portsto_edit', {{ $ports_to->id }} , '{{ $ports_to->port_name }}',{{$ports_to->days_est_arrival}} );">Edit</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $portsto->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div> 
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/table.js') }}"></script>
@endpush