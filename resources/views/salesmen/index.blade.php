@extends('loggedin.layout')

@section('content')

@if (session('message'))

<div class="alert alert-success">
    <strong>Successful Action!</strong><br>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row">
<div class="col-lg-12">
       
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Salesmen Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_salesmen_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{route('table.salesmen.post') }}">
                            <input type="hidden" name="table" value="salesmen">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_salesmen">
                            <input type="hidden"  id="table_salesmen_edit_id" name="edit_id" value="">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_salesmen_edit_name" name='name' type="text" value="" class="form-control" >
                                </div>
                           
                            </div>
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_salesmen_addnew' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{route('table.salesmen.post') }}">
                            <input type="hidden" name="table" value="salesmen">{{ csrf_field() }}<input type="hidden" name="action" value="add_salesmen">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter  Name" class="form-control" >
                                </div>
                           
                            </div>
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New Salesman</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_salesmen_addnew');">Add New Salesman</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group pull-right"><form class="form-horizontal" role="form" method="get" action="{{route('table.salesmen.index') }}">
                                <input type="hidden" name="table" value='salesmen'>
                                <input type="search" name="search_salesmen" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($salesmen as $salesman)
                            <tr>
                                <td>{{ $salesman->id }}</td>
                                <td>{{ $salesman->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_div('#table_salesmen_edit', {{ $salesman->id }} , '{{ $salesman->name }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($salesman->active == 1)
                                    <form id="delete_salesman{{ $salesman->id }}" method="POST" action="{{route('table.salesmen.post') }}">
                                        <input type="hidden" name="delete_salesman_id" value="{{ $salesman->id }}"><input type="hidden" name="table" value="salesmen"><input type="hidden" name="action" value="delete_salesmen">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($salesman->active == 0)
                                    <form id="activate_salesman{{ $salesman->id }}" method="POST" action="{{route('table.salesmen.post') }}">
                                        <input type="hidden" name="activate_salesman_id" value="{{ $salesman->id }}"><input type="hidden" name="table" value="salesmen"><input type="hidden" name="action" value="activate_salesmen">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $salesmen->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
           
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/table.js') }}"></script>
@endpush