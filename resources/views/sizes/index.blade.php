@extends('loggedin.layout')

@section('content')

@if (session('message'))

<div class="alert alert-success">
    <strong>Successful Action!</strong><br>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row">
<div class="col-lg-12">
       
    
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Sizes Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_sizes_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{route('table.size.post') }}">
                            <input type="hidden" name="table" value="sizes">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_sizes">
                            <input type="hidden"  id="table_sizes_edit_id" name="edit_id" value="">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>code</label>
                                    <input id="table_sizes_edit_code" name='code' type="text" value="" class="form-control" maxlength="2">
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_sizes_edit_name" name='name' type="text" value="" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                            
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_sizes_addnew' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{route('table.size.post') }}">
                            <input type="hidden" name="table" value="sizes">{{ csrf_field() }}<input type="hidden" name="action" value="add_sizes">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Code</label>
                                    <input name='code' type="text" placeholder="Enter  " class="form-control" maxlength="2">
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter  Name" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                            
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New Size</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_sizes_addnew');">Add New Size</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group pull-right"><form class="form-horizontal" role="form" method="get" action="{{route('table.size.index') }}">
                                <input type="hidden" name="table" value='sizes'>
                                <input type="search" name="search_sizes" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($sizes as $size)
                            <tr>
                                <td>{{ $size->id }}</td>
                                <td>{{ $size->code }}</td>
                                <td>{{ $size->name }}</td>
                                 
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_code_div('#table_sizes_edit', {{ $size->id }} , '{{ $size->name }}','{{$size->code}}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($size->active == 1)
                                    <form id="delete_size{{ $size->id }}" method="POST" action="{{route('table.size.post') }}">
                                        <input type="hidden" name="delete_size_id" value="{{ $size->id }}"><input type="hidden" name="table" value="sizes"><input type="hidden" name="action" value="delete_sizes">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($size->active == 0)
                                    <form id="activate_size{{ $size->id }}" method="POST" action="{{route('table.size.post') }}">
                                        <input type="hidden" name="activate_size_id" value="{{ $size->id }}"><input type="hidden" name="table" value="sizes"><input type="hidden" name="action" value="activate_sizes">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $sizes->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/table.js') }}"></script>
@endpush