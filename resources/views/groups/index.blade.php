@extends('loggedin.layout')

@section('content')
<div class="row">
    
        @if (session('message'))
        <div class="alert alert-success">
            <strong>Successful Action!</strong><br><br>
            <ul>
                <li>{{ session('message') }}</li>
            </ul>
        </div>
        @endif
        @if (session('error'))
        <div class="alert alert-danger">
            <strong>Whoops!</strong> {{ session('error') }}<br>
        </div>
        @endif


</div>
<div class="row">
<div class="col-sm-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>List Group</h5>
        </div>
        <div class="ibox-content">
        <a href='#'  data-toggle="modal" data-target="#modal_create" class="btn btn-info">New Group</a>
        <table id="group-table" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Name</th>
                        <th>Code</th>
                        <th></th>
                        <th></th>
                       
                    </tr>
                    <tr id="tr-primary">
                        <th></th>
                        <th>name</th>
                        <th>code</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
              </table>
        </div>
    </div>
</div>
</div>

@include('groups.form_create')
@include('groups.form_edit')
@endsection
@push('styles2')
<link rel="stylesheet" href="{{ asset('css/plugins/select2/select2.css')}}">
<link rel="stylesheet" href="{{ asset('css/plugins/select2/select2-bootstrap.css')}}">
<link rel="stylesheet" href="{{ asset('js/plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('plugins/magnific-popup/magnific-popup.css')}}">
<style>
.column_search{
    width:100%;
}
tr {
    cursor: pointer;
}
td.details-control {
    background: url(../img/details_open.png) no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url(../img/details_close.png) no-repeat center center;
}
td.rem-control{
    text-align:center;
}
.has-error .form-control {
    border:1px solid rgb(248, 148, 6) !important;
}
.has-error .select2-container{
    border:1px solid rgb(248, 148, 6) !important;
}
.has-error .control-label {
    color: rgb(103, 106, 108) !important;
}
</style>
@endpush
@push('scripts')
<script src="{{asset('js/plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('plugins/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<script src="{{ asset('js/plugins/validate/jquery.validate.min.js') }}"></script>
<script>
 $( document ).ready(function() {
 
$('#tr-primary th').each( function () {
        var title = $(this).text();
        if(title!="" && (title == 'name' || title == 'code')){
            $(this).html( '<input type="text" name='+title+' class="column_search"/>' );
        }
    } );

var gTable =    $('#group-table').DataTable( {
                orderCellsTop: true,
                processing: true,
                serverSide: true,
                "scrollX": true,
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false,
                "order": [[ 0, "desc" ]],
                "columnDefs": [
                    {
                        "targets": [0,3],
                        "searchable": false,
                        "visible": false,
                    }
                ],
                ajax: {
                    url: "{{route('group.basic-data')}}",
                    data: function (d) {
                        d.name = $('input[name=name]').val();
                        d.code = $('input[name=code]').val();
                        
                    
                    }
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'groups.name'},
                    {data: 'code', name: 'groups.code'},
                    {data: 'description', name: 'groups.description'},
                    {
                        "className":      'details-control',
                        "orderable":      false,
                        "searchable":      false,
                        "data":           null,
                        "defaultContent": ''
                    }
                    
                ]
            });

    
    $( 'input[type=text]'  ).on( 'keyup',function () {
        gTable.draw();
    });
    $("#group-table").on('click', 'td', function() {
        var colIndex = gTable.cell(this).index().column;
        if (colIndex!=4){
            var row_index= gTable.cell(this).index().row;
            var group_id=gTable.cell(row_index, 0).data();
            $('#group-form-edit').attr('action', "groups/"+group_id);
            $('#edit_item_id').val(null).trigger('change');
            $('#edit_name').val(gTable.cell(row_index, 1).data());
            $('#edit_description').val(gTable.cell(row_index, 3).data());
        // Fetch the preselected item, and add to the control
        var edit_item_select = $('#edit_item_id');
        $.ajax({
            type: 'GET',
            url: 'groups/'+group_id+'/groupItems',
        }).then(function (data) {
            data.forEach( function(valor, indice, array) {
                // create the option and append to Select2
                var option = new Option(valor.name, valor.id, true, true);
                edit_item_select.append(option).trigger('change');
            
            });
            // manually trigger the `select2:select` event
            edit_item_select.trigger({
                type: 'select2:select',
                params: {
                    data: data
                }
            });
        });
        $('#modal_edit').modal('show')  
           
        }
    });
    $('#group-table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row =  gTable.row(tr);
        var tableId =  'items-' +row.data().id;

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(format(row.data(),tableId)).show();
            initTable(tableId, row.data());
            tr.addClass('shown');
            tr.next().find('td').addClass('no-padding bg-gray');
        }
    });
    function initTable(tableId, data) {
        $('#' + tableId).DataTable({
            processing: true,
            serverSide: true,
            'paging'      : false,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : false,
            'info'        : false,
            'autoWidth'   : false,
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                {
                    "targets": [0],
                    "searchable": false,
                    "visible": false,
                },
                { "targets":[5],
                    "searchable": false,
                    "orderable": false, 
                    "createdCell": function(td, cellData, rowData, row, col) {
                            $(td).addClass('rem-control');
                    }
                }
            ],
            ajax: data.details_url,
            columns: [
                { data: 'id', name: 'id' },
                {data: 'img', name: 'img',orderable:false,searchable: false},
                { data: 'name', name: 'name' },
                { data: 'code', name: 'code' },
                { data: 'status_name', name: 'status_name' },
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            "initComplete": function( settings, json ) {
                    $('.test-popup-link').magnificPopup({
                    type: 'image'
                    // other options
                    });

                }
        })
    }
    function format ( data , tableId) {
        var result = "  <table class='table details-table table-bordered table-hover' id="+tableId+"><thead>";
        result += "<tr >";
            result += "<th>id</th>";
            result += "<th>image</th>";
            result += "<th>name</th>";
            result += "<th>code</th>";
            result += "<th>status</th>";
            result += "<th></th>";
        result += "</tr>";
        result += "</thead></table>";

        return result;


    }
    
    var select2_config={
		allowClear: true,
		width: "100%",
		delay: 250,
		ajax: {
			url: "{{route('group.get_items')}}",
			dataType: 'json',
			type: "POST",
			data: function(params) {
				return {
					q: params.term,
					page: params.page || 1,
					page_limit: 30,
				};
			},
			processResults: function(data, params) {
				params.page = params.page || 1;
				return {
					results: data.items,
					pagination: {
						more: (params.page * 30) < data.total
					}
				};
			},
			cache: true,
            headers:{
                    'X-CSRF-TOKEN':'{{csrf_token()}}'
                }
		},
		placeholder: " ",
		escapeMarkup: function (markup) { return markup; },
		minimumInputLength: 1,
		templateResult: formatRepo,
  		templateSelection: formatRepoSelection
	};

$(".select2").select2(select2_config);

	function formatRepo (repo) {
		if (repo.loading) {
			return repo.text;
		}
		var markup = "<div class='select2-result-repository clearfix  valign-wrapper'>";
				markup += "<div class='col-xs-5'>";
					if(repo.img !== null){
					markup +="<img src='" + repo.img + "' width='50' height='50' /> " ;
					}
				markup += "</div>";
				markup += "<div class='col-xs-7'> &nbsp;&nbsp;";
					markup +=repo.text ;
				markup += "</div>";
		markup += "</div>";
		return markup;
	}

	function formatRepoSelection (repo) {
		return repo.style_id || repo.text;
	}

       

        $('form#group-form').validate({
            rules: {
                
                name: {
                    required: true,
                }
            },
            highlight: function (element, errorClass, validClass) {
                var elem = $(element);
                if (elem.hasClass("select2-hidden-accessible")) {
                    $("#select2-" + elem.attr("id") + "-container").parent().addClass(errorClass); 
                } else {
                    elem.addClass(errorClass);
                }
                },    
                unhighlight: function (element, errorClass, validClass) {
                    var elem = $(element);
                    if (elem.hasClass("select2-hidden-accessible")) {
                        $("#select2-" + elem.attr("id") + "-container").parent().removeClass(errorClass);
                    } else {
                        elem.removeClass(errorClass);
                    }
                },
                errorPlacement: function(error, element) {
                var elem = $(element);
                if (elem.hasClass("select2-hidden-accessible")) {
                    element = $("#select2-" + elem.attr("id") + "-container").parent(); 
                    error.insertAfter(element);
                } else {
                    error.insertAfter(element);
                }
            }
        });


    });
    function delete_groups_item(act_group,act_item){
        var tableId =  'items-' +act_group;
        $.ajax({
                url: "groups/"+act_group+"/item/"+act_item,
                type:'POST',
                data: {
                    _token:'{{csrf_token()}}'
                },
                success: function(data) {
                    $('#'+tableId).DataTable().draw();
                   console.log(JSON.stringify(data));
                }
            });
    }
</script>
@endpush