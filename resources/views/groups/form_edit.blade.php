<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
    <form class="form-horizontal" id="group-form-edit" method="POST"  >
                {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Item</h4>
                </div>
                <div class="modal-body">
                   <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        
                        {{ Form::label('name', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                            {{Form::text('name',null,['id'=>'edit_name','class' => 'form-control','placeholder' => 'Required'])}}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                        
                        {{ Form::label('description', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::textarea('description',null,['id'=>'edit_description','class' => 'form-control','rows'=>2])}}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('item_id') ? ' has-error' : '' }}">
                        {{ Form::label('item', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                        {{ Form::select('item_id', [],null, ['id'=>'edit_item_id','name'=>'item_id[]','multiple'=>'multiple', 'class'=>'form-control select2','placeholder' => '']) }}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button id="btn_change_status" type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
  </div>