@extends('loggedin.layout')

@section('content')

<div class="row">
    
        @if (session('message'))
        <div class="alert alert-success">
            <strong>Successful Action!</strong><br><br>
            <ul>
                <li>{{ session('message') }}</li>
            </ul>
        </div>
        @endif
        @if (count($errors) > 0)
        <div class="alert alert-warning alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="alert alert-warning alert-dismissable" id="alert-val">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
        </div>
       

</div>
<div class="row">
<div class="col-sm-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>New Group</h5>
        </div>
        <div class="ibox-content">
            <form class="form-horizontal" id="group-form" method="POST" action="{{ route('group.store') }}" >
                
                {{ csrf_field() }}
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        
                        {{ Form::label('name', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                            {{Form::text('name',null,['class' => 'form-control','placeholder' => 'Required'])}}
                        </div>
                </div>
                <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                    
                    {{ Form::label('description', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                    {{Form::textarea('description',null,['class' => 'form-control','rows'=>2])}}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('item_id') ? ' has-error' : '' }}">
                    {{ Form::label('group', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                    {{ Form::select('item_id', [],null, ['name'=>'item_id[]','multiple'=>'multiple', 'class'=>'form-control select2','placeholder' => '']) }}
                    </div>
                </div>
               
            <div class="form-group">
                <div class="col-lg-offset-4 col-lg-8">
                    <button class="btn btn-primary" type="submit">New Group</button>
               
                </div>
            </div>

            </form>
        </div>
    </div>
</div>
</div>
@endsection
@push('styles2')
<link rel="stylesheet" href="{{ asset('css/plugins/select2/select2.css')}}">
<link rel="stylesheet" href="{{ asset('css/plugins/select2/select2-bootstrap.css')}}">

<style>
.has-error .form-control {
    border:1px solid rgb(248, 148, 6) !important;
}
.has-error .select2-container{
    border:1px solid rgb(248, 148, 6) !important;
}
.has-error .control-label {
    color: rgb(103, 106, 108) !important;
}
</style>
@endpush
@push('scripts')
<script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('js/plugins/validate/jquery.validate.min.js') }}"></script>
<script>
$(document).ready(function(){
    
    
    var alert = $("#alert-val");
    alert.hide();

    var select2_config={
		allowClear: true,
		width: "100%",
		delay: 250,
		ajax: {
			url: "{{route('group.get_items')}}",
			dataType: 'json',
			type: "POST",
			data: function(params) {
				return {
					q: params.term,
					page: params.page || 1,
					page_limit: 30,
				};
			},
			processResults: function(data, params) {
				params.page = params.page || 1;
				return {
					results: data.items,
					pagination: {
						more: (params.page * 30) < data.total
					}
				};
			},
			cache: true,
            headers:{
                    'X-CSRF-TOKEN':'{{csrf_token()}}'
                }
		},
		placeholder: " ",
		escapeMarkup: function (markup) { return markup; },
		minimumInputLength: 1,
		templateResult: formatRepo,
  		templateSelection: formatRepoSelection
	};

$(".select2").select2(select2_config);

	function formatRepo (repo) {
		if (repo.loading) {
			return repo.text;
		}
		var markup = "<div class='select2-result-repository clearfix  valign-wrapper'>";
				markup += "<div class='col-xs-6'>";
					if(repo.img !== null){
					markup +="<img src='" + repo.img + "' width='50' height='50' /> " ;
					}
				markup += "</div>";
				markup += "<div class='col-xs-6'> &nbsp;&nbsp;";
					markup +=repo.text ;
				markup += "</div>";
		markup += "</div>";
		return markup;
	}

	function formatRepoSelection (repo) {
		return repo.style_id || repo.text;
	}

       

        $('form#group-form').validate({
            rules: {
                
                name: {
                    required: true,
                }
            },
            highlight: function (element, errorClass, validClass) {
                var elem = $(element);
                if (elem.hasClass("select2-hidden-accessible")) {
                    $("#select2-" + elem.attr("id") + "-container").parent().addClass(errorClass); 
                } else {
                    elem.addClass(errorClass);
                }
                },    
                unhighlight: function (element, errorClass, validClass) {
                    var elem = $(element);
                    if (elem.hasClass("select2-hidden-accessible")) {
                        $("#select2-" + elem.attr("id") + "-container").parent().removeClass(errorClass);
                    } else {
                        elem.removeClass(errorClass);
                    }
                },
                errorPlacement: function(error, element) {
                var elem = $(element);
                if (elem.hasClass("select2-hidden-accessible")) {
                    element = $("#select2-" + elem.attr("id") + "-container").parent(); 
                    error.insertAfter(element);
                } else {
                    error.insertAfter(element);
                }
            }
        });
           
});
    
</script>



@endpush
