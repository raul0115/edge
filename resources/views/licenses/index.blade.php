@extends('loggedin.layout')

@section('content')

@if (session('message'))

<div class="alert alert-success">
    <strong>Successful Action!</strong><br>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row">
<div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>License Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_licenses_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{route('table.license.post') }}">
                            <input type="hidden" name="table" value="licenses">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_licenses">
                            <input type="hidden"  id="table_licenses_edit_id" name="edit_id" value="">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Code</label>
                                    <input id="table_licenses_edit_code" name='code' type="text" value="" class="form-control" maxlength="1">
                                </div>
                           
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_licenses_edit_name" name='name' type="text" value="" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Fama</label>
                                    <select id='table_licenses_edit_fama_require'  name='fama_require' class="form-control" style="height: 34px;">
                                            <option selected="selected" value="">Yes/No</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                </div>
                           
                            </div>
                            
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_licenses_addnew' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{route('table.license.post') }}">
                            <input type="hidden" name="table" value="licenses">
                            {{ csrf_field() }}
                            <input type="hidden" name="action" value="add_licenses">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Code</label>
                                    <input name='code' type="text" placeholder="Enter code" class="form-control" maxlength="1">
                                </div>
                           
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter License Name" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Fama</label>
                                    <select id='fama_require'  name='fama_require' class="form-control" style="height: 34px;">
                                            <option selected="selected" value="">Yes/No</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                </div>
                           
                            </div>
                           
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New License</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_licenses_addnew');">Add New License</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group pull-right"><form class="form-horizontal" role="form" method="get" action="{{route('table.license.index') }}">
                                <input type="hidden" name="table" value='licenses'> 
                                <input type="search" name="search_licenses" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Fama</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($licenses as $license)
                            <tr>
                                <td>{{ $license->id }}</td>
                                <td>{{ $license->code }}</td>
                                <td>{{ $license->name }}</td>
                                <td>{{ $license->fama() }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_license_div('#table_licenses_edit', {{ $license->id }} , '{{ $license->name }}','{{$license->fama_require}}','{{ $license->code }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($license->active == 1)
                                    <form id="delete_license{{ $license->id }}" method="POST" action="{{route('table.license.post') }}">
                                        <input type="hidden" name="delete_license_id" value="{{ $license->id }}"><input type="hidden" name="table" value="licenses"><input type="hidden" name="action" value="delete_licenses">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($license->active == 0)
                                    <form id="activate_license{{ $license->id }}" method="POST" action="{{route('table.license.post') }}">
                                        <input type="hidden" name="activate_license_id" value="{{ $license->id }}"><input type="hidden" name="table" value="licenses"><input type="hidden" name="action" value="activate_licenses">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $licenses->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>  
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/table.js') }}"></script>
@endpush