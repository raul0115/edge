@extends('loggedin.layout')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Change Password<small></small></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
<form class="form-horizontal" role="form" method="POST" action="{{ url('/edit-password') }}">
                          {!! csrf_field() !!}
                        <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                            <label class="col-md-3 control-label">Old Password</label>
                             <div class="col-md-4">
                                <input type="password" class="form-control" name="password">


                                @if ($errors->has('old_password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span>
                                @endif
                              
                            </div>
                        </div>
                          

                        <div class="form-group{{ $errors->has('newpassword') ? ' has-error' : '' }}">
                            <label class="col-md-3 control-label">New Password</label>
                             <div class="col-md-4">
                                <input type="password" class="form-control" name="newpassword">


                                @if ($errors->has('newpassword'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('newpassword') }}</strong>
                                    </span>
                                @endif
                              
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('newpassword_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-3 control-label">Confirm New Password</label>
                             <div class="col-md-4">
                                <input type="password" class="form-control" name="newpassword_confirmation">

                                @if ($errors->has('newpassword_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('newpassword_confirmation') }}</strong>
                                    </span>
                                @endif
                              
                            </div>
                        </div>
                           <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>Update
                                </button>
                            </div>
                        </div>

                    </form>
            </div>
        </div>
    </div>
</div>

@if (session('user.password.edit.status'))
 	{{ session('user.password.edit.status') }}
@endif

@endsection