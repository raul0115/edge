@extends('loggedin.layout')

@section('content')

@if (session('message'))
<div class="alert alert-success">
    <strong>Successful Action!</strong><br><br>
    <ul>
                    <li>{{ session('message') }}</li>
    </ul>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
            @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
            @endforeach
    </ul>
</div>
@endif

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Edit Production User Settings<small> User may still not be able to see some options if disabled on user level.</small></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>

                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" style="display: none;">
                <form  method="POST" action="{{ url('/usermanagement/groupSubmit') }}" class="form-horizontal">
                    {{ csrf_field() }}
                    <input name='group_type' type='hidden' value="Production">
                    <div class="form-group"><label class="col-sm-2 control-label"><small class="text-navy">Don't forget to click save changes</small></label>

                        <div class="col-sm-10">
                            @foreach ($col_definations as $rows)
                            <div class="i-checks"><label> <input name='{{ $rows->col_name }}' type='hidden' value="0"><input type="checkbox" name='{{ $rows->col_name }}' value="1" @if($rows->Production==1) checked="checked" @endif > <i></i> {{$rows->col_discription }} </label></div>
                            @endforeach
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="submit">Cancel</button>
                            <button class="btn btn-primary" type="submit">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Edit QC Users Settings<small> User may still not be able to see some options if disabled on user level.</small></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>

                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" style="display: none;">
                <form  method="POST" action="{{ url('/usermanagement/groupSubmit') }}" class="form-horizontal">
                    {{ csrf_field() }}
                    <input name='group_type' type='hidden' value="QC">
                    <div class="form-group"><label class="col-sm-2 control-label"><small class="text-navy">Don't forget to click save changes</small></label>

                        <div class="col-sm-10">
                            @foreach ($col_definations as $rows)
                            <div class="i-checks"><label> <input name='{{ $rows->col_name }}' type='hidden' value="0"><input type="checkbox" name='{{ $rows->col_name }}' value="1" @if($rows->QC==1) checked="checked" @endif > <i></i> {{$rows->col_discription }} </label></div>
                            @endforeach
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="submit">Cancel</button>
                            <button class="btn btn-primary" type="submit">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Edit Sales User Settings<small> User may still not be able to see some options if disabled on user level.</small></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>

                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" style="display: none;">
                <form  method="POST" action="{{ url('/usermanagement/groupSubmit') }}" class="form-horizontal">
                    {{ csrf_field() }}
                    <input name='group_type' type='hidden' value="Sales">
                    <div class="form-group"><label class="col-sm-2 control-label"><small class="text-navy">Don't forget to click save changes</small></label>

                        <div class="col-sm-10">
                            @foreach ($col_definations as $rows)
                            <div class="i-checks"><label> <input name='{{ $rows->col_name }}' type='hidden' value="0"><input type="checkbox" name='{{ $rows->col_name }}' value="1" @if($rows->Sales==1) checked="checked" @endif > <i></i> {{$rows->col_discription }} </label></div>
                            @endforeach
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="submit">Cancel</button>
                            <button class="btn btn-primary" type="submit">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Edit Factory User Settings<small> User may still not be able to see some options if disabled on user level.</small></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>

                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" style="display: none;">
                <form  method="POST" action="{{ url('/usermanagement/groupSubmit') }}" class="form-horizontal">
                    {{ csrf_field() }}
                    <input name='group_type' type='hidden' value="Factory">
                    <div class="form-group"><label class="col-sm-2 control-label"><small class="text-navy">Don't forget to click save changes</small></label>

                        <div class="col-sm-10">
                            @foreach ($col_definations as $rows)
                            <div class="i-checks"><label> <input name='{{ $rows->col_name }}' type='hidden' value="0"><input type="checkbox" name='{{ $rows->col_name }}' value="1" @if($rows->Factory==1) checked="checked" @endif > <i></i> {{$rows->col_discription }} </label></div>
                            @endforeach
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="submit">Cancel</button>
                            <button class="btn btn-primary" type="submit">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection