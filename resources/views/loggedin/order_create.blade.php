@extends('loggedin.layout')

@section('content')

<div class="row">
    
        @if (session('message'))
        <div class="alert alert-success">
            <strong>Successful Action!</strong><br><br>
            <ul>
                <li>{{ session('message') }}</li>
            </ul>
        </div>
        @endif
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

</div>
<div class="row">
<div class="col-sm-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>New Order</h5>
        </div>
        <div class="ibox-content">
            <form class="form-horizontal" method="POST" action="{{ url('/orders/store') }}" >
                
                {{ csrf_field() }}
                <div class="form-group {{ $errors->has('po_number') ? ' has-error' : '' }}">
                        
                    {{ Form::label('Po Number', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('po_number',old('po_number'),['class' => 'form-control'])}}
                        @if ($errors->has('po_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('po_number') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('comments') ? ' has-error' : '' }}">
                    
                    {{ Form::label('Comments', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::textarea('comments',old('comments'),['class' => 'form-control','rows'=>2])}}
                            @if ($errors->has('comments'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('comments') }}</strong>
                                </span>
                            @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('value') ? ' has-error' : '' }}">
                    
                    {{ Form::label('Value $', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('value',number_format(old('value')),['class' => 'form-control'])}}
                            @if ($errors->has('value'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('value') }}</strong>
                                </span>
                            @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('fty_produc') ? ' has-error' : '' }}">
                    {{ Form::label('fty_produc', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        <select class="form-control select2" name="fty_produc" id="fty_produc">
                            <option value="">Select Factories</option>
                            @foreach ($tableData['factories'] as $factory)
                                <option @if($factory->id==old('fty_produc')) selected="selected" @endif value="{{ $factory->id }}">{{ $factory->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('fty_produc'))
                            <span class="help-block">
                                <strong>{{ $errors->first('fty_produc') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('export_cos') ? ' has-error' : '' }}">
                        {{ Form::label('export_cos', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                            <select class="form-control select2" name="export_cos" id="export_cos">
                                <option value="">Select Export Co</option>
                                @foreach ($tableData['export_cos'] as $export_cos)
                                    <option @if($export_cos->id==old('export_cos')) selected="selected" @endif  value="{{ $export_cos->id }}">{{ $export_cos->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('export_cos'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('export_cos') }}</strong>
                                </span>
                            @endif
                        </div>
                </div>
                <div id="div_fama" class="form-group {{ $errors->has('licenses.*') ? ' has-error' : '' }}">
                        
                    {{ Form::label('FAMA', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                    {{Form::text('fama',null,['class' => 'form-control','disabled' => 'disabled','id'=>'fama'])}}
                           
                            <span clas="help-block" style="display:none;">
                                <strong style="color:#a94442;"></strong>
                            </span>
                    </div>
                </div>
                
               
                <div class="form-group {{ $errors->has('product_styles') ? ' has-error' : '' }}">
                        
                    {{ Form::label('product_styles', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                            <select class="form-control select2-mul" name="product_styles[]" multiple="multiple" id="product_styles" data-placeholder="Select Product Style">
                                @foreach ($tableData['product_styles'] as $product_style)
                                    <option {{collect(old('product_styles'))->contains($product_style->id)?' selected' : ''}}  value="{{ $product_style->id }}">{{ $product_style->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('product_styles'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('product_styles') }}</strong>
                                </span>
                            @endif
                        </div>
                </div>
                <div class="form-group {{ $errors->has('product_types') ? ' has-error' : '' }}">
                        
                    {{ Form::label('product_types', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                            <select class="form-control select2-mul" name="product_types[]" multiple="multiple" id="product_types" data-placeholder="Select Product Types">
                                @foreach ($tableData['product_types'] as $product_type)
                                    <option {{collect(old('product_types'))->contains($product_type->id)?' selected' : ''}}  value="{{ $product_type->id }}">{{ $product_type->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('product_types'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('product_types') }}</strong>
                                </span>
                            @endif
                        </div>
                </div> 
                <div class="form-group {{ $errors->has('licenses') ? ' has-error' : '' }}">
                        
                    {{ Form::label('licenses', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                            <select class="form-control select2-mul" name="licenses[]" multiple="multiple" id="licenses" data-placeholder="Select Licenses" >
                                @foreach ($tableData['licenses'] as $license)
                                    <option {{collect(old('licenses'))->contains($license->id)?' selected' : ''}}  value="{{ $license->id }}">{{ $license->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('licenses'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('licenses') }}</strong>
                                </span>
                            @endif
                        </div>
                </div>
                <div class="form-group {{ $errors->has('customers') ? ' has-error' : '' }}">
                        
                    {{ Form::label('customers', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                            <select class="form-control select2-mul" name="customers[]" multiple="multiple" id="customers" data-placeholder="Select Customers" >
                                @foreach ($tableData['customers'] as $customer)
                                    <option {{collect(old('customers'))->contains($customer->id)?' selected' : ''}}  value="{{ $customer->id }}">{{ $customer->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('customers'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('customers') }}</strong>
                                </span>
                            @endif
                        </div>
                </div>
                
                <div class="form-group {{ $errors->has('customer_po_number') ? ' has-error' : '' }}">
                    
                    {{ Form::label('customer_po_number', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('customer_po_number',old('customer_po_number'),['class' => 'form-control'])}}
                            @if ($errors->has('customer_po_number'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('customer_po_number') }}</strong>
                                </span>
                            @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('date_placed') ? ' has-error' : '' }}">
                    
                    {{ Form::label('date_placed', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                            <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="date_placed" id="date_placed" class="form-control date-pick" value="{{old('date_placed',\Carbon\Carbon::now()->format('M d Y'))}}">
                            </div>
                            @if ($errors->has('date_placed'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('date_placed') }}</strong>
                                </span>
                            @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('qty_ordered') ? ' has-error' : '' }}">
                    
                    {{ Form::label('qty_ordered', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('qty_ordered',old('qty_ordered'),['class' => 'form-control'])}}
                            @if ($errors->has('qty_ordered'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('qty_ordered') }}</strong>
                                </span>
                            @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('tickets_needed') ? ' has-error' : '' }}">
                    {{ Form::label('tickets_needed', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        <select class="form-control" name="tickets_needed" id="tickets_needed">
                                <option  selected="selected" value="">Select Yes/No</option>
                                <option @if(old('tickets_needed')=="1") selected="selected" @endif value="1">Yes</option>
                                <option @if(old('tickets_needed')=="0") selected="selected" @endif value="0">No</option>
                        </select>
                        @if ($errors->has('tickets_needed'))
                            <span class="help-block">
                                <strong>{{ $errors->first('tickets_needed') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('tickets_ordered') ? ' has-error' : '' }}">
                    
                    {{ Form::label('tickets_ordered', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                            <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="tickets_ordered" id="tickets_ordered" class="form-control date-pick" value="{{old('tickets_ordered')}}">
                            </div>
                            @if ($errors->has('tickets_ordered'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('tickets_ordered') }}</strong>
                                </span>
                            @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('shipping_from') ? ' has-error' : '' }}">
                    {{ Form::label('shipping_from', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        <select class="form-control select2" name="shipping_from" id="shipping_from">
                            <option value="">Select Shipping From</option>
                            @foreach ($tableData['ports'] as $port)
                                <option @if($port->id==old('shipping_from')) selected="selected" @endif  value="{{  $port->id }}">{{ $port->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('shipping_from'))
                            <span class="help-block">
                                <strong>{{ $errors->first('shipping_from') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('shipping_to') ? ' has-error' : '' }}">
                    {{ Form::label('shipping_to', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        <select class="form-control select2" name="shipping_to" id="shipping_to">
                            <option value="">Select Shipping to</option>
                            @foreach ($tableData['ports_to'] as $port_to)
                                <option @if($port_to->id==old('shipping_to')) selected="selected" @endif  value="{{  $port_to->id }}">{{ $port_to->port_name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('shipping_to'))
                            <span class="help-block">
                                <strong>{{ $errors->first('shipping_to') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('finish_prod') ? ' has-error' : '' }}">
                    
                    {{ Form::label('finish_prod', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                            <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="finish_prod" id="finish_prod" class="form-control date-pick" value="{{old('finish_prod')}}">
                            </div>
                            @if ($errors->has('finish_prod'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('finish_prod') }}</strong>
                                </span>
                            @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('estimated_arr') ? ' has-error' : '' }}">
                    
                    {{ Form::label('estimated_arr', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                            <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="estimated_arr" id="estimated_arr" class="form-control date-pick" value="{{old('estimated_arr')}}" disabled >
                            </div>
                            @if ($errors->has('estimated_arr'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('estimated_arr') }}</strong>
                                </span>
                            @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('customer_cancel') ? ' has-error' : '' }}">
                    
                    {{ Form::label('customer_cancel', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div id="div_customer_cancel" class="col-md-6 col-sm-5 col-xs-6">
                            <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="customer_cancel" id="customer_cancel" class="form-control date-pick" value="{{old('customer_cancel')}}">
                            </div>
                            @if ($errors->has('customer_cancel'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('customer_cancel') }}</strong>
                                </span>
                            @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('safety_test') ? ' has-error' : '' }}">
                    {{ Form::label('safety_test', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        <select class="form-control" name="safety_test" id="safety_test">
                                <option selected="selected" value="">Select Yes/No</option>
                                <option @if(old('safety_test')=="1") selected="selected" @endif value="1">Yes</option>
                                <option @if(old('safety_test')=="0") selected="selected" @endif value="0">No</option>
                        </select>
                        @if ($errors->has('safety_test'))
                            <span class="help-block">
                                <strong>{{ $errors->first('safety_test') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('safety_test_on') ? ' has-error' : '' }}">
                    
                    {{ Form::label('safety_test_on', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                            <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="safety_test_on" id="safety_test_on" class="form-control date-pick" value="{{old('safety_test_on')}}" disabled>
                            </div>
                            @if ($errors->has('safety_test_on'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('safety_test_on') }}</strong>
                                </span>
                            @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('pro_photo') ? ' has-error' : '' }}">
                    {{ Form::label('pro_photo', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        <select class="form-control" name="pro_photo" id="pro_photo">
                                <option  selected="selected" value="">Select Yes/No</option>
                                <option @if(old('pro_photo')=="1") selected="selected" @endif value="1">Yes</option>
                                <option @if(old('pro_photo')=="0") selected="selected" @endif value="0">No</option>
                        </select>
                        @if ($errors->has('pro_photo'))
                            <span class="help-block">
                                <strong>{{ $errors->first('pro_photo') }}</strong>
                            </span>
                        @endif
                    </div>
            </div>
                <div class="form-group {{ $errors->has('salesmen') ? ' has-error' : '' }}">
                
                {{ Form::label('salesmen', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
            <div class="col-md-6 col-sm-5 col-xs-6">
                    <select class="form-control select2-mul" name="salesmen[]" multiple="multiple" id="salesmen" data-placeholder="Select Salesmen">
                            @foreach ($tableData['salesmen'] as $salesman)
                            <option {{collect(old('salesmen'))->contains($salesman->id)?' selected' : ''}}  value="{{ $salesman->id }}">{{ $salesman->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('salesmen'))
                        <span class="help-block">
                            <strong>{{ $errors->first('salesmen') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-4 col-lg-8">
                    <a href="{{url('orders/open')}}" class="btn btn-info">List open P.Os</a>
                    <button class="btn btn-primary" type="submit">New P.O</button>
               
                </div>
            </div>

            </form>
        </div>
    </div>
</div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
<script>
    $('.select2').select2();
    $('.select2-mul').select2({
        tags: true,
        multiple: true,
        width: '100%'
    });
    $('.date-pick').datepicker({
            format:'M d yyyy',
             autoclose: true
    });
    function obtener_fama(){
        if($('#export_cos').val()){
            var ruta = "{{url('orders/obtener_fama/')}}";
                $.get(ruta+'/'+$('#export_cos').val(), function(data) {
                    $('#fama').empty();
                    $('#fama').val(data);
            
                });
            }else{
                $('#fama').val('');
        }
    }
    function obtener_arrival(){
        if($('#shipping_to').val() && $('#finish_prod').val()){
            var ruta = "{{url('orders/obtener_arrival')}}";
            var to=$('#shipping_to').val() ;
            var finish=$('#finish_prod').val();
                $.get(ruta+'?port_to='+to+'&finish_prod='+finish, function(data) {
                    $('#estimated_arr').empty();
                    $('#estimated_arr').val(data);
                    validarCustomerCancel();
            
                });
        }else{
            $('#estimated_arr').val("");
        }
    }
    function obtener_test_on(){
        if($('#finish_prod').val() && $('#safety_test').val()==1){
            var ruta = "{{url('orders/obtener_test_on')}}";
            var finish=$('#finish_prod').val();
                $.get(ruta+'?finish_prod='+finish, function(data) {
                    $('#safety_test_on').empty();
                    $('#safety_test_on').val(data);
            
                });
            
        }else{
            $('#safety_test_on').val("");
        }
    }
    obtener_fama();
    obtener_arrival();
    obtener_test_on();
    validarLicencias();
    $('#export_cos').change(function(e) {
        e.preventDefault();
        obtener_fama();
        validarLicencias();
    
    });
    $('#shipping_to').change(function(e) {
        e.preventDefault();
        obtener_arrival();
    
    });
    $('#finish_prod').change(function(e) {
        e.preventDefault();
        obtener_arrival();
        obtener_test_on();
    
    });
    $('#safety_test').change(function(e) {
        e.preventDefault();
        obtener_test_on();
    
    });

      
    $("#licenses").change(function(e){
            e.preventDefault();
            validarLicencias();

    }); 
    $('#customer_cancel').change(function(e) {
        e.preventDefault();
        validarCustomerCancel();
    
    });

    function validarLicencias(){
        var _token = $("input[name='_token']").val();
    var export_cos = $("select[name='export_cos']").val();
    var licenses = $("select[name='licenses[]']").val();

        $.ajax({
            url: "/orders/validarLicencias",
            type:'POST',
            data: {
                _token:_token,
                export_cos:export_cos,
                licenses:licenses
                },
            success: function(data) {
                if(!$.isEmptyObject(data.error)){
                    $('#div_fama').addClass('has-error');
                    $('#div_fama span').show();
                    $('#div_fama strong').html(data.error[0]);
                }else{
                    $('#div_fama').removeClass('has-error');
                    $('#div_fama span').hide();
                }
            }
        });
    }
    function validarCustomerCancel(){
        if($('#shipping_to').val() && $('#finish_prod').val() && $('#customer_cancel').val()){
            var ruta = "{{url('orders/validar_customer_cancel')}}";
            var to=$('#shipping_to').val() ;
            var finish=$('#finish_prod').val();
            var customer_cancel=$('#customer_cancel').val();
                $.get(ruta+'?port_to='+to+'&finish_prod='+finish+'&customer_cancel='+customer_cancel, function(data) {
                 
                  if(data=='1'){
                    $('#div_customer_cancel').removeClass('has-error');
                  }else{
                    $('#div_customer_cancel').addClass('has-error');
                  }
            
                });
        }else{
            $('#div_customer_cancel').removeClass('has-error');
        }
    }
</script>

@endpush
