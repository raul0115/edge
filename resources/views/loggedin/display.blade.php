@extends('loggedin.layout')

@section('content')

@if (session('message'))
<div class="alert alert-success">
							<strong>Successful Action!</strong><br><br>
							<ul>
									<li>{{ session('message') }}</li>
							</ul>
						</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
                @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                @endforeach
        </ul>
</div>
@endif


<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Edit your own display settings<small> Uncheck the select boxes to hide the fields from your own search result. You can check them again if you want to see them again.</small></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>

                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content"">
                <form  method="POST" action="{{ url('/usermanagement/displayRightsSubmit') }}" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="form-group"><label class="col-sm-2 control-label"><small class="text-navy">Don't forget to click save changes</small></label>

                        <div class="col-sm-10">
                            @if($col_definations!=null)
                                @foreach ($col_definations as $rows)
                                <div class="i-checks"><label> <input name='{{ $rows->col_name }}' type='hidden' value="0"><input type="checkbox" name='{{ $rows->col_name }}' value="1" @if($rows->role_current==1) checked="checked" @endif > <i></i> {{$rows->col_discription }} </label></div>
                                @endforeach
                            @else
                            <small class="text-navy">Your admin has assign you no categorise yet. Contact the website administrator.</small>
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection

