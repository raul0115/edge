<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@include('loggedin.head')

<body>
    <div id="wrapper">
        @include('loggedin.nav_left')

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        @include('loggedin.nav_top')
        
        </div>


        <div class="wrapper wrapper-content">
        
@yield('content')

        </div>


        @include('loggedin.footer')
        
        </div>
      {{--  @include('loggedin.right_sidebar')--}}
    </div>

    <!-- Mainly scripts -->
    <script src="{{ asset('js/jquery-2.1.1.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

    <!-- Flot -->
    <script src="{{ asset('js/plugins/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.spline.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.symbol.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/curvedLines.js') }}"></script>

    <!-- Peity -->
    <script src="{{ asset('js/plugins/peity/jquery.peity.min.js') }}"></script>
    <script src="{{ asset('js/demo/peity-demo.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('js/inspinia.js') }}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>

    <!-- jQuery UI -->
    <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

    <!-- Jvectormap 
    <script src="{{ asset('js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>-->

    <!-- Sparkline
    <script src="{{ asset('js/plugins/sparkline/jquery.sparkline.min.js') }}"></script> -->

    <!-- Sparkline demo data 
    <script src="{{ asset('js/demo/sparkline-demo.js') }}"></script> -->

    <!-- ChartJS
    <script src="{{ asset('js/plugins/chartJs/Chart.min.js') }}"></script>-->
    <!-- icheck
    <script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>    -->
   <!-- Data picker -->
   <script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
   <script src="{{ asset('js/plugins/sweetalert/sweetalert2.all.js') }}"></script>
   
   @if(Request::path() == 'orders/open' or Request::path() == 'orders/closed')
   <!-- Select 2 PUT IF CONDITION HERE-->
   <script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
   
   <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
   <script src="{{ asset('js/plugins/dataTables/dataTables.bootstrap.min.js') }}"></script>
   <script src="{{ asset('js/plugins/dataTables/dataTables.fixedColumns.min.js') }}"></script>
   <script src="{{ asset('js/plugins/dataTables/dataTables.colReorder.min.js') }}"></script>
   <script>
    $(function() {
        $( document ).tooltip({track: true});
        $(document).on('click', '.dropdown-menu', function (e) {
            e.stopPropagation();
        });
    });
    
    $(document).ready(function(){
            dataTable = $('#order_table').DataTable({
                "bPaginate": false,
                "aaSorting": [],
                scrollY:        "600px",
                scrollX:        true,
                scrollCollapse: true,
                paging:         true,
                dom: "frtip",
                pageLength: {{$page}},
              
                fixedColumns:   {
                    leftColumns: 2
                },
                colReorder: true      
            });
            $("#searchbox_order").keyup(function() {
               dataTable.search($(this).val()).draw() ;
            });  

        });
      
   </script>
    @endif
    
    <script>
        $(document).ready(function(){
            $('#data_5 .input-daterange').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                format: 'yyyy-mm-dd',
            }, new Date());
            $('#fama_exp_date').datepicker({
                autoclose: true,
                format:'M d yyyy'
            }).datepicker("setDate", new Date());
            $('#table_export_cos_edit_fama_exp_date').datepicker({
                autoclose: true,
                format:'M d yyyy'
            });
        
        }); 

    </script>
     @stack('scripts')

</body>
</html>
