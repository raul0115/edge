@extends('loggedin.layout')

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>User Settings/Details page </h2>
                        <br>
                    <p>User ID:    {{ $userObj->id }}</p>
                    <p>User Name:  {{ $userObj->name }}</p>
                    <p>User Email: {{ $userObj->email }}</p>
                    <p>User Role: {{ $su_role}}</p>
@if(Auth::user()->hasRole('Admin')) 
<a href="{{ url('/usermanagement/newUpdateUsers/'.$userObj->id)}}" class="btn btn-sm btn-primary">Update</a>  
@endif

                </div>
                <div class="col-lg-2">
                </div>
            </div>
    <br>
@if (isset($tableData['factories']) and  $tableData['factories'] != null)
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>This is a Factory user. Associate this user to a factory. </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>

                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form  method="POST" action="{{ url('/usermanagement/assignFactorySubmit') }}" class="form-horizontal">
                    <input name='user_id' type='hidden' value="{{ $userObj->id }}">
                    {{ csrf_field() }}
                    <div class="form-group"> 
                        <label class="col-sm-2 control-label" for="sub_fty_produc_value">Select Factory</label>
                        <div class="col-sm-10" >
                            <select class="form-control" name="sub_fty_produc_value" id="sub_fty_produc_value">
                                <option value="">Select Factories</option>
                                @foreach ($tableData['factories'] as $factory)
                                 <option @if(isset($tableData['factory_ID']->factory_id) and $tableData['factory_ID']->factory_id === $factory->id) selected="selected" @endif 
                                             value="{{ $factory->id }}">{{ $factory->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endif


@if (isset($tableData['salesmen']) and $tableData['salesmen'] != null)
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>This is a Salesmen user. Associate this user to a Salesmen. </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>

                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form  method="POST" action="{{ url('/usermanagement/assignSalesmenSubmit') }}" class="form-horizontal">
                    <input name='user_id' type='hidden' value="{{ $userObj->id }}">
                    {{ csrf_field() }}
                    <div class="form-group"> 
                        <label class="col-sm-2 control-label" for="sub_salesmen_value">Select Salesmen</label>
                        <div class="col-sm-10" >
                            <select class="form-control" name="sub_salesmen_value" id="sub_salesmen_value">
                                <option value="">Select Salesmen</option>
                                @foreach ($tableData['salesmen'] as $salesmen)
                                 <option @if(isset($tableData['salesmen_ID']->salesmen_id) and $tableData['salesmen_ID']->salesmen_id === $salesmen->id) selected="selected" @endif 
                                             value="{{ $salesmen->id }}">{{ $salesmen->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endif


@if ($userObj->id and !$userObj->hasRole('Admin'))
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>User Fields Permission <small>Some fields will not appear depending on the user type.</small></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>

                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form   method="POST" action="{{ url('/usermanagement/fieldRightsSubmit') }}" class="form-horizontal">
                    <input name='user_id' type='hidden' value="{{ $userObj->id }}">
                    {{ csrf_field() }}
                    <div class="form-group"><label class="col-sm-2 control-label"><small class="text-navy">Don't forget to click save changes</small></label>
                        <div class="col-sm-10">
                            @if($col_definations!=null)
                                @foreach ($col_definations as $rows)
                                    <div class="i-checks"><label> <input name='{{ $rows->col_name }}' type='hidden' value="0"><input type="checkbox" name='{{ $rows->col_name }}' value="1" @if($rows->role_current==1) checked="checked" @endif > <i></i> {{$rows->col_discription }} </label></div>
                                @endforeach
                            @else
                            <small class="text-navy">This group has access to no categories. Change the user type or check the options under group management.</small>
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="button" onclick="window.location.href ='{{ asset('usermanagement/users') }}'">Cancel</button>
                            <button class="btn btn-primary" type="submit">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endif

@endsection