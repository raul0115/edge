@extends('loggedin.layout')

@section('content')

@if (session('message'))
<div class="alert alert-success">
    <strong>Successful Action!</strong><br><br>
    <ul>
        <li>{{ session('message') }}</li>
    </ul>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<script type="text/javascript">
function display_addnew_div(ID){
$(ID).toggle();
}
</script>
<div class="row m-b-lg m-t-lg">
                <div class="col-md-5">

                    <div class="profile-image">
                        <img src="/img/profile_small.jpg" class="img-circle circle-border m-b-md" alt="profile">
                    </div>
                    <div class="profile-info">
                        <div class="">
                            <div>
                                <h2 class="no-margins">
                                    {{ Auth::user()->name }}
                                </h2>
                                <h4>{{auth()->user()->mi_role()}} User</h4>
                                <small>

<form action="{{ url('/edit-profile') }}">
    <button type="submit" class="btn btn-primary">Edit Username</button>
</form>
<br>
<form action="{{ url('/edit-password') }}">
    <button type="submit"  class="btn btn-primary">Edit Password</button>
</form>
<br>
<form action="{{ url('/usermanagement/display') }}">
    <button type="submit" class="btn btn-primary">Display Management</button>
</form>

                                </small>
                            </div>
                        </div>
                    </div>
                </div>
    

            </div>
@endsection
