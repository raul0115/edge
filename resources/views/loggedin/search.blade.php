@extends('loggedin.layout')

@section('content')


@if (session('message'))
<div class="alert alert-success">
    <strong>Successful Action!</strong><br><br>
    <ul>
        <li>{{ session('message') }}</li>
    </ul>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

            @php
            $display = \App\Http\Controllers\PermissionController::checkUserEditPermissions();
            @endphp
            
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Advanced Search<small></small></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-12 col-md-12 b-r">
                        <h3 class="m-t-none m-b">Search Fields.</h3>
                        <form role="form" class="form-inline" method="POST" action="/orders/search">
                            {{ csrf_field() }}
                            <input type="hidden" name="action" value="search" />
                            <input type="hidden" name="action_type" value="search_field" />
                            <div class="form-group">
                                <select name="search_field" onchange="" class="input-sm form-control input-s-sm inline">
                                    <option value="" selected="selected">Select text field type</option>
@if(isset($display['po_number']) and $display['po_number']==1)                  <option value="po_number">PO number</option>@endif
@if(isset($display['comments']) and $display['comments']==1)                    <option value="comments">Comments</option>@endif
@if(isset($display['value']) and $display['value']==1)                          <option value="value">Value $</option>@endif
@if(isset($display['fty_produc']) and $display['fty_produc']==1)                <option value="fty_produc">Factory Producing</option>@endif
@if(isset($display['export_cos']) and $display['export_cos']==1)                <option value="export_cos">Export Co</option>@endif
@if(isset($display['prod_style']) and $display['prod_style']==1)                <option value="prod_style">Product Style</option>@endif
@if(isset($display['prod_type']) and $display['prod_type']==1)                  <option value="prod_type">Product Type</option>@endif
@if(isset($display['licenses']) and $display['licenses']==1)                    <option value="licenses">Licenses</option>@endif
@if(isset($display['customers']) and $display['customers']==1)                  <option value="customers">Customers</option>@endif
@if(isset($display['customer_po_number']) and $display['customer_po_number']==1)<option value="customer_po_number">Customer PO number</option>@endif
@if(isset($display['qty_ordered']) and $display['qty_ordered']==1)              <option value="qty_ordered">Quantity Ordered</option>@endif
@if(isset($display['shipping_from']) and $display['shipping_from']==1)          <option value="shipping_from">Shipping From</option>@endif
@if(isset($display['shipping_to']) and $display['shipping_to']==1)              <option value="shipping_to">Shipping To</option>@endif
@if(isset($display['cbm']) and $display['cbm']==1)                              <option value="cbm">CBM</option>@endif
@if(isset($display['book_container']) and $display['book_container']==1)        <option value="book_container">Book Container</option>@endif
@if(isset($display['david_ins_comment']) and $display['david_ins_comment']==1)  <option value="david_ins_comment">David Inspection Comments</option>@endif
@if(isset($display['svn_no']) and $display['svn_no']==1)                        <option value="svn_no">SVN Number</option>@endif
@if(isset($display['freight_forwarder']) and $display['freight_forwarder']==1)  <option value="freight_forwarder">Freight Forwarder</option>@endif
@if(isset($display['salesmen']) and $display['salesmen']==1)                    <option value="salesmen">Salesmen</option>@endif
                                </select></div>
                            <div class="form-group"> 
                                <input type="search" name="search_text"  placeholder="Enter Text" class="form-control">
                            </div>
                                <button class="btn btn-sm btn-primary" type="submit"><strong>Search Text</strong></button>
                                <!--<label> <input type="checkbox" name='exact_match' class="i-checks">Exact match (faster results)</label>-->
                        </form>
                    </div>
                        <hr>
<div class="col-sm-12 col-md-12 b-r">

                        <h3 class="m-t-none m-b">Search orders by dates.</h3>
                        <form role="form" class="form-inline" method="POST" action="{{ url('orders/search')}}">
                            {{ csrf_field() }}
                            <input type="hidden" name="action" value="search" />
                            <input type="hidden" name="action_type" value="search_date" />
                            <div class="form-group">
                                <select name="search_field" onchange="" class="input-sm form-control input-s-sm inline">
                                    <option value="" selected="selected">Select Date type</option>
@if(isset($display['date_placed']) and $display['date_placed']==1)              <option value="date_placed">Date Placed</option>@endif
@if(isset($display['tickets_ordered']) and $display['tickets_ordered']==1)      <option value="tickets_ordered">Tickets Ordered</option>@endif
@if(isset($display['tickets_received']) and $display['tickets_received']==1)    <option value="tickets_received">Tickets Received</option>@endif
@if(isset($display['finish_prod']) and $display['finish_prod']==1)              <option value="finish_prod">Finished Product</option>@endif
@if(isset($display['estimated_arr']) and $display['estimated_arr']==1)          <option value="estimated_arr">Estimate Arrival</option>@endif
@if(isset($display['customer_cancel']) and $display['customer_cancel']==1)      <option value="customer_cancel">Customer Cancel</option>@endif
@if(isset($display['act_etd_forwarder']) and $display['act_etd_forwarder']==1)  <option value="act_etd_forwarder">Actual ETD from Forwarder</option>@endif
@if(isset($display['act_eta_forwarder']) and $display['act_eta_forwarder']==1)  <option value="act_eta_forwarder">Actual ETA from Forwarder</option>@endif
@if(isset($display['safety_test_on']) and $display['safety_test_on']==1)        <option value="safety_test_on">Safety Test Need On</option><option value="rec_comm_inv_pking">Did we receive Comm Inv& Pking List for us?</option>@endif
@if(isset($display['fty_prod_safety_test']) and $display['fty_prod_safety_test']==1)<option value="fty_prod_safety_test">Fty Submitted Prod for Safety Test</option>@endif
@if(isset($display['rec_comm_inv_pking']) and $display['rec_comm_inv_pking']==1)<option value="rec_comm_inv_pking">Did we receive Comm Inv& Pking List for us?</option>@endif
@if(isset($display['rec_comm_inv_pking_home']) and $display['rec_comm_inv_pking_home']==1)<option value="rec_comm_inv_pking_home">Did we receive Comm Inv& Pking List for AtHome?</option>@endif
@if(isset($display['original_fcr']) and $display['original_fcr']==1)            <option value="original_fcr">Did we Receive Original FCR?</option>@endif
@if(isset($display['ctpat_form']) and $display['ctpat_form']==1)                <option value="ctpat_form">Did we Receive CTPAT form?</option>@endif
@if(isset($display['telex_release']) and $display['telex_release']==1)          <option value="telex_release">Did we receive Telex Release B/L?</option>@endif
@if(isset($display['okay_to_pay']) and $display['okay_to_pay']==1)              <option value="okay_to_pay">OK to Pay?</option>@endif
@if(isset($display['did_we_pay']) and $display['did_we_pay']==1)                <option value="did_we_pay">Did we Pay?</option>@endif
@if(isset($display['send_diann']) and $display['send_diann']==1)                <option value="send_diann">What date we sent to Diann</option>@endif
                                </select></div>
                            <div class="form-group" id="data_5">
                                <label class="font-noraml">Range select</label>
                                <div class="input-daterange input-group" id="datepicker">
                                    <input type="text" class="input-sm form-control" name="start" value=""/>
                                    <span class="input-group-addon">to</span>
                                    <input type="text" class="input-sm form-control" name="end" value="" />
                                </div>
                            </div>
                                <button class="btn btn-sm btn-primary" type="submit"><strong>Search Date</strong></button>
                        </form>
</div>
                        <br>
                        <hr>

<div class="col-sm-12 col-md-12 b-r">
                        <h3 class="m-t-none m-b">Search orders by Yes/No fields</h3>
                        <form  class="form-inline" role="form" method="POST" action="{{ url('orders/search')}}">
                            {{ csrf_field() }}
                            <input type="hidden" name="action" value="search" />
                            <input type="hidden" name="action_type" value="search_yes_no" />
                            <div class="form-group"><select name="search_field" onchange="" class="input-sm form-control input-s-sm inline">
                                    <option value="" selected="selected">Select Field</option>
@if(isset($display['open']) and $display['open']==1)                            <option value="open">PO still Open</option>@endif
@if(isset($display['tickets_needed']) and $display['tickets_needed']==1)        <option value="tickets_needed">Tickets Needed?</option>@endif
@if(isset($display['safety_test']) and $display['safety_test']==1)              <option value="safety_test">Safety Test Needed?</option>@endif
@if(isset($display['safety_test_pass']) and $display['safety_test_pass']==1)    <option value="safety_test_pass">Safety Test Passed?</option>@endif
@if(isset($display['david_ins_result']) and $display['david_ins_result']==1)    <option value="david_ins_result">David Inspection Results (Yes for Pass)</option>@endif
@if(isset($display['pro_photo']) and $display['pro_photo']==1)                  <option value="pro_photo">Receive Professional Photography?</option>@endif
@if(isset($display['receive_whse']) and $display['receive_whse']==1)            <option value="receive_whse">Did we receive in Whse?</option>@endif
                                </select></div>
                            <div class="form-group">
                                <select name="field_value" onchange="" class="input-sm form-control input-s-sm inline">
                                    <option value="" selected="selected">Select Yes/No</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                                <button class="btn btn-sm btn-primary" type="submit"><strong>Search Y/N</strong></button>
                        </form>
                        <br>
                        <hr>
                    </div>
<!--                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-8">
                                <form id='start_new_po' style="display:none;" role="form" method="POST" action="{{ url('/ordermanagement/startNewOrder') }}" class="form-inline" autocomplete="off">
                                    {{ csrf_field() }}
                                    <div class="form-group"><input type="text" name="new_orde_po" placeholder="Enter New Import PO #" class="form-control"/></div>

                                    <div class="form-group"><button class="btn btn-primary " type="submit" >Add</button></div>
                                </form>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Search results</h4>
                                <p class="text-center">
                                    <a href=""><i class="fa fa-search big-icon"></i></a>
                                </p>
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>

@if(isset($orders))
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Search Results<small></small></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                
               @if (!$orders->total())
                <div class="row"><div class="text-center"><h3>No order records found</h3></div></div>
                @elseif($orders)
                <div class="m-b-lg" style="margin-bottom: 6px;">
                    <div class="input-group" style="width: 100%">
                        <input type="text" id="searchbox_order" placeholder="Search and filter" class=" form-control">
                    </div>
                </div>
                <div class="table-responsive">
            @php
            $display = \App\Http\Controllers\PermissionController::checkUserDispalyPermissions();
            $styleR = "style=background-color:#F2F5A9;";//
            $styleG = "style=background-color:#1ab394;";//
            $styleY = "style=background-color:gray;cursor:not-allowed;";//#eee
            @endphp
<table id="order_table" class="table table-striped table-bordered">
    <thead>
        <tr>
@if(isset($display['order_id']) and $display['order_id']==1)                    <th>A</th>@endif
@if(isset($display['comments']) and $display['comments']==1)                    <th>B</th>@endif
@if(isset($display['po_number']) and $display['po_number']==1)                  <th>C</th>@endif
@if(isset($display['open']) and $display['open']==1)                            <th>D</th>@endif
@if(isset($display['value']) and $display['value']==1)                          <th>E</th>@endif
@if(isset($display['fty_produc']) and $display['fty_produc']==1)                <th>F</th>@endif
@if(isset($display['export_cos']) and $display['export_cos']==1)                <th>G</th>@endif
@if(isset($display['export_cos']) and $display['export_cos']==1)                <th>H</th>@endif
@if(isset($display['prod_style']) and $display['prod_style']==1)                <th>I</th>@endif
@if(isset($display['prod_type']) and $display['prod_type']==1)                  <th>J</th>@endif
@if(isset($display['licenses']) and $display['licenses']==1)                    <th>K</th>@endif
@if(isset($display['customers']) and $display['customers']==1)                  <th>L</th>@endif
@if(isset($display['customer_po_number']) and $display['customer_po_number']==1)<th>M</th>@endif
@if(isset($display['date_placed']) and $display['date_placed']==1)              <th>N</th>@endif
@if(isset($display['qty_ordered']) and $display['qty_ordered']==1)              <th>O</th>@endif
@if(isset($display['tickets_needed']) and $display['tickets_needed']==1)        <th>P</th>@endif
@if(isset($display['tickets_ordered']) and $display['tickets_ordered']==1)      <th>Q</th>@endif
@if(isset($display['tickets_received']) and $display['tickets_received']==1)    <th>R</th>@endif
@if(isset($display['shipping_from']) and $display['shipping_from']==1)          <th>S</th>@endif
@if(isset($display['shipping_to']) and $display['shipping_to']==1)              <th>T</th>@endif
@if(isset($display['finish_prod']) and $display['finish_prod']==1)              <th>U</th>@endif
@if(isset($display['estimated_arr']) and $display['estimated_arr']==1)          <th>V</th>@endif
@if(isset($display['customer_cancel']) and $display['customer_cancel']==1)      <th>W</th>@endif
@if(isset($display['cbm']) and $display['cbm']==1)                              <th>X</th>@endif
@if(isset($display['book_container']) and $display['book_container']==1)        <th>Y</th>@endif
@if(isset($display['act_etd_forwarder']) and $display['act_etd_forwarder']==1)  <th>Z</th>@endif
@if(isset($display['act_eta_forwarder']) and $display['act_eta_forwarder']==1)  <th>AA</th>@endif
@if(isset($display['safety_test']) and $display['safety_test']==1)              <th>AB</th>@endif
@if(isset($display['safety_test_on']) and $display['safety_test_on']==1)        <th>AC</th>@endif
@if(isset($display['fty_prod_safety_test']) and $display['fty_prod_safety_test']==1)<th>AD</th>@endif
@if(isset($display['safety_test_pass']) and $display['safety_test_pass']==1)    <th>AE</th>@endif
@if(isset($display['david_ins_result']) and $display['david_ins_result']==1)    <th>AF</th>@endif
@if(isset($display['david_ins_comment']) and $display['david_ins_comment']==1)  <th>AG</th>@endif
@if(isset($display['svn_no']) and $display['svn_no']==1)                        <th>AH</th>@endif
@if(isset($display['freight_forwarder']) and $display['freight_forwarder']==1)  <th>AI</th>@endif
@if(isset($display['rec_comm_inv_pking']) and $display['rec_comm_inv_pking']==1)<th>AJ</th>@endif
@if(isset($display['rec_comm_inv_pking_home']) and $display['rec_comm_inv_pking_home']==1)<th>AK</th>@endif
@if(isset($display['original_fcr']) and $display['original_fcr']==1)            <th>AL</th>@endif
@if(isset($display['ctpat_form']) and $display['ctpat_form']==1)                <th>AM</th>@endif
@if(isset($display['telex_release']) and $display['telex_release']==1)          <th>AN</th>@endif
@if(isset($display['pro_photo']) and $display['pro_photo']==1)                  <th>AO</th>@endif
@if(isset($display['okay_to_pay']) and $display['okay_to_pay']==1)              <th>AP</th>@endif
@if(isset($display['did_we_pay']) and $display['did_we_pay']==1)                <th>AQ</th>@endif
@if(isset($display['send_diann']) and $display['send_diann']==1)                <th>AR</th>@endif
@if(isset($display['receive_whse']) and $display['receive_whse']==1)            <th>AS</th>@endif
@if(isset($display['salesmen']) and $display['salesmen']==1)                    <th>AT</th>@endif
@if(isset($display['files']) and $display['files']==1)                          <th>AU</th>@endif
        </tr>
        <tr>
@if(isset($display['order_id']) and $display['order_id']==1)                    <th>Order ID</th> @endif
@if(isset($display['comments']) and $display['comments']==1)                    <th>Comments</th> @endif
@if(isset($display['po_number']) and $display['po_number']==1)                  <th>PO Number</th> @endif
@if(isset($display['open']) and $display['open']==1)                            <th>Status</th> @endif
@if(isset($display['value']) and $display['value']==1)                          <th>Value</th> @endif
@if(isset($display['fty_produc']) and $display['fty_produc']==1)                <th>Factory Producting</th> @endif
@if(isset($display['export_cos']) and $display['export_cos']==1)                <th>Export Co.</th> @endif
@if(isset($display['export_cos']) and $display['export_cos']==1)                <th>FAMA</th> @endif
@if(isset($display['prod_style']) and $display['prod_style']==1)                <th>Product Style</th> @endif
@if(isset($display['prod_type']) and $display['prod_type']==1)                  <th>Product Type</th> @endif
@if(isset($display['licenses']) and $display['licenses']==1)                    <th>Licenses</th> @endif
@if(isset($display['customers']) and $display['customers']==1)                  <th>Customer</th> @endif
@if(isset($display['customer_po_number']) and $display['customer_po_number']==1)<th>Customer PO #</th> @endif
@if(isset($display['date_placed']) and $display['date_placed']==1)              <th>Date Placed</th> @endif
@if(isset($display['qty_ordered']) and $display['qty_ordered']==1)              <th>Qty Ordered</th> @endif
@if(isset($display['tickets_needed']) and $display['tickets_needed']==1)        <th>Tickets Needed?</th> @endif
@if(isset($display['tickets_ordered']) and $display['tickets_ordered']==1)      <th>Tickets ordered</th> @endif
@if(isset($display['tickets_received']) and $display['tickets_received']==1)    <th>Tickets Received</th> @endif
@if(isset($display['shipping_from']) and $display['shipping_from']==1)          <th>Shipping From</th> @endif
@if(isset($display['shipping_to']) and $display['shipping_to']==1)              <th>Shipping To</th> @endif
@if(isset($display['finish_prod']) and $display['finish_prod']==1)              <th>Finish Prod</th> @endif
@if(isset($display['estimated_arr']) and $display['estimated_arr']==1)          <th>Estimated Arr</th> @endif
@if(isset($display['customer_cancel']) and $display['customer_cancel']==1)      <th>Customer Cancel</th> @endif
@if(isset($display['cbm']) and $display['cbm']==1)                              <th>CBM</th> @endif
@if(isset($display['book_container']) and $display['book_container']==1)        <th>Booking or Container #</th> @endif
@if(isset($display['act_etd_forwarder']) and $display['act_etd_forwarder']==1)  <th>Actual ETD from Forwarder</th> @endif
@if(isset($display['act_eta_forwarder']) and $display['act_eta_forwarder']==1)  <th>Actual ETA from Forwarder</th> @endif
@if(isset($display['safety_test']) and $display['safety_test']==1)              <th>Safety Test Needed?</th> @endif
@if(isset($display['safety_test_on']) and $display['safety_test_on']==1)        <th>Safety Test Need On</th> @endif
@if(isset($display['fty_prod_safety_test']) and $display['fty_prod_safety_test']==1)<th>Fty Submitted Prod for Safety Test</th> @endif
@if(isset($display['safety_test_pass']) and $display['safety_test_pass']==1)    <th>Safety Test Passed?</th> @endif
@if(isset($display['david_ins_result']) and $display['david_ins_result']==1)    <th>David Inspection Results</th> @endif
@if(isset($display['david_ins_comment']) and $display['david_ins_comment']==1)  <th>David Inspection comments</th> @endif
@if(isset($display['svn_no']) and $display['svn_no']==1)                        <th>SVN# (Autonumber)</th> @endif
@if(isset($display['freight_forwarder']) and $display['freight_forwarder']==1)  <th>Freight Forwarder</th> @endif
@if(isset($display['rec_comm_inv_pking']) and $display['rec_comm_inv_pking']==1)<th>Did we receive Comm Inv& Pking List for us?</th> @endif
@if(isset($display['rec_comm_inv_pking_home']) and $display['rec_comm_inv_pking_home']==1)<th>Did we receive Comm Inv& Pking List for AtHome?</th> @endif
@if(isset($display['original_fcr']) and $display['original_fcr']==1)            <th>Did we Receive Original FCR?</th> @endif
@if(isset($display['ctpat_form']) and $display['ctpat_form']==1)                <th>Did we Receive CTPAT form?</th> @endif
@if(isset($display['telex_release']) and $display['telex_release']==1)          <th>Did we receive Telex Release B/L?</th> @endif
@if(isset($display['pro_photo']) and $display['pro_photo']==1)                  <th>Receive Professional Photography?</th> @endif
@if(isset($display['okay_to_pay']) and $display['okay_to_pay']==1)              <th>OK to Pay?</th> @endif
@if(isset($display['did_we_pay']) and $display['did_we_pay']==1)                <th>Did we Pay?</th> @endif
@if(isset($display['send_diann']) and $display['send_diann']==1)                <th>What date we sent to Diann</th> @endif
@if(isset($display['receive_whse']) and $display['receive_whse']==1)            <th>Did we receive in Whse?</th> @endif
@if(isset($display['salesmen']) and $display['salesmen']==1)                    <th>Salesman</th> @endif
@if(isset($display['files']) and $display['files']==1)                          <th>Files</th> @endif
        </tr>
    </thead>
    <tbody>
        @foreach ($orders as $order)
        <tr>
            <!--Licensing Conditions or FAMA Condition -->
            @php  $Lic_Con = $Lic_Title = null;
            if($order->ExCFama==0 and (strpos($order->licensesNames, 'Marvel') !== false)) {$Lic_Con = true;$Lic_Title="Factory unapproved to produce Disney product.";}
            @endphp
            <!-- Delivery Condition -->
            @php $del_Con = null;
            if( ($order->act_eta_forwarder == null and $order->customer_cancel != null and strtotime($order->estimated_arr) > strtotime($order->customer_cancel)) ) { $del_Con = true; $del_Title="This date is before Estimated Arr";} 
            elseif($order->act_eta_forwarder != null and $order->customer_cancel != null and strtotime($order->act_eta_forwarder) > strtotime($order->customer_cancel) ) { $del_Con = true;  $del_Title="This date is before Actual ETA";} 
            @endphp
            <!-- Safety Test Conditions -->
            @php
            $safety_Title1 = $safety_Con1 = $safety_Title2 = $safety_Con2 = null;
            $date1=date_create($order->finish_prod);
            $date2=date_create($order->safety_test_on);
            $diff=date_diff($date1,$date2);
            if ($order->safety_test == 1 and $order->fty_prod_safety_test == null and $diff->format('%a') >= 21){$safety_Con1=true;$safety_Title1= "'Fty Submitted Prod' not entered and Safety Test is 21 days before Finish Prod";}
            if ($order->safety_test == 1 and ($order->safety_test_pass==null or $order->safety_test_pass==0) and $diff->format('%a') >= 10){$safety_Con2=true;$safety_Title2= "Safety Test is not Passed or is 10 days before Finish Prod";}
            @endphp
            <!-- CBM Conditions -->
            @php
            $CBM_Title = $CBM_Con = null;
            if ($safety_Con1==true or $safety_Con2 == true or $Lic_Con == true){$CBM_Con=true;$CBM_Title= "FAMA or Safety conditions are not met";}
            @endphp
            <!-- Payment Conditions -->
            @php 
            $OKPay_Title = $OKPay_Con = null;
            if (\App\Http\Controllers\TriggerEventsController::validateOkayToPay($order->id, $order->customersNames)) {$OKPay_Title = "You can now fill this field"; $OKPay_Con=true;}
            @endphp
            <!-- SVN Conditions -->
            @php 
            $SVN_Title = 'FAMA, Safety or Payment Conditions not meta';
            @endphp
            <!-- ETD Conditions -->
            @php 
            $d1=date_create(date('Y-m-d H:i:s'));//now
            if (!isset($order->act_etd_forwarder)){
                $d2=date_create($order->finish_prod );
                $df=date_diff($d1,$d2);
                $days_before_etd = $df->format('%d');//edt was this many days before today.
            }
            elseif(isset($order->act_etd_forwarder)){
                $d2=date_create($order->act_etd_forwarder);
                $df=date_diff($d1,$d2);
                $days_before_etd = $df->format('%d');//edt was this many days before today.
            }
            @endphp
@if(isset($display['order_id']) and $display['order_id']==1)                    <td>{{ $order->id }}</td> @endif
@if(isset($display['comments']) and $display['comments']==1)                    <td id="comments_t_{{ $order->id }}" class="td_ellipsis" title="{{ $order->comments }}" ondblclick="submit_textarea_fields({{ $order->id }}, 'comments', 'Comments', '{{ $order->comments }}')" >{{ $order->comments }}</td> @endif
@if(isset($display['po_number']) and $display['po_number']==1)                  <td id="po_number_t_{{ $order->id }}" ondblclick="submit_text_fields({{ $order->id }}, 'po_number', 'PO Number','{{ $order->po_number }}');">{{ $order->po_number }}</td> @endif
@if(isset($display['open']) and $display['open']==1)                            <td ondblclick="">@if($order->open==1) <span class=" label label-primary">OPEN</span> @elseif($order->open==0) <span class=" label label-danger">CLOSED</span> @endif</td> @endif
@if(isset($display['value']) and $display['value']==1)                          <td id="value_t_{{ $order->id }}" ondblclick="submit_numeric_fields({{ $order->id }}, 'value', 'Value $', '{{ $order->value }}')">$ {{ number_format($order->value) }}</td> @endif
@if(isset($display['fty_produc']) and $display['fty_produc']==1)                <td id="fty_produc_t_{{ $order->id }}" @if($order->ftyName )  title="{{ $order->ftyVId }}" @endif ondblclick="submit_fty_produc_field({{ $order->id }}, '{{ $order->fty_produc }}', '{{ $order->ftyVId }}')" @if(!$order->ftyName ) {{ $styleR }} title="Field required" @endif>{{ $order->ftyName }}</td> @endif
@if(isset($display['export_cos']) and $display['export_cos']==1)                <td id="export_cos_t_{{ $order->id }}" ondblclick="submit_export_cos_field({{ $order->id }}, '{{ $order->ExCName }}', '{{ $order->ExCFama }}', '{{ $order->export_cos }}')" @if($Lic_Con) {{ $styleR }} title="{{$Lic_Title}}" @endif @if(!$order->ExCName ) {{ $styleR }} title="Field required" @endif>{{ $order->ExCName }}</td> @endif
@if(isset($display['export_cos']) and $display['export_cos']==1)                <td id="export_cos_fema_{{ $order->id }}">@if($order->ExCFama===1) Yes @elseif($order->ExCFama===0) No @endif</td> @endif
@if(isset($display['prod_style']) and $display['prod_style']==1)                <td id="product_styles_t_{{ $order->id }}" ondblclick="submit_product_styles_field({{ $order->id }}, '{{ $order->prod_styleNames }}', '{{ $order->prod_styleId }}')">{{ $order->prod_styleNames }}</td> @endif
@if(isset($display['prod_type']) and $display['prod_type']==1)                  <td id="product_types_t_{{ $order->id }}" ondblclick="submit_product_types_field({{ $order->id }}, '{{ $order->prod_typesNames }}', '{{ $order->prod_typesId }}')">{{ $order->prod_typesNames }}</td> @endif
@if(isset($display['licenses']) and $display['licenses']==1)                    <td id="licenses_t_{{ $order->id }}" ondblclick="submit_licenses_field({{ $order->id }}, '{{ $order->licensesNames }}', '{{ $order->licensesId }}')" @if(!$order->licensesNames ) {{ $styleR }} title="Field required" @endif>{{ $order->licensesNames }}</td> @endif
@if(isset($display['customers']) and $display['customers']==1)                  <td id="customers_t_{{ $order->id }}" ondblclick="submit_customers_field({{ $order->id }}, '{{ $order->customersNames }}', '{{ $order->customersId }}')">{{ $order->customersNames }}</td> @endif
@if(isset($display['customer_po_number']) and $display['customer_po_number']==1)<td id="customer_po_number_t_{{ $order->id }}" ondblclick="submit_text_fields({{ $order->id }}, 'customer_po_number', 'Customer PO #', '{{ $order->customer_po_number }}');">{{ $order->customer_po_number }}</td> @endif
@if(isset($display['date_placed']) and $display['date_placed']==1)              <td id="date_placed_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'date_placed', 'Date Placed', '{{ $order->date_placed }}')">{{ $order->date_placed }}</td> @endif
@if(isset($display['qty_ordered']) and $display['qty_ordered']==1)              <td id="qty_ordered_t_{{ $order->id }}" ondblclick="submit_numeric_fields({{ $order->id }}, 'qty_ordered', 'Qty Ordered', '{{ $order->qty_ordered }}')">{{ $order->qty_ordered }}</td> @endif
@if(isset($display['tickets_needed']) and $display['tickets_needed']==1)        <td id="tickets_needed_t_{{ $order->id }}" ondblclick="submit_yes_no_fields({{ $order->id }}, 'tickets_needed', 'Tickets Needed?', '{{ $order->tickets_needed }}')">@if($order->tickets_needed===1) Yes @elseif($order->tickets_needed===0) No @endif</td> @endif
@if(isset($display['tickets_ordered']) and $display['tickets_ordered']==1)      <td id="tickets_ordered_t_{{ $order->id }}"  ondblclick="submit_tickets_ordered_field({{ $order->id }}, '{{ $order->tickets_orderedDates }}')">{{ $order->tickets_orderedDates }}</td> @endif
@if(isset($display['tickets_received']) and $display['tickets_received']==1)    <td id="tickets_received_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'tickets_received', 'Tickets Received', '{{ $order->tickets_received }}')">{{ $order->tickets_received }}</td> @endif
@if(isset($display['shipping_from']) and $display['shipping_from']==1)          <td id="shipping_from_t_{{ $order->id }}" ondblclick="submit_ports_field({{ $order->id }}, 'shipping_from', 'Shipping From', '{{ $order->port_ShipF_Name }}', '{{ $order->shipping_from }}')">{{ $order->port_ShipF_Name }}</td> @endif
@if(isset($display['shipping_to']) and $display['shipping_to']==1)              <td id="shipping_to_t_{{ $order->id }}" ondblclick="submit_ports_field({{ $order->id }}, 'shipping_to', 'Shipping To', '{{ $order->port_ShipT_Name }}', '{{ $order->shipping_to }}')">{{ $order->port_ShipT_Name }}</td> @endif
@if(isset($display['finish_prod']) and $display['finish_prod']==1)              <td id="finish_prod_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'finish_prod', 'Finish Product', '{{ $order->finish_prod }}')">{{ $order->finish_prod }}</td> @endif
@if(isset($display['estimated_arr']) and $display['estimated_arr']==1)          <td id="estimated_arr_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'estimated_arr', 'Estimated Arrival', '{{ $order->estimated_arr }}')">{{ $order->estimated_arr }}</td> @endif
@if(isset($display['customer_cancel']) and $display['customer_cancel']==1)      <td id="customer_cancel_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'customer_cancel', 'Customer Cancel', '{{ $order->customer_cancel }}')" @if( $del_Con )  {{ $styleR }} title="{{$del_Title}}" @endif>{{ $order->customer_cancel }}</td> @endif
@if(isset($display['cbm']) and $display['cbm']==1)                              <td id="cbm_t_{{ $order->id }}" @if( $CBM_Con ) {{ $styleR }} title="{{$CBM_Title}}" ondblclick="submit_cbm_fields({{ $order->id }}, 'cbm', 'CBM', '{{ $order->cbm }}', 1)" @else ondblclick="submit_cbm_fields({{ $order->id }}, 'cbm', 'CBM', '{{ $order->cbm }}', 0)" @endif>{{ $order->cbm }}</td> @endif
@if(isset($display['book_container']) and $display['book_container']==1)        <td id="book_container_t_{{ $order->id }}" ondblclick="submit_text_fields({{ $order->id }}, 'book_container', 'Booking or Container #', '{{ $order->book_container }}');">{{ $order->book_container }}</td> @endif
@if(isset($display['act_etd_forwarder']) and $display['act_etd_forwarder']==1)  <td id="act_etd_forwarder_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'act_etd_forwarder', 'Actual ETD from Forwarder', '{{ $order->act_etd_forwarder }}')">{{ $order->act_etd_forwarder }}</td> @endif
@if(isset($display['act_eta_forwarder']) and $display['act_eta_forwarder']==1)  <td id="act_eta_forwarder_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'act_eta_forwarder', 'Actual ETA from Forwarder', '{{ $order->act_eta_forwarder }}')">{{ $order->act_eta_forwarder }}</td> @endif
@if(isset($display['safety_test']) and $display['safety_test']==1)              <td id="safety_test_t_{{ $order->id }}" ondblclick="submit_yes_no_fields({{ $order->id }}, 'safety_test', 'Safety Test Needed?', '{{ $order->safety_test }}')" @if( $safety_Con1 ) {{ $styleR }} title="{{$safety_Title1}}" @endif @if($order->safety_test ===null ) {{ $styleR }} title="Field required" @endif>@if($order->safety_test===1) Yes @elseif($order->safety_test===0) No @endif</td> @endif
@if(isset($display['safety_test_on']) and $display['safety_test_on']==1)        <td id="safety_test_on_t_{{ $order->id }}" @if($order->safety_test===0) {{ $styleY }} @else ondblclick="submit_date_fields({{ $order->id }}, 'safety_test_on', 'Safety Test Need On', '{{ $order->safety_test_on }}')" @endif >{{ $order->safety_test_on }}</td> @endif
@if(isset($display['fty_prod_safety_test']) and $display['fty_prod_safety_test']==1)<td id="fty_prod_safety_test_t_{{ $order->id }}"  @if($order->safety_test===0) {{ $styleY }} @else ondblclick="submit_date_fields({{ $order->id }}, 'fty_prod_safety_test', 'Fty Submitted Prod for Safety Test', '{{ $order->fty_prod_safety_test }}')" @endif >{{ $order->fty_prod_safety_test }}</td> @endif
@if(isset($display['safety_test_pass']) and $display['safety_test_pass']==1)    <td id="safety_test_pass_t_{{ $order->id }}"  @if($order->safety_test===0) {{ $styleY }} @endif ondblclick="submit_yes_no_fields({{ $order->id }}, 'safety_test_pass', 'Safety Test Passed?', '{{ $order->safety_test_pass }}')" @if( $safety_Con2 ) {{ $styleR }} title="{{$safety_Title2}}" @endif>@if($order->safety_test_pass===1) Yes @elseif($order->safety_test_pass===0) No @endif</td> @endif
@if(isset($display['david_ins_result']) and $display['david_ins_result']==1)    <td id="david_ins_result_t_{{ $order->id }}" ondblclick="submit_pass_fail_fields({{ $order->id }}, 'david_ins_result', 'David Inspection Results', '{{ $order->david_ins_result }}')">@if($order->david_ins_result===1) Pass @elseif($order->david_ins_result===0) Fail @endif</td> @endif
@if(isset($display['david_ins_comment']) and $display['david_ins_comment']==1)  <td id="david_ins_comment_t_{{ $order->id }}" class="td_ellipsis" title="{{ $order->david_ins_comment }}" ondblclick="submit_textarea_fields({{ $order->id }}, 'david_ins_comment', 'David Inspection comments', '{{ $order->david_ins_comment }}')"  >{{ $order->david_ins_comment }}</td> @endif
@if(isset($display['svn_no']) and $display['svn_no']==1)                        <td id="svn_t_{{ $order->id }}" @if($order->svn_no==null and ($CBM_Con or !$OKPay_Con)) ondblclick="submit_svn_field({{ $order->id }},1)" {{ $styleR }} title="{{ $SVN_Title }}" @elseif($CBM_Con or !$OKPay_Con) {{ $styleR }} title="{{ $SVN_Title }}" @elseif ($order->svn_no==null) ondblclick="submit_svn_field({{ $order->id }})" {{ $styleG }} @endif>{{ $order->svn_no }}</td> @endif
@if(isset($display['freight_forwarder']) and $display['freight_forwarder']==1)  <td id="freight_forwarders_t_{{ $order->id }}" ondblclick="submit_freight_forwarders_field({{ $order->id }}, '{{ $order->FFName }}', '{{ $order->freight_forwarder }}')">{{ $order->FFName }}</td> @endif
@if(isset($display['rec_comm_inv_pking']) and $display['rec_comm_inv_pking']==1)<td id="rec_comm_inv_pking_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'rec_comm_inv_pking', 'Did we receive Comm Inv& Pking List for us?', '{{ $order->rec_comm_inv_pking }}')">{{ $order->rec_comm_inv_pking }}</td> @endif
@if(isset($display['rec_comm_inv_pking_home']) and $display['rec_comm_inv_pking_home']==1)<td id="rec_comm_inv_pking_home_t_{{ $order->id }}" @if ( strpos($order->customersNames, 'tHome') == false) {{ $styleY }} title="Only for AtHome" @else  ondblclick="submit_date_fields({{ $order->id }}, 'rec_comm_inv_pking_home', 'Did we receive Comm Inv& Pking List for AtHome?', '{{ $order->rec_comm_inv_pking_home }}')" @endif>{{ $order->rec_comm_inv_pking_home }}</td> @endif
@if(isset($display['original_fcr']) and $display['original_fcr']==1)            <td id="original_fcr_t_{{ $order->id }}" @if ( strpos($order->customersNames, 'obby Lobby') == false and strpos($order->customersNames, 'tHome') == false) {{ $styleY }} title="Only for Hobby Lobby or AtHome" @else ondblclick="submit_date_fields({{ $order->id }}, 'original_fcr', 'Did we Receive Original FCR?', '{{ $order->original_fcr }}')" @endif>{{ $order->original_fcr }}</td> @endif
@if(isset($display['ctpat_form']) and $display['ctpat_form']==1)                <td id="ctpat_form_t_{{ $order->id }}" @if ( strpos($order->customersNames, 'obby Lobby') == false) {{ $styleY }} title="Only for Hobby Lobby" @else ondblclick="submit_date_fields({{ $order->id }}, 'ctpat_form', 'Did we Receive CTPAT form?', '{{ $order->ctpat_form }}')" @endif>{{ $order->ctpat_form }}</td> @endif
@if(isset($display['telex_release']) and $display['telex_release']==1)          <td id="telex_release_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'telex_release', 'Did we receive Telex Release B/L?', '{{ $order->telex_release }}')">{{ $order->telex_release }}</td> @endif
@if(isset($display['pro_photo']) and $display['pro_photo']==1)                  <td id="pro_photo_t_{{ $order->id }}" ondblclick="submit_yes_no_fields({{ $order->id }}, 'pro_photo', 'Receive Professional Photography?', '{{ $order->pro_photo }}')">@if($order->pro_photo===1) Yes @elseif($order->pro_photo===0) No @endif</td> @endif
@if(isset($display['okay_to_pay']) and $display['okay_to_pay']==1)              <td id="okay_to_pay_t_{{ $order->id }}" @if( $OKPay_Con and $order->okay_to_pay == null ) ondblclick="submit_date_fields({{ $order->id }}, 'okay_to_pay', 'OK to Pay?', '{{ $order->okay_to_pay }}')"  {{ $styleG }} title="{{$OKPay_Title}}" @endif>{{ $order->okay_to_pay }}</td> @endif
@if(isset($display['did_we_pay']) and $display['did_we_pay']==1)                <td id="did_we_pay_t_{{ $order->id }}" @if( $order->okay_to_pay and $order->did_we_pay ==null ) ondblclick="submit_date_fields({{ $order->id }}, 'did_we_pay', 'Did we Pay?', '{{ $order->did_we_pay }}')"  {{ $styleG }} title="You can now fill this field" @endif>{{ $order->did_we_pay }}</td> @endif
@if(isset($display['send_diann']) and $display['send_diann']==1)                <td id="send_diann_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'send_diann', 'What date we sent to Diann', '{{ $order->send_diann }}')">{{ $order->send_diann }}</td> @endif
@if(isset($display['receive_whse']) and $display['receive_whse']==1)            <td id="receive_whse_t_{{ $order->id }}" ondblclick="submit_yes_no_fields({{ $order->id }}, 'receive_whse', 'Did we receive in Whse?', '{{ $order->receive_whse }}')">@if($order->receive_whse===1) Yes @elseif($order->receive_whse===0) No @endif</td> @endif
@if(isset($display['salesmen']) and $display['salesmen']==1)                    <td id="salesmen_t_{{ $order->id }}" ondblclick="submit_salesmen_field({{ $order->id }}, '{{ $order->salesmenNames }}', '{{ $order->salesmenId }}')">{{ $order->salesmenNames }}</td> @endif
@if(isset($display['files']) and $display['files']==1)                          <td id="files_t_{{ $order->id }}" >     <a onclick="view_files({{ $order->id }})">View</a> / <br/><a onclick="upload_files({{ $order->id }})">Upload</a></td> @endif
        </tr>
        @endforeach
    </tbody>
</table>{{ $orders->links() }}
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

@endif

@endsection
