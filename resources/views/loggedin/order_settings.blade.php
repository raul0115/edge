@extends('loggedin.layout')

@section('content')
@if (session('message'))
<div class="alert alert-success">
    <strong>Successful Action!</strong><br><br>
    <ul>
                    <li>{{ session('message') }}</li>
    </ul>
</div>
@endif
<div class="row">
<div class="col-lg-6">
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Days For Safety Test Needed On</h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
            <a class="close-link">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <form class="form-horizontal" method="POST" action="{{ url('/ordermanagement/submitDaysSafetyTestOn') }}" >
            {{ csrf_field() }}
            <div class="form-group {{ $errors->has('option_value') ? ' has-error' : '' }}">
                        
                    {{ Form::label('Days For Safety Test On', null, ['class' => 'col-lg-4 control-label']) }}
                    <div class="col-lg-8">
                        {{Form::text('option_value',old('option_value', $data['safety_test_on']),['class' => 'form-control'])}}
                        @if ($errors->has('option_value'))
                            <span class="help-block">
                                <strong>{{ $errors->first('option_value') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            <div class="form-group">
                <div class="col-lg-offset-4 col-lg-8">
                    <button class="btn btn-sm btn-white" type="submit">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
    
    
    
<div class="col-lg-6">
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Orders Display Limit</h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-wrench"></i>
            </a>
            <a class="close-link">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <form class="form-horizontal" method="POST" action="{{ url('/ordermanagement/submitOrderDisplayLimits') }}" >
            <p>Number of orders to be displayed on open and cosed order pages until you click the next button on pagination </p>{{ csrf_field() }}
            <div class="form-group"><label class="col-lg-4 control-label">Open orders</label>
                <div class="col-lg-8"><input name='open' value="{{ $data['open'] }}" type="number" min="0" placeholder="Open order pagination limit" class="form-control">
                </div>
            </div>
            <div class="form-group"><label class="col-lg-4 control-label">Closed orders</label>
                <div class="col-lg-8"><input name='closed' value="{{ $data['closed'] }}" type="number" min="0" placeholder="Closed order pagination limit" class="form-control"></div>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-4 col-lg-8">
                    <button class="btn btn-sm btn-white" type="submit">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
    @if(Auth::user()->hasRole('Admin'))
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Show/Hide column List P.O and Order column</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
    
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: none;">
                    <form  method="POST" action="{{ url('/usermanagement/groupSubmit') }}" class="form-horizontal">
                        {{ csrf_field() }}
                        <input name='group_type' type='hidden' value="List">
                        <div class="form-group"><label class="col-sm-2 control-label"><small class="text-navy">Don't forget to click save changes</small></label>
    
                            <div class="col-sm-10">
                                @foreach ($col_definations as $rows)
                                <div class="i-checks">
                                    <label> 
                                        <input name='{{ $rows->col_name }}' type='hidden' value="0">
                                        <input type="checkbox" name='{{ $rows->col_name }}' value="1" @if($rows->List==1) checked="checked" @endif > 
                                        <i></i> {{$rows->col_discription }} 
                                        <input name='{{ $rows->col_name.'-order' }}' type='text' value="{{$rows->Order}}" style="width:50px;margin-left:20px;">
                                    </label>
                                </div>
                               
                                @endforeach
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
    
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-white" type="submit">Cancel</button>
                                <button class="btn btn-primary" type="submit">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif

</div>
@endsection
