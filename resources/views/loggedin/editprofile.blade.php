@extends('loggedin.layout')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Edit User Details<small></small></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/edit-profile') }}">
                          {!! csrf_field() !!}
                          <input type="hidden" name="id" value="{{ $user->id }}">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="col-md-3 control-label">Name</label>

                            <div class="col-md-4">
                                <input type="text" class="form-control" name="name" value="{{ $errors->has('name') ? '' : $user->name }}" >

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif

                              
                        </div>
                           
                           
                        </div>

                           <div class="form-group">
                            <label class="col-md-3 control-label">Email</label>

                             <div class="col-md-4">
                                <input type="email" disabled class="form-control" name="email" value="{{ $user->email }}" >

                              
                            </div>
                           </div>

                      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-3 control-label">Password</label>
                             <div class="col-md-4">
                                <input type="password" class="form-control" name="password">


                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                              
                            </div>
                        </div>
                        <!--  
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-3 control-label">Confirm Password</label>
                             <div class="col-md-4">
                                <input type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                              
                            </div>
                        </div>-->
                           <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>Update
                                </button>
                            </div>
                        </div>

                    </form>
            </div>
        </div>
    </div>
</div>
@endsection