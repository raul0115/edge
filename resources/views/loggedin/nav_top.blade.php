<nav class="navbar navbar-static-top white-bg" role="navigation">
    @php 
    function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
    'y' => 'year',
    'm' => 'month',
    'w' => 'week',
    'd' => 'day',
    'h' => 'hour',
    'i' => 'minute',
    's' => 'second',
    );
    foreach ($string as $k => &$v) {
    if ($diff->$k) {
    $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
    } else {
    unset($string[$k]);
    }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
    } 
@endphp <div class="alert alert-danger text-center">Sandbox</div>
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            
            @if(Request::path() == 'orders/open' or Request::path() == 'orders/closed')
                <form role="search" class="navbar-form-custom" method="POST" action="/orders/search">
                    <div class="form-group"> 
                                <input type="hidden" name="search_field"  value="po_number" />
                                <input type="hidden" name="action_type" value="search_field" />
                        {{ csrf_field() }}
                        <input name="search_text" type="text" placeholder="Search by PO #..." class="form-control" name="top_search" id="top_search">
                    </div>
                </form>
            @endif
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <!--<span class="m-r-sm text-muted welcome-message">Welcome to your dashboard.</span>-->
                </li>
                <li class="dropdown" id='notification_top_nav'>
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell"></i>  
                        <span class="label label-primary">{{auth()->user()->unreadNotifications()->count()?auth()->user()->unreadNotifications()->count():null}}</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                    @forelse(auth()->user()->unreadNotifications()->limit(4)->get() as $notification)
                        <li id="notification_nav_{{$loop->index}}">
                            <div class="alert alert-success {{$notification->data['css_class']}}">
                                <!--<button class="close" onclick="remove_nav({{$loop->index}})" type="button">×</button>-->
<!--                                    <i class="fa fa-twitter fa-fw"></i> -->{{ $notification->data['message'] }}
                                <span class="pull-right text-muted small">{{ time_elapsed_string($notification->created_at) }}</span>
                            </div>
                        </li>
                    @empty
                        <li>
                            <a href="#">
                                <div>
                                    You have no unread notifications.
                                </div>
                            </a>
                        </li>
                    @endforelse
                    
                    @if(auth()->user()->notifications()->count()>=1)
                        <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="{{ asset('notifications') }}">
                                    <strong>See All Notifications</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    @endif
                    </ul>
                </li>


                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                    </form>
                </li>
            </ul>

        </nav>