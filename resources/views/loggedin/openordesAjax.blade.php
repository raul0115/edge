
            <div class="ibox-content">
                <div class="row">
                                    <div class="col-sm-4"><button class="btn btn-xl btn-primary inline" onclick="display_addnew_div('#start_new_po')" type="button"><strong>Start New Import PO</strong></button> </div>
                                    <div class="col-sm-8">
                                    <form id='start_new_po' style="display:none;" role="form" method="POST" action="{{ url('/ordermanagement/startNewOrder') }}" class="form-inline" autocomplete="off">
                                        {{ csrf_field() }}
                                        <div class="form-group"><input type="text" name="new_orde_po" placeholder="Enter New Import PO #" class="form-control"/></div>

                                        <div class="form-group"><button class="btn btn-primary " type="submit" >Add</button></div>
                                    </form>
                                        </div>
                                </div>
                
               @if (!$orders->total())
                <div class="row">
                    <div class="text-center"><h3>No order records found</h3></div></div>
                @elseif($orders)
<div class="table-responsive">
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Order ID</th>
            <th>Comments</th>
            <th>PO Number</th>
            <!--<th>Status</th>-->
            <th>Value</th>
            <th>Factory Producting</th>
            <!--<th>Vendor ID</th>-->
            <th>Export Co.</th>
            <th>FAMA</th>
            <th>Product Style</th>
            <th>Product Type</th>
            <th>Licenses</th>
            <th>Customer</th>
            <th>Customer PO #</th>
            <th>Date Placed</th>
            <th>Qty Ordered</th>
            <th>Tickets Needed?</th>
            <th>Tickets ordered</th>
            <th>Tickets Received</th>
            <th>Shipping From</th>
            <th>Shipping To</th>
            <th>Finish Prod</th>
            <th>Estimated Arr</th>
            <th>Customer Cancel</th>
            <th>CBM</th>
            <th>Booking or Container #</th>
            <th>Actual ETD from Forwarder</th>
            <th>Actual ETA from Forwarder</th>
            <th>Safety Test Needed?</th>
            <th>Safety Test Need On</th>
            <th>Fty Submitted Prod for Safety Test</th>
            <th>Safety Test Passed?</th>
            <th>David Inspection Results</th>
            <th>David Inspection comments</th>
            <th>SVN# (Autonumber)</th>
            <th>Freight Forwarder</th>
            <th>Did we receive Comm Inv& Pking List for us?</th>
            <th>Did we receive Comm Inv& Pking List for AtHome?</th>
            <th>Did we Receive Original FCR?</th>
            <th>Did we Receive CTPAT form?</th>
            <th>Did we receive Telex Release B/L?</th>
            <th>Receive Professional Photography?</th>
            <th>OK to Pay?</th>
            <th>Did we Pay?</th>
            <th>What date we sent to Diann</th>
            <th>Did we receive in Whse?</th>
            <th>Salesman</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($orders as $order)
        <tr>
            <!--Licensing Conditions or FAMA Condition -->
            @php  $Lic_Con = $Lic_Title = null;
            if($order->ExCFama==0 and (strpos($order->licensesNames, 'Marvel') !== false)) {$Lic_Con = true;$Lic_Title="Factory unapproved to produce Disney product.";}
            @endphp
            <!-- Delivery Condition -->
            @php $del_Con = null;
            if( ($order->act_eta_forwarder == null and $order->customer_cancel != null and strtotime($order->estimated_arr) > strtotime($order->customer_cancel)) ) { $del_Con = true; $del_Title="This date is before Estimated Arr";} 
            elseif($order->act_eta_forwarder != null and $order->customer_cancel != null and strtotime($order->act_eta_forwarder) > strtotime($order->customer_cancel) ) { $del_Con = true;  $del_Title="This date is before Actual ETA";} 
            @endphp
            <!-- Safety Test Conditions -->
            @php
            $safety_Title1 = $safety_Con1 = $safety_Title2 = $safety_Con2 = null;
            $date1=date_create($order->finish_prod);
            $date2=date_create($order->safety_test_on);
            $diff=date_diff($date1,$date2);
            if ($order->safety_test == 1 and $order->fty_prod_safety_test == null and $diff->format('%d days') >= 21){$safety_Con1=true;$safety_Title1= "'Fty Submitted Prod' not entered and Safety Test is 21 days before Finish Prod";}
            if ($order->safety_test == 1 and !$order->safety_test_pass and $diff->format('%d days') >= 10){$safety_Con2=true;$safety_Title2= "Safety Test is not Passed or is 10 days before Finish Prod";}
            @endphp
            <!-- CBM Conditions -->
            @php
            $CBM_Title = $CBM_Con = null;
            if ($order->cbm!=null and ($safety_Con1==true or $safety_Con2 == true or $Lic_Con == true) ){$CBM_Con=true;$CBM_Title= "FAMA or Safety conditions are not met";}
            @endphp
            <!-- Payment Conditions -->
            @php 
            $OKPay_Title = $OKPay_Con = null;
            if (\App\Http\Controllers\TriggerEventsController::validateOkayToPay($order->id, $order->customersNames)) {$OKPay_Title = "You can now fill this field"; $OKPay_Con=true;}
            @endphp
            <td>{{ $order->id }} </td>
            <td id="comments_t_{{ $order->id }}" ondblclick="submit_textarea_fields({{ $order->id }}, 'comments', 'Comments', '{{ $order->comments }}')">{{ $order->comments }}</td>
            <td id="po_number_t_{{ $order->id }}" ondblclick="submit_text_fields({{ $order->id }}, 'po_number', 'PO Number','{{ $order->po_number }}');">{{ $order->po_number }}</td>
            <!--<td ondblclick="">@if($order->open==1) <span class=" label label-primary">OPEN</span> @elseif($order->open==0) <span class=" label label-danger">CLOSED</span> @endif</td>-->
            <td id="value_t_{{ $order->id }}" ondblclick="submit_numeric_fields({{ $order->id }}, 'value', 'Value $', '{{ $order->value }}')">{{ $order->value }}</td>
            <td id="fty_produc_t_{{ $order->id }}" title="{{ $order->ftyVId }}" ondblclick="submit_fty_produc_field({{ $order->id }}, '{{ $order->fty_produc }}', '{{ $order->ftyVId }}')">{{ $order->ftyName }}</td>
            <!--<td id="vendor-id"></td>-->
            <td id="export_cos_t_{{ $order->id }}" ondblclick="submit_export_cos_field({{ $order->id }}, '{{ $order->ExCName }}', '{{ $order->ExCFama }}', '{{ $order->export_cos }}')" @if($Lic_Con) style="background-color: red;" title="{{$Lic_Title}}" @endif >{{ $order->ExCName }}</td>
            <td id="export_cos_fema_{{ $order->id }}" id="fama">@if($order->ExCFama===1) Yes @elseif($order->ExCFama===0) No @endif</td>
            <td id="product_styles_t_{{ $order->id }}" ondblclick="submit_product_styles_field({{ $order->id }}, '{{ $order->prod_styleNames }}', '{{ $order->prod_styleId }}')">{{ $order->prod_styleNames }}</td>
            <td id="product_types_t_{{ $order->id }}" ondblclick="submit_product_types_field({{ $order->id }}, '{{ $order->prod_typesNames }}', '{{ $order->prod_typesId }}')">{{ $order->prod_typesNames }}</td>
            <td id="licenses_t_{{ $order->id }}" ondblclick="submit_licenses_field({{ $order->id }}, '{{ $order->licensesNames }}', '{{ $order->licensesId }}')">{{ $order->licensesNames }}</td>
            <td id="customers_t_{{ $order->id }}" ondblclick="submit_customers_field({{ $order->id }}, '{{ $order->customersNames }}', '{{ $order->customersId }}')">{{ $order->customersNames }}</td>
            <td id="customer_po_number_t_{{ $order->id }}" ondblclick="submit_text_fields({{ $order->id }}, 'customer_po_number', 'Customer PO #', '{{ $order->customer_po_number }}');">{{ $order->customer_po_number }}</td>
            <td id="date_placed_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'date_placed', 'Date Placed', '{{ $order->date_placed }}')">{{ $order->date_placed }}</td>
            <td id="qty_ordered_t_{{ $order->id }}" ondblclick="submit_numeric_fields({{ $order->id }}, 'qty_ordered', 'Qty Ordered', '{{ $order->qty_ordered }}')">{{ $order->qty_ordered }}</td>
            <td id="tickets_needed_t_{{ $order->id }}" ondblclick="submit_yes_no_fields({{ $order->id }}, 'tickets_needed', 'Tickets Needed?', '{{ $order->tickets_needed }}')">@if($order->tickets_needed===1) Yes @elseif($order->tickets_needed===0) No @endif</td>
            <td id="tickets_ordered_t_{{ $order->id }}"  ondblclick="submit_tickets_ordered_field({{ $order->id }}, '{{ $order->tickets_orderedDates }}')">{{ $order->tickets_orderedDates }}</td>
            <td id="tickets_received_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'tickets_received', 'Tickets Received', '{{ $order->tickets_received }}')">{{ $order->tickets_received }}</td>
            <td id="shipping_from_t_{{ $order->id }}" ondblclick="submit_ports_field({{ $order->id }}, 'shipping_from', 'Shipping From', '{{ $order->port_ShipF_Name }}', '{{ $order->shipping_from }}')">{{ $order->port_ShipF_Name }}</td>
            <td id="shipping_to_t_{{ $order->id }}" ondblclick="submit_ports_field({{ $order->id }}, 'shipping_to', 'Shipping To', '{{ $order->port_ShipT_Name }}', '{{ $order->shipping_to }}')">{{ $order->port_ShipT_Name }}</td>
            <td id="finish_prod_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'finish_prod', 'Finish Product', '{{ $order->finish_prod }}')">{{ $order->finish_prod }}</td>
            <td id="estimated_arr_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'estimated_arr', 'Estimated Arrival', '{{ $order->estimated_arr }}')">{{ $order->estimated_arr }}</td>
            <td id="customer_cancel_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'customer_cancel', 'Customer Cancel', '{{ $order->customer_cancel }}')" @if( $del_Con )  style="background-color: red;" title="{{$del_Title}}" @endif>{{ $order->customer_cancel }}</td>
            <td id="cbm_t_{{ $order->id }}" ondblclick="submit_numeric_fields({{ $order->id }}, 'cbm', 'CBM', '{{ $order->cbm }}')" @if( $CBM_Con )  style="background-color: red;" title="{{$CBM_Title}}" @endif>{{ $order->cbm }}</td>
            <td id="book_container_t_{{ $order->id }}" ondblclick="submit_text_fields({{ $order->id }}, 'book_container', 'Booking or Container #', '{{ $order->book_container }}');">{{ $order->book_container }}</td>
            <td id="act_etd_forwarder_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'act_etd_forwarder', 'Actual ETD from Forwarder', '{{ $order->act_etd_forwarder }}')">{{ $order->act_etd_forwarder }}</td>
            <td id="act_eta_forwarder_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'act_eta_forwarder', 'Actual ETA from Forwarder', '{{ $order->act_eta_forwarder }}')">{{ $order->act_eta_forwarder }}</td>
            <td id="safety_test_t_{{ $order->id }}" ondblclick="submit_yes_no_fields({{ $order->id }}, 'safety_test', 'Safety Test Needed?', '{{ $order->safety_test }}')" @if( $safety_Con1 ) style="background-color: red;" title="{{$safety_Title1}}" @endif>@if($order->safety_test===1) Yes @elseif($order->safety_test===0) No @endif</td>
            <td id="safety_test_on_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'safety_test_on', 'Safety Test Need On', '{{ $order->safety_test_on }}')">{{ $order->safety_test_on }}</td>
            <td id="fty_prod_safety_test_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'fty_prod_safety_test', 'Fty Submitted Prod for Safety Test', '{{ $order->fty_prod_safety_test }}')">{{ $order->fty_prod_safety_test }}</td>
            <td id="safety_test_pass_t_{{ $order->id }}" ondblclick="submit_yes_no_fields({{ $order->id }}, 'safety_test_pass', 'Safety Test Passed?', '{{ $order->safety_test_pass }}')" @if( $safety_Con2 ) style="background-color: red;" title="{{$safety_Title2}}" @endif>@if($order->safety_test_pass===1) Yes @elseif($order->safety_test_pass===0) No @endif</td>
            <td id="david_ins_result_t_{{ $order->id }}" ondblclick="submit_pass_fail_fields({{ $order->id }}, 'david_ins_result', 'David Inspection Results', '{{ $order->david_ins_result }}')">@if($order->david_ins_result===1) Pass @elseif($order->david_ins_result===0) Fail @endif</td>
            <td id="david_ins_comment_t_{{ $order->id }}" ondblclick="submit_textarea_fields({{ $order->id }}, 'david_ins_comment', 'David Inspection comments', '{{ $order->david_ins_comment }}')">{{ $order->david_ins_comment }}</td>
            <td id="svn_t_{{ $order->id }}" ondblclick="submit_svn_field({{ $order->id }})" @if( ($CBM_Con or !$OKPay_Con))  style="background-color: red;" title="FAMA, Safety or Payment Conditions not met" @elseif (!$order->svn_no)  style="background-color: green;" @endif>{{ $order->svn_no }}</td>
            <td id="freight_forwarders_t_{{ $order->id }}" ondblclick="submit_freight_forwarders_field({{ $order->id }}, '{{ $order->FFName }}', '{{ $order->freight_forwarder }}')">{{ $order->FFName }}</td>
            <td id="rec_comm_inv_pking_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'rec_comm_inv_pking', 'Did we receive Comm Inv& Pking List for us?', '{{ $order->rec_comm_inv_pking }}')">{{ $order->rec_comm_inv_pking }}</td>
            <td id="rec_comm_inv_pking_home_t_{{ $order->id }}" @if ( strpos($order->customersNames, 'tHome') == false) style="background-color: gray;" title="Only for AtHome" @else  ondblclick="submit_date_fields({{ $order->id }}, 'rec_comm_inv_pking_home', 'Did we receive Comm Inv& Pking List for AtHome?', '{{ $order->rec_comm_inv_pking_home }}')" @endif>{{ $order->rec_comm_inv_pking_home }}</td>
            <td id="original_fcr_t_{{ $order->id }}" @if ( strpos($order->customersNames, 'obby Lobby') == false and strpos($order->customersNames, 'tHome') == false) style="background-color: gray;" title="Only for Hobby Lobby or AtHome" @else ondblclick="submit_date_fields({{ $order->id }}, 'original_fcr', 'Did we Receive Original FCR?', '{{ $order->original_fcr }}')" @endif>{{ $order->original_fcr }}</td>
            <td id="ctpat_form_t_{{ $order->id }}" @if ( strpos($order->customersNames, 'obby Lobby') == false) style="background-color: gray;" title="Only for Hobby Lobby" @else ondblclick="submit_date_fields({{ $order->id }}, 'ctpat_form', 'Did we Receive CTPAT form?', '{{ $order->ctpat_form }}')" @endif>{{ $order->ctpat_form }}</td>
            <td id="telex_release_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'telex_release', 'Did we receive Telex Release B/L?', '{{ $order->telex_release }}')">{{ $order->telex_release }}</td>
            <td id="pro_photo_t_{{ $order->id }}" ondblclick="submit_yes_no_fields({{ $order->id }}, 'pro_photo', 'Receive Professional Photography?', '{{ $order->pro_photo }}')">@if($order->pro_photo===1) Yes @elseif($order->pro_photo===0) No @endif</td>
            <td id="okay_to_pay_t_{{ $order->id }}" @if( $OKPay_Con and $order->okay_to_pay == null ) ondblclick="submit_date_fields({{ $order->id }}, 'okay_to_pay', 'OK to Pay?', '{{ $order->okay_to_pay }}')"  style="background-color: green;" title="{{$OKPay_Title}}" @endif>{{ $order->okay_to_pay }}</td>
            <td id="did_we_pay_t_{{ $order->id }}" @if( $order->okay_to_pay and $order->did_we_pay ==null ) ondblclick="submit_date_fields({{ $order->id }}, 'did_we_pay', 'Did we Pay?', '{{ $order->did_we_pay }}')"  style="background-color: green;" title="You can now fill this field" @endif>{{ $order->did_we_pay }}</td>
            <td id="send_diann_t_{{ $order->id }}" ondblclick="submit_date_fields({{ $order->id }}, 'send_diann', 'What date we sent to Diann', '{{ $order->send_diann }}')">{{ $order->send_diann }}</td>
            <td id="receive_whse_t_{{ $order->id }}" ondblclick="submit_yes_no_fields({{ $order->id }}, 'receive_whse', 'Did we receive in Whse?', '{{ $order->receive_whse }}')">@if($order->receive_whse===1) Yes @elseif($order->receive_whse===0) No @endif</td>
            <td id="salesmen_t_{{ $order->id }}" ondblclick="submit_salesmen_field({{ $order->id }}, '{{ $order->salesmenNames }}', '{{ $order->salesmenId }}')">{{ $order->salesmenNames }}</td>
        </tr>
        @endforeach

    </tbody>
</table>
                </div>
                {{ $orders->links() }}
                @endif
            </div>