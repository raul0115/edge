@extends('loggedin.layout')

<script type="text/javascript">
function display_addnew_div(tableID){
$(tableID).toggle();
}</script>
@section('content')

<div id="dialog" style="background-color: #e7eaec; display: none;">
                            <form action="" method="post">
                            <label class=" control-label" for="po_number">PO Number</label>
                            <input type="text" name="po_number" value="" class="form-control"/>
                            <button type="submit" class="btn btn-sm">Submit</button>
                            </form>
                        </div>
@if($getOrder)
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Requested Order Detail<small></small></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <!--  <div class="row ">
                    <div class="col-sm-12 form-horizontal order_blade">
                        <div class="form-group" >
                            <form action="" method="post">
                            <label class="col-sm-2 control-label" for="po_number">PO Number</label>
                            <div class="col-sm-2"><input type="text" name="po_number" value="{{ $getOrder->po_number }}" disabled="disabled" class="form-control"/></div>
                            <label class="col-lg-2 control-label" for="open">Status</label>
                            <div class="col-lg-2">  @if($getOrder->open==1) <span class=" label label-primary" style="display: block;margin: 5px 15px;">OPEN</span> @elseif($getOrder->open==0) <span class=" label label-danger"  style="display: block;margin: 5px 15px;">CLOSED</span> @endif</div>
                            <label class="col-lg-2  control-label" for="value">Value</label>
                            <div class="col-lg-2">@if($getOrder->value)<input type="number" name="value" value="{{  $getOrder->value }}" disabled="disabled"  class="form-control"/> @else <input type="text" name="po_number" value="Not yet set" disabled="disabled"  class="form-control"/> @endif</div>
                            </form>
                        </div>
                        <div class="form-group">
                            </div>
                        
                        <div class="form-group">
                            </div>
                        
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="comments">Comments</label>
                            <div class="col-lg-10">  <textarea name='comments' class="form-control">{{  $getOrder->value }}</textarea></div>
                        </div>  
                        
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="fty_produc">Factory Producting</label>
                            <div class="col-lg-2"></div>
                            <label class="col-lg-2 control-label" for="fty_produc">Factory Vendor ID</label>
                            <div class="col-lg-2"></div>
                            
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="export_cos">Export Co</label>
                            <div class="col-lg-2"></div>
                            <label class="col-lg-2 control-label" for="fty_produc">FAMA</label>
                            <div class="col-lg-2"></div>
                            
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="prod_style">Product Style</label>
                             <div class="col-lg-2"></div>
                            
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="prod_type">Product Type (Multichoice)</label>
                            <div class="col-lg-4"></div>
                            
                        </div>  
                        
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="licenses">Licenses</label>
                            <div class="col-lg-4"></div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="customers">Customers</label>
                            <div class="col-lg-4"></div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="customer_po_number">Customer PO Number</label>
                            <div class="col-lg-4"><input type="text" name="customers" value="{{ $getOrder->customers }}" disabled="disabled" class="form-control"/></div>
                        </div>
                        
                        <div class="form-group" id="data_1">
                            <label class="col-lg-2 control-label" for="date_placed">Date Placed</label>
                            <div class="col-lg-2 input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="date_placed" class="form-control" value="03/04/2014">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="qty_ordered">Qty Ordered</label>
                            <div class="col-lg-2"><input type="number" name="qty_ordered" value="{{ $getOrder->customers }}" disabled="disabled" class="form-control"/></div>
                        </div>
                        
                        <div class="form-group">
                            <form class="form-horizontal">
                                <label class="col-lg-2 control-label">Tickets Needed?</label>
                                <div class="col-lg-2"><select id="" name="tickets_needed" class="input-sm form-control input-s-sm inline" style="height: 34px;"><option value="">Not selected</option><option value="1">Yes</option><option value="0">No</option></select></div>
                            </form>
                        </div>
                        
                        <div class="form-group" id='tickets_ordered'>
                            <form class="form-horizontal">
                                <label class="col-lg-2 control-label">Tickets Ordered (only if ticket needed is Yes)</label>

                            </form>
                        </div>
                        
                        <div class="form-group" id='tickets_received'>
                            <form class="form-horizontal">
                                <label class="col-lg-2 control-label">Tickets Received</label>
                                <div class="col-lg-2 input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="tickets_received" class="form-control" value="03/04/2014">
                                </div>
                            </form>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="shipping_from">Shipping From</label>
                            <div class="col-lg-8"></div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="shipping_to">Shipping To</label>
                            <div class="col-lg-8"></div>
                        </div>
                        
                        <div class="form-group" id='finish_prod'>
                            <form class="form-horizontal">
                                <label class="col-lg-2 control-label">Finished Product</label>
                                <div class="col-lg-2 input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="finish_prod" class="form-control" value="">
                                </div>
                            </form>
                        </div>
                        <div class="form-group" id='estimated_arr'>
                            <form class="form-horizontal">
                                <label class="col-lg-2 control-label">Estimate Arrival</label>
                                <div class="col-lg-2 input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="estimated_arr" class="form-control" value="">
                                </div>
                            </form>
                        </div>
                        
                        <div class="form-group" id='customer_cancel'>
                            <form class="form-horizontal">
                                <label class="col-lg-2 control-label">Customer Cancel</label>
                                <div class="col-lg-2 input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="customer_cancel" class="form-control" value="">
                                </div>
                            </form>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="cbm">CBM</label>
                            <div class="col-lg-2">  <input type="text" name="cbm" value="{{ $getOrder->cbm }}" disabled="disabled" class="form-control"/></div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="book_container">Booking or Container #</label>
                            <div class="col-lg-8"><input type="text" name="book_container" value="{{ $getOrder->customers }}" disabled="disabled" class="form-control"/></div>
                        </div>
                        
                        <div class="form-group" id='act_etd_forwarder'>
                            <form class="form-horizontal">
                                <label class="col-lg-2 control-label">Actual ETD from Forwarder</label>
                                <div class="col-lg-2 input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="act_etd_forwarder" class="form-control" value="">
                                </div>
                            </form>
                        </div>
                        
                        <div class="form-group" id='act_eta_forwarder'>
                            <form class="form-horizontal">
                                <label class="col-lg-2 control-label">Actual ETA from Forwarder</label>
                                <div class="col-lg-2 input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="act_eta_forwarder" class="form-control" value="">
                                </div>
                            </form>
                        </div>
                        
                        <div class="form-group">
                            <form class="form-horizontal">
                                <label class="col-lg-2 control-label">Safety Test Needed?</label>
                                <div class="col-lg-2"><select id="" name="safety_test" class="input-sm form-control input-s-sm inline" style="height: 34px;"><option value="">Not selected</option><option value="1">Yes</option><option value="0">No</option></select></div>
                            </form>
                        </div>
                        
                        <div class="form-group" id='safety_test_on'>
                            <form class="form-horizontal">
                                <label class="col-lg-2 control-label">Safety Test On</label>
                                <div class="col-lg-2 input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="safety_test_on" class="form-control" value="">
                                </div>
                            </form>
                        </div>
                        <div class="form-group" id='fty_prod_safety_test'>
                            <form class="form-horizontal">
                                <label class="col-lg-2 control-label">Fty Submitted Prod for Safety Test</label>
                                <div class="col-lg-2 input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="fty_prod_safety_test" class="form-control" value="">
                                </div>
                            </form>
                        </div>
                        
                        <div class="form-group">
                            <form class="form-horizontal">
                                <label class="col-lg-2 control-label">Safety Test Pass</label>
                                <div class="col-lg-2"><select id="" name="safety_test_pass" class="input-sm form-control input-s-sm inline" style="height: 34px;"><option value="">Not selected</option><option value="1">Yes</option><option value="0">No</option></select></div>
                            </form>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="david_ins_comment">David Inspection comments</label>
                            <div class="col-lg-10">  <textarea name='david_ins_comment' class="form-control">{{  $getOrder->value }}</textarea></div>
                        </div>  
                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="svn_no">SVN# (Autonumber)</label>
                            <div class="col-sm-2"><input type="text" name="svn_no" value="{{ $getOrder->po_number }}" disabled="disabled" class="form-control"/></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="freight_forwarder">Freight Forwarder</label>
                            <div class="col-sm-2"></div>
                        </div>
                        
                        <div class="form-group">
                            <form class="form-horizontal">
                                <label class="col-lg-2 control-label">Did we receive Comm Inv& Pking List for us?</label>
                                <div class="col-lg-2"><select id="" name="rec_comm_inv_pking" class="input-sm form-control input-s-sm inline" style="height: 34px;"><option value="">Not selected</option><option value="1">Yes</option><option value="0">No</option></select></div>
                            </form>
                        </div> 
                        
                        <div class="form-group">
                        <form class="form-horizontal">
                            <label class="col-lg-2 control-label">Did we receive Comm Inv& Pking List for AtHome?</label>
                            <div class="col-lg-2"><select id="" name="rec_comm_inv_pking_home" class="input-sm form-control input-s-sm inline" style="height: 34px;"><option value="">Not selected</option><option value="1">Yes</option><option value="0">No</option></select></div>
                        </form>
                        </div>
                        
                        <div class="form-group">
                        <form class="form-horizontal">
                            <label class="col-lg-2 control-label">Did we Receive Original FCR?</label>
                            <div class="col-lg-2"><select id="" name="original_fcr" class="input-sm form-control input-s-sm inline" style="height: 34px;"><option value="">Not selected</option><option value="1">Yes</option><option value="0">No</option></select></div>
                        </form>
                        </div>
                        
                        <div class="form-group">
                        <form class="form-horizontal">
                            <label class="col-lg-2 control-label">Did we Receive CTPAT form?</label>
                            <div class="col-lg-2"><select id="" name="ctpat_form" class="input-sm form-control input-s-sm inline" style="height: 34px;"><option value="">Not selected</option><option value="1">Yes</option><option value="0">No</option></select></div>
                        </form>
                        </div>
                        
                        <div class="form-group">
                        <form class="form-horizontal">
                            <label class="col-lg-2 control-label">Did we receive Telex Release B/L?</label>
                            <div class="col-lg-2"><select id="" name="telex_release" class="input-sm form-control input-s-sm inline" style="height: 34px;"><option value="">Not selected</option><option value="1">Yes</option><option value="0">No</option></select></div>
                        </form>
                        </div>
                        
                        <div class="form-group">
                        <form class="form-horizontal">
                            <label class="col-lg-2 control-label">Receive Professional Photography?</label>
                            <div class="col-lg-2"><select id="" name="pro_photo" class="input-sm form-control input-s-sm inline" style="height: 34px;"><option value="">Not selected</option><option value="1">Yes</option><option value="0">No</option></select></div>
                        </form>
                        </div>
                        
                        <div class="form-group">
                        <form class="form-horizontal">
                            <label class="col-lg-2 control-label">Did we receive in Whse?</label>
                            <div class="col-lg-2"><select id="" name="receive_whse" class="input-sm form-control input-s-sm inline" style="height: 34px;"><option value="">Not selected</option><option value="1">Yes</option><option value="0">No</option></select></div>
                        </form>
                        </div>
                        
                        
                        <div class="form-group" id='okay_to_pay'>
                            <form class="form-horizontal">
                                <label class="col-lg-2 control-label">okay_to_pay</label>
                                <div class="col-lg-2 input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="okay_to_pay" class="form-control" value="">
                                </div>
                            </form>
                        </div>
                        
                        
                        <div class="form-group" id='did_we_pay'>
                            <form class="form-horizontal">
                                <label class="col-lg-2 control-label">did_we_pay</label>
                                <div class="col-lg-2 input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="did_we_pay" class="form-control" value="">
                                </div>
                            </form>
                        </div>
                        
                        
                        <div class="form-group" id='send_diann'>
                            <form class="form-horizontal">
                                <label class="col-lg-2 control-label">send_diann</label>
                                <div class="col-lg-2 input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="send_diann" class="form-control" value="">
                                </div>
                            </form>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="salesmen">Salesmen</label>
                            <div class="col-lg-8">  </div>
                        </div>
                        
                    </div>
                    
                    
                    <br>
<br><br><br><br><br><br>

                </div>-->
                
                <!--
                <tr>
                                <td>{{ $order->id }}</td>
                                <td>Comments</td>
                                <td id="target">{{ $order->po_number }}</td>
                                <td>@if($order->open==1) <span class=" label label-primary">OPEN</span> @elseif($order->open==0) <span class=" label label-danger">CLOSED</span> @endif</td>
                                <td>Value</td>
                                <td>Factory Producting</td>
                                <td>Vendor ID</td>
                                <td>Export Co.</td>
                                <td>Product Style</td>
                                <td>Product Type</td>
                                <td>Licenses</td>
                                <td>Customer</td>
                                <td>Customer PO #</td>
                                <td>Date Placed</td>
                                <td>Qty Ordered</td>
                                <td>Tickets Needed?</td>
                                <td>Tickets ordered</td>
                                <td>Tickets Received</td>
                                <td>Shipping From</td>
                                <td>Shipping To</td>
                                <td>Finish Prod</td>
                                <td>Estimated Arr</td>
                                <td>Customer Cancel</td>
                                <td>CBM</td>
                                <td>Booking or Container #</td>
                                <td>Actual ETD from Forwarder</td>
                                <td>Actual ETA from Forwarder</td>
                                <td>Safety Test Needed?</td>
                                <td>Safety Test Need On</td>
                                <td>Fty Submitted Prod for Safety Test</td>
                                <td>Safety Test Passed?</td>
                                <td>David Inspection Results</td>
                                <td>David Inspection comments</td>
                                <td>SVN# (Autonumber)</td>
                                <td>Freight Forwarder</td>
                                <td>Did we receive Comm Inv& Pking List for us?</td>
                                <td>Did we receive Comm Inv& Pking List for AtHome?</td>
                                <td>Did we Receive Original FCR?</td>
                                <td>Did we Receive CTPAT form?</td>
                                <td>Did we receive Telex Release B/L?</td>
                                <td>Receive Professional Photography?</td>
                                <td>OK to Pay?</td>
                                <td>Did we Pay?</td>
                                <td>What date we sent to Diann</td>
                                <td>Did we receive in Whse?</td>
                                <td>Salesman</td>
                            </tr>
                -->
                
                
                            
            </div>
        </div>
    </div>

</div>
@endif


<div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Search Orders Different Field Types<small></small></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-4 b-r">
                                <h3 class="m-t-none m-b">Search Fields.</h3>
                                <form role="form" method="POST" action="{{ url('/orders')}}">{{ csrf_field() }}
                                    <input type="hidden" name="action" value="search" />
                                    <input type="hidden" name="action_type" value="search_field" />
                                    <div class="form-group">
                                        <select name="search_field" onchange="" class="input-sm form-control input-s-sm inline">
                                            <option value="" selected="selected">Select text field type</option>
                                            <option value="po_number">PO number</option>
                                            <option value="comments">Comments</option>
                                            <option value="value">Value $</option>
                                            <option value="fty_produc">Factory Producing</option>
                                            <option value="fty_produc">Factory Vendor ID</option>
                                            <option value="export_cos">Export Co</option>
                                            <option value="prod_style">Product Style</option>
                                            <option value="prod_type">Product Type</option>
                                            <option value="licenses">Licenses</option>
                                            <option value="customers">Customers</option>
                                            <option value="customer_po_number">Customer PO number</option>
                                            <option value="qty_ordered">Quantity Ordered</option>
                                            <option value="shipping_from">Shipping From</option>
                                            <option value="shipping_to">Shipping To</option>
                                            <option value="cbm">CBM</option>
                                            <option value="book_container">Book Container</option>
                                            <option value="david_ins_comment">David Inspection Comments</option>
                                            <option value="svn_no">SVN Number</option>
                                            <option value="freight_forwarder">Freight Forwarder</option>
                                            <option value="salesmen">Salesmen</option>
                                        </select></div>
                                    <div class="form-group"> <input type="search" placeholder="Enter Text" class="form-control"></div>
                                    <div>
                                        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Search Text</strong></button>
                                        <label> <input type="checkbox" name='exact_match' class="i-checks">Exact match (faster results)</label>
                                    </div>
                                </form>
                                
                                <hr>
                                
                                
                                <h3 class="m-t-none m-b">Search Dates.</h3>
                                <form role="form" method="POST" action="{{ url('/orders')}}">{{ csrf_field() }}
                                    <input type="hidden" name="action" value="search" />
                                    <input type="hidden" name="action_type" value="search_date" />
                                    <div class="form-group">
                                        <select name="search_field" onchange="" class="input-sm form-control input-s-sm inline">
                                            <option value="" selected="selected">Select Date type</option>
                                            <option value="date_placed">Date Placed</option>
                                            <option value="tickets_ordered">Tickets Ordered</option>
                                            <option value="tickets_received">Tickets Received</option>
                                            <option value="shipping_from">Shipping From</option>
                                            <option value="finish_prod">Finished Product</option>
                                            <option value="estimated_arr">Estimate Arrival</option>
                                            <option value="customer_cancel">Customer Cancel</option>
                                            <option value="act_etd_forwarder">Actual ETD from Forwarder</option>
                                            <option value="act_eta_forwarder">Actual ETA from Forwarder</option>
                                            <option value="safety_test_on">Safety Test Need On</option>
                                            <option value="fty_prod_safety_test">Fty Submitted Prod for Safety Test</option>
                                            <option value="okay_to_pay">OK to Pay?</option>
                                            <option value="did_we_pay">Did we Pay?</option>
                                            <option value="send_diann">What date we sent to Diann</option>
                                        </select></div>
                                    <div class="form-group" id="data_5">
                                <label class="font-noraml">Range select</label>
                                <div class="input-daterange input-group" id="datepicker">
                                    <input type="text" class="input-sm form-control" name="start" value=""/>
                                    <span class="input-group-addon">to</span>
                                    <input type="text" class="input-sm form-control" name="end" value="@php echo date('m/d/Y');@endphp" />
                                </div>
                            </div>
                                    <div>
                                        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Search Date</strong></button>
                                    </div>
                                </form>
                                
                                <br>
                                 <hr>
                                
                                
                                <h3 class="m-t-none m-b">Search Yes/No Fields</h3>
                                <form role="form" method="POST" action="{{ url('/orders')}}">{{ csrf_field() }}
                                    <input type="hidden" name="action" value="search" />
                                    <input type="hidden" name="action_type" value="search_yes_no" />
                                        <div class="form-group"><select name="search_field" onchange="" class="input-sm form-control input-s-sm inline">
                                            <option value="" selected="selected">Select Field</option>
                                            <option value="open">PO still Open</option>
                                            <option value="tickets_needed">Tickets Needed?</option>
                                            <option value="safety_test">Safety Test Needed?</option>
                                            <option value="safety_test_pass">Safety Test Passed?</option>
                                            <option value="david_ins_result">David Inspection Results (Yes for Pass)</option>
                                            <option value="rec_comm_inv_pking">Did we receive Comm Inv& Pking List for us?</option>
                                            <option value="rec_comm_inv_pking_home">Did we receive Comm Inv& Pking List for AtHome?</option>
                                            <option value="original_fcr">Did we Receive Original FCR?</option>
                                            <option value="ctpat_form">Did we Receive CTPAT form?</option>
                                            <option value="telex_release">Did we receive Telex Release B/L?</option>
                                            <option value="pro_photo">Receive Professional Photography?</option>
                                            <option value="receive_whse">Did we receive in Whse?</option>
                                            </select></div>
                                    <div class="form-group">
                                        <select name="userRole" onchange="" class="input-sm form-control input-s-sm inline">
                                            <option value="" selected="selected">Select Yes/No</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <div>
                                        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Search Y/N</strong></button>
                                    </div>
                                </form>
                                 <br>
                                 <hr>
                            </div>
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-4"><button class="btn btn-xl btn-primary inline" onclick="display_addnew_div('#start_new_po')" type="button"><strong>Start New Import PO</strong></button> </div>
                                    <div class="col-sm-8">
                                    <form id='start_new_po' style="display:none;" role="form" method="POST" action="{{ url('/ordermanagement/startNewOrder') }}" class="form-inline" autocomplete="off">
                                        {{ csrf_field() }}
                                        <div class="form-group"><input type="text" name="new_orde_po" placeholder="Enter New Import PO #" class="form-control"/></div>

                                        <div class="form-group"><button class="btn btn-primary " type="submit" >Add</button></div>
                                    </form>
                                        </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-12">
                                    <h4>Search results</h4>
                                    <p class="text-center">
                                        <a href=""><i class="fa fa-search big-icon"></i></a>
                                    </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                
            </div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>View All Order PO</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">
                @if (!$orders->total())
                <div class="row"><div class="text-center"><h3>No order records found</h3></div></div>
                @elseif($orders)
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>

                                <th>ID</th>
                                <th>PO Number</th>
                                <th>Status</th>
                                <th>View Order</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $order)
                            <tr>
                                <td>{{ $order->id }}</td>
                                <td id="target">{{ $order->po_number }}</td>
                                <td>@if($order->open==1) <span class=" label label-primary">OPEN</span> @elseif($order->open==0) <span class=" label label-danger">CLOSED</span> @endif</td>
                                <td>
                                 <form method="POST" action="{{ url('/orders') }}"><input type="hidden" name="orders_id" value="{{ $order->id }}">{{ csrf_field() }}<button type="submit" class='btn-xs btn-info'>View</button></form>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                {{ $orders->links() }}
                @endif
            </div>
        </div>
    </div>
</div>

@endsection