@if (count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
@endif
<form action="/upload" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="file_order_id" value="0" />
    Select Field
    <select name="column_type" >
        <option value="rec_comm_inv_pking">Comm Inv, Pking List</option>
        <option value="original_fcr">Original FCR</option>
        <option value="ctpat_form">CTPAT form</option>
        <option value="telex_release">Telex Release B/L</option>
    </select>
    <br />
    <input type="file" name="file_upload_field[]" multiple />
    <br />
    <input type="submit" value="Upload" />
</form>