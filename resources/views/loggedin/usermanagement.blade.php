@extends('loggedin.layout')

@section('content')

<script src="{{ asset('js/sorttable.js') }}"></script>

<script type="text/javascript">function changeRoles(Id){
$("#updateButton" + Id).html("<button id='Submit" + Id + "' type='submit' class='btn-xs'>Update</button>");
}
function delete_users(id){
if (confirm("Are you sure you want to delete user. Click OK to continue")){
$('form#delete_user' + id).submit();
}
};</script>
<div class="row">

    @if (session('message'))
    <div class="alert alert-success">
        <strong>Successful Action!</strong><br><br>
        <ul>
            <li>{{ session('message') }}</li>
        </ul>
    </div>
    @endif

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>


@if ($editID)
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>User Fields Permission <small>Some fields will not appear depending on the user type.</small></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>

                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form   method="POST" action="{{ url('/usermanagement/fieldRightsSubmit') }}" class="form-horizontal">
                    <input name='user_id' type='hidden' value="{{ $editID }}">
                    {{ csrf_field() }}
                    <div class="form-group"><label class="col-sm-2 control-label"><small class="text-navy">Don't forget to click save changes</small></label>
                        <div class="col-sm-10"> 
                            @if($col_definations!=null)
                                @foreach ($col_definations as $rows)
                                    <div class="i-checks"><label> <input name='{{ $rows->col_name }}' type='hidden' value="0"><input type="checkbox" name='{{ $rows->col_name }}' value="1" @if($rows->role_current==1) checked="checked" @endif > <i></i> {{$rows->col_discription }} </label></div>
                                @endforeach
                            @else
                            <small class="text-navy">This group has access to no categories. Change the user type or check the options under group management.</small>
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="button" onclick="window.location.href ='{{ asset('usermanagement/users') }}'">Cancel</button>
                            <button class="btn btn-primary" type="submit">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>      
@endif


<div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Create New Users</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form role="form"  method="POST" action="{{ url('/usermanagement/createUser') }}" class="form-inline" autocomplete="off">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="sr-only">User Full Name</label>
                            <input type="username" name="username" placeholder="Enter user full name" id="username" class="form-control" value="{{ old('username') }}">
                            @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="sr-only">Email address</label>
                            <input type="email" name="email" placeholder="Enter email" id="email" class="form-control" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="Password" class="sr-only">Password</label>
                            <input  autocomplete="new-password" type="text" name="password" placeholder="Password" id="Password" class="form-control">
                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
    
                        <div class="form-group{{ $errors->has('userRole') ? ' has-error' : '' }}">
                        {{ Form::select('userRole', $roles, old('userRole'), ['id' => 'role','class'=>'input-sm form-control input-s-sm inline select2','style'=>'height: 34px;','placeholder' => 'Select User Role']) }}
                            @if ($errors->has('userRole'))
                            <span class="help-block">
                                <strong>{{ $errors->first('userRole') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div id ="factory" class="form-group{{ $errors->has('factory_id') ? ' has-error' : '' }} hidden">
                            <select id="factory_id" name="factory_id" class="input-sm form-control input-s-sm inline" style="height: 34px;">
                            </select>
                            @if ($errors->has('factory_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('factory_id') }}</strong>
                            </span>
                            @endif
                        </div>
        
                        <button class="btn btn-primary" type="submit" style="margin-bottom: 0px; border-bottom-width: 0px;">Create User</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Users' Permission Page </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">
                @if($searchOrders=='not found')
                <div class="row"><div class="text-center">No such User found.</div></div>
                @endif
                @if (!$users->total())
                <div class="row"><div class="text-center"><h3>No order records found</h3></div></div>
                @elseif($users)
                <div class="row">
                    <div class="col-sm-3 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="POST" action="{{ url('/usermanagement/users') }}"><input type="hidden" name="_token" value="{{ csrf_token() }}"><input type="search" name="userEmail" class="form-control input-sm" placeholder="Search User Email" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>

                                <th>User ID</th>
                                <th>User Name</th>
                                <th>User email</th>
                                <th>User Type/Group</th>
                                <th>Settings</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td><form method="POST" action="{{ url('/usermanagement/updateUsers') }}"><input type="hidden" name="userID" value="{{ $user->id }}">{{ csrf_field() }}
                                {{ Form::select('userRole', $roles,$user->RoleName, ['class'=>'input-sm form-control input-s-sm inline select2','onchange'=>"changeRoles($user->id)",'placeholder' => 'No User Role Defined']) }}
                                        
                                        <span id="updateButton{{ $user->id }}"></span> </form>
                                </td>
                                <td>    
                                    @if($user->RoleName !== 'Admin') 
                                        <a href="{{ url('/usermanagement/userSettings/'.$user->id)}}">Settings</a> 
                                    @endif
                                  
                                </td>
                                <td>  
                                   @if($user->active == 1) <form id="delete_user{{ $user->id }}" method="POST" action="{{ url('/usermanagement/deleteUsers') }}"><input type="hidden" name="DeleteuserID" value="{{ $user->id }}">{{ csrf_field() }}<button id='delete_button{{ $user->id }}' onclick="delete_users({{ $user->id }})" type="button" class='btn-xs btn-danger'>Deactivate</button></form>
                                   @elseif($user->active == 0)<form id="delete_user{{ $user->id }}" method="POST" action="{{ url('/usermanagement/activateUsers') }}"><input type="hidden" name="ActivateuserID" value="{{ $user->id }}">{{ csrf_field() }}<button id='activate_button{{ $user->id }}' type="submit" class='btn-xs btn-primary'>Activate</button></form> @endif
                                  
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                {{ $users->links() }}
                @endif
            </div>
        </div>
    </div>
</div>



@endsection
@push('scripts')
<script>
    $('#role').change(function(e) {
        e.preventDefault();
        if($(this).val()=="Factory"){
            $('#factory').removeClass('hidden');

            $.get('/usermanagement/factories', function(data) {
                $('#factory_id').empty();
                var option = $("<option></option>")
                .attr("value","")		                  
                .text('Seleccione');
                $('#factory_id').append(option);
              
                $.each(data, function(key, value) {
                    var option = $("<option></option>")
                        .attr("value",key)		                  
                        .text(value);
                    $('#factory_id').append(option);
                });
            });

        }else{
            $('#factory').addClass('hidden');
        }
    });

</script>

@endpush