@if (session('message'))
<div class="alert alert-success">
    <strong>Successful Action!</strong><br><br>
    <ul>
        <li>{{ session('message') }}</li>
    </ul>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

