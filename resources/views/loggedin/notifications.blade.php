@extends('loggedin.layout')

@section('content')

<script>
    
    function remove_nav(id, div_id) {
        $.ajax({
        type: "POST",
            url: 'notifications/delete_notifications',
            data: { notification_id: id, _token: '{{csrf_token()}}' },
            success: function (data) {
                if(data.success) {
                   // document.getElementById(div_id).remove();
                   var  node = document.getElementById(div_id);
                        node.parentNode.removeChild(node);
//                    $( "#"+div_id ).remove();
                        get_top_notifications();
                }
                if (typeof data.error !== 'undefined') {
                    swal({
                        title: "Error: "+data.error,
                        type: "error",
                    });
                }  
                    
                
            },
            error: function (data, textStatus, errorThrown) {
                console.log(data);
                swal({
                    title: "Server or Network error",
                    type: "error",
                });

            },
        });
    }
    
    function get_top_notifications(){
        $.ajax({
        type: "GET",
            url: 'notifications/unreadNotificationsTopMenu',
            success: function (data) { 
                console.log(data);
                $('#notification_top_nav').html(data);
            }
        });
    }
</script>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>All Notifications</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">

                @forelse(auth()->user()->notifications as $notification)
                    <div id="close_notification_{{$loop->index}}" class="alert @if(isset($notification->data['css_class'])) {{ $notification->data['css_class'] }} @endif alert-dismissable">
                       <button class="close" onclick="remove_nav('{{$notification->id}}', 'close_notification_{{$loop->index}}')" type="button">×</button>
                        {{ $notification->data['message'] }} <span class="pull-right text-muted small">{{ $notification->created_at }}</span>
                    </div>
                @empty

                    No notifications found

                @endforelse

            </div>
        </div>
        </div>
    </div>
</div>
@endsection