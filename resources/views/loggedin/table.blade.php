@extends('loggedin.layout')
<script type="text/javascript">

function display_addnew_div(tableID){
$(tableID).toggle();
}
function display_edit_div(tableID, editID, editName){
$(tableID).toggle();
$(tableID+ "_name").val(editName);
$(tableID+ "_id").val(editID);
}
function display_edit_code_div(tableID, editID, editName,editCode){
$(tableID).toggle();
$(tableID+ "_name").val(editName);
$(tableID+ "_code").val(editCode);
$(tableID+ "_id").val(editID);
}
function display_edit_character_div(tableID, editID, editName,editCode,editLicenseId){
$(tableID).toggle();
$(tableID+ "_name").val(editName);
$(tableID+ "_code").val(editCode);
$(tableID+ "_license_id").val(editLicenseId);
$(tableID+ "_id").val(editID);
}

function display_edit_license_div(tableID, editID, editName,fama,code){
$(tableID).toggle();
$(tableID+ "_name").val(editName);
$(tableID+ "_fama_require").val(fama).change();
$(tableID+ "_code").val(code);
$(tableID+ "_id").val(editID);
}
function display_edit_portsto_div(tableID, editID, editPortName,days_est_arrival){
$(tableID).toggle();
$(tableID+ "_port_name").val(editPortName);
$(tableID+ "_days_est_arrival").val(days_est_arrival).change();
$(tableID+ "_id").val(editID);
}

function display_edit_factory_div(tableID, editID, editName, vendor_id,full_name,address1,address2,city,province,zip,country,phone,bank_name,bank_address,bank_swift,usd_bank_acct) {
$(tableID).toggle();
$(tableID+ "_name").val(editName);
$(tableID+ "_id").val(editID);
$(tableID+ "_vendor_id").val(vendor_id);
$(tableID+ "_full_name").val(full_name); 
$(tableID+ "_address1").val(address1); 
$(tableID+ "_address2").val(address2); 
$(tableID+ "_city").val(city); 
$(tableID+ "_province").val(province); 
$(tableID+ "_zip").val(zip); 
$(tableID+ "_country").val(country); 
$(tableID+ "_phone").val(phone); 
$(tableID+ "_bank_name").val(bank_name); 
$(tableID+ "_bank_address").val(bank_address);
$(tableID+ "_bank_swift").val(bank_swift);
$(tableID+ "_usd_bank_acct").val(usd_bank_acct);   
}
function display_edit_ecportco_div(tableID, editID, editName, fama,fama_exp_date) {
$(tableID).toggle();
$(tableID+ "_name").val(editName);
$(tableID+ "_id").val(editID);
$(tableID+ "_fama_exp_date").val(fama_exp_date);
$(tableID+ "_fama_exp_date").datepicker({
   keyboardNavigation: false,
   forceParse: false,
   autoclose: true,
   format: 'yyyy-mm-dd',
});
$(tableID+ "_fama").val(fama).change();

}
</script>
@section('content')

@if (session('message'))

<div class="alert alert-success">
    <strong>Successful Action!</strong><br>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif



    <div class="row">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Factories Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_factories_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="factories">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_factories">
                            <input type="hidden"  id="table_factories_edit_id" name="edit_id" value="">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_factories_edit_name" name='name' type="text" value="" class="form-control" >
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Vendor ID</label>
                                    <input id="table_factories_edit_vendor_id" name='vendor_id' type="text" value="" class="form-control" >
                                </div>
                               
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Full Name</label>
                                        <input id="table_factories_edit_full_name" name='full_name' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Address1</label>
                                        <input id="table_factories_edit_address1" name='address1' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Address2</label>
                                        <input id="table_factories_edit_address2" name='address2' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>City</label>
                                        <input id="table_factories_edit_city" name='city' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Province</label>
                                        <input id="table_factories_edit_province" name='province' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Zip</label>
                                        <input id="table_factories_edit_zip" name='zip' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Country</label>
                                        <input id="table_factories_edit_country" name='country' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input id="table_factories_edit_phone" name='phone' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bank Name</label>
                                        <input id="table_factories_edit_bank_name" name='bank_name' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bank Address</label>
                                        <input id="table_factories_edit_bank_address" name='bank_address' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bank Swift</label>
                                        <input id="table_factories_edit_bank_swift" name='bank_swift' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Usd Bank Acct</label>
                                        <input id="table_factories_edit_usd_bank_acct" name='usd_bank_acct' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            
                            
                            <button class="btn btn-primary pull-right" type="submit"><strong>Edit</strong></button>
                                   
                            
                            <div class="clearfix"></div>
                        
                           
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_factories_addnew' class="col-sm-12" style="display:none;" >
                        <form role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="factories">{{ csrf_field() }}<input type="hidden" name="action" value="add_factories">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_factories_edit_name" name='name' type="text" value="" class="form-control" >
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Vendor ID</label>
                                    <input name='vendor_id' type="text" placeholder="Enter Vendor ID" class="form-control" >
                                </div>
                               
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Full Name</label>
                                        <input name='full_name' type="text" placeholder="Enter Full Name" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Address1</label>
                                        <input name='address1' type="text" placeholder="Enter Address1" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Address2</label>
                                        <input name='address2' type="text" placeholder="Enter Address2" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>City</label>
                                        <input name='city' type="text" placeholder="Enter City" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Province</label>
                                        <input name='province' type="text" placeholder="Enter Province" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Zip</label>
                                        <input name='zip' type="text" placeholder="Enter Zip" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Country</label>
                                        <input name='country' type="text" placeholder="Enter Country" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input name='phone' type="text" placeholder="Enter Phone" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bank Name</label>
                                        <input name='bank_name' type="text" placeholder="Enter Bank Name" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bank Address</label>
                                        <input name='bank_address' type="text" placeholder="Enter Bank Address" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bank Swift</label>
                                        <input name='bank_swift' type="text" placeholder="Enter Bank Swift" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Usd Bank Acct</label>
                                        <input name='usd_bank_acct' type="text" placeholder="Enter Usd Bank Acct" class="form-control" >
                                    </div>
                                   
                            </div>
                            
                            <button class="btn btn-primary pull-right" type="submit"><strong>Add New</strong></button>
                            <div class="clearfix"></div>
                            
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_factories_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='factories'> 
                                <input type="search" name="search_factories" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Vendor ID</th>
                                <th>Full Name</th>
                                <th>Address1</th>
                                <th>Address2</th>
                                <th>City</th>
                                <th>Province</th>
                                <th>Zip</th>
                                <th>Country</th>
                                <th>Phone</th>
                                <th>Bank Name</th>
                                <th>BAnk Address</th>
                                <th>Bank Swift</th>
                                <th>Usd Bank Acct</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($factories as $factory)
                            <tr>
                                <td>{{ $factory->id }}</td>
                                <td>{{ $factory->name }}</td>
                                <td>{{ $factory->vendor_id }}</td>
                                <td>{{ $factory->full_name }}</td>
                                <td>{{ $factory->address1 }}</td>
                                <td>{{ $factory->address2 }}</td>
                                <td>{{ $factory->city }}</td>
                                <td>{{ $factory->province }}</td>
                                <td>{{ $factory->zip }}</td>
                                <td>{{ $factory->country }}</td>
                                <td>{{ $factory->phone }}</td>
                                <td>{{ $factory->bank_name }}</td>
                                <td>{{ $factory->bank_address }}</td>
                                <td>{{ $factory->bank_swift }}</td>
                                <td>{{ $factory->usd_bank_acct }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_factory_div('#table_factories_edit', {{ $factory->id }} , '{{ $factory->name }}', '{{ $factory->vendor_id }}','{{ $factory->full_name }}','{{ $factory->address1 }}','{{ $factory->address2 }}','{{ $factory->city }}','{{ $factory->province }}','{{ $factory->zip }}','{{ $factory->country }}','{{ $factory->phone }}','{{ $factory->bank_name }}','{{ $factory->bank_address }}','{{ $factory->bank_swift }}','{{ $factory->usd_bank_acct }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($factory->active == 1)
                                    <form id="delete_factory{{ $factory->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_factory_id" value="{{ $factory->id }}"><input type="hidden" name="table" value="factories"><input type="hidden" name="action" value="delete_factories">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($factory->active == 0)
                                    <form id="activate_factory{{ $factory->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_factory_id" value="{{ $factory->id }}"><input type="hidden" name="table" value="factories"><input type="hidden" name="action" value="activate_factories">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $factories->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>    
            
            
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Export Co Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_export_cos_edit' class="col-sm-12" style="display:none;" >
                        <form role="form" method="POST" action="{{ url('/tables') }}">
                            {{csrf_field()}}
                            <input type="hidden" name="table" value="export_cos">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_export_cos">
                            <input type="hidden"  id="table_export_cos_edit_id" name="edit_id" value="">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_export_cos_edit_name" name='name' type="text" value="" class="form-control" >
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Fama</label>
                                    <select id='table_export_cos_edit_fama'  name='fama' class="form-control" style="height: 34px;">
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Fama Exp Date</label>
                                        <div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" name="fama_exp_date" id="table_export_cos_edit_fama_exp_date" class="form-control" value="">
                                        </div>
                                    </div>
                               
                            </div>
            
                            <button class="btn btn-primary pull-right" type="submit" style="margin-top:22px;"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_export_cos_addnew' class="col-sm-12" style="display:none;" >
                        <form role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="export_cos">
                            {{ csrf_field() }}
                            <input type="hidden" name="action" value="add_export_cos">
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input name='name' type="text" placeholder="Enter Export Name" class="form-control" >
                                    </div>
                               
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Fama</label>
                                        <select  name='fama' class="form-control" style="height: 34px;">
                                                <option value="1">Yes</option><option value="0">No</option>
                                            </select>
                                    </div>
                               
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Fama Exp Date</label>
                                        <div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" name="fama_exp_date" id="fama_exp_date" class="form-control" value="{{\Carbon\Carbon::now()->format('M d Y')}}">
                                        </div>
                                    </div>
                               
                            </div>
                            
                            <button class="btn btn-primary pull-right" type="submit"><strong>Add New</strong></button>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_export_cos_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='export_cos'> 
                                <input type="search" name="search_export_cos" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>FAMA</th>
                                <th>Fama Exp Date</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($export_cos as $export_co)
                            <tr>
                                <td>{{ $export_co->id }}</td>
                                <td>{{ $export_co->name }}</td>
                                <td>@if ($export_co->fama == 1) Yes @elseif ($export_co->fama == 0) No @endif </td>
                                <td>{{($export_co->fama_exp_date)?$export_co->fama_exp_date->format('M d Y'):null }}</td>
                                
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_ecportco_div('#table_export_cos_edit', {{ $export_co->id }} , '{{ $export_co->name }}', {{ $export_co->fama }},'{{ ($export_co->fama_exp_date)?$export_co->fama_exp_date->format('M d Y'):null }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($export_co->active == 1)
                                    <form id="delete_export_co{{ $export_co->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_export_co_id" value="{{ $export_co->id }}"><input type="hidden" name="table" value="export_cos"><input type="hidden" name="action" value="delete_export_cos">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($export_co->active == 0)
                                    <form id="activate_export_co{{ $export_co->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_export_co_id" value="{{ $export_co->id }}"><input type="hidden" name="table" value="export_cos"><input type="hidden" name="action" value="activate_export_cos">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $export_cos->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>        
    
            
    </div>




<div class="row">
    
    
    
    <div class="col-sm-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Customers Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_customers_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="customers">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_customers">
                            <input type="hidden"  id="table_customers_edit_id" name="edit_id" value="">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>code</label>
                                    <input id="table_customers_edit_code" name='code' type="text" value="" class="form-control" maxlength="2">
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_customers_edit_name" name='name' type="text" value="" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
        
                            <button class="btn btn-primary pull-right"  style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_customers_addnew' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="customers">{{ csrf_field() }}<input type="hidden" name="action" value="add_customers">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Code</label>
                                    <input name='code' type="text" placeholder="Enter  " class="form-control" maxlength="1">
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter Customer Name" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
        
                            <button class="btn btn-primary pull-right"  style="margin-top:22px;" type="submit"><strong>Add New</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_customers_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='customers'> 
                                <input type="search" name="search_customers" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($customers as $customer)
                            <tr>
                                <td>{{ $customer->id }}</td>
                                <td>{{ $customer->code }}</td>
                                <td>{{ $customer->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_code_div('#table_customers_edit', {{ $customer->id }} , '{{ $customer->name }}','{{ $customer->code }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($customer->active == 1)
                                    <form id="delete_customer{{ $customer->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_customer_id" value="{{ $customer->id }}"><input type="hidden" name="table" value="customers"><input type="hidden" name="action" value="delete_customers">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($customer->active == 0)
                                    <form id="activate_customer{{ $customer->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_customer_id" value="{{ $customer->id }}"><input type="hidden" name="table" value="customers"><input type="hidden" name="action" value="activate_customers">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $customers->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
    
    
    
    
    <div class="col-sm-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Ports Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_ports_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="ports">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_ports">
                            <input type="hidden"  id="table_ports_edit_id" name="edit_id" value="">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_ports_edit_name" name='name' type="text" value="" class="form-control" >
                                </div>
                           
                            </div>
        
                           
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_ports_addnew' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="ports">{{ csrf_field() }}<input type="hidden" name="action" value="add_ports">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter Port Name" class ="form-control" >
                                </div>
                           
                            </div>
        
                           
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_ports_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value='ports'> 
                                <input type="search" name="search_ports" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($ports as $port)
                            <tr>
                                <td>{{ $port->id }}</td>
                                <td>{{ $port->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_div('#table_ports_edit', {{ $port->id }} , '{{ $port->name }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($port->active == 1)
                                    <form id="delete_port{{ $port->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_port_id" value="{{ $port->id }}"><input type="hidden" name="table" value="ports"><input type="hidden" name="action" value="delete_ports">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($port->active == 0)
                                    <form id="activate_port{{ $port->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_port_id" value="{{ $port->id }}"><input type="hidden" name="table" value="ports"><input type="hidden" name="action" value="activate_ports">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $ports->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Ports To Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_portsto_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="portsto">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_portsto">
                            <input type="hidden"  id="table_portsto_edit_id" name="edit_id" value="">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Port Name</label>
                                    <input id="table_portsto_edit_port_name" name='port_name' type="text" value="" class="form-control" >
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Days Est Arrival</label>
                                    <input id="table_portsto_edit_days_est_arrival" name='days_est_arrival' type="text" value="" class="form-control" >
                                </div>
                           
                            </div>
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_portsto_addnew' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="portsto">
                            {{ csrf_field() }}
                            <input type="hidden" name="action" value="add_portsto">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Port Name</label>
                                    <input name='port_name' type="text" placeholder="Enter Port Name" class="form-control" >
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label>Days Est Arrival</label>
                                        <input name='days_est_arrival' type="text" placeholder="Enter Days Est Arrival" class="form-control" >
                                    </div>
                                </div>
                           
                            </div>
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_portsto_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='ports_to'> 
                                <input type="search" name="search_portsto" class="form-control input-sm" placeholder="Search Port Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Port Name</th>
                                <th>Days Est Arrival</th>
                                <th>Edit</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($portsto as $ports_to)
                            <tr>
                                <td>{{ $ports_to->id }}</td>
                                <td>{{ $ports_to->port_name }}</td>
                                <td>{{ $ports_to->days_est_arrival }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_portsto_div('#table_portsto_edit', {{ $ports_to->id }} , '{{ $ports_to->port_name }}',{{$ports_to->days_est_arrival}} );">Edit</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $portsto->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>  
           
            
   <div class="col-sm-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>License Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_licenses_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="licenses">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_licenses">
                            <input type="hidden"  id="table_licenses_edit_id" name="edit_id" value="">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Code</label>
                                    <input id="table_licenses_edit_code" name='code' type="text" value="" class="form-control" maxlength="1">
                                </div>
                           
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_licenses_edit_name" name='name' type="text" value="" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Fama</label>
                                    <select id='table_licenses_edit_fama_require'  name='fama_require' class="form-control" style="height: 34px;">
                                            <option selected="selected" value="">Yes/No</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                </div>
                           
                            </div>
                            
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_licenses_addnew' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="licenses">
                            {{ csrf_field() }}
                            <input type="hidden" name="action" value="add_licenses">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Code</label>
                                    <input name='code' type="text" placeholder="Enter code" class="form-control" maxlength="1">
                                </div>
                           
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter License Name" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Fama</label>
                                    <select id='fama_require'  name='fama_require' class="form-control" style="height: 34px;">
                                            <option selected="selected" value="">Yes/No</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                </div>
                           
                            </div>
                           
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_licenses_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='licenses'> 
                                <input type="search" name="search_licenses" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Fama</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($licenses as $license)
                            <tr>
                                <td>{{ $license->id }}</td>
                                <td>{{ $license->code }}</td>
                                <td>{{ $license->name }}</td>
                                <td>{{ $license->fama() }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_license_div('#table_licenses_edit', {{ $license->id }} , '{{ $license->name }}','{{$license->fama_require}}','{{ $license->code }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($license->active == 1)
                                    <form id="delete_license{{ $license->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_license_id" value="{{ $license->id }}"><input type="hidden" name="table" value="licenses"><input type="hidden" name="action" value="delete_licenses">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($license->active == 0)
                                    <form id="activate_license{{ $license->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_license_id" value="{{ $license->id }}"><input type="hidden" name="table" value="licenses"><input type="hidden" name="action" value="activate_licenses">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $licenses->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>  
            
    <div class="clearfix"></div>
    <div class="col-sm-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Product styles Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_product_styles_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="product_styles">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_product_styles">
                            <input type="hidden"  id="table_product_styles_edit_id" name="edit_id" value="">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_product_styles_edit_name" name='name' type="text" value="" class="form-control" >
                                </div>
                           
                            </div>
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_product_styles_addnew' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="product_styles">{{ csrf_field() }}<input type="hidden" name="action" value="add_product_styles">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter Product Style" class="form-control" >
                                </div>
                           
                            </div>
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_product_styles_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='product_styles'> 
                                <input type="search" name="search_product_styles" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($product_styles as $product_style)
                            <tr>
                                <td>{{ $product_style->id }}</td>
                                <td>{{ $product_style->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_div('#table_product_styles_edit', {{ $product_style->id }} , '{{ $product_style->name }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($product_style->active == 1)
                                    <form id="delete_product_style{{ $product_style->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_product_style_id" value="{{ $product_style->id }}"><input type="hidden" name="table" value="product_styles"><input type="hidden" name="action" value="delete_product_styles">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($product_style->active == 0)
                                    <form id="activate_product_style{{ $product_style->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_product_style_id" value="{{ $product_style->id }}"><input type="hidden" name="table" value="product_styles"><input type="hidden" name="action" value="activate_product_styles">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $product_styles->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
    
    
    
    
    <div class="col-sm-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Product Types Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_product_types_edit' class="col-sm-12" style="display:none;" >
                        <form role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="product_types">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_product_types">
                            <input type="hidden"  id="table_product_types_edit_id" name="edit_id" value="">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_product_types_edit_name" name='name' type="text" value="" class="form-control" >
                                </div>
                           
                            </div>
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_product_types_addnew' class="col-sm-12" style="display:none;" >
                        <form role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="product_types">
                            {{ csrf_field() }}
                            <input type="hidden" name="action" value="add_product_types">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter Product Type" class p="form-control" >
                                </div>
                           
                            </div>
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_product_types_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='product_types'> 
                                <input type="search" name="search_product_types" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($product_types as $product_type)
                            <tr>
                                <td>{{ $product_type->id }}</td>
                                <td>{{ $product_type->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_div('#table_product_types_edit', {{ $product_type->id }} , '{{ $product_type->name }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($product_type->active == 1)
                                    <form id="delete_product_type{{ $product_type->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_product_type_id" value="{{ $product_type->id }}"><input type="hidden" name="table" value="product_types"><input type="hidden" name="action" value="delete_product_types">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($product_type->active == 0)
                                    <form id="activate_product_type{{ $product_type->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_product_type_id" value="{{ $product_type->id }}"><input type="hidden" name="table" value="product_types"><input type="hidden" name="action" value="activate_product_types">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $product_types->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
            
    <div class="clearfix"></div>
            
   <div class="col-sm-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Freight Forwarder Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_freight_forwarders_edit' class="col-sm-12" style="display:none;" >
                        <form class="form" role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="freight_forwarders">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_freight_forwarders">
                            <input type="hidden"  id="table_freight_forwarders_edit_id" name="edit_id" value="">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_freight_forwarders_edit_name" name='name' type="text" value="" class="form-control" >
                                </div>
                           
                            </div>
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_freight_forwarders_addnew' class="col-sm-12" style="display:none;" >
                        <form role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="freight_forwarders">{{ csrf_field() }}<input type="hidden" name="action" value="add_freight_forwarders">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter Fre For Name" class="form-control" >
                                </div>
                           
                            </div>
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New</strong></button>
                            <div class="clearfix"></div>

                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_freight_forwarders_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='freight_forwarders'>
                                <input type="search" name="search_freight_forwarders" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($freight_forwarders as $freight_forwarder)
                            <tr>
                                <td>{{ $freight_forwarder->id }}</td>
                                <td>{{ $freight_forwarder->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_div('#table_freight_forwarders_edit', {{ $freight_forwarder->id }} , '{{ $freight_forwarder->name }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($freight_forwarder->active == 1)
                                    <form id="delete_freight_forwarder{{ $freight_forwarder->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_freight_forwarder_id" value="{{ $freight_forwarder->id }}"><input type="hidden" name="table" value="freight_forwarders"><input type="hidden" name="action" value="delete_freight_forwarders">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($freight_forwarder->active == 0)
                                    <form id="activate_freight_forwarder{{ $freight_forwarder->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_freight_forwarder_id" value="{{ $freight_forwarder->id }}"><input type="hidden" name="table" value="freight_forwarders"><input type="hidden" name="action" value="activate_freight_forwarders">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $freight_forwarders->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
    
    <div class="col-sm-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Salesmen Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_salesmen_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="salesmen">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_salesmen">
                            <input type="hidden"  id="table_salesmen_edit_id" name="edit_id" value="">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_salesmen_edit_name" name='name' type="text" value="" class="form-control" >
                                </div>
                           
                            </div>
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_salesmen_addnew' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="salesmen">{{ csrf_field() }}<input type="hidden" name="action" value="add_salesmen">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter  Name" class="form-control" >
                                </div>
                           
                            </div>
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_salesmen_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='salesmen'>
                                <input type="search" name="search_salesmen" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($salesmen as $salesman)
                            <tr>
                                <td>{{ $salesman->id }}</td>
                                <td>{{ $salesman->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_div('#table_salesmen_edit', {{ $salesman->id }} , '{{ $salesman->name }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($salesman->active == 1)
                                    <form id="delete_salesman{{ $salesman->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_salesman_id" value="{{ $salesman->id }}"><input type="hidden" name="table" value="salesmen"><input type="hidden" name="action" value="delete_salesmen">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($salesman->active == 0)
                                    <form id="activate_salesman{{ $salesman->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_salesman_id" value="{{ $salesman->id }}"><input type="hidden" name="table" value="salesmen"><input type="hidden" name="action" value="activate_salesmen">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $salesmen->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Materials Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_materials_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="materials">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_materials">
                            <input type="hidden"  id="table_materials_edit_id" name="edit_id" value="">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>code</label>
                                    <input id="table_materials_edit_code" name='code' type="text" value="" class="form-control" maxlength="1">
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_materials_edit_name" name='name' type="text" value="" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                             
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_materials_addnew' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="materials">{{ csrf_field() }}<input type="hidden" name="action" value="add_materials">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Code</label>
                                    <input name='code' type="text" placeholder="Enter  " class="form-control" maxlength="1">
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter  Name" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                           
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_materials_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='materials'>
                                <input type="search" name="search_materials" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($materials as $material)
                            <tr>
                                <td>{{ $material->id }}</td>
                                <td>{{ $material->code }}</td>
                                <td>{{ $material->name }}</td>
                                
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_code_div('#table_materials_edit', {{ $material->id }} , '{{ $material->name }}','{{$material->code}}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($material->active == 1)
                                    <form id="delete_material{{ $material->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_material_id" value="{{ $material->id }}"><input type="hidden" name="table" value="materials"><input type="hidden" name="action" value="delete_materials">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($material->active == 0)
                                    <form id="activate_material{{ $material->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_material_id" value="{{ $material->id }}"><input type="hidden" name="table" value="materials"><input type="hidden" name="action" value="activate_materials">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $materials->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Sizes Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_sizes_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="sizes">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_sizes">
                            <input type="hidden"  id="table_sizes_edit_id" name="edit_id" value="">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>code</label>
                                    <input id="table_sizes_edit_code" name='code' type="text" value="" class="form-control" maxlength="2">
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_sizes_edit_name" name='name' type="text" value="" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                            
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_sizes_addnew' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="sizes">{{ csrf_field() }}<input type="hidden" name="action" value="add_sizes">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Code</label>
                                    <input name='code' type="text" placeholder="Enter  " class="form-control" maxlength="2">
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter  Name" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                            
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_sizes_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='sizes'>
                                <input type="search" name="search_sizes" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($sizes as $size)
                            <tr>
                                <td>{{ $size->id }}</td>
                                <td>{{ $size->code }}</td>
                                <td>{{ $size->name }}</td>
                                 
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_code_div('#table_sizes_edit', {{ $size->id }} , '{{ $size->name }}','{{$size->code}}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($size->active == 1)
                                    <form id="delete_size{{ $size->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_size_id" value="{{ $size->id }}"><input type="hidden" name="table" value="sizes"><input type="hidden" name="action" value="delete_sizes">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($size->active == 0)
                                    <form id="activate_size{{ $size->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_size_id" value="{{ $size->id }}"><input type="hidden" name="table" value="sizes"><input type="hidden" name="action" value="activate_sizes">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $sizes->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>features Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_features_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="features">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_features">
                            <input type="hidden"  id="table_features_edit_id" name="edit_id" value="">
                            
                             <div class="col-md-4">
                                <div class="form-group">
                                    <label>code</label>
                                    <input id="table_features_edit_code" name='code' type="text" value="" class="form-control" maxlength="1">
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_features_edit_name" name='name' type="text" value="" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_features_addnew' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="features">{{ csrf_field() }}<input type="hidden" name="action" value="add_features">
                           
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Code</label>
                                    <input name='code' type="text" placeholder="Enter  " class="form-control" maxlength="1">
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter  Name" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_features_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='features'>
                                <input type="search" name="search_features" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($features as $feature)
                            <tr>
                                <td>{{ $feature->id }}</td>
                                 <td>{{ $feature->code }}</td>
                                <td>{{ $feature->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_code_div('#table_features_edit', {{ $feature->id }} , '{{ $feature->name }}','{{$feature->code}}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($feature->active == 1)
                                    <form id="delete_feature{{ $feature->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_feature_id" value="{{ $feature->id }}"><input type="hidden" name="table" value="features"><input type="hidden" name="action" value="delete_features">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($feature->active == 0)
                                    <form id="activate_feature{{ $feature->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_feature_id" value="{{ $feature->id }}"><input type="hidden" name="table" value="features"><input type="hidden" name="action" value="activate_features">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $features->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
   
    <div class="col-sm-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>characteres Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_characteres_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="characteres">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_characteres">
                            <input type="hidden"  id="table_characteres_edit_id" name="edit_id" value="">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>code</label>
                                    <input id="table_characteres_edit_code" name='code' type="text" value="" class="form-control" maxlength="2">
                                </div>
                           
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_characteres_edit_name" name='name' type="text" value="" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>License</label>
                                    {{ Form::select('license_id', [], null, ['id' => 'table_characteres_edit_license_id','class'=>'form-control select2','placeholder' => 'Seleccione']) }}
                                </div>
                           
                            </div>
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_characteres_addnew' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="characteres">{{ csrf_field() }}<input type="hidden" name="action" value="add_characteres">
                           
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Code</label>
                                    <input name='code' type="text" placeholder="Enter  " class="form-control" maxlength="2">
                                </div>
                           
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter  Name" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>License</label>
                                    {{ Form::select('license_id', [], null, ['id' => 'character_license_id','class'=>'form-control select2','placeholder' => 'Seleccione']) }}
                                </div>
                               
                            </div>
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_characteres_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='characteres'>
                                <input type="search" name="search_characteres" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Lic</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($characteres as $character)
                            <tr>
                                <td>{{ $character->id }}</td>
                                 <td>{{ $character->code }}</td>
                                <td>{{ $character->name }}</td>
                                 <td>{{ $character->license->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_character_div('#table_characteres_edit', {{ $character->id }} , '{{ $character->name }}','{{$character->code}}','{{ $character->license_id }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($character->active == 1)
                                    <form id="delete_character{{ $character->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_character_id" value="{{ $character->id }}"><input type="hidden" name="table" value="characteres"><input type="hidden" name="action" value="delete_characteres">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($character->active == 0)
                                    <form id="activate_character{{ $character->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_character_id" value="{{ $character->id }}"><input type="hidden" name="table" value="characteres"><input type="hidden" name="action" value="activate_characteres">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $characteres->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Mounting Table</h5>
                
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_mountings_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="mountings">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_mountings">
                            <input type="hidden"  id="table_mountings_edit_id" name="edit_id" value="">
                           
                             <div class="col-md-4">
                                <div class="form-group">
                                    <label>code</label>
                                    <input id="table_mountings_edit_code" name='code' type="text" value="" class="form-control" maxlength="1">
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_mountings_edit_name" name='name' type="text" value="" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_mountings_addnew' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="mountings">{{ csrf_field() }}<input type="hidden" name="action" value="add_mountings">
                            
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Code</label>
                                    <input name='code' type="text" placeholder="Enter  " class="form-control" maxlength="1">
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter  Name" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_mountings_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='mountings'>
                                <input type="search" name="search_mountings" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($mountings as $mounting)
                            <tr>
                                <td>{{ $mounting->id }}</td>
                                 <td>{{ $mounting->code }}</td>
                                <td>{{ $mounting->name }}</td>
                                
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_code_div('#table_mountings_edit', {{ $mounting->id }} , '{{ $mounting->name }}','{{$mounting->code}}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($mounting->active == 1)
                                    <form id="delete_mounting{{ $mounting->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_mounting_id" value="{{ $mounting->id }}"><input type="hidden" name="table" value="mountings"><input type="hidden" name="action" value="delete_mountings">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($mounting->active == 0)
                                    <form id="activate_mounting{{ $mounting->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_mounting_id" value="{{ $mounting->id }}"><input type="hidden" name="table" value="mountings"><input type="hidden" name="action" value="activate_mountings">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $mountings->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>

                           
    </div>
       

@endsection
@push('scripts')
<script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
<script>
    

    licenses=<?=json_encode($data_licenses)?>;
    var data_licenses = $.map(licenses, function (obj) {
        obj.text = obj.text || obj.name; // replace name with the property used for the text
        delete obj.name;
        delete obj.active;
        delete obj.code;
        delete obj.fama_require;
        return obj;
    });
    //console.log('materials',materials); 
    $('#character_license_id').select2({       
        width: "100%",
        tags: false,
        data: data_licenses
    });
    $('#table_characteres_edit_license_id').select2({       
        width: "100%",
        tags: false,
        data: data_licenses
    });

</script>

@endpush