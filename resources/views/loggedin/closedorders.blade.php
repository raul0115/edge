@extends('loggedin.layout')

@section('content')
@if (session('message'))
<div class="alert alert-success">
    <strong>Successful Action!</strong><br><br>
    <ul>
        <li>{{ session('message') }}</li>
    </ul>
</div>
@endif
{{-- @if($getOrder)  --}}
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Closed Orders<small></small></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                
                
            @if (count($orders)<=0)
                <div class="row">
                    <div class="text-center"><h3>No order records found</h3></div></div>
            @elseif($orders)
            <div class="table-responsive">
            @php
                $display = \App\Http\Controllers\PermissionController::checkUserDispalyPermissionsList();
                
                $cant_display = count($display);
                $letras = \App\Http\Controllers\PermissionController::getDisplayLetras($cant_display);
               
                $styleA = "style=background-color:#F2F5A9;";//
                $styleR = "style=background-color:#F58888;";//
                $styleG = "style=background-color:#1ab394;";//
                $styleY = "style=background-color:gray;cursor:not-allowed;";//#eee
            @endphp
<table id="order_table" class="table-striped table-bordered order-column" cellspacing="0" width="100%">
    <thead>

        <tr>
            <th></th>
            @foreach ($letras as $let)
                <th>{{$let}}</th>
            @endforeach
        </tr>
    
        <tr>
            <th title="Details / To Closed">Details</th>
                @foreach($display as $dis)
                    @switch($dis)
                        @case('order_id')
                            <th title="Order ID">Ord. ID</th>
                        @break
                        @case('po_number')
                            <th title="PO Number">PO #</th>
                        @break
                        @case('comments')
                            <th title="Comments">Comm</th>
                        @break
                        @case('open')
                            <th title="Status">Status</th>
                        @break
                        @case('value')
                            <th title="Value">Value</th>
                        @break
                        @case('fty_produc')
                            <th title="Factory Producting">Fty</th>
                        @break
                        @case('export_cos')
                            <th title="Export Co.">Exp Co.</th>
                        @break
                        @case('fama')
                            <th title="FAMA">Fama</th> 
                        @break
                        @case('prod_style')
                            <th title="Main Product Style(S)">Style#</th> 
                        @break
                        @case('prod_type')
                            <th title="Product Type">Prod Typ</th>
                        @break
                        @case('licenses')
                        <th title="Licenses">Lic</th>
                        @break
                        @case('customers')
                        <th title="Customer">Cust</th>
                        @break
                        @case('customer_po_number')
                        <th title="Customer PO #">Cust PO</th>
                        @break
                        @case('date_placed')
                        <th title="Date Placed">Placed</th>
                        @break
                        @case('qty_ordered')
                        <th title="Qty Ordered">Qty</th> 
                        @break
                        @case('tickets_needed')
                        <th title="Tickets Needed?">Need Tix?</th>
                        @break
                        @case('tickets_ordered')
                        <th title="Tickets ordered?">Tix Ord?</th>
                        @break
                        @case('tickets_received')
                        <th title="Tickets Received?">Tix Recv?</th>
                        @break
                        @case('shipping_from')
                        <th title="Shipping From">Ship Fr</th>
                        @break
                        @case('shipping_to')
                        <th title="Shipping To">Ship To</th>
                        @break
                        @case('estimated_arr')
                        <th title="Estimated Arr">Est Arr</th>
                        @break
                        @case('finish_prod')
                        <th title="Finish Pro">Fin Pro</th>
                        @break
                        @case('customer_cancel')
                        <th title="Customer Cancel Date">Cust Cxl</th>
                        @break
                        @case('cbm')
                        <th title="CBM">CBM</th>
                        @break
                        @case('book_container')
                        <th title="Booking or Container #">Bk/Ctr #</th>
                        @break
                        @case('act_etd_forwarder')
                        <th title="Actual ETD from Forwarder">Actu ETD</th>
                        @break
                        @case('act_eta_forwarder')
                        <th title="Actual ETA from Forwarder">Actu ETA</th>
                        @break
                        @case('safety_test')
                        <th title="Safety Test Needed?">Test Need?</th>
                        @break
                        @case('safety_test_on')
                        <th title="Safety Test Need By">Test By</th>
                        @break
                        @case('fty_prod_safety_test')
                        <th title="Fty Sent for Testing">Sent 4 Test</th>
                        @break
                        @case('safety_test_pass')
                        <th title="Safety Test Passed?">Test Pass?</th>
                        @break
                        @case('david_ins_result')
                        <th title="David Inspection Results">Insp Reslt</th>
                        @break
                        @case('david_ins_comment')
                        <th title="David Inspection comments">Insp Cmmt</th> 
                        @break
                        @case('svn_no')
                        <th title="SVN#">SVN#</th>
                        @break
                        @case('freight_forwarder')
                        <th  title="Freight Forwarder">Forwdr</th> 
                        @break
                        @case('rec_comm_inv_pking')
                        <th title="Did we recv our Comm Inv/Pking List?">Edge PL/Inv</th>
                        @break
                        @case('rec_comm_inv_pking_home')
                        <th title="Did we recv Comm Inv/Pking List for AtHome?">Athm PL/Inv</th>
                        @break
                        @case('original_fcr')
                        <th title="Did we Recv Orig FCR?">FCR?</th>
                        @break
                        @case('ctpat_form')
                        <th title="Did we Recv CTPAT?">CTPAT?</th>
                        @break
                        @case('telex_release')
                        <th title="is B/L Telexed Released">Telex Rls</th>
                        @break
                        @case('pro_photo')
                        <th title="Did we Recv Prof Photography?">Photo</th>
                        @break
                        @case('okay_to_pay')
                        <th title="OK to Pay?">Ok Pay?</th>
                        @break
                        @case('did_we_pay')
                        <th title="Did we Pay Fty?">Paid?</th>
                        @break
                        @case('send_diann')
                        <th title="What date we sent to G&A(Diann)">G&A</th>
                        @break
                        @case('receive_whse')
                        <th title="Received in warehouse?">Whse?</th>
                        @break
                        @case('salesmen')
                        <th title="Salesman">Salesman</th>
                        @break
                        @case('files')
                        <th title="Uploads doc for this order">Files</th>
                        @break
                        @default
                            <th>no se<th>
                        @break
                    @endswitch
                @endforeach         
            </tr>
    </thead>
    <tbody>
        @foreach ($orders as $order)
        <tr>
            <!--Licensing Conditions or FAMA Condition -->
            @php  $Lic_Con = $Lic_Title = null;
            if($order->ExCFama==0 and (strpos($order->licensesNames, 'Marvel') !== false)) {$Lic_Con = true;$Lic_Title="Factory unapproved to produce Disney product.";}
            @endphp
            <!-- Delivery Condition -->
            @php $del_Con = null;
            if( ($order->act_eta_forwarder == null and $order->customer_cancel != null and strtotime($order->estimated_arr) > strtotime($order->customer_cancel)) ) { $del_Con = true; $del_Title="This date is before Estimated Arr";} 
            elseif($order->act_eta_forwarder != null and $order->customer_cancel != null and strtotime($order->act_eta_forwarder) > strtotime($order->customer_cancel) ) { $del_Con = true;  $del_Title="This date is before Actual ETA";} 
            @endphp
            <!-- Safety Test Conditions -->
            @php
            $safety_Title1 = $safety_Con1 = $safety_Title2 = $safety_Con2 = null;
            $date1=date_create($order->finish_prod);
            $date2=date_create(date('Y-m-d H:i:s'));
            $diff=date_diff($date1,$date2);
            if ($order->safety_test == 1 and $order->fty_prod_safety_test == null and $diff->format('%a') <= 21){$safety_Con1=true;$safety_Title1= "'Fty Submitted Prod' not entered and Safety Test is 21 days before Finish Prod";}
            if ($order->safety_test == 1 and ($order->safety_test_pass==null or $order->safety_test_pass==0) and $diff->format('%a') <= 10){$safety_Con2=true;$safety_Title2= "Safety Test is not Passed or is 10 days before Finish Prod";}
            @endphp
            <!-- CBM Conditions -->
            @php
            $CBM_Title = $CBM_Con = null;
            if ($safety_Con1==true or $safety_Con2 == true or $Lic_Con == true){$CBM_Con=true;$CBM_Title= "FAMA or Safety conditions are not met";}
            @endphp
            <!-- Payment Conditions -->
            @php 
            $OKPay_Title = $OKPay_Con = null;
            if (\App\Http\Controllers\TriggerEventsController::validateOkayToPay($order->ipo, $order->customersNames)) {$OKPay_Title = "You can now fill this field"; $OKPay_Con=true;}
            @endphp
            <!-- SVN Conditions -->
            @php 
            $SVN_Title = 'FAMA, Safety or Payment Conditions not meta';
            @endphp
            <!-- ETD Conditions -->
            @php 
            $d1=date_create(date('Y-m-d H:i:s'));//now
            if (!isset($order->act_etd_forwarder)){
                $d2=date_create($order->finish_prod );
                $df=date_diff($d1,$d2);
                $days_before_etd = $df->format('%a');//edt was this many days before today.
            }
            elseif(isset($order->act_etd_forwarder)){
                $d2=date_create($order->act_etd_forwarder);
                $df=date_diff($d1,$d2);
                $days_before_etd = $df->format('%a');//edt was this many days before today.
            }
            @endphp
            <td style="text-align: center;">
                <a href="{{ url('/orders/'.$order->ipo)}}" class="btn-xs btn-primary">Details</a>
            </td>
        @foreach($display as $dis)
            
            @switch($dis)
                @case('order_id')
                    <td>{{ $order->ipo }}</td>
                    @break
                @case('po_number')
                    <td id="po_number_t_{{ $order->ipo }}">{{ $order->po_number }}</td>
                    @break
                @case('comments')
                    <td id="comments_t_{{ $order->ipo }}" class="td_ellipsis" title="{{ $order->comments }}" >{{ $order->comments }}</td> 
                    @break
                @case('open')
                    <td>@if($order->open==1) <span class=" label label-primary">OPEN</span> @elseif($order->open==0) <span class=" label label-danger">CLOSED</span> @endif</td>
                    @break
                @case('value')
                    <td id="value_t_{{ $order->ipo }}">$ {{ number_format($order->value) }}</td>
                    @break
                @case('fty_produc')
                    <td id="fty_produc_t_{{ $order->ipo }}" @if($order->ftyName )  title="{{ $order->ftyVId }}" @endif  @if(!$order->ftyName ) {{ $styleA }} title="Field required" @endif>{{ $order->ftyName }}</td>
                    @break
                @case('export_cos')
                <td id="export_cos_t_{{ $order->ipo }}"  @if($Lic_Con) {{ $styleR }} title="{{$Lic_Title}}" @endif @if(!$order->ExCName ) {{ $styleA }} title="Field required" @endif>{{ $order->ExCName }}</td> 
                @break
                @case('fama')
                <td id="export_cos_fema_{{ $order->ipo }}">@if($order->ExCFama===1) Yes @elseif($order->ExCFama===0) No @endif</td>
                @break
                @case('prod_style')
                <td id="product_styles_t_{{ $order->ipo }}">{{ $order->prod_styleNames }}</td>
                @break
                @case('prod_type')
                <td id="product_types_t_{{ $order->ipo }}">{{ $order->prod_typesNames }}</td>
                @break
                @case('licenses')
                <td id="licenses_t_{{ $order->ipo }}"  @if(!$order->licensesNames ) {{ $styleA }} title="Field required" @endif>{{ $order->licensesNames }}</td> 
                @break
                @case('customers')
                <td id="customers_t_{{ $order->ipo }}">{{ $order->customersNames }}</td>
                @break
                @case('customer_po_number')
                <td id="customer_po_number_t_{{ $order->ipo }}">{{ $order->customer_po_number }}</td>
                @break
                @case('date_placed')
                <td id="date_placed_t_{{ $order->ipo }}">
                        @php
                            if($order->date_placed){
                            $dater=date_create($order->date_placed);
                            echo date_format($dater,"M d Y");
                            }
                        @endphp
                    </td> 
                @break
                @case('qty_ordered')
                <td id="qty_ordered_t_{{ $order->ipo }}">{{ $order->qty_ordered }}</td>
                @break
                @case('tickets_needed')
                <td id="tickets_needed_t_{{ $order->ipo }}" >@if($order->tickets_needed===1) Yes @elseif($order->tickets_needed===0) No @endif</td>
                @break
                @case('tickets_ordered')
                <td id="tickets_ordered_t_{{ $order->ipo }}">{{ $order->tickets_orderedDates }}</td>
                @break
                @case('tickets_received')
                <td id="tickets_received_t_{{ $order->ipo }}">
                        @php
                        if($order->tickets_received){
                            $dater=date_create($order->tickets_received);
                            echo date_format($dater,"M d Y");
                        }
                        @endphp
                    </td> 
                @break
                @case('shipping_from')
                <td id="shipping_from_t_{{ $order->ipo }}">{{ $order->port_ShipF_Name }}</td>
                @break
                @case('shipping_to')
                <td id="shipping_to_t_{{ $order->ipo }}">{{ $order->port_ShipT_Name }}</td>
                @break
                @case('estimated_arr')
                <td id="estimated_arr_t_{{ $order->ipo }}">
                        @php
                        if($order->estimated_arr){
                            $dater=date_create($order->estimated_arr);
                            echo date_format($dater,"M d Y");
                        }
                        @endphp
                    </td>
                @break
                @case('finish_prod')
                <td id="finish_prod_t_{{ $order->ipo }}">
                        @php
                        if($order->finish_prod){
                            $dater=date_create($order->finish_prod);
                            echo date_format($dater,"M d Y");
                        }
                        @endphp
                    </td>
                @break
                @case('customer_cancel')
                <td id="customer_cancel_t_{{ $order->ipo }}" @if( $del_Con )  {{ $styleA }} title="{{$del_Title}}" @endif>
                        @php
                        if($order->customer_cancel){
                            $dater=date_create($order->customer_cancel);
                            echo date_format($dater,"M d Y");
                        }
                        @endphp
                    </td>
                @break
                @case('cbm')
                <td id="cbm_t_{{ $order->ipo }}" @if( $CBM_Con ) {{ $styleA }} title="{{$CBM_Title}}" @endif>{{ $order->cbm }}</td>
                @break
                @case('book_container')
                <td id="book_container_t_{{ $order->ipo }}">{{ $order->book_container }}</td>
                @break
                @case('act_etd_forwarder')
                <td id="act_etd_forwarder_t_{{ $order->ipo }}">
                        @php
                            if($order->act_etd_forwarder){
                                $dater=date_create($order->act_etd_forwarder);
                                echo date_format($dater,"M d Y");
                            }
                        @endphp
                    </td> 
                @break
                @case('act_eta_forwarder')
                <td id="act_eta_forwarder_t_{{ $order->ipo }}">
                        @php
                            if($order->act_eta_forwarder){
                                $dater=date_create($order->act_eta_forwarder);
                                echo date_format($dater,"M d Y");
                            }
                        @endphp
                    </td> 
                @break
                @case('safety_test')
                <td id="safety_test_t_{{ $order->ipo }}" @if( $safety_Con1 ) {{ $styleA }} title="{{$safety_Title1}}" @endif @if($order->safety_test ===null ) {{ $styleA }} title="Field required" @endif>@if($order->safety_test===1) Yes @elseif($order->safety_test===0) No @endif</td> 
               
                @break
                @case('safety_test_on')
                <td id="safety_test_on_t_{{ $order->ipo }}" @if($order->safety_test===0) {{ $styleY }} @endif >
                        @php
                        if($order->safety_test_on){
                            $dater=date_create($order->safety_test_on);
                            echo date_format($dater,"M d Y");
                        }
                        @endphp
                    </td> 
                @break
                @case('fty_prod_safety_test')
                <td id="fty_prod_safety_test_t_{{ $order->ipo }}"  @if($order->safety_test===0) {{ $styleY }}@endif >
                        @php
                        if($order->fty_prod_safety_test){
                            $dater=date_create($order->fty_prod_safety_test);
                            echo date_format($dater,"M d Y");
                        }
                        @endphp
                    </td> 
                @break
                @case('safety_test_pass')
                <td id="safety_test_pass_t_{{ $order->ipo }}"  @if($order->safety_test===0) {{ $styleY }} @endif @if( $safety_Con2 ) {{ $styleA }} title="{{$safety_Title2}}" @endif>@if($order->safety_test_pass===1) Yes @elseif($order->safety_test_pass===0) No @endif</td>
                @break
                @case('david_ins_result')
                <td id="david_ins_result_t_{{ $order->ipo }}">@if($order->david_ins_result===1) Pass @elseif($order->david_ins_result===0) Fail @endif</td>
                @break
                @case('david_ins_comment')
                <td id="david_ins_comment_t_{{ $order->ipo }}" class="td_ellipsis" title="{{ $order->david_ins_comment }}" >{{ $order->david_ins_comment }}</td>
                @break
                @case('svn_no')
                <td id="svn_t_{{ $order->ipo }}" @if($order->svn_no==null and ($CBM_Con or !$OKPay_Con)) {{ $styleA }} title="{{ $SVN_Title }}" @elseif($CBM_Con or !$OKPay_Con) {{ $styleA }} title="{{ $SVN_Title }}" @elseif ($order->svn_no==null) {{ $styleG }} @endif>{{ $order->svn_no }}</td>
                @break
                @case('freight_forwarder')
                <td id="freight_forwarders_t_{{ $order->ipo }}">{{ $order->FFName }}</td> 
                @break
                @case('rec_comm_inv_pking')
                <td id="rec_comm_inv_pking_t_{{ $order->ipo }}">
                        @php
                            if($order->rec_comm_inv_pking){
                                $dater=date_create($order->rec_comm_inv_pking);
                                echo date_format($dater,"M d Y");
                            }
                        @endphp
                    </td> 
                @break
                @case('rec_comm_inv_pking_home')
                <td id="rec_comm_inv_pking_home_t_{{ $order->ipo }}" @if ( strpos($order->customersNames, 'tHome') == false) {{ $styleY }} title="Only for AtHome" @endif>
                        @php
                            if($order->rec_comm_inv_pking_home){
                                $dater=date_create($order->rec_comm_inv_pking_home);
                                echo date_format($dater,"M d Y");
                            }
                        @endphp
                    </td> 
                @break
                @case('original_fcr')
                <td id="original_fcr_t_{{ $order->ipo }}" @if ( strpos($order->customersNames, 'obby Lobby') == false and strpos($order->customersNames, 'tHome') == false) {{ $styleY }} title="Only for Hobby Lobby or AtHome" @endif>
                        @php
                        if($order->original_fcr){
                            $dater=date_create($order->original_fcr);
                            echo date_format($dater,"M d Y");
                        }
                        @endphp
                    </td> 
                @break
                @case('ctpat_form')
                    <td id="ctpat_form_t_{{ $order->ipo }}" @if ( strpos($order->customersNames, 'obby Lobby') == false) {{ $styleY }} title="Only for Hobby Lobby" @endif>
                        @php
                        if($order->ctpat_form){
                            $dater=date_create($order->ctpat_form);
                            echo date_format($dater,"M d Y");
                        }
                        @endphp
                    </td> 
                @break
                @case('telex_release')
                <td id="telex_release_t_{{ $order->ipo }}">
                        @php
                            if($order->telex_release){
                                $dater=date_create($order->telex_release);
                                echo date_format($dater,"M d Y");
                            }
                        @endphp
                    </td>
                @break
                @case('pro_photo')
                    <td id="pro_photo_t_{{ $order->ipo }}">@if($order->pro_photo===1) Yes @elseif($order->pro_photo===0) No @endif</td>
                @break
                @case('okay_to_pay')
                <td id="okay_to_pay_t_{{ $order->ipo }}" @if( $OKPay_Con and $order->okay_to_pay == null )  {{ $styleG }} title="{{$OKPay_Title}}" @endif>
                        @php
                        if($order->okay_to_pay){
                            $dater=date_create($order->okay_to_pay);
                            echo date_format($dater,"M d Y");
                        }
                         @endphp
                    </td>
                @break
                @case('did_we_pay')
                <td id="did_we_pay_t_{{ $order->ipo }}" @if( $order->okay_to_pay and $order->did_we_pay ==null ) {{ $styleG }} title="You can now fill this field" @endif>
                        @php
                        if($order->did_we_pay){
                            $dater=date_create($order->did_we_pay);
                            echo date_format($dater,"M d Y");
                        }
                         @endphp
                    </td> 
                @break
                @case('send_diann')
                <td id="send_diann_t_{{ $order->ipo }}">
                        @php
                        if($order->send_diann){
                            $dater=date_create($order->send_diann);
                            echo date_format($dater,"M d Y");
                        }
                    @endphp
                    </td> 
                @break
                @case('receive_whse')
                <td id="receive_whse_t_{{ $order->ipo }}">@if($order->receive_whse===1) Yes @elseif($order->receive_whse===0) No @endif</td>
                @break
                @case('salesmen')
                <td id="salesmen_t_{{ $order->ipo }}">{{ $order->salesmenNames }}</td>
                @break
                @case('files')
                <td id="files_t_{{ $order->ipo }}" >    View / <br/>Upload</td>
                @break    
                
                @default
                @break
            @endswitch
        @endforeach
        </tr>
        @endforeach
    </tbody>
</table>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
{{-- @endif --}}
@endsection