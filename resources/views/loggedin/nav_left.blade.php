<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <span>
                        <img alt="image" class="img-circle" src="/img/profile_small.jpg" />
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{{ Auth::user()->name }}</strong>
                            </span>
                            <span class="text-muted text-xs block">
                            {{auth()->user()->mi_role()}}
                            User
                                <b class="caret"></b>
                            </span>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="{{ asset('usermanagement/profile') }}">Profile</a></li>
                        <li><a href="{{ asset('usermanagement/display') }}">Display Management</a></li>
                        <!--                            <li><a href="mailbox.html">Mailbox</a></li>-->
                        <li class="divider"></li>
                        <li><a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                                Logout
                            </a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li class="{{ Request::path() == '/' ? 'active' : '' }}{{ Request::path() == 'home' ? 'active' : '' }}">
                <a href="{{ asset('home') }}"><i class="fa fa-tachometer"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <li class="{{ Request::path() == 'usermanagement/profile' ? 'active' : '' }}">
                <a href="{{ asset('usermanagement/profile') }}"><i class="fa fa-newspaper-o"></i> <span class="nav-label">Profile</span></a>
            </li>
            @if(auth()->user()->hasRole('Admin'))
            <li class="{{ Request::path() == 'usermanagement/users' ? 'active' : '' }}">
                <a href="{{ asset('usermanagement/users') }}"><i class="fa fa-user"></i> <span class="nav-label">User Management</span></a>
            </li>
            <li class="{{ Request::path() == 'usermanagement/group' ? 'active' : '' }}">
                <a href="{{ asset('usermanagement/group') }}"><i class="fa fa-users"></i> <span class="nav-label">Group Management</span></a>
            </li>
           
            <li class="{{ (Request::is('table/*')) ? 'active' : '' }}">
            <a href="#"><i class="fa fa-table"></i> <span class="nav-label">Tables Management</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse" style="height: 0px;">
                        <li class="{{ Request::path() == 'table/factories' ? 'active' : '' }}">
                            <a href="{{ route('table.factory.index') }}"><span class="nav-label">Factories</span></a>
                        </li>
                        <li class="{{ Request::path() == 'table/export_cos' ? 'active' : '' }}">
                            <a href="{{ route('table.exportco.index') }}"><span class="nav-label">Export Cos</span></a>
                        </li>  
                        <li class="{{ Request::path() == 'table/customers' ? 'active' : '' }}">
                            <a href="{{ route('table.customer.index') }}"><span class="nav-label">Customers</span></a>
                        </li> 
                        <li class="{{ Request::path() == 'table/ports' ? 'active' : '' }}">
                            <a href="{{ route('table.port.index') }}"><span class="nav-label">Ports</span></a>
                        </li>   
                        <li class="{{ Request::path() == 'table/portsto' ? 'active' : '' }}">
                            <a href="{{ route('table.portto.index') }}"><span class="nav-label">Ports To</span></a>
                        </li>  
                        <li class="{{ Request::path() == 'table/licenses' ? 'active' : '' }}">
                            <a href="{{ route('table.license.index') }}"><span class="nav-label">Licenses</span></a>
                        </li> 
                        <li class="{{ Request::path() == 'table/product_styles' ? 'active' : '' }}">
                            <a href="{{ route('table.product_style.index') }}"><span class="nav-label">Product Styles</span></a>
                        </li> 
                        <li class="{{ Request::path() == 'table/product_types' ? 'active' : '' }}">
                            <a href="{{ route('table.product_type.index') }}"><span class="nav-label">Product Types</span></a>
                        </li>
                        <li class="{{ Request::path() == 'table/freight_forwarders' ? 'active' : '' }}">
                            <a href="{{ route('table.freight_forwarder.index') }}"><span class="nav-label">Freight Forwarders</span></a>
                        </li>     
                        <li class="{{ Request::path() == 'table/salesmen' ? 'active' : '' }}">
                            <a href="{{ route('table.salesmen.index') }}"><span class="nav-label">Salesmen</span></a>
                        </li>
                        <li class="{{ Request::path() == 'table/materials' ? 'active' : '' }}">
                            <a href="{{ route('table.material.index') }}"><span class="nav-label">Materials</span></a>
                        </li> 
                        <li class="{{ Request::path() == 'table/sizes' ? 'active' : '' }}">
                            <a href="{{ route('table.size.index') }}"><span class="nav-label">Sizes</span></a>
                        </li>
                        <li class="{{ Request::path() == 'table/features' ? 'active' : '' }}">
                            <a href="{{ route('table.feature.index') }}"><span class="nav-label">Feature</span></a>
                        </li> 
                        <li class="{{ Request::path() == 'table/characteres' ? 'active' : '' }}">
                            <a href="{{ route('table.character.index') }}"><span class="nav-label">Characteres</span></a>
                        </li> 
                        <li class="{{ Request::path() == 'table/mounting' ? 'active' : '' }}">
                            <a href="{{ route('table.mounting.index') }}"><span class="nav-label">Mounting</span></a>
                        </li> 

                    </ul>
            </li>
            <li class="{{ Request::path() == 'orders/settings' ? 'active' : '' }}">
                <a href="{{ asset('orders/settings') }}"><i class="fa fa-cog"></i> <span class="nav-label">Order Settings</span></a>
            </li>
           
            @endif
            <li class="{{ (Request::is('items/*')) ? 'active' : '' }}{{ (Request::is('groups/*')) ? 'active' : '' }}">
            <a href="#"><i class="fa fa-table"></i> <span class="nav-label">Items</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse" style="height: 0px;">
                    <li class="{{ Request::path() == 'items/index' ? 'active' : '' }}">
                        <a href="{{ route('item.index') }}"><i class="fa fa-cube"></i> <span class="nav-label">List Item</span></a>
                    </li>
                    <li class="{{ Request::path() == 'items/create' ? 'active' : '' }}">
                        <a href="{{ route('item.create') }}"><i class="fa fa-cube"></i> <span class="nav-label">Create Item</span></a>
                    </li>
                    <li class="{{ Request::path() == 'groups/index' ? 'active' : '' }}">
                        <a href="{{ route('group.index') }}"><i class="fa fa-cube"></i> <span class="nav-label">Group</span></a>
                    </li>
                </ul>
            </li>
           
            <!--    <li class="{{ Request::path() == 'usermanagement/display' ? 'active' : '' }}">
                        <a href="{{ asset('usermanagement/display') }}"><i class="fa fa-list-alt"></i> <span class="nav-label">Hide/Display Fields</span></a>
                    </li>-->
            
            
            <li class="{{ Request::path() == 'orders/open' ? 'active' : '' }}">
                <a href="{{ asset('orders/open') }}"><i class="fa fa-folder-open-o"></i> <span class="nav-label">Open Orders</span></a>
            </li>

            <li class="{{ Request::path() == 'orders/closed' ? 'active' : '' }}">
                <a href="{{ asset('orders/closed') }}"><i class="fa fa-folder"></i> <span class="nav-label">Closed Orders</span></a>
            </li>
            <li class="{{ Request::path() == 'orders/search' ? 'active' : '' }}">
                <a href="{{ asset('orders/search') }}"><i class="fa fa-search"></i> <span class="nav-label">Advanced Search</span></a>
            </li>
        </ul>
    </div>
</nav>