@extends('loggedin.layout')

@section('content')
<script type="text/javascript">
    function view_files(id){
        $('#files_table').html($('#sk-spinner').clone().show());
        $('#view_files').dialog({
            minHeight:0,
            title:"View Files",
            modal: true,
            open: function() {
                    $('.ui-widget-overlay').on('click', function() {
                        $('#view_files').dialog('close');
                    })
            },
        });
       
        var update_td_id = "files_t_"+id;
        $.ajax({
        type: "POST",
        url: '/view_files',
        data: { order_id: id, _token: '{{csrf_token()}}' },
        success: function (data) {
           console.log("Success");
           $('#view_files').html(data);
    //        if (typeof data.error !== 'undefined') {
    //            $('#'+update_td_id).html();// remove progress annimation and add old value again
    //        
    //            
    //        }
        },
        error: function (data, textStatus, errorThrown) {
            console.log(data);
            $('#view_files').html(data);// remove progress annimation and add old value again
            
        },
        });
        
    }
    
    function upload_files(order_id){
        $('#file_order_id').val(order_id);
        $('#upload_files').dialog({
            minHeight:0,
            title:"Upload Files",
            modal: true,
            open: function() {
                $('.ui-widget-overlay').on('click', function() {
                    $('#upload_files').dialog('close');
                })
            },
        });
    }
    function download_file(file_name){
        window.location="/download/"+file_name;
    }
    function del_file(file_name, id){
        if({{$importPo->open}}){
        $('#files_table').html($('#sk-spinner').clone().show());
        del_request(file_name, id);
        }
    }
    
    function del_request(file_name, id){
        if({{$importPo->open}}){
            $.ajax({
            type: "GET",
                url: '/delete_file/'+file_name,
                success: function (data) {
                    if (data.success=='DELETED') {
                        view_files(id);
                    }
                }
            });
        }
    }

    
</script>

<div class="row">
    
        @if (session('message'))
        <div class="alert alert-success">
            <strong>Successful Action!</strong><br><br>
            <ul>
                <li>{{ session('message') }}</li>
            </ul>
        </div>
        @endif
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

</div>
<div id="view_files" class="" style="background-color: #e7eaec; display: none; ">
    
</div>
<div id="upload_files" class="" style="background-color: #e7eaec; display: none; ">
<form action="{{url('/upload')}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" id='file_order_id' name="order_id" value="{{$importPo->id}}" />
    Select Field
    <div class="form-group"><select class="form-control" name="column_type" >
        <option value="">Select a file type</option>
        <option value="rec_comm_inv_pking">Comm Inv, Pking List</option>
        <option value="original_fcr">ORG/Lacey</option>
        <option value="ctpat_form">CTPAT form</option>
        <option value="telex_release">Telex Release B/L</option>
        </select></div>
    <input type="file" name="file_upload_field[]" multiple />
    <input type="submit" value="Upload" />
</form>
</div>
<div class="row">
<div class="col-sm-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Edit Order</h5>
        </div>
        <div class="ibox-content">
            <form class="form-horizontal" method="POST" action="{{ url('/orders/'.$importPo->id) }}" >
                
                {{ csrf_field() }}

                @if(isset($display['order_id']) and $display['order_id']==1) 
                <div class="form-group">
                    
                    {{ Form::label('order_id', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                    {{Form::text('order_id',$importPo->id,['class' => 'form-control','disabled' => 'disabled'])}}
                    </div>
                </div>
                @endif
               
                <div class="form-group">
                    
                    {{ Form::label('status', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                    {{Form::text('open',$importPo->open_cerrada(),['class' => 'form-control','disabled' => 'disabled'])}}
                    </div>
                </div>
               
                @if(isset($display['po_number']) and $display['po_number']==1)

                <div class="form-group {{ $errors->has('po_number') ? ' has-error' : '' }}">
                        	
                    {{ Form::label('Po Number', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('po_number',$importPo->po_number,$importPo->input_editable())}}
                            @if ($errors->has('po_number'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('po_number') }}</strong>
                                </span>
                            @endif
                    </div>
                </div>
                @endif
                @if(isset($display['comments']) and $display['comments']==1) 
                <div class="form-group {{ $errors->has('comments') ? ' has-error' : '' }}">
                    
                    {{ Form::label('Comments', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::textarea('comments',$importPo->comments,$importPo->input_editable(true))}}
                            @if ($errors->has('comments'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('comments') }}</strong>
                                </span>
                            @endif
                    </div>
                </div>
                @endif
                @if(isset($display['value']) and $display['value']==1) 
                <div class="form-group {{ $errors->has('value') ? ' has-error' : '' }}">
                    
                    {{ Form::label('Value $', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('value',number_format($importPo->value),$importPo->input_editable())}}
                            @if ($errors->has('value'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('value') }}</strong>
                                </span>
                            @endif
                    </div>
                </div>
                @endif
                @if(isset($display['fty_produc']) and $display['fty_produc']==1)
                <div class="form-group{{ $errors->has('fty_produc') ? ' has-error' : '' }}">
                    {{ Form::label('fty_produc', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        <select class="form-control select2" name="fty_produc" id="fty_produc" {{($importPo->open)?" ":" disabled"}}>
                            <option value="">Select Factories</option>
                            @foreach ($tableData['factories'] as $factory)
                                <option @if($factory->id==$importPo->fty_produc) selected="selected" @endif value="{{ $factory->id }}">{{ $factory->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('fty_produc'))
                            <span class="help-block">
                                <strong>{{ $errors->first('fty_produc') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                @endif
                @if(isset($display['export_cos']) and $display['export_cos']==1) 
                <div class="form-group{{ $errors->has('export_cos') ? ' has-error' : '' }}">
                        {{ Form::label('export_cos', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                            <select class="form-control select2" name="export_cos" id="export_cos" {{($importPo->open)?" ":" disabled"}}>
                                <option value="">Select Export Co</option>
                                @foreach ($tableData['export_cos'] as $export_cos)
                                    <option @if($export_cos->id==$importPo->export_cos) selected="selected" @endif  value="{{ $export_cos->id }}">{{ $export_cos->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('export_cos'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('export_cos') }}</strong>
                                </span>
                            @endif
                        </div>
                </div>
                @endif
                @if(isset($display['export_cos']) and $display['export_cos']==1) 
                <div id="div_fama" class="form-group {{ $errors->has('licenses.*') ? ' has-error' : '' }}">
                        
                    {{ Form::label('FAMA', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                    {{Form::text('fama',$importPo->fama(),['class' => 'form-control','disabled' => 'disabled','id'=>'fama'])}}
                       
                            <span class="help-block" style="display:none;">
                                <strong></strong>
                            </span>
                    </div>
                </div>
                @endif
                @if(isset($display['prod_style']) and $display['prod_style']==1) 
                <div class="form-group {{ $errors->has('product_styles') ? ' has-error' : '' }}">
                        
                    {{ Form::label('product_styles', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                            <select class="form-control select2-mul" name="product_styles[]" multiple="multiple" id="product_styles" data-placeholder="Select Product Style" {{($importPo->open)?" ":" disabled"}}>
                                @foreach ($tableData['product_styles'] as $product_style)
                                    <option {{collect($importPo->product_styles->pluck('id'))->contains($product_style->id)?' selected' : ''}}  value="{{ $product_style->id }}">{{ $product_style->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('product_styles'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('product_styles') }}</strong>
                                </span>
                            @endif
                        </div>
                </div>
                @endif
                @if(isset($display['prod_type']) and $display['prod_type']==1)
                <div class="form-group {{ $errors->has('product_types') ? ' has-error' : '' }}">
                        
                    {{ Form::label('product_types', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                            <select class="form-control select2-mul" name="product_types[]" multiple="multiple" id="product_types" data-placeholder="Select Product Types" {{($importPo->open)?" ":" disabled"}}>
                                @foreach ($tableData['product_types'] as $product_type)
                                    <option {{collect($importPo->product_types->pluck('id'))->contains($product_type->id)?' selected' : ''}}  value="{{ $product_type->id }}">{{ $product_type->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('product_types'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('product_types') }}</strong>
                                </span>
                            @endif
                        </div>
                </div>
                @endif
                @if(isset($display['licenses']) and $display['licenses']==1) 
                <div class="form-group {{ $errors->has('licenses') ? ' has-error' : '' }}">
                        
                    {{ Form::label('licenses', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                            <select class="form-control select2-mul" name="licenses[]" multiple="multiple" id="licenses" data-placeholder="Select Licenses" {{($importPo->open)?" ":" disabled"}}>
                                @foreach ($tableData['licenses'] as $license)

                                    <option {{collect(old('licenses',$importPo->licenses->pluck('id')))->contains($license->id)?' selected' : ''}}  value="{{ $license->id }}">{{ $license->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('licenses'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('licenses') }}</strong>
                                </span>
                            @endif
                        </div>
                </div>
                @endif
                @if(isset($display['customers']) and $display['customers']==1)
                <div class="form-group {{ $errors->has('customers') ? ' has-error' : '' }}">
                        
                    {{ Form::label('customers', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                            <select class="form-control select2-mul" name="customers[]" multiple="multiple" id="customers" data-placeholder="Select Customers" {{($importPo->open)?" ":" disabled"}}>
                                @foreach ($tableData['customers'] as $customer)
                                    <option {{collect($importPo->customers->pluck('id'))->contains($customer->id)?' selected' : ''}}  value="{{ $customer->id }}">{{ $customer->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('customers'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('customers') }}</strong>
                                </span>
                            @endif
                        </div>
                </div>
                @endif
                @if(isset($display['customer_po_number']) and $display['customer_po_number']==1)
                <div class="form-group {{ $errors->has('customer_po_number') ? ' has-error' : '' }}">
                    
                    {{ Form::label('customer_po_number', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('customer_po_number',$importPo->customer_po_number,$importPo->input_editable())}}
                            @if ($errors->has('customer_po_number'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('customer_po_number') }}</strong>
                                </span>
                            @endif
                    </div>
                </div>
                @endif
                @if(isset($display['date_placed']) and $display['date_placed']==1)
                <div class="form-group {{ $errors->has('date_placed') ? ' has-error' : '' }}">
                        
                        {{ Form::label('date_placed', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                                <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="date_placed" id="date_placed" class="form-control date-pick" value="{{($importPo->import_po_date->date_placed==null)?'':$importPo->import_po_date->date_placed->format('M d Y')}}" {{($importPo->open)?" ":" disabled"}}>
                                </div>
                                @if ($errors->has('date_placed'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('date_placed') }}</strong>
                                    </span>
                                @endif
                        </div>
                </div>
                @endif
                @if(isset($display['qty_ordered']) and $display['qty_ordered']==1)
                <div class="form-group {{ $errors->has('qty_ordered') ? ' has-error' : '' }}">
                    
                    {{ Form::label('qty_ordered', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('qty_ordered',$importPo->qty_ordered,$importPo->input_editable())}}
                            @if ($errors->has('qty_ordered'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('qty_ordered') }}</strong>
                                </span>
                            @endif
                    </div>
                </div>
                @endif
                @if(isset($display['tickets_needed']) and $display['tickets_needed']==1) 
                <div class="form-group{{ $errors->has('tickets_needed') ? ' has-error' : '' }}">
                    {{ Form::label('tickets_needed', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        <select class="form-control" name="tickets_needed" id="tickets_needed" {{($importPo->open)?" ":" disabled"}}>
                                <option value="">Select Yes/No</option>
                                <option @if($importPo->tickets_needed===1) selected="selected" @endif value="1">Yes</option>
                                <option @if($importPo->tickets_needed===0) selected="selected" @endif value="0">No</option>
                        </select>
                        @if ($errors->has('tickets_needed'))
                            <span class="help-block">
                                <strong>{{ $errors->first('tickets_needed') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                @endif
                @if(isset($display['tickets_ordered']) and $display['tickets_ordered']==1)
                <div class="form-group {{ $errors->has('tickets_ordered') ? ' has-error' : '' }}">
                    
                    {{ Form::label('tickets_ordered', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                            <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="tickets_ordered" id="tickets_ordered" class="form-control date-pick" value="{{($importPo->ticket_ordered()->count()<=0)?'':$importPo->ticket_ordered->tickets_ordered->format('M d Y')}}" {{($importPo->open)?" ":" disabled"}}>
                            </div>
                            @if ($errors->has('tickets_ordered'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('tickets_ordered') }}</strong>
                                </span>
                            @endif
                    </div>
                </div>
                @endif
                @if(isset($display['tickets_received']) and $display['tickets_received']==1)
                <div class="form-group {{ $errors->has('tickets_received') ? ' has-error' : '' }}">
                        
                        {{ Form::label('tickets_received', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                                <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="tickets_received" id="tickets_received" class="form-control date-pick" value="{{($importPo->import_po_date->tickets_received==null)?'':$importPo->import_po_date->tickets_received->format('M d Y')}}" {{($importPo->open)?" ":" disabled"}}>
                                </div>
                                @if ($errors->has('tickets_received'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tickets_received') }}</strong>
                                    </span>
                                @endif
                        </div>
                </div>
                @endif
                @if(isset($display['shipping_from']) and $display['shipping_from']==1)
                <div class="form-group{{ $errors->has('shipping_from') ? ' has-error' : '' }}">
                    {{ Form::label('shipping_from', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        <select class="form-control select2" name="shipping_from" id="shipping_from" {{($importPo->open)?" ":" disabled"}}>
                            <option value="">Select Shipping From</option>
                            @foreach ($tableData['ports'] as $port)
                                <option @if($port->id==$importPo->shipping_from) selected="selected" @endif  value="{{  $port->id }}">{{ $port->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('shipping_from'))
                            <span class="help-block">
                                <strong>{{ $errors->first('shipping_from') }}</strong>
                            </span>
                        @endif
                    </div>
            </div>
            @endif
            @if(isset($display['shipping_to']) and $display['shipping_to']==1) 
            <div class="form-group{{ $errors->has('shipping_to') ? ' has-error' : '' }}">
                    {{ Form::label('shipping_to', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        <select class="form-control select2" name="shipping_to" id="shipping_to" {{($importPo->open)?" ":" disabled"}}>
                            <option value="">Select Shipping to</option>
                            @foreach ($tableData['ports_to'] as $ports_to)
                                <option @if($ports_to->id==$importPo->shipping_to) selected="selected" @endif  value="{{  $ports_to->id }}">{{ $ports_to->port_name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('shipping_to'))
                            <span class="help-block">
                                <strong>{{ $errors->first('shipping_to') }}</strong>
                            </span>
                        @endif
                    </div>
            </div>
            @endif
            @if(isset($display['estimated_arr']) and $display['estimated_arr']==1)
            <div class="form-group {{ $errors->has('estimated_arr') ? ' has-error' : '' }}">
                    
                    {{ Form::label('estimated_arr', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                            <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="estimated_arr" id="estimated_arr" class="form-control date-pick" value="{{($importPo->import_po_date->estimated_arr==null)?'':$importPo->import_po_date->estimated_arr->format('M d Y')}}" disabled>
                            </div>
                            @if ($errors->has('estimated_arr'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('estimated_arr') }}</strong>
                                </span>
                            @endif
                    </div>
            </div>
            @endif
            @if(isset($display['finish_prod']) and $display['finish_prod']==1) 
                <div class="form-group {{ $errors->has('finish_prod') ? ' has-error' : '' }}">
                        
                        {{ Form::label('finish_prod', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                                <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="finish_prod" id="finish_prod" class="form-control date-pick" value="{{($importPo->import_po_date->finish_prod==null)?'':$importPo->import_po_date->finish_prod->format('M d Y')}}" {{($importPo->open)?" ":" disabled"}}>
                                </div>
                                @if ($errors->has('finish_prod'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('finish_prod') }}</strong>
                                    </span>
                                @endif
                        </div>
                </div>
                @endif
               
                @if(isset($display['customer_cancel']) and $display['customer_cancel']==1)
                <div id="div_customer_cancel" class="form-group {{ $errors->has('customer_cancel') ? ' has-error' : '' }}">
                    
                    {{ Form::label('customer_cancel', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                            <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="customer_cancel" id="customer_cancel" class="form-control date-pick" value="{{($importPo->import_po_date->customer_cancel==null)?'':$importPo->import_po_date->customer_cancel->format('M d Y')}}" {{($importPo->open)?" ":" disabled"}}>
                            </div>
                            @if ($errors->has('customer_cancel'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('customer_cancel') }}</strong>
                                </span>
                            @endif
                    </div>
                </div>
                @endif
                @if(isset($display['cbm']) and $display['cbm']==1)
                <div id="div_cbm" class="form-group {{ $errors->has('cbm') ? ' has-error' : '' }}">
                    
                    {{ Form::label('cbm', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                    <input type="number" min="0" name="cbm" id="cbm" value="{{$importPo->cbm}}" class="form-control" {{($importPo->open)?" ":" disabled"}}/>
                    <span class="help-block" style="display:none;">
                        <strong></strong>
                    </span>
                    </div>
                </div>
                @endif
                @if(isset($display['book_container']) and $display['book_container']==1)
                <div class="form-group {{ $errors->has('book_container') ? ' has-error' : '' }}">
                    
                    {{ Form::label('book_container', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('book_container',$importPo->book_container,$importPo->input_editable())}}
                            @if ($errors->has('book_container'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('book_container') }}</strong>
                                </span>
                            @endif
                    </div>
                </div>
                @endif
                @if(isset($display['act_etd_forwarder']) and $display['act_etd_forwarder']==1)
                <div id="div_act_etd_forwarder" class="form-group {{ $errors->has('act_etd_forwarder') ? ' has-error' : '' }}">
                        
                        {{ Form::label('act_etd_forwarder', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                                <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="act_etd_forwarder" id="act_etd_forwarder" class="form-control date-pick" value="{{($importPo->import_po_date->act_etd_forwarder==null)?'':$importPo->import_po_date->act_etd_forwarder->format('M d Y')}}" {{($importPo->open)?" ":" disabled"}}>
                                </div>
                               
                                    <span id="span_act_etd_forwarder" class="help-block">
                                    <strong id="strong_act_etd_forwarder"></strong>
                                    </span>
                                
                        </div>
                </div>
                @endif
                @if(isset($display['act_eta_forwarder']) and $display['act_eta_forwarder']==1) 
                <div class="form-group {{ $errors->has('act_eta_forwarder') ? ' has-error' : '' }}">
                        
                        {{ Form::label('act_eta_forwarder', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                                <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="act_eta_forwarder" id="act_eta_forwarder" class="form-control date-pick" value="{{($importPo->import_po_date->act_eta_forwarder==null)?'':$importPo->import_po_date->act_eta_forwarder->format('M d Y')}}" {{($importPo->open)?" ":" disabled"}}>
                                </div>
                                @if ($errors->has('act_eta_forwarder'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('act_eta_forwarder') }}</strong>
                                    </span>
                                @endif
                        </div>
                </div>
                @endif
                @if(isset($display['safety_test']) and $display['safety_test']==1)
                <div class="form-group{{ $errors->has('safety_test') ? ' has-error' : '' }}">
                    {{ Form::label('safety_test', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        <select class="form-control" name="safety_test" id="safety_test" {{($importPo->open)?" ":" disabled"}}>
                                <option value="">Select Yes/No</option>
                                <option @if($importPo->safety_test===1) selected="selected" @endif value="1">Yes</option>
                                <option @if($importPo->safety_test===0) selected="selected" @endif value="0">No</option>
                        </select>
                        @if ($errors->has('safety_test'))
                            <span class="help-block">
                                <strong>{{ $errors->first('safety_test') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                @endif
                @if(isset($display['safety_test_on']) and $display['safety_test_on']==1)  
                <div class="form-group {{ $errors->has('safety_test_on') ? ' has-error' : '' }}">
                        
                        {{ Form::label('safety_test_on', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                                <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="safety_test_on" id="safety_test_on" class="form-control date-pick" value="{{($importPo->import_po_date->safety_test_on==null)?'':$importPo->import_po_date->safety_test_on->format('M d Y')}}" disabled>
                                </div>
                                @if ($errors->has('safety_test_on'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('safety_test_on') }}</strong>
                                    </span>
                                @endif
                        </div>
                </div>
                @endif
                @if(isset($display['fty_prod_safety_test']) and $display['fty_prod_safety_test']==1)
                <div class="form-group {{ $errors->has('fty_prod_safety_test') ? ' has-error' : '' }}">
                    
                    {{ Form::label('fty_prod_safety_test', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                            <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="fty_prod_safety_test" id="fty_prod_safety_test" class="form-control date-pick" value="{{($importPo->import_po_date->fty_prod_safety_test==null)?'':$importPo->import_po_date->fty_prod_safety_test->format('M d Y')}}" {{($importPo->open)?" ":" disabled"}}>
                            </div>
                            @if ($errors->has('fty_prod_safety_test'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('fty_prod_safety_test') }}</strong>
                                </span>
                            @endif
                    </div>
                </div>
                @endif
                @if(isset($display['safety_test_pass']) and $display['safety_test_pass']==1)
                <div class="form-group{{ $errors->has('safety_test_pass') ? ' has-error' : '' }}">
                    {{ Form::label('safety_test_pass', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        <select class="form-control" name="safety_test_pass" id="safety_test_pass" {{($importPo->open)?" ":" disabled"}}>
                                <option value="">Select Yes/No</option>
                                <option @if($importPo->safety_test_pass===1) selected="selected" @endif value="1">Yes</option>
                                <option @if($importPo->safety_test_pass===0) selected="selected" @endif value="0">No</option>
                        </select>
                        @if ($errors->has('safety_test_pass'))
                            <span class="help-block">
                                <strong>{{ $errors->first('safety_test_pass') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                @endif
                @if(isset($display['david_ins_result']) and $display['david_ins_result']==1)
                <div class="form-group{{ $errors->has('david_ins_result') ? ' has-error' : '' }}">
                    {{ Form::label('david_ins_result', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        <select class="form-control" name="david_ins_result" id="david_ins_result" {{($importPo->open)?" ":" disabled"}}>
                                <option value="">Select Pass/Fail</option>
                                <option @if($importPo->david_ins_result===1) selected="selected" @endif value="1">Pass</option>
                                <option @if($importPo->david_ins_result===0) selected="selected" @endif value="0">Fail</option>
                        </select>
                        @if ($errors->has('david_ins_result'))
                            <span class="help-block">
                                <strong>{{ $errors->first('david_ins_result') }}</strong>
                            </span>
                        @endif
                    </div>
            </div>
            @endif
            @if(isset($display['david_ins_comment']) and $display['david_ins_comment']==1)
            <div class="form-group {{ $errors->has('david_ins_comment') ? ' has-error' : '' }}">
                    
                    {{ Form::label('david_ins_comment', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::textarea('david_ins_comment',$importPo->david_ins_comment,$importPo->input_editable(true))}}
                            @if ($errors->has('david_ins_comment'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('david_ins_comment') }}</strong>
                                </span>
                            @endif
                    </div>
            </div>
            @endif
            @if(isset($display['svn_no']) and $display['svn_no']==1)   
            <div class="form-group {{ $errors->has('svn_no') ? ' has-error' : '' }}">
                
                {{ Form::label('svn_no', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                <div class="col-md-6 col-sm-5 col-xs-6">
                {{Form::text('svn_no',$importPo->svn_no,['class' => 'form-control','disabled' => 'disabled'])}}
                </div>
            </div>
            @endif
            @if(isset($display['freight_forwarder']) and $display['freight_forwarder']==1)
            <div class="form-group{{ $errors->has('freight_forwarder') ? ' has-error' : '' }}">
                {{ Form::label('freight_forwarder', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                <div class="col-md-6 col-sm-5 col-xs-6">
                    <select class="form-control select2" name="freight_forwarder" id="freight_forwarder" {{($importPo->open)?" ":" disabled"}}>
                            <option value="">Select Freight Forwarders</option>
                            @foreach ($tableData['freight_forwarders'] as $freight_forwarder)
                                <option @if($freight_forwarder->id==$importPo->freight_forwarder) selected="selected" @endif value="{{ $freight_forwarder->id }}">{{ $freight_forwarder->name }}</option>
                            @endforeach
                    </select>
                    @if ($errors->has('freight_forwarder'))
                        <span class="help-block">
                            <strong>{{ $errors->first('freight_forwarder') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            @endif
            @if(isset($display['rec_comm_inv_pking']) and $display['rec_comm_inv_pking']==1)
            <div id="div_rec_comm_inv_pking" class="form-group {{ $errors->has('rec_comm_inv_pking') ? ' has-error' : '' }}">
                        
                        {{ Form::label('rec_comm_inv_pking', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                                <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="rec_comm_inv_pking" id="rec_comm_inv_pking" class="form-control date-pick" value="{{($importPo->import_po_date->rec_comm_inv_pking==null)?'':$importPo->import_po_date->rec_comm_inv_pking->format('M d Y')}}" {{($importPo->open)?" ":" disabled"}}>
                                </div>
                                @if ($errors->has('rec_comm_inv_pking'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rec_comm_inv_pking') }}</strong>
                                    </span>
                                @endif
                        </div>
            </div>
            @endif
                @if(isset($display['rec_comm_inv_pking_home']) and $display['rec_comm_inv_pking_home']==1)
                <div id="div_rec_comm_inv_pking_home" class="form-group {{ $errors->has('rec_comm_inv_pking_home') ? ' has-error' : '' }}">
                        
                        {{ Form::label('rec_comm_inv_pking_home', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                                <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="rec_comm_inv_pking_home" id="rec_comm_inv_pking_home" class="form-control date-pick" value="{{($importPo->import_po_date->rec_comm_inv_pking_home==null)?'':$importPo->import_po_date->rec_comm_inv_pking_home->format('M d Y')}}" {{($importPo->open)?" ":" disabled"}}>
                                </div>
                                @if ($errors->has('rec_comm_inv_pking_home'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rec_comm_inv_pking_home') }}</strong>
                                    </span>
                                @endif
                        </div>
                </div>
                @endif
                @if(isset($display['original_fcr']) and $display['original_fcr']==1)
                <div id="div_original_fcr" class="form-group {{ $errors->has('original_fcr') ? ' has-error' : '' }}">
                        
                        {{ Form::label('ORG/Lacey', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                                <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="original_fcr" id="original_fcr" class="form-control date-pick" value="{{($importPo->import_po_date->original_fcr==null)?'':$importPo->import_po_date->original_fcr->format('M d Y')}}" {{($importPo->open)?" ":" disabled"}}>
                                </div>
                                @if ($errors->has('original_fcr'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('original_fcr') }}</strong>
                                    </span>
                                @endif
                        </div>
                </div>
                @endif
                @if(isset($display['ctpat_form']) and $display['ctpat_form']==1) 
                <div id="div_ctpat_form" class="form-group {{ $errors->has('ctpat_form') ? ' has-error' : '' }}">
                        
                        {{ Form::label('ctpat_form', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                                <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="ctpat_form" id="ctpat_form" class="form-control date-pick" value="{{($importPo->import_po_date->ctpat_form==null)?'':$importPo->import_po_date->ctpat_form->format('M d Y')}}" {{($importPo->open)?" ":" disabled"}}>
                                </div>
                                @if ($errors->has('ctpat_form'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ctpat_form') }}</strong>
                                    </span>
                                @endif
                        </div>
                </div>
                @endif
                @if(isset($display['telex_release']) and $display['telex_release']==1)
                <div id="div_telex_release" class="form-group {{ $errors->has('telex_release') ? ' has-error' : '' }}">
                        
                        {{ Form::label('telex_release', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                                <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="telex_release" id="telex_release" class="form-control date-pick" value="{{($importPo->import_po_date->telex_release==null)?'':$importPo->import_po_date->telex_release->format('M d Y')}}" {{($importPo->open)?" ":" disabled"}}>
                                </div>
                                @if ($errors->has('telex_release'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('telex_release') }}</strong>
                                    </span>
                                @endif
                        </div>
                </div>
                @endif
                @if(isset($display['pro_photo']) and $display['pro_photo']==1) 
                <div class="form-group{{ $errors->has('pro_photo') ? ' has-error' : '' }}">
                    {{ Form::label('pro_photo', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                        <select class="form-control" name="pro_photo" id="pro_photo" {{($importPo->open)?" ":" disabled"}}>
                                <option value="">Select Yes/No</option>
                                <option @if($importPo->pro_photo===1) selected="selected" @endif value="1">Yes</option>
                                <option @if($importPo->pro_photo===0) selected="selected" @endif value="0">No</option>
                        </select>
                        @if ($errors->has('pro_photo'))
                            <span class="help-block">
                                <strong>{{ $errors->first('pro_photo') }}</strong>
                            </span>
                        @endif
                    </div>
            </div>
            @endif
            @if(isset($display['okay_to_pay']) and $display['okay_to_pay']==1)
                <div class="form-group {{ $errors->has('okay_to_pay') ? ' has-error' : '' }}">
                        
                        {{ Form::label('okay_to_pay', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                                <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="okay_to_pay" id="okay_to_pay" class="form-control date-pick" value="{{($importPo->import_po_date->okay_to_pay==null)?'':$importPo->import_po_date->okay_to_pay->format('M d Y')}}" {{($importPo->open)?" ":" disabled"}}>
                                </div>
                                @if ($errors->has('okay_to_pay'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('okay_to_pay') }}</strong>
                                    </span>
                                @endif
                        </div>
                </div>
                @endif
                @if(isset($display['did_we_pay']) and $display['did_we_pay']==1)
                <div class="form-group {{ $errors->has('did_we_pay') ? ' has-error' : '' }}">
                        
                        {{ Form::label('did_we_pay', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                                <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="did_we_pay" id="did_we_pay" class="form-control date-pick" value="{{($importPo->import_po_date->did_we_pay==null)?'':$importPo->import_po_date->did_we_pay->format('M d Y')}}" {{($importPo->open)?" ":" disabled"}}>
                                </div>
                                @if ($errors->has('did_we_pay'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('did_we_pay') }}</strong>
                                    </span>
                                @endif
                        </div>
                </div>
                @endif
                @if(isset($display['send_diann']) and $display['send_diann']==1)
                <div class="form-group {{ $errors->has('send_diann') ? ' has-error' : '' }}">
                        
                        {{ Form::label('send_diann', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                                <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="send_diann" id="send_diann" class="form-control date-pick" value="{{($importPo->import_po_date->send_diann==null)?'':$importPo->import_po_date->send_diann->format('M d Y')}}" {{($importPo->open)?" ":" disabled"}}>
                                </div>
                                @if ($errors->has('send_diann'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('send_diann') }}</strong>
                                    </span>
                                @endif
                        </div>
                </div>
                @endif  
                @if(isset($display['receive_whse']) and $display['receive_whse']==1)
                    <div class="form-group{{ $errors->has('receive_whse') ? ' has-error' : '' }}">
                            {{ Form::label('receive_whse', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                            <div class="col-md-6 col-sm-5 col-xs-6">
                                <select class="form-control" name="receive_whse" id="receive_whse" {{($importPo->open)?" ":" disabled"}}>
                                        <option value="">Select Yes/No</option>
                                        <option @if($importPo->receive_whse===1) selected="selected" @endif value="1">Yes</option>
                                        <option @if($importPo->receive_whse===0) selected="selected" @endif value="0">No</option>
                                </select>
                                @if ($errors->has('receive_whse'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('receive_whse') }}</strong>
                                    </span>
                                @endif
                            </div>
                    </div>
                    @endif
                    @if(isset($display['salesmen']) and $display['salesmen']==1) 
                    <div class="form-group {{ $errors->has('salesmen') ? ' has-error' : '' }}">
                            
                        {{ Form::label('salesmen', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                                <select class="form-control select2-mul" name="salesmen[]" multiple="multiple" id="salesmen" data-placeholder="Select Salesmen" {{($importPo->open)?" ":" disabled"}}>
                                        @foreach ($tableData['salesmen'] as $salesman)
                                        <option {{collect($importPo->salesmen->pluck('id'))->contains($salesman->id)?' selected' : ''}}  value="{{ $salesman->id }}">{{ $salesman->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('salesmen'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('salesmen') }}</strong>
                                    </span>
                                @endif
                            </div>
                    </div>
                    @endif
                    @if(isset($display['files']) and $display['files']==1)      
                    <div class="form-group">
                        
                        {{ Form::label('files', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                            <p><a onclick="view_files({{ $importPo->id }})">View</a>
                            @if($importPo->open) / <br/><a onclick="upload_files({{ $importPo->id }})">Upload</a>@endif
                        </p>
                        </div>
                    </div>
                    @endif
                    
            
                @if($importPo->open)
                <div class="form-group">
                    <div class="col-lg-offset-4 col-lg-8">
                        <a href="{{url('orders/open')}}" class="btn btn-info">List open P.Os </a>
                        <button class="btn btn-primary" type="submit">Update</button>
                    </form>
                    @if(Auth::user()->hasRole(['Admin','Production']))
                        <form id="close_ord" method="POST" action="{{ url('/orders/movetoclosed/'.$importPo->id) }}" style="display:inline;">
                            {{ csrf_field() }}
                            <button  type="button" onclick="closed_orders({{ $importPo->id }})" class='btn btn-warning'>Close Order</button>
                        </form> 
                       
                    @endif
                    @if(Auth::user()->hasRole('Admin'))
                        <form id="del_ord" method="POST" action="{{ url('/orders/delete/'.$importPo->id) }}" style="display:inline;">
                            <input type="hidden" name="DeleteorderID" value="{{ $importPo->id }}">
                            {{ csrf_field() }}
                            <button id='delete_button{{ $importPo->id }}' onclick="delete_orders({{ $importPo->id }})" type="button" class='btn btn-danger'>Delete</button>
                        </form>
                    @endif
                    </div>
                </div>
                @else
                    </form>
                    @if(Auth::user()->hasRole('Admin'))
                    <div class="col-lg-offset-4 col-lg-8">
                            <a href="{{url('orders/closed')}}" class="btn btn-info">List closed P.Os</a>
                            <form id="open_ord" method="POST" action="{{ url('/orders/movetoopen/'.$importPo->id) }}" style="display:inline;">
                                {{ csrf_field() }}
                                <button  type="button" onclick="open_orders({{ $importPo->id }})" class='btn btn-warning'>Move to open order</button>
                            </form> 
                            <form id="del_ord" method="POST" action="{{ url('/orders/delete/'.$importPo->id) }}" style="display:inline;">
                                <input type="hidden" name="DeleteorderID" value="{{ $importPo->id }}">
                                {{ csrf_field() }}
                                <button id='delete_button{{ $importPo->id }}' onclick="delete_orders({{ $importPo->id }})" type="button" class='btn btn-danger'>Delete</button>
                            </form>
                    </div>
                    @endif
                @endif
           
        </div>
    </div>
</div>
</div>
<div id='sk-spinner' class="sk-spinner sk-spinner-wave" style="display: none;">
        <div class="sk-rect1"></div>
        <div class="sk-rect2"></div>
        <div class="sk-rect3"></div>
        <div class="sk-rect4"></div>
        <div class="sk-rect5"></div>
    </div>
@endsection
@push('scripts')
<script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
<script>

 var atHome= false;
 var hobbyLobby =  false;

$('#span_act_etd_forwarder').hide();
 @if ($errors->has('act_etd_forwarder'))
    $('#span_act_etd_forwarder').show();
    $('#strong_act_etd_forwarder').empty();
    $('#strong_act_etd_forwarder').html('{{ $errors->first('act_etd_forwarder')}}');
 @endif


    $('.select2').select2();
    $('.select2-mul').select2({
        tags: true,
        multiple: true,
        width: '100%'
    });
    $('.date-pick').datepicker({
            format:'M d yyyy',
             autoclose: true
            });
    function delete_orders(id){
    if (confirm("Are you sure you want to delete this P.O? Click OK to continue")){
        $('#del_ord').submit();
    }
    }
    function closed_orders(id){
        if (confirm("Are you sure you want to move this P.O to Closed orders? Click OK to continue")){
            $('#close_ord').submit();
        }
    }
    function open_orders(id){
        if (confirm("Are you sure you want to move this P.O to open orders? Click OK to continue")){
            $('#open_ord').submit();
        }
    }
    function obtener_arrival(){
        if($('#shipping_to').val() && $('#finish_prod').val()){
            var ruta = "{{url('orders/obtener_arrival')}}";
            var to=$('#shipping_to').val() ;
            var finish=$('#finish_prod').val();
                $.get(ruta+'?port_to='+to+'&finish_prod='+finish, function(data) {
                    $('#estimated_arr').empty();
                    $('#estimated_arr').val(data);
                    validarCustomerCancel();
                });
        }else{
            $('#estimated_arr').val("");
        }
    }
    function obtener_test_on(){
        if($('#finish_prod').val() &&  $('#safety_test').val()==1){
            var ruta = "{{url('orders/obtener_test_on')}}";
            var finish=$('#finish_prod').val();
                $.get(ruta+'?finish_prod='+finish, function(data) {
                    $('#safety_test_on').empty();
                    $('#safety_test_on').val(data);
            
                });
        }else{
            $('#safety_test_on').val("");
        }
    }
    function obtener_fama(){
        if($('#export_cos').val()){
            var ruta = "{{url('orders/obtener_fama/')}}";
                $.get(ruta+'/'+$('#export_cos').val(), function(data) {
                    $('#fama').empty();
                    $('#fama').val(data);
            
                });
            }else{
                $('#fama').val('');
        }
    }
        
    $("#licenses").change(function(e){
            e.preventDefault();
            validarLicencias();

    }); 

    function validarLicencias(){
        var _token = $("input[name='_token']").val();
    var export_cos = $("select[name='export_cos']").val();
    var licenses = $("select[name='licenses[]']").val();

        $.ajax({
            url: "/orders/validarLicencias",
            type:'POST',
            data: {
                _token:_token,
                export_cos:export_cos,
                licenses:licenses
                },
            success: function(data) {
                if(!$.isEmptyObject(data.error)){
                    $('#div_fama').addClass('has-error');
                    $('#div_fama span').show();
                    $('#div_fama strong').html(data.error[0]);
                }else{
                    $('#div_fama').removeClass('has-error');
                    $('#div_fama span').hide();
                }
            }
        });
    }
    function validarCBM(){
        var _token = $("input[name='_token']").val();
        var cbm = $("input[name='cbm']").val();
        var finish_prod = $("input[name='finish_prod']").val();
        if(cbm!=""){
            $.ajax({
                url: "/orders/validarCBM",
                type:'POST',
                data: {
                    _token:_token,
                    finish_prod:finish_prod,
                    cbm:cbm
                    },
                success: function(data) {
                    if(!$.isEmptyObject(data.error)){
                        $('#div_cbm').addClass('has-error');
                        $('#div_cbm span').show();
                        $('#div_cbm strong').html(data.error[0]);
                    }else{
                        $('#div_fama').removeClass('has-error');
                        $('#div_fama span').hide();
                    }
                }
            });
        }
    }
    function validarCamposEtd(validar=true){
        var rec_comm_inv_pking = $("input[name='rec_comm_inv_pking']").val();
        var rec_comm_inv_pking_home = $("input[name='rec_comm_inv_pking_home']").val();
        var original_fcr= $("input[name='original_fcr']").val();
        var ctpat_form= $("input[name='ctpat_form']").val();
        var telex_release= $("input[name='telex_release']").val();

         if(rec_comm_inv_pking=="" && validar){
            $('#div_rec_comm_inv_pking').addClass('has-error');
         }else{
             if(validar){
                $('#div_rec_comm_inv_pking').removeClass('has-error');
             }
           
         }

          if(rec_comm_inv_pking_home=="" && atHome){
            $("input[name='rec_comm_inv_pking_home']").prop('disabled', false);
            $('#div_rec_comm_inv_pking_home').addClass('has-error');
         }else{
             if(atHome){
                $("input[name='rec_comm_inv_pking_home']").prop('disabled', false);
             }else{
                $("input[name='rec_comm_inv_pking_home']").val("");
                $("input[name='rec_comm_inv_pking_home']").prop('disabled', true);
             }
            $('#div_rec_comm_inv_pking_home').removeClass('has-error');
         }
         if(original_fcr=="" && (atHome || hobbyLobby)){
            $("input[name='original_fcr']").prop('disabled', false);
            $('#div_original_fcr').addClass('has-error');
         }else{
             if(atHome || hobbyLobby){
                $("input[name='original_fcr']").prop('disabled', false);
             }else{
                $("input[name='original_fcr']").val("");
                $("input[name='original_fcr']").prop('disabled', true);
             }
            $('#div_original_fcr').removeClass('has-error');
         }
         if(ctpat_form=="" && hobbyLobby){
            $("input[name='ctpat_form']").prop('disabled', false);
            $('#div_ctpat_form').addClass('has-error');
         }else{
            if(hobbyLobby){
                $("input[name='ctpat_form']").prop('disabled', false);
             }else{
                $("input[name='ctpat_form']").val("");
                $("input[name='ctpat_form']").prop('disabled', true);
             }
            $('#div_ctpat_form').removeClass('has-error');
         }
         if(telex_release==""  && validar){
            $('#div_telex_release').addClass('has-error');
         }else{
            if(validar){
            $('#div_telex_release').removeClass('has-error');
            }
         }


    }
    function validarETD(){
        var _token = $("input[name='_token']").val();
        var  act_etd_forwarder= $("input[name='act_etd_forwarder']").val();
        var rec_comm_inv_pking = $("input[name='rec_comm_inv_pking']").val();
        var rec_comm_inv_pking_home = $("input[name='rec_comm_inv_pking_home']").val();
        var original_fcr= $("input[name='original_fcr']").val();
        var ctpat_form= $("input[name='ctpat_form']").val();
        var telex_release= $("input[name='telex_release']").val();
        if(act_etd_forwarder!=""){
            $.ajax({
                url: "/orders/validarETD",
                type:'POST',
                data: {
                    _token:_token,
                    act_etd_forwarder:act_etd_forwarder,
                    rec_comm_inv_pking:rec_comm_inv_pking,
                    rec_comm_inv_pking_home:rec_comm_inv_pking_home,
                    org_lacey:original_fcr,
                    ctpat_form:ctpat_form,
                    telex_release:telex_release
                    },
                success: function(data) {
                    if(!$.isEmptyObject(data.error)){
                        $('#div_act_etd_forwarder').addClass('has-error');
                        $('#span_act_etd_forwarder').show();
                        $('#strong_act_etd_forwarder').empty();
                        $('#strong_act_etd_forwarder').html(data.error[0]);
                    }else{
                        $('#div_act_etd_forwarder').removeClass('has-error');
                        $('#strong_act_etd_forwarder').empty();
                        $('#span_act_etd_forwarder').hide();
                       
                    }
                    validarCamposEtd();
                }
            });
        }
    }
    function validarCustomers(){
    var _token = $("input[name='_token']").val();
    var customers = $("select[name='customers[]']").val();

        $.ajax({
            url: "/orders/validarCustomers",
            type:'POST',
            data: {
                _token:_token,
               
                customers:customers
                },
            success: function(data) {
                if(data.success==0){
                    atHome= false;
                    hobbyLobby =  false;
                }else if(data.success==1){
                    atHome= true;
                    hobbyLobby =  false;
                }else if(data.success==2){
                    atHome= false;
                    hobbyLobby =  true;
                }else if(data.success==3){
                    atHome= true;
                    hobbyLobby =  true;
                }
                validarCamposEtd(false);
            }
        });
    }
    $('#shipping_to').change(function(e) {
        e.preventDefault();
        obtener_arrival();
    
    });
    $('#finish_prod').change(function(e) {
        e.preventDefault();
        obtener_arrival();
        obtener_test_on();
        validarCBM();
    });
    $('#export_cos').change(function(e) {
        e.preventDefault();
        obtener_fama();
        validarLicencias();
    
    });
      
    $("#licenses").change(function(e){
            e.preventDefault();
            validarLicencias();

    }); 
    $("#cbm").change(function(e){
            e.preventDefault();
            validarCBM();

    }); 
    $('#safety_test').change(function(e) {
        e.preventDefault();
        obtener_test_on();
    
    });
    $('#act_etd_forwarder').change(function(e) {
        e.preventDefault();
        validarETD();
    
    });
    $('#customer_cancel').change(function(e) {
        e.preventDefault();
        validarCustomerCancel();
    
    });
    $('#customers').change(function(e) {
        e.preventDefault();
       
        validarCustomers();
    
    });
    function validarCustomerCancel(){
        if($('#shipping_to').val() && $('#finish_prod').val() && $('#customer_cancel').val()){
            var ruta = "{{url('orders/validar_customer_cancel')}}";
            var to=$('#shipping_to').val() ;
            var finish=$('#finish_prod').val();
            var customer_cancel=$('#customer_cancel').val();
                $.get(ruta+'?port_to='+to+'&finish_prod='+finish+'&customer_cancel='+customer_cancel, function(data) {
                 
                  if(data=='1'){
                    $('#div_customer_cancel').removeClass('has-error');
                  }else{
                    $('#div_customer_cancel').addClass('has-error');
                  }
            
                });
        }else{
            $('#div_customer_cancel').removeClass('has-error');
        }
    }
    
    obtener_fama();
    obtener_arrival();
    obtener_test_on();
    validarLicencias();
    validarCBM();
    validarCustomers();
    validarETD();
    
</script>

@endpush
