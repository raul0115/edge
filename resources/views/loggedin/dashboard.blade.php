@extends('loggedin.layout')

@section('content')

@if (session('message'))
<div class="alert alert-success">
    <strong>Successful Action!</strong><br><br>
    <ul>
        <li>{{ session('message') }}</li>
    </ul>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row">
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <!--<span class="label label-success pull-right">Monthly</span>-->
                <h5>Total Orders</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">{{ number_format($data['OpenCount']) }}</h1>
                <!--<div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>-->
                <small>Open Orders</small>
            </div>
        </div>
    </div>
    
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <!--<span class="label label-success pull-right">Monthly</span>-->
                <h5>Value Sum</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">$ {{ number_format($data['valueOpenCount']) }}</h1>
                <!--<div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>-->
                <small>Open Orders</small>
            </div>
        </div>
    </div>
    
    
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <!--<span class="label label-success pull-right">Monthly</span>-->
                <h5>CBM Sum</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">{{ $data['cbmOpenCount'] }}</h1>
                <!--<div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>-->
                <small>Open Orders</small>
            </div>
        </div>
    </div>
    
    
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <!--<span class="label label-success pull-right">Monthly</span>-->
                <h5>Total Quantity Ordered</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">{{ number_format($data['qtyOrderedOpenCount']) }}</h1>
                <!--<div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>-->
                <small>Open Orders</small>
            </div>
        </div>
    </div>
</div>
 @if(auth()->user()->hasRole('Admin')) 
<div class="row">
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <!--<span class="label label-success pull-right">Monthly</span>-->
                <h5>Cron Last runtime</h5>
            </div>
            <div class="ibox-content">
                @if($data['cronRunTime'] == null)
                <small>The cron never ran</small>
                @else
                <small>{{ $data['cronRunTime'] }}</small>
                @endif
                <!--<div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>-->
                <!---->
            </div>
        </div>
    </div>
</div>
 @endif

<!--time_elapsed_string($notification->created_at)-->

@endsection