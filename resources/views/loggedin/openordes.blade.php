@extends('loggedin.layout')

@section('content')

<script type="text/javascript">
function view_files(id){
    $('#files_table').html($('#sk-spinner').clone().show());
    $('#view_files').dialog({
        minHeight:0,
        title:"View Files",
        modal: true,
        open: function() {
                $('.ui-widget-overlay').on('click', function() {
                    $('#view_files').dialog('close');
                })
        },
    });
    
    var update_td_id = "files_t_"+id;
    $.ajax({
    type: "POST",
    url: '/view_files',
    data: { order_id: id, _token: '{{csrf_token()}}' },
    success: function (data) {
       console.log("Success");
       $('#view_files').html(data);
//        if (typeof data.error !== 'undefined') {
//            $('#'+update_td_id).html();// remove progress annimation and add old value again
//        
//            
//        }
    },
    error: function (data, textStatus, errorThrown) {
        console.log(data);
        $('#view_files').html(data);// remove progress annimation and add old value again
        
    },
});
}

function upload_files(order_id){
    $('#file_order_id').val(order_id);
    $('#upload_files').dialog({
        minHeight:0,
        title:"Upload Files",
        modal: true,
        open: function() {
            $('.ui-widget-overlay').on('click', function() {
                $('#upload_files').dialog('close');
            })
        },
    });
}
function download_file(file_name){
    window.location="/download/"+file_name;
}
function del_file(file_name, id){
    $('#files_table').html($('#sk-spinner').clone().show());
    del_request(file_name, id);
}

function del_request(file_name, id){
        $.ajax({
        type: "GET",
            url: '/delete_file/'+file_name,
            success: function (data) {
                if (data.success=='DELETED') {
                    view_files(id);
                }
            }
        });
}
function remove_nav(id){
var remove_id = 'close_notification_'+id;
var elem = document.getElementById(remove_id);
    elem.parentNode.removeChild(elem);
}
function multi_select(idsData, select_id){
//$("#"+select_id).find('option').attr("selected",false) ;
$("#"+select_id).val([]);//clear previously selected
var dataarray=idsData.split(",");//Make an array
$("#"+select_id).val(dataarray);// Set the value
$("#sub_salesmen_value,#sub_licenses_value,#sub_customers_value,#sub_product_styles_value,#sub_product_types_value ").select2({
        tags: true,
        multiple: true,
        width: '100%'
    });
}
function display_addnew_div(ID){
$(ID).toggle();
}
function submit_textarea_ajax(){
//close dialog after submit is clicked
$('#submit_textarea_fields').dialog('close');
//the id of td that is getting edited
var update_td_id = $('#sub_textarea_field_name').val()+"_t_"+$('#sub_textarea_order_id').val();
//show progress annimation
$('#'+update_td_id).html($('#sk-spinner').clone().show());
var form = $('#submit_textarea_form');
$.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function (data) {
        // remove progress annimation and add new value
        $('#'+update_td_id).text(data.newValue);
        //We remove ondbclick event so to add a new ondblclick event for next time
        //document.getElementById(update_td_id).removeAttribute('ondblclick');
        //We add the new ondblclick event to that row id
        document.getElementById(update_td_id).setAttribute('ondblclick', "submit_textarea_fields( " +$('#sub_textarea_order_id').val()+ ",'"+ $('#sub_textarea_field_name').val()+ "', '"+$('#sub_textarea_field_label').val()+"','" + data.newValue+ "');"  );
        if (typeof data.error !== 'undefined') {
            $('#'+update_td_id).html($('#sub_textarea_field_old_val').val());// remove progress annimation and add old value again
        
            swal({
                title: "Error: "+data.error,
                type: "error",
            });
        }
    },
    error: function (data, textStatus, errorThrown) {
        $('#'+update_td_id).html($('#sub_textarea_field_old_val').val());// remove progress annimation and add old value again
        swal({
            title: "Server or Network error",
            type: "error",
        });
    },
});

}
function submit_textarea_fields(order_id, field_name, field_label, field_value){
document.getElementById('submit_textarea_form').reset();
$('#sub_textarea_order_id').val(order_id);
$('#sub_textarea_field_name').val(field_name);
$('#sub_textarea_field_label').val(field_label);
$('#sub_textarea_field_old_val').val(field_value);
$('#sub_textarea_field_value').text(field_value);
$('#submit_textarea_fields').dialog({
    minHeight:0,
    title:field_label,
    modal: true,
    open: function() {
            $('.ui-widget-overlay').on('click', function() {
                $('#submit_textarea_fields').dialog('close');
            })
    },
});
}
function submit_text_ajax(){
//close dialog after submit is clicked
$('#submit_text_fields').dialog('close');
//the id of td that is getting edited
var update_td_id = $('#sub_txt_field_name').val()+"_t_"+$('#sub_txt_order_id').val();
//show progress annimation
var selector="";
var po= false;
if($('#sub_txt_field_name').val()=="po_number"){
    selector =$('.'+update_td_id);
    po=true;
}else{
    selector =$('#'+update_td_id);
}
selector.html($('#sk-spinner').clone().show());
var form = $('#submit_text_form');
$.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function (data) {


            if (typeof data.error !== 'undefined') {
                selector.text($('#sub_txt_field_old_val').val());
                swal({
                    title: "Error: "+data.error,
                    type: "error",
                });
            }else{
                selector.text(data.newValue)
                if(po){
                document.getElementsByClassName(update_td_id)[1].setAttribute('ondblclick', "submit_text_fields( " +$('#sub_txt_order_id').val()+ ",'"+ $('#sub_txt_field_name').val()+ "', '"+$('#sub_txt_field_label').val()+"','" +data.newValue+ "');"  );
            
                }else{
                    document.getElementById(update_td_id).setAttribute('ondblclick', "submit_text_fields( " +$('#sub_txt_order_id').val()+ ",'"+ $('#sub_txt_field_name').val()+ "', '"+$('#sub_txt_field_label').val()+"','" +data.newValue+ "');"  );
       
                }
            }
            
       
    },
    error: function (data, textStatus, errorThrown) {
    
        selector.text($('#sub_txt_field_old_val').val());
        swal({
            title: "Server or Network error",
            type: "error",
        });
    },
});

}


function submit_text_fields(order_id, field_name, field_label, field_value){
$('#sub_txt_order_id').val(order_id);
$('#sub_txt_field_name').val(field_name);
$('#sub_txt_field_label').val(field_label);
$('#sub_txt_field_old_val').val(field_value);
$('#sub_txt_field_value').val(field_value);
$('#submit_text_fields').dialog({
    minHeight:0,
    title:field_label,
    modal: true,
    open: function() {
            $('.ui-widget-overlay').on('click', function() {
                $('#submit_text_fields').dialog('close');
            })
    },
});
}

function validarETD(mi_id){
    

   var rec_comm_inv_pking= $("#rec_comm_inv_pking_t_"+mi_id);
   var rec_comm_inv_pking_home= $("#rec_comm_inv_pking_home_t_"+mi_id);
   var original_fcr= $("#original_fcr_t_"+mi_id);
   var ctpat_form= $("#ctpat_form_t_"+mi_id);
   var telex_release= $("#telex_release_t_"+mi_id);
   if(rec_comm_inv_pking.text().length==0){
        rec_comm_inv_pking.css("background-color", "#F58888");
   }else{
        rec_comm_inv_pking.css("background-color", "transparent");
   }
   if( rec_comm_inv_pking_home.text().length==0){
    rec_comm_inv_pking_home.css("background-color", "#F58888");
   }else{
    rec_comm_inv_pking_home.css("background-color", "transparent");
   }
   if(original_fcr.text().length==0){
    original_fcr.css("background-color", "#F58888");
   }else{
    original_fcr.css("background-color", "transparent");
   }
   if(ctpat_form.text().length==0){
    ctpat_form.css("background-color", "#F58888");
   }else{
    ctpat_form.css("background-color", "transparent");
   }
   if(telex_release.text().length==0){
    telex_release.css("background-color", "#F58888");
   }else{
    telex_release.css("background-color", "transparent");
   }
   
}
function submit_date_ajax(){
    var adate = $("#sub_date_field_value").datepicker("getDate");
    if(adate=="Invalid Date"){
        adate = null;
    }

        $('#sub_date_field_value').val($.datepicker.formatDate("yy-mm-dd", adate));


        //close dialog after submit is clicked
        $('#submit_date_fields').dialog('close');
        //the id of td that is getting edited
        var update_td_id = $('#sub_date_field_name').val()+"_t_"+$('#sub_date_order_id').val();
        //show progress annimation
        $('#'+update_td_id).html($('#sk-spinner').clone().show());
        var mi_id=$('#sub_date_order_id').val();
        var mi_name =$('#sub_date_field_name').val();
        var form = $('#submit_date_form');
        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize(),
            success: function (data) {

                if (typeof data.error !== 'undefined') {
                    var d =  $.datepicker.parseDate('yy-mm-dd', $('#sub_date_field_old_val').val());
                    $('#'+update_td_id).text(d.toString().substr(4, 11));
                // $('#'+update_td_id).html($('#sub_date_field_old_val').val());// remove progress annimation and add old value again
                    swal({
                        title: "Error: "+data.error,
                        type: "error",
                    });
                    document.getElementById(update_td_id).setAttribute('ondblclick', "submit_date_fields( " +$('#sub_date_order_id').val()+ ",'"+ $('#sub_date_field_name').val()+ "', '"+$('#sub_date_field_label').val()+"','" +$('#sub_date_field_old_val').val()+ "');"  );
                }else{
                    if(data.newValue!=null){
                        // remove progress annimation and add new value
                        var d =  $.datepicker.parseDate('yy-mm-dd', data.newValue);
                        $('#'+update_td_id).text(d.toString().substr(4, 11));
                        
                            validarETD(mi_id);
                        


                    }else{
                        $('#'+update_td_id).text("");
                        data.newValue = "";
                    }
                    //We remove ondbclick event so to add a new ondblclick event for next time
                    //document.getElementById(update_td_id).removeAttribute('ondblclick');
                    //We add the new ondblclick event to that row id
                    document.getElementById(update_td_id).setAttribute('ondblclick', "submit_date_fields( " +$('#sub_date_order_id').val()+ ",'"+ $('#sub_date_field_name').val()+ "', '"+$('#sub_date_field_label').val()+"','" +data.newValue+ "');"  );
            
                }
            },
            error: function (data, textStatus, errorThrown) {
                    $('#'+update_td_id).html($('#sub_date_field_old_val').val());// remove progress annimation and add old value again
                swal({
                    title: "Server or Network error",
                    type: "error",
                });
            },
        });

        

}
function submit_date_fields(order_id, field_name, field_label, field_value){
$('#sub_date_order_id').val(order_id);
$('#sub_date_field_name').val(field_name);
$('#sub_date_field_label').val(field_label);
$('#sub_date_field_old_val').val(field_value);
$('#sub_date_field_value').val(field_value);
$('#sub_date_field_value').datepicker('remove');//removing old datepicker so that 'setDates' work
   if(field_value){
    var d = new Date($('#sub_date_field_value').val());
     var sFecha = ((d.getDate() +1 )+"-" + (d.getMonth() + 1) + "-" + d.getFullYear());
   }else{
    var d = new Date();
    var sFecha = (d.getDate()+"-" + (d.getMonth() + 1) + "-" + d.getFullYear());
   }
    
     var a = d.setDate(d.getDate());
    $('#sub_date_field_value').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: 'd MM yyyy',
    });
    $('#sub_date_field_value').datepicker('setDate', sFecha);
$('#submit_date_fields').dialog({
    minHeight:0,
    title:field_label,
    modal: true,
    open: function() {
            $('.ui-widget-overlay').on('click', function() {
                $('#submit_date_fields').dialog('close');
            })
    },
});
}
function submit_yes_no_ajax(){
//close dialog after submit is clicked
$('#submit_yes_no_fields').dialog('close');
//the id of td that is getting edited
var update_td_id = $('#sub_yes_no_name').val()+"_t_"+$('#sub_yes_no_order_id').val();
//show progress annimation
$('#'+update_td_id).html($('#sk-spinner').clone().show());
var form = $('#submit_yes_no_form');
$.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function (data) {
        // remove progress annimation and add new value
        set_y_n(update_td_id, data.newValue);
        //We remove ondbclick event so to add a new ondblclick event for next time
        //document.getElementById(update_td_id).removeAttribute('ondblclick');
        //We add the new ondblclick event to that row id
        document.getElementById(update_td_id).setAttribute('ondblclick', "submit_yes_no_fields( " +$('#sub_yes_no_order_id').val()+ ",'"+ $('#sub_yes_no_name').val()+ "', '"+$('#sub_yes_no_label').val()+"','"+$('#sub_yes_no_value').val()+"');"  );
        if (typeof data.error !== 'undefined') {
            
            set_y_n(update_td_id, $('#sub_yes_no_old_val').val());// remove progress annimation and add old value again
            swal({
                title: "Error: "+data.error,
                type: "error",
            });
        }
    },
    error: function (data, textStatus, errorThrown) {
            set_y_n(update_td_id, $('#sub_yes_no_old_val').val());// remove progress annimation and add old value again
        swal({
            title: "Server or Network error",
            type: "error",
        });
    },
});
}
function get_y_n_td_bool(td_tag_id){
    var value = $('#'+td_tag_id).val();
    if (value == 1) {return "Yes";}
    if (value == 0) {return "No";}
}
function set_y_n(td_id, bool){
    if (bool == 1) {var set = "Yes";}
    if (bool == 0) {var set = "No";}
    if (bool == null) {var set = null;}
    $('#'+td_id).text(set);
}
function submit_yes_no_fields(order_id, field_name, field_label, field_value){
$('#sub_yes_no_order_id').val(order_id);
$('#sub_yes_no_name').val(field_name);
$('#sub_yes_no_label').val(field_label);
$('#sub_yes_no_old_val').val(field_value);
$('#sub_yes_no_value').val(field_value);
$('#submit_yes_no_fields').dialog({
    minHeight:0,
    title:field_label,
    modal: true,
    open: function() {
            $('.ui-widget-overlay').on('click', function() {
                $('#submit_yes_no_fields').dialog('close');
            })
    },
});
}
function submit_pass_fail_ajax(){
//close dialog after submit is clicked
$('#submit_pass_fail_fields').dialog('close');
//the id of td that is getting edited
var update_td_id = $('#sub_pass_fail_name').val()+"_t_"+$('#sub_pass_fail_order_id').val();
//show progress annimation
$('#'+update_td_id).html($('#sk-spinner').clone().show());
var form = $('#submit_pass_fail_form');
$.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function (data) {
        // remove progress annimation and add new value
        set_p_f(update_td_id, data.newValue);
        //We add the new ondblclick event to that row id
        document.getElementById(update_td_id).setAttribute('ondblclick', "submit_pass_fail_fields( " +$('#sub_pass_fail_order_id').val()+ ",'"+ $('#sub_pass_fail_name').val()+ "', '"+$('#sub_pass_fail_label').val()+"','"+$('#sub_pass_fail_value').val()+"');"  );
        if (typeof data.error !== 'undefined') {
            
            set_p_f(update_td_id, $('#sub_pass_fail_old_val').val());// remove progress annimation and add old value again
            swal({
                title: "Error: "+data.error,
                type: "error",
            });
        }
    },
    error: function (data, textStatus, errorThrown) {
            set_p_f(update_td_id, $('#sub_pass_fail_old_val').val());// remove progress annimation and add old value again
        swal({
            title: "Server or Network error",
            type: "error",
        });
    },
});
}
function set_p_f(td_id, bool){
    if (bool == 1) {var set = "Pass";}
    if (bool == 0) {var set = "Fail";}
    if (bool == null) {var set = null;}
    $('#'+td_id).text(set);
}
function submit_pass_fail_fields(order_id, field_name, field_label, field_value){
$('#sub_pass_fail_order_id').val(order_id);
$('#sub_pass_fail_name').val(field_name);
$('#sub_pass_fail_label').val(field_label);
$('#sub_pass_fail_old_val').val(field_value);
$('#sub_pass_fail_value').val(field_value);
$('#submit_pass_fail_fields').dialog({
    minHeight:0,
    title:field_label,
    modal: true,
    open: function() {
            $('.ui-widget-overlay').on('click', function() {
                $('#submit_pass_fail_fields').dialog('close');
            })
    },
});
}
function submit_num_ajax(){
    
    //close dialog after submit is clicked
$('#submit_numeric_fields').dialog('close');
//the id of td that is getting edited
var update_td_id = $('#sub_num_name').val()+"_t_"+$('#sub_num_order_id').val();
//show progress annimation
$('#'+update_td_id).html($('#sk-spinner').clone().show());
var form = $('#submit_numeric_form');
$.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function (data) {
        // remove progress annimation and add new value
        if($('#sub_num_name').val() == 'value') 
        {$('#'+update_td_id).text(data.newValueMoney);}
        else{$('#'+update_td_id).text(data.newValue);}
        //We add the new ondblclick event to that row id
        document.getElementById(update_td_id).setAttribute('ondblclick', "submit_numeric_fields( " +$('#sub_num_order_id').val()+ ",'"+ $('#sub_num_name').val()+ "', '"+$('#sub_num_label').val()+"','" + $('#sub_num_value').val()+ "');"  );
        if (typeof data.error !== 'undefined') {
            $('#'+update_td_id).html($('#sub_num_old_val').val());// remove progress annimation and add old value again
            swal({
                title: "Error: "+data.error,
                type: "error",
            });
        }
    },
    error: function (data, textStatus, errorThrown) {
            $('#'+update_td_id).html($('#sub_num_old_val').val());// remove progress annimation and add old value again
        swal({
            title: "Server or Network error",
            type: "error",
        });
    },
});
    
}
function submit_numeric_fields(order_id, field_name, field_label, field_value ){
$('#sub_num_order_id').val(order_id);
$('#sub_num_name').val(field_name);
$('#sub_num_label').val(field_label);
$('#sub_num_old_val').val(field_value);
$('#sub_num_value').val(field_value);
$('#submit_numeric_fields').dialog({
    minHeight:0,
    title:field_label,
    modal: true,
    open: function() {
            $('.ui-widget-overlay').on('click', function() {
                $('#submit_numeric_fields').dialog('close');
            })
    },
});
}

function submit_cbm_ajax(){
    //close dialog after submit is clicked
$('#submit_cbm_fields').dialog('close');
//the id of td that is getting edited
var update_td_id = $('#sub_cbm_name').val()+"_t_"+$('#sub_cbm_order_id').val();
//show progress annimation
$('#'+update_td_id).html($('#sk-spinner').clone().show());
var form = $('#submit_cbm_form');
$.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function (data) {
        // remove progress annimation and add new value
        $('#'+update_td_id).text(data.newValue);
        //We add the new ondblclick event to that row id
        document.getElementById(update_td_id).setAttribute('ondblclick', "submit_cbm_fields( " +$('#sub_cbm_order_id').val()+ ",'"+ $('#sub_cbm_name').val()+ "', '"+$('#sub_cbm_label').val()+"','" + $('#sub_cbm_value').val()+ "');"  );
        if (typeof data.error !== 'undefined') {
            $('#'+update_td_id).html($('#sub_cbm_old_val').val());// remove progress annimation and add old value again
            swal({
                title: "Error: "+data.error,
                type: "error",
            });
        }
    },
    error: function (data, textStatus, errorThrown) {
            $('#'+update_td_id).html($('#sub_cbm_old_val').val());// remove progress annimation and add old value again
        swal({
            title: "Server or Network error",
            type: "error",
        });
    },
});
}
function submit_cbm_fields(order_id, field_name, field_label, field_value, warning){
$('#submit_cbm_warning').html('');
if(warning == 1 ) {
$('#submit_cbm_warning').html('Warning: FAMA or SafetyConditions not met. Please make sure to meet these conditions before entering the value');
}
$('#sub_cbm_order_id').val(order_id);
$('#sub_cbm_name').val(field_name);
$('#sub_cbm_label').val(field_label);
$('#sub_cbm_old_val').val(field_value);
$('#sub_cbm_value').val(field_value);
$('#submit_cbm_fields').dialog({
    minHeight:0,
    title:field_label,
    modal: true,
    open: function() {
            $('.ui-widget-overlay').on('click', function() {
                $('#submit_cbm_fields').dialog('close');
            })
    },
});
}
function submit_fty_produc_field(order_id, current_field_selected, VID,name){
$('#sub_fty_produc_order_id').val(order_id);
$('#sub_fty_produc_old_val').val(current_field_selected);
$('#sub_fty_produc_vid_old_val').val(VID);
$('#sub_fty_produc_value').val(current_field_selected);
$('#sub_fty_produc_old_name').val(name);
$('#submit_fty_produc_field').dialog({
    minHeight:0,
    title: 'Factory Producing',
    modal: true,
    open: function() {
            $('.ui-widget-overlay').on('click', function() {
                $('#submit_fty_produc_field').dialog('close');
            })
    },
});
}

function submit_fty_produc_ajax(){
//close dialog after submit is clicked
$('#submit_fty_produc_field').dialog('close');
//the id of td that is getting edited
var update_td_id = "fty_produc_t_"+$('#sub_fty_produc_order_id').val();
//show progress annimation
$('#'+update_td_id).html($('#sk-spinner').clone().show());
var form = $('#submit_fty_produc_form');
$.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function (data) {
        // remove progress annimation and add new value
        $('#'+update_td_id).text(data.newValue);
        //We add the new ondblclick event to that row id
        document.getElementById(update_td_id).setAttribute('ondblclick', "submit_fty_produc_field( " +$('#sub_fty_produc_order_id').val()+ ",'"+ data.newId+ "','"+ data.VID+ "','"+data.newValue+"');"  );
        document.getElementById(update_td_id).setAttribute('title',data.VID);
        if (typeof data.error !== 'undefined') {
            
            $('#'+update_td_id).html($('#sub_fty_produc_old_name').val());
            document.getElementById(update_td_id).setAttribute('ondblclick', "submit_fty_produc_field( " +$('#sub_fty_produc_order_id').val()+ ",'"+$('#sub_fty_produc_old_val').val()+ "','"+$('#sub_fty_produc_vid_old_val').val()+ "','"+$('#sub_fty_produc_old_name').val()+"');"  );
            document.getElementById(update_td_id).setAttribute('title',$('#sub_fty_produc_vid_old_val').val());
            swal({
                title: "Error: "+data.error,
                type: "error",
            });
        }
    },
    error: function (data, textStatus, errorThrown) {
         $('#'+update_td_id).html($('#sub_fty_produc_old_name').val());
         document.getElementById(update_td_id).setAttribute('ondblclick', "submit_fty_produc_field( " +$('#sub_fty_produc_order_id').val()+ ",'"+$('#sub_fty_produc_old_val').val()+ "','"+$('#sub_fty_produc_vid_old_val').val()+ "','"+$('#sub_fty_produc_old_name').val()+"');"  );
         document.getElementById(update_td_id).setAttribute('title',$('#sub_fty_produc_old_val').val());
        swal({
            title: "Server or Network error",
            type: "error",
        });
    },
});
}
function submit_export_cos_ajax(){
//close dialog after submit is clicked
$('#submit_export_cos_field').dialog('close');
//the id of td that is getting edited
var update_td_id = "export_cos_t_"+$('#sub_export_cos_order_id').val();
//show progress annimation
$('#'+update_td_id).html($('#sk-spinner').clone().show());
var form = $('#submit_export_cos_form');
$.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function (data) {
        // remove progress annimation and add new value
        $('#'+update_td_id).text(data.newValue);
        $('#export_cos_fema_'+$('#sub_export_cos_order_id').val()).text(data.fama);
        //We add the new ondblclick event to that row id
        document.getElementById(update_td_id).setAttribute('ondblclick', "submit_export_cos_field( " +$('#sub_export_cos_order_id').val()+ ",'"+ data.newId+ "','"+ data.fama+ "','"+ data.newId+ "');"  );
        if (typeof data.error !== 'undefined') {
            $('#'+update_td_id).html($('#sub_export_cos_old_val').val());
            
            document.getElementById(update_td_id).setAttribute('ondblclick', "submit_export_cos_field( " +$('#sub_export_cos_order_id').val()+ ",'"+$('#sub_export_cos_old_val').val()+ "','"+$('#sub_export_cos_fama_old_val').val()+ "','"+ $('#sub_export_cos_value_old').val()+ "');"  );
            $('#export_cos_fema_'+$('#sub_export_cos_order_id').val()).html($('#sub_export_cos_fama_old_val').val());
            
            swal({
                title: "Error: "+data.error,
                type: "error",
            });
        }
    },
    error: function (data, textStatus, errorThrown) {
         $('#'+update_td_id).html($('#sub_export_cos_old_val').val());
         document.getElementById(update_td_id).setAttribute('ondblclick', "submit_export_cos_field( " +$('#sub_export_cos_order_id').val()+ ",'"+$('#sub_export_cos_old_val').val()+ "','"+$('#sub_export_cos_fama_old_val').val()+ "','"+ $('#sub_export_cos_value_old').val()+ "');"  );
         $('#export_cos_fema_'+$('#sub_export_cos_order_id').val()).html($('#sub_export_cos_fama_old_val').val());
        swal({
            title: "Server or Network error",
            type: "error",
        });
    },
});
}
function submit_export_cos_field(order_id, current_field_text, current_fama_val, current_field_val){
$('#sub_export_cos_order_id').val(order_id);
$('#sub_export_cos_old_val').val(current_field_text);
$('#sub_export_cos_fama_old_val').val(current_fama_val);
$('#sub_export_cos_value_old').val(current_field_val);
$('#sub_export_cos_value').val(current_field_val);
$('#submit_export_cos_field').dialog({
    minHeight:0,
    title: 'Select Export Co',
    modal: true,
    open: function() {
            $('.ui-widget-overlay').on('click', function() {
                $('#submit_export_cos_field').dialog('close');
            })
    },
});
}
function submit_ports_ajax(){
//close dialog after submit is clicked
$('#submit_ports_field').dialog('close');
//the id of td that is getting edited
var update_td_id = $('#sub_ports_name').val()+"_t_"+$('#sub_ports_order_id').val();
//show progress annimation
$('#'+update_td_id).html($('#sk-spinner').clone().show());
var form = $('#submit_ports_form');
$.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function (data) {
        // remove progress annimation and add new value
        $('#'+update_td_id).text(data.newValue);
        //We add the new ondblclick event to that row id
        document.getElementById(update_td_id).setAttribute('ondblclick', "submit_ports_field( " +$('#sub_ports_order_id').val()+ ",'"+ $('#sub_ports_name').val()+ "', '"+$('#sub_ports_label').val()+"','" +data.newValue+ "', '"+$('#sub_ports_value').val()+"');"  );
        if (typeof data.error !== 'undefined') {
            $('#'+update_td_id).html($('#sub_ports_old_val').val());
            swal({
                title: "Error: "+data.error,
                type: "error",
            });
        }
    },
    error: function (data, textStatus, errorThrown) {
         $('#'+update_td_id).html($('#sub_ports_old_val').val());
        swal({
            title: "Server or Network error",
            type: "error",
        });
    },
});
}
function submit_portsto_ajax(){
//close dialog after submit is clicked
$('#submit_ports_to').dialog('close');

//the id of td that is getting edited
var update_td_id = $('#sub_portsto_name').val()+"_t_"+$('#sub_portsto_order_id').val();
//show progress annimation
$('#'+update_td_id).html($('#sk-spinner').clone().show());
var form = $('#submit_portsto_form');
$.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function (data) {
        // remove progress annimation and add new value
        $('#'+update_td_id).text(data.newValue);
        //We add the new ondblclick event to that row id
        document.getElementById(update_td_id).setAttribute('ondblclick', "submit_ports_to( " +$('#sub_portsto_order_id').val()+ ",'"+ $('#sub_portsto_name').val()+ "', '"+$('#sub_portsto_label').val()+"','" +data.newValue+ "', '"+$('#sub_portsto_value').val()+"');"  );
        if (typeof data.error !== 'undefined') {
            $('#'+update_td_id).html($('#sub_portsto_old_val').val());
            document.getElementById(update_td_id).setAttribute('ondblclick', "submit_ports_to( " +$('#sub_portsto_order_id').val()+ ",'"+ $('#sub_portsto_name').val()+ "', '"+$('#sub_portsto_label').val()+"','" +$('#sub_portsto_old_val').val()+ "', '"+$('#sub_portsto_value_old').val()+"');"  );
            swal({
                title: "Error: "+data.error,
                type: "error",
            });
        }
    },
    error: function (data, textStatus, errorThrown) {
         $('#'+update_td_id).html($('#sub_portsto_old_val').val());
         document.getElementById(update_td_id).setAttribute('ondblclick', "submit_ports_to( " +$('#sub_portsto_order_id').val()+ ",'"+ $('#sub_portsto_name').val()+ "', '"+$('#sub_portsto_label').val()+"','" +$('#sub_ports_old_val').val()+ "', '"+$('#sub_portsto_value_old').val()+"');"  );
        swal({
            title: "Server or Network error",
            type: "error",
        });
    },
});
}
function submit_ports_field(order_id, field_name, field_label, field_value, currentSelect){
$('#sub_ports_order_id').val(order_id);
$('#sub_ports_name').val(field_name);
$('#sub_ports_label').val(field_label);
$('#sub_ports_old_val').val(field_value);
$('#sub_ports_value').val(currentSelect);
$('#submit_ports_field').dialog({
    minHeight:0,
    title:field_label,
    modal: true,
    open: function() {
            $('.ui-widget-overlay').on('click', function() {
                $('#submit_ports_field').dialog('close');
            })
    },
});
}
function submit_ports_to(order_id, field_name, field_label, field_value, currentSelect){
    console.log(order_id);
$('#sub_portsto_order_id').val(order_id);
$('#sub_portsto_name').val(field_name);
$('#sub_portsto_label').val(field_label);
$('#sub_portsto_old_val').val(field_value);
$('#sub_portsto_value_old').val(currentSelect);
$('#sub_portsto_value').val(currentSelect);
$('#submit_ports_to').dialog({
    minHeight:0,
    title:field_label,
    modal: true,
    open: function() {
            $('.ui-widget-overlay').on('click', function() {
                $('#submit_ports_to').dialog('close');
            })
    },
});
}
function submit_freight_forwarders_ajax(){
//close dialog after submit is clicked
$('#submit_freight_forwarders_field').dialog('close');
//the id of td that is getting edited
var update_td_id = "freight_forwarders_t_"+$('#sub_freight_forwarders_order_id').val();
//show progress annimation
$('#'+update_td_id).html($('#sk-spinner').clone().show());
var form = $('#submit_freight_forwarders_form');
$.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function (data) {
        // remove progress annimation and add new value
        $('#'+update_td_id).text(data.newValue);
        //We add the new ondblclick event to that row id
        document.getElementById(update_td_id).setAttribute('ondblclick', "submit_freight_forwarders_field( " +$('#sub_freight_forwarders_order_id').val()+ ",'"+ data.newValue+ "','"+ data.newId+ "');" );
        if (typeof data.error !== 'undefined') {
            $('#'+update_td_id).html($('#sub_freight_forwarders_old_val').val());
            swal({
                title: "Error: "+data.error,
                type: "error",
            });
        }
    },
    error: function (data, textStatus, errorThrown) {
         $('#'+update_td_id).html($('#sub_freight_forwarders_old_val').val());
        swal({
            title: "Server or Network error",
            type: "error"
        });
    },
});
}
function submit_freight_forwarders_field(order_id, current_field_text, current_field_value){
$('#sub_freight_forwarders_order_id').val(order_id);
$('#sub_freight_forwarders_old_val').val(current_field_text);
$('#sub_freight_forwarders_value').val(current_field_value);
$('#submit_freight_forwarders_field').dialog({
    minHeight:0,
    title: 'Select Freight Forwarder',
    modal: true,
    open: function() {
            $('.ui-widget-overlay').on('click', function() {
                $('#submit_freight_forwarders_field').dialog('close');
            })
    },
});
}
function submit_product_styles_ajax(){
//close dialog after submit is clicked
$('#submit_product_styles_field').dialog('close');
//the id of td that is getting edited
var update_td_id = "product_styles_t_"+$('#sub_product_styles_order_id').val();
//show progress annimation
$('#'+update_td_id).html($('#sk-spinner').clone().show());
var form = $('#submit_product_styles_form');
$.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function (data) {
        // remove progress annimation and add new value
        $('#'+update_td_id).text(data.newValue);
        //We add the new ondblclick event to that row id
        document.getElementById(update_td_id).setAttribute('ondblclick', "submit_product_styles_field( " +$('#sub_product_styles_order_id').val()+ ",'"+ data.newValue+ "','"+data.newIds+"');" );
        if (typeof data.error !== 'undefined') {
            $('#'+update_td_id).html($('#sub_product_styles_old_val').val());
            document.getElementById(update_td_id).setAttribute('ondblclick', "submit_product_styles_field( " +$('#sub_product_styles_order_id').val()+ ",'"+$('#sub_product_styles_old_val').val()+ "','"+$('#sub_product_styles_previous_ids').val()+"');" );
            swal({
                title: "Error: "+data.error,
                type: "error"
            });
        }
    },
    error: function (data, textStatus, errorThrown) {
         $('#'+update_td_id).html($('#sub_product_styles_old_val').val());
         document.getElementById(update_td_id).setAttribute('ondblclick', "submit_product_styles_field( " +$('#sub_product_styles_order_id').val()+ ",'"+$('#sub_product_styles_old_val').val()+ "','"+$('#sub_product_styles_previous_ids').val()+"');" );
        swal({
            title: "Server or Network error",
            type: "error"
        });
    },
});
}
function submit_product_styles_field(order_id, current_field_selected, ids){
multi_select(ids, 'sub_product_styles_value');
$('#sub_product_styles_order_id').val(order_id);
$('#sub_product_styles_old_val').val(current_field_selected);
$('#sub_product_styles_previous_ids').val(ids);
$('#submit_product_styles_field').dialog({
    minHeight:0,
    title: 'Select Product Styles',
    modal: true,
    open: function() {
            $('.ui-widget-overlay').on('click', function() {
                $('#submit_product_styles_field').dialog('close');
            })
    },
});
}
function submit_product_types_ajax(){
//close dialog after submit is clicked
$('#submit_product_types_field').dialog('close');
//the id of td that is getting edited
var update_td_id = "product_types_t_"+$('#sub_product_types_order_id').val();
//show progress annimation
$('#'+update_td_id).html($('#sk-spinner').clone().show());
var form = $('#submit_product_types_form');
$.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function (data) {
        // remove progress annimation and add new value
        $('#'+update_td_id).text(data.newValue);
        //We add the new ondblclick event to that row id
        document.getElementById(update_td_id).setAttribute('ondblclick', "submit_product_types_field( " +$('#sub_product_types_order_id').val()+ ",'"+ data.newValue+ "','"+data.newIds+"');" );
        if (typeof data.error !== 'undefined') {
            $('#'+update_td_id).html($('#sub_product_types_old_val').val());
            document.getElementById(update_td_id).setAttribute('ondblclick', "submit_product_types_field( " +$('#sub_product_types_order_id').val()+ ",'"+ $('#sub_product_types_old_val').val()+ "','"+$('#sub_product_types_previous_ids').val()+"');" );
            swal({
                title: "Error: "+data.error,
                type: "error"
            });
        }
    },
    error: function (data, textStatus, errorThrown) {
        $('#'+update_td_id).html($('#sub_product_types_old_val').val());
        document.getElementById(update_td_id).setAttribute('ondblclick', "submit_product_types_field( " +$('#sub_product_types_order_id').val()+ ",'"+ $('#sub_product_types_old_val').val()+ "','"+$('#sub_product_types_previous_ids').val()+"');" );
        swal({
            title: "Server or Network error",
            type: "error"
        });
    },
});
}
function submit_product_types_field(order_id, current_field_selected, ids){
multi_select(ids, 'sub_product_types_value');
$('#sub_product_types_order_id').val(order_id);
$('#sub_product_types_old_val').val(current_field_selected);
$('#sub_product_types_previous_ids').val(ids);
$('#submit_product_types_field').dialog({
    minHeight:0,
    title: 'Select Product Types',
    modal: true,
    open: function() {
            $('.ui-widget-overlay').on('click', function() {
                $('#submit_product_types_field').dialog('close');
            })
    },
});
}
function submit_licenses_ajax(){
//close dialog after submit is clicked
$('#submit_licenses_field').dialog('close');
//the id of td that is getting edited
var update_td_id = "licenses_t_"+$('#sub_licenses_order_id').val();
//show progress annimation
$('#'+update_td_id).html($('#sk-spinner').clone().show());
var form = $('#submit_licenses_form');
$.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function (data) {
        // remove progress annimation and add new value
        $('#'+update_td_id).text(data.newValue);
        //We add the new ondblclick event to that row id
        document.getElementById(update_td_id).setAttribute('ondblclick', "submit_licenses_field( " +$('#sub_licenses_order_id').val()+ ",'"+ data.newValue+ "','"+data.newIds+"');" );
        if (typeof data.error !== 'undefined') {
            $('#'+update_td_id).html($('#sub_licenses_old_val').val());
            document.getElementById(update_td_id).setAttribute('ondblclick', "submit_licenses_field( " +$('#sub_licenses_order_id').val()+ ",'"+ $('#sub_licenses_old_val').val()+ "','"+$('#sub_licenses_previous_ids').val()+"');" );
            swal({
                title: "Error: "+data.error,
                type: "error"
            });
        }
    },
    error: function (data, textStatus, errorThrown) {
        $('#'+update_td_id).html($('#sub_licenses_old_val').val());
        document.getElementById(update_td_id).setAttribute('ondblclick', "submit_licenses_field( " +$('#sub_licenses_order_id').val()+ ",'"+ $('#sub_licenses_old_val').val()+ "','"+$('#sub_licenses_previous_ids').val()+"');" );
        swal({
            title: "Server or Network error",
            type: "error"
        });
    },
});
}
function submit_licenses_field(order_id, current_field_selected, ids){
multi_select(ids, 'sub_licenses_value');
$('#sub_licenses_order_id').val(order_id);
$('#sub_licenses_old_val').val(current_field_selected);
$('#sub_licenses_previous_ids').val(ids);
$('#submit_licenses_field').dialog({
    minHeight:0,
    title: 'Select Licenses',
    modal: true,
    open: function() {
            $('.ui-widget-overlay').on('click', function() {
                $('#submit_licenses_field').dialog('close');
            })
    },
});
}
function submit_customers_ajax(){
//close dialog after submit is clicked
$('#submit_customers_field').dialog('close');
//the id of td that is getting edited
var update_td_id = "customers_t_"+$('#sub_customers_order_id').val();
//show progress annimation
$('#'+update_td_id).html($('#sk-spinner').clone().show());
var form = $('#submit_customers_form');
$.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function (data) {
        // remove progress annimation and add new value
        $('#'+update_td_id).text(data.newValue);
        //We add the new ondblclick event to that row id
        document.getElementById(update_td_id).setAttribute('ondblclick', "submit_customers_field( " +$('#sub_customers_order_id').val()+ ",'"+ data.newValue+ "','"+data.newIds+"');" );
        if (typeof data.error !== 'undefined') {
            $('#'+update_td_id).html($('#sub_customers_old_val').val());
            swal({
                title: "Error: "+data.error,
                type: "error"
            });
        }
    },
    error: function (data, textStatus, errorThrown) {
        $('#'+update_td_id).html($('#sub_customers_old_val').val());
        swal({
            title: "Server or Network error",
            type: "error"
        });
    },
});
}
function submit_customers_field(order_id, current_field_selected, ids){
multi_select(ids, 'sub_customers_value');
$('#sub_customers_order_id').val(order_id);
$('#sub_customers_old_val').val(current_field_selected);
$('#sub_customers_previous_ids').val(ids);
$('#submit_customers_field').dialog({
    minHeight:0,
    title: 'Select Customers',
    modal: true,
    open: function() {
            $('.ui-widget-overlay').on('click', function() {
                $('#submit_customers_field').dialog('close');
            })
    },
});
}
function submit_salesmen_ajax(){
//close dialog after submit is clicked
$('#submit_salesmen_field').dialog('close');
//the id of td that is getting edited
var update_td_id = "salesmen_t_"+$('#sub_salesmen_order_id').val();
//show progress annimation
$('#'+update_td_id).html($('#sk-spinner').clone().show());
var form = $('#submit_salesmen_form');
$.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function (data) {
        // remove progress annimation and add new value
        $('#'+update_td_id).text(data.newValue);
        //We add the new ondblclick event to that row id
        document.getElementById(update_td_id).setAttribute('ondblclick', "submit_salesmen_field(" +$('#sub_salesmen_order_id').val()+ ",'"+data.newValue+ "','"+data.newIds+"');" );
        if (typeof data.error !== 'undefined') {
            $('#'+update_td_id).html($('#sub_salesmen_old_val').val());
            swal({
                title: "Error: "+data.error,
                type: "error"
            });
        }
    },
    error: function (data, textStatus, errorThrown) {
        $('#'+update_td_id).html($('#sub_salesmen_old_val').val());
        swal({
            title: "Server or Network error",
            type: "error"
        });
    },
});
}
function submit_salesmen_field(order_id, current_field_selected, ids){
multi_select(ids, 'sub_salesmen_value');
$('#sub_salesmen_order_id').val(order_id);
$('#sub_salesmen_old_val').val(current_field_selected);
$('#sub_salesmen_previous_ids').val(ids);
$('#submit_salesmen_field').dialog({
    minHeight:0,
    title: 'Select Salesmen',
    modal: true,
    open: function() {
            $('.ui-widget-overlay').on('click', function() {
                $('#submit_salesmen_field').dialog('close');
            })
    },
});
}
function submit_tickets_ordered_ajax(){
//close dialog after submit is clicked
$('#submit_tickets_ordered_field').dialog('close');
//the id of td that is getting edited
var update_td_id = "tickets_ordered_t_"+$('#submit_tickets_ordered_order_id').val();
//show progress annimation
$('#'+update_td_id).html($('#sk-spinner').clone().show());
var form = $('#submit_tickets_ordered_form');
$.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function (data) {
        // remove progress annimation and add new value
        $('#'+update_td_id).text(data.newValue);
        //We add the new ondblclick event to that row id
        document.getElementById(update_td_id).setAttribute('ondblclick', "submit_tickets_ordered_field(" +$('#submit_tickets_ordered_order_id').val()+ ",'"+ data.newId+ "');" );
        if (typeof data.error !== 'undefined') {
            $('#'+update_td_id).html($('#submit_tickets_ordered_old_val').val());
            swal({
                title: "Error: "+data.error,
                type: "error"
            });
        }
    },
    error: function (data, textStatus, errorThrown) {
        $('#'+update_td_id).html($('#submit_tickets_ordered_old_val').val());
        swal({
            title: "Server or Network error",
            type: "error"
        });
    },
});
}
function submit_tickets_ordered_field(order_id, current_field_selected){
$('#submit_tickets_ordered_order_id').val(order_id);
$('#submit_tickets_ordered_old_val').val(current_field_selected);
$('#submit_tickets_ordered_value').datepicker({
    keyboardNavigation: false,
    forceParse: false,
    //calendarWeeks: true,
    autoclose: true,
    format: 'yyyy-mm-dd'
});
$('#submit_tickets_ordered_field').dialog({
    minHeight:0,
    title:'Tickets Ordered',
    modal: true,
    open: function() {
            $('.ui-widget-overlay').on('click', function() {
                $('#submit_tickets_ordered_field').dialog('close');
            })
    },
});
}
function submit_svn_field(order_id, warning){
$('#submit_svn_order_id').val(order_id);
$('#submit_svn_warning').html('');
if(warning == 1 ) {
$('#submit_svn_warning').html('Warning: FAMA, Safety or Payment Conditions not met. Please make sure to meet these conditions before generating SVN');
}
$('#submit_svn_field').dialog({
    minHeight:0,
    title: 'Generate SVN',
    modal: true,
    open: function() {
            $('.ui-widget-overlay').on('click', function() {
                $('#submit_svn_field').dialog('close');
            })
    },
});
}
function submit_svn_ajax(){
//close dialog after submit is clicked
$('#submit_svn_field').dialog('close');
//the id of td that is getting edited
var update_td_id = "svn_t_"+$('#submit_svn_order_id').val();
//show progress annimation
$('#'+update_td_id).html($('#sk-spinner').clone().show());
var form = $('#submit_svn_form');
$.ajax({
    type: form.attr('method'),
    url: form.attr('action'),
    data: form.serialize(),
    success: function (data) {
        // remove progress annimation and add new value
        $('#'+update_td_id).text(data.newValue);
        //document.getElementById(update_td_id).setAttribute('ondblclick', "submit_svn_field(" +$('#submit_svn_order_id')");" );
        document.getElementById(update_td_id).removeAttribute('ondblclick');
        if (typeof data.error !== 'undefined') {
            //$('#'+update_td_id).html($('#submit_tickets_ordered_old_val').val());
            swal({
                title: "Error: "+data.error,
                type: "error"
            });
        }
    },
    error: function (data, textStatus, errorThrown) {
        //$('#'+update_td_id).html($('#submit_tickets_ordered_old_val').val());
        swal({
            title: "Server or Network error",
            type: "error"
        });
    },
});
}
</script>
@if (session('message'))
<div class="alert alert-success">
    <strong>Successful Action!</strong><br><br>
    <ul>
        <li>{{ session('message') }}</li>
    </ul>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div id="view_files" class="" style="background-color: #e7eaec; display: none; ">
    
</div>
<div id="upload_files" class="" style="background-color: #e7eaec; display: none; ">
<form action="/upload" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" id='file_order_id' name="order_id" value="0" />
    Select Field
    <div class="form-group"><select class="form-control" name="column_type" >
        <option value="">Select a file type</option>
        <option value="rec_comm_inv_pking">Comm Inv, Pking List</option>
        <option value="original_fcr">Original FCR</option>
        <option value="ctpat_form">CTPAT form</option>
        <option value="telex_release">Telex Release B/L</option>
        </select></div>
    <input type="file" name="file_upload_field[]" multiple />
    <input type="submit" value="Upload" />
</form>
</div>
<div id="submit_svn_field" class="" style="background-color: #e7eaec; display: none; ">
    <form id="submit_svn_form" action="{{ url('/orders/submit_svn_field') }}" method="post"  >{{ csrf_field() }}
        <input type="hidden" name="submit_svn_order_id" id="submit_svn_order_id" value=""  />
        <p id="submit_svn_warning" style="color: red;" ></p>
        <div style="/* margin:  auto; *//* width: 50%; */justify-content: center;display: flex;"><button type="button" onclick="submit_svn_ajax()" class="btn btn-sm">Generate SVN</button></div>
    </form>
</div>
<div id="submit_tickets_ordered_field" style="background-color: #e7eaec; display: none;">
    <form id="submit_tickets_ordered_form" action="{{ url('/orders/submit_tickets_ordered_field') }}" method="post" >{{ csrf_field() }}
        <input type="hidden" name="submit_tickets_ordered_order_id" id="submit_tickets_ordered_order_id" value="" />
        <input type="hidden" name="submit_tickets_ordered_old_val" id="submit_tickets_ordered_old_val" value="" />
        <div class="input-group date">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" name="submit_tickets_ordered_value" id="submit_tickets_ordered_value" class="form-control" value="">
        </div>
        <br>
        <div style="/* margin:  auto; *//* width: 50%; */justify-content: center;display: flex;"><button type="button" onclick="submit_tickets_ordered_ajax()" class="btn btn-sm">Update</button></div>
    </form>
</div>
<div id="submit_salesmen_field" style="background-color: #e7eaec; display: none;">
    <form id="submit_salesmen_form" action="{{ url('/orders/submit_salesmen_field') }}" method="post" >{{ csrf_field() }}
        <input type="hidden" name="sub_salesmen_order_id" id="sub_salesmen_order_id" value="" />
        <input type="hidden" name="sub_salesmen_old_val" id="sub_salesmen_old_val" value="" />
        <input type="hidden" name="sub_salesmen_previous_ids" id="sub_salesmen_previous_ids" value="" />
        <div class="form-group"><select class="form-control" name="sub_salesmen_value[]" id="sub_salesmen_value" multiple="multiple">
            @foreach ($tableData['salesmen'] as $salesman)
            <option value="{{ $salesman->id }}">{{ $salesman->name }}</option>
            @endforeach
            </select></div>
        <br>
        <div style="/* margin:  auto; *//* width: 50%; */justify-content: center;display: flex;"><button type="button" onclick="submit_salesmen_ajax()" class="btn btn-sm">Update</button></div>
    </form>
</div>
<div id="submit_customers_field" style="background-color: #e7eaec; display: none;">
    <form id="submit_customers_form" action="{{ url('/orders/submit_customers_field') }}" method="post" >{{ csrf_field() }}
        <input type="hidden" name="sub_customers_order_id" id="sub_customers_order_id" value="" />
        <input type="hidden" name="sub_customers_old_val" id="sub_customers_old_val" value="" />
        <input type="hidden" name="sub_customers_previous_ids" id="sub_customers_previous_ids" value="" />
        <div class="form-group"><select class="form-control" name="sub_customers_value[]" id="sub_customers_value" multiple="multiple">
            @foreach ($tableData['customers'] as $customer)
            <option value="{{ $customer->id }}">{{ $customer->name }}</option>
            @endforeach
            </select></div>
        <br>
        <div style="/* margin:  auto; *//* width: 50%; */justify-content: center;display: flex;"><button type="button" onclick="submit_customers_ajax()" class="btn btn-sm">Update</button></div>
    </form>
</div>
<div id="submit_licenses_field" style="background-color: #e7eaec; display: none;">
    <form id="submit_licenses_form" action="{{ url('/orders/submit_licenses_field') }}" method="post" >{{ csrf_field() }}
        <input type="hidden" name="sub_licenses_order_id" id="sub_licenses_order_id" value="" />
        <input type="hidden" name="sub_licenses_old_val" id="sub_licenses_old_val" value="" />
        <input type="hidden" name="sub_licenses_previous_ids" id="sub_licenses_previous_ids" value="" />
        <div class="form-group"><select class="form-control" name="sub_licenses_value[]" id="sub_licenses_value" multiple="multiple">
            @foreach ($tableData['licenses'] as $license)
            <option value="{{ $license->id }}">{{ $license->name }}</option>
            @endforeach
            </select></div>
        <br>
        <div style="/* margin:  auto; *//* width: 50%; */justify-content: center;display: flex;"><button type="button" onclick="submit_licenses_ajax()" class="btn btn-sm">Update</button></div>
    </form>
</div>
<div id="submit_product_types_field" style="background-color: #e7eaec; display: none;">
    <form id="submit_product_types_form" action="{{ url('/orders/submit_product_types_field') }}" method="post" >{{ csrf_field() }}
        <input type="hidden" name="sub_product_types_order_id" id="sub_product_types_order_id" value="" />
        <input type="hidden" name="sub_product_types_old_val" id="sub_product_types_old_val" value="" />
        <input type="hidden" name="sub_product_types_previous_ids" id="sub_product_types_previous_ids" value="" />
        <div class="form-group"><select class="form-control" name="sub_product_types_value[]" id="sub_product_types_value" multiple="multiple">
            @foreach ($tableData['product_types'] as $product_type)
            <option value="{{ $product_type->id }}">{{ $product_type->name }}</option>
            @endforeach
            </select></div>
        <br>
        <div style="/* margin:  auto; *//* width: 50%; */justify-content: center;display: flex;"><button type="button" onclick="submit_product_types_ajax()" class="btn btn-sm">Update</button></div>
    </form>
</div>
<div id="submit_product_styles_field" style="background-color: #e7eaec; display: none;">
    <form id="submit_product_styles_form" action="{{ url('/orders/submit_product_styles_field') }}" method="post" >{{ csrf_field() }}
        <input type="hidden" name="sub_product_styles_order_id" id="sub_product_styles_order_id" value="" />
        <input type="hidden" name="sub_product_styles_old_val" id="sub_product_styles_old_val" value="" />
        <input type="hidden" name="sub_product_styles_previous_ids" id="sub_product_styles_previous_ids" value="" />
        <div class="form-group"><select class="form-control" name="sub_product_styles_value[]" id="sub_product_styles_value" multiple="multiple">
            @foreach ($tableData['product_styles'] as $product_style)
            <option value="{{ $product_style->id }}">{{ $product_style->name }}</option>
            @endforeach
            </select></div>
        <br>
        <div style="/* margin:  auto; *//* width: 50%; */justify-content: center;display: flex;"><button type="button" onclick="submit_product_styles_ajax()" class="btn btn-sm">Update</button></div>
    </form>
</div>
<div id="submit_freight_forwarders_field" style="background-color: #e7eaec; display: none;">
    <form id="submit_freight_forwarders_form" action="{{ url('/orders/submit_freight_forwarders_field') }}" method="post" >{{ csrf_field() }}
        <input type="hidden" name="sub_freight_forwarders_order_id" id="sub_freight_forwarders_order_id" value="" />
        <input type="hidden" name="sub_freight_forwarders_old_val" id="sub_freight_forwarders_old_val" value="" />
        <div class="form-group"><select class="form-control" name="sub_freight_forwarders_value" id="sub_freight_forwarders_value">
            <option value="">Select Freight Forwarders</option>
            @foreach ($tableData['freight_forwarders'] as $freight_forwarder)
            <option value="{{ $freight_forwarder->id }}">{{ $freight_forwarder->name }}</option>
            @endforeach
            </select></div>
        <div style="/* margin:  auto; *//* width: 50%; */justify-content: center;display: flex;"><button type="button" onclick="submit_freight_forwarders_ajax()" class="btn btn-sm">Update</button></div>
    </form>
</div>
<div id="submit_ports_field" style="background-color: #e7eaec; display: none;">
    <form id="submit_ports_form" action="{{ url('/orders/submit_ports_field') }}" method="post" >{{ csrf_field() }}
        <input type="hidden" name="sub_ports_order_id" id="sub_ports_order_id" value="" />
        <input type="hidden" name="sub_ports_name" id="sub_ports_name" value="" />
        <input type="hidden" name="sub_ports_old_val" id="sub_ports_old_val" value="" />
        <input type="hidden" name="sub_ports_label" id="sub_ports_label" value="" />
        <div class="form-group"><select class="form-control" name="sub_ports_value" id="sub_ports_value">
            <option value="">Select Ports</option>
            @foreach ($tableData['ports'] as $port)
            <option value="{{ $port->id }}">{{ $port->name }}</option>
            @endforeach
            </select></div>
        <div style="/* margin:  auto; *//* width: 50%; */justify-content: center;display: flex;"><button type="button" onclick="submit_ports_ajax()" class="btn btn-sm">Update</button></div>
    </form>
</div>
<div id="submit_ports_to" style="background-color: #e7eaec; display: none;">
    <form id="submit_portsto_form" action="{{ url('/orders/submit_portsto_field') }}" method="post" >{{ csrf_field() }}
        <input type="hidden" name="sub_ports_order_id" id="sub_portsto_order_id" value="" />
        <input type="hidden" name="sub_ports_name" id="sub_portsto_name" value="" />
        <input type="hidden" name="sub_ports_old_val" id="sub_portsto_old_val" value="" />
        <input type="hidden" name="sub_ports_label" id="sub_portsto_label" value="" />
        <input type="hidden" name="sub_portsto_value_old" id="sub_portsto_value_old" value="" />
        <div class="form-group"><select class="form-control" name="sub_portsto_value" id="sub_portsto_value">
            <option value="">Select Ports</option>
            @foreach ($tableData['ports_to'] as $ports_to)
            <option value="{{ $ports_to->id }}">{{ $ports_to->port_name }}</option>
            @endforeach
            </select></div>
        <div style="/* margin:  auto; *//* width: 50%; */justify-content: center;display: flex;">
        <button type="button" onclick="submit_portsto_ajax()" class="btn btn-sm">Update</button>
    </div>
    </form>
</div>
<div id="submit_export_cos_field" style="background-color: #e7eaec; display: none;">
    <form id="submit_export_cos_form" action="{{ url('/orders/submit_export_cos_field') }}" method="post" >{{ csrf_field() }}
        <input type="hidden" name="sub_export_cos_order_id" id="sub_export_cos_order_id" value="" />
        <input type="hidden" name="sub_export_cos_old_val" id="sub_export_cos_old_val" value="" />
        <input type="hidden" name="sub_export_cos_fama_old_val" id="sub_export_cos_fama_old_val" value="" />
        <input type="hidden" name="sub_export_cos_value_old" id="sub_export_cos_value_old" value="" />
        <div class="form-group"><select class="form-control" name="sub_export_cos_value" id="sub_export_cos_value">
            <option value="">Select Export Co</option>
            @foreach ($tableData['export_cos'] as $export_cos)
            <option value="{{ $export_cos->id }}">{{ $export_cos->name }}</option>
            @endforeach
            </select></div>
        <div style="/* margin:  auto; *//* width: 50%; */justify-content: center;display: flex;"><button type="button" onclick="submit_export_cos_ajax()" class="btn btn-sm">Update</button></div>
    </form>
</div>
<div id="submit_fty_produc_field" style="background-color: #e7eaec; display: none;">
    <form id="submit_fty_produc_form"  action="{{ url('/orders/submit_fty_produc_field') }}" method="post" >{{ csrf_field() }}
        <input type="hidden" name="sub_fty_produc_order_id" id="sub_fty_produc_order_id" value="" />
        <input type="hidden" name="sub_fty_produc_old_val" id="sub_fty_produc_old_val" value="" />
        <input type="hidden" name="sub_fty_produc_vid_old_val" id="sub_fty_produc_vid_old_val" value="" />
        <input type="hidden" name="sub_fty_produc_old_name" id="sub_fty_produc_old_name" value="" />
        <div class="form-group"><select class="form-control" name="sub_fty_produc_value" id="sub_fty_produc_value">
            <option value="">Select Factories</option>
            @foreach ($tableData['factories'] as $factory)
            <option value="{{ $factory->id }}">{{ $factory->name }}</option>
            @endforeach
        </select></div>
        <div style="/* margin:  auto; *//* width: 50%; */justify-content: center;display: flex;"><button type="button" onclick="submit_fty_produc_ajax()" class="btn btn-sm">Update</button></div>
    </form>
</div>
<div id="submit_numeric_fields" style="background-color: #e7eaec; display: none;">
    <form  id="submit_numeric_form"  action="{{ url('/orders/submit_numeric_fields') }}" method="post"  >{{ csrf_field() }}
        <input type="hidden" name="sub_num_order_id" id="sub_num_order_id" value=""  />
        <input type="hidden" name="sub_num_name" id="sub_num_name" value="" />
        <input type="hidden" name="sub_num_label" id="sub_num_label" value="" />
        <input type="hidden" name="sub_num_old_val" id="sub_num_old_val" value="" />
        <input type="number" min="0" name="sub_num_value" id="sub_num_value" value="" class="form-control"/>
        <br>
        <div style="/* margin:  auto; *//* width: 50%; */justify-content: center;display: flex;">
            <button type="button" onclick="submit_num_ajax()" class="btn btn-sm">Update</button></div>
    </form>
</div>
<div id="submit_cbm_fields" style="background-color: #e7eaec; display: none;">
    <form  id="submit_cbm_form" action="{{ url('/orders/submit_cbm_field') }}" method="post"  >{{ csrf_field() }}
        <p id="submit_cbm_warning" style="color: red;" ></p>
        <input type="hidden" name="sub_cbm_order_id" id="sub_cbm_order_id" value=""  />
        <input type="hidden" name="sub_cbm_name" id="sub_cbm_name" value="" />
        <input type="hidden" name="sub_cbm_label" id="sub_cbm_label" value="" />
        <input type="hidden" name="sub_cbm_label" id="sub_cbm_label" value="" />
        <input type="number" min="0" name="sub_cbm_value" id="sub_cbm_value" value="" class="form-control"/>
        <br>
        <div style="/* margin:  auto; *//* width: 50%; */justify-content: center;display: flex;"><button type="button" onclick="submit_cbm_ajax()" class="btn btn-sm">Update</button></div>
    </form>
</div>
<div id="submit_pass_fail_fields" style="background-color: #e7eaec; display: none;">
    <form id="submit_pass_fail_form" action="{{ url('/orders/submit_pass_fail_fields') }}" method="post" >{{ csrf_field() }}
        <input type="hidden" name="sub_pass_fail_order_id" id="sub_pass_fail_order_id" value="" />
        <input type="hidden" name="sub_pass_fail_name" id="sub_pass_fail_name" value="" />
        <input type="hidden" name="sub_pass_fail_label" id="sub_pass_fail_label" value="" />
        <input type="hidden" name="sub_pass_fail_old_val" id="sub_pass_fail_old_val" value="" />
        <div class="form-group"><select class="form-control" name="sub_pass_fail_value" id="sub_pass_fail_value">
            <option value="">Select Pass/Fail</option>
            <option value="1">Pass</option>
            <option value="0">Fail</option>
            </select></div>
        <div style="/* margin:  auto; *//* width: 50%; */justify-content: center;display: flex;"><button type="button" onclick="submit_pass_fail_ajax()" class="btn btn-sm">Update</button></div>
    </form>
</div>
<div id="submit_yes_no_fields" style="background-color: #e7eaec; display: none;">
    <form id="submit_yes_no_form" action="{{ url('/orders/submit_yes_no_fields') }}" method="post" >{{ csrf_field() }}
        <input type="hidden" name="sub_yes_no_order_id" id="sub_yes_no_order_id" value="" />
        <input type="hidden" name="sub_yes_no_name" id="sub_yes_no_name" value="" />
        <input type="hidden" name="sub_yes_no_label" id="sub_yes_no_label" value="" />
        <input type="hidden" name="sub_yes_no_old_val" id="sub_yes_no_old_val" value="" />
        <div class="form-group"><select class="form-control" name="sub_yes_no_value" id="sub_yes_no_value">
            <option value="">Select Yes/No</option>
            <option value="1">Yes</option>
            <option value="0">No</option>
            </select></div>
        <div style="/* margin:  auto; *//* width: 50%; */justify-content: center;display: flex;"><button type="button" onclick="submit_yes_no_ajax()" class="btn btn-sm">Update</button></div>
    </form>
</div>
<div id="submit_date_fields" style="background-color: #e7eaec; display: none;">
    <form id="submit_date_form" action="{{ url('/orders/submit_date_fields') }}" method="post"  >{{ csrf_field() }}
        <input type="hidden" name="sub_date_order_id" id="sub_date_order_id" value=""  />
        <input type="hidden" name="sub_date_field_name" id="sub_date_field_name" value="" />
        <input type="hidden" name="sub_date_field_label" id="sub_date_field_label" value="" />
        <input type="hidden" name="sub_date_field_old_val" id="sub_date_field_old_val" value="" />
        <div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" name="sub_date_field_value" id="sub_date_field_value" class="form-control" value="">
        </div>
            <br>
            <div style="/* margin:  auto; *//* width: 50%; */justify-content: center;display: flex;"><button type="button" onclick="submit_date_ajax()" class="btn btn-sm">Update</button></div>
    </form>
</div>
<div id="submit_text_fields" style="background-color: #e7eaec; display: none;">
    <form id="submit_text_form"  action="{{ url('/orders/submit_text_fields') }}" method="post" >{{ csrf_field() }}
        <input type="hidden" name="sub_txt_order_id" id="sub_txt_order_id" value="" />
        <input type="hidden" name="sub_txt_field_name" id="sub_txt_field_name" value="" />
        <input type="hidden" name="sub_txt_field_label" id="sub_txt_field_label" value="" />
        <input type="hidden" name="sub_txt_field_old_val" id="sub_txt_field_old_val" value="" />
        <input type="text" name="sub_txt_field_value" id="sub_txt_field_value" value="" class="form-control"/>
        <br>
        <div style="/* margin:  auto; *//* width: 50%; */justify-content: center;display: flex;"><button type="button" onclick="submit_text_ajax()" class="btn btn-sm">Update</button></div>
    </form>
</div>
<div id="submit_textarea_fields" class="" style="background-color: #e7eaec; display: none;">
    <form id="submit_textarea_form" action="{{ url('/orders/submit_textarea_fields') }}" method="post"  >{{ csrf_field() }}
        <input type="hidden" name="sub_textarea_order_id" id="sub_textarea_order_id" value=""  />
        <input type="hidden" name="sub_textarea_field_name" id="sub_textarea_field_name" value="" />
        <input type="hidden" name="sub_textarea_field_label" id="sub_textarea_field_label" value="" />
        <input type="hidden" name="sub_textarea_field_old_val" id="sub_textarea_field_old_val" value="" />
        <textarea style="width: 100%;" name="sub_textarea_field_value" id='sub_textarea_field_value'></textarea>
        <div style="/* margin:  auto; *//* width: 50%; */justify-content: center;display: flex;"><button type="button" onclick="submit_textarea_ajax()" class="btn btn-sm">Update</button></div>
    </form>
</div>

{{-- @if($getOrder)  --}}
<div class="row">
    <div class="col-lg-12">
            <h5>Open Orders<small></small></h5>
            <div class="row">
                    
                    <div class="col-sm-6">
                            @if(Auth::user()->hasRole(['Admin','Production']))
                            <a href="{{ url('/orders/create') }}" class="btn btn-xl btn-primary">Start New Import PO</a>
                            @endif
                        </div>
                    <div class="col-sm-4 col-sm-offset-2">
                            <div class="input-group inline" style="width: 100%;margin:10px 0px;">
                                   <label for="searchbox_order" style="position:relative;top:7px;">Search</label>
                                    <input type="text" id="searchbox_order" name ="searchbox_order" placeholder="Search and filter" class=" form-control" style="width:85%; float:right">
                                </div>
                    </div>
                    <div class="clearfix"></div>
            </div>
                
            @if (count($orders)<=0)
                <div class="row"><div class="text-center"><h3>No order records found</h3></div></div>
            @elseif($orders)
            <div>
            @php
             $display = \App\Http\Controllers\PermissionController::checkUserDispalyPermissionsList();
            
            $cant_display = count($display);
            $letras = \App\Http\Controllers\PermissionController::getDisplayLetras($cant_display);
           
            $styleA = "style=background-color:#F2F5A9;";//
            $styleR = "style=background-color:#F58888;";//
            $styleG = "style=background-color:#1ab394;";//
            $styleY = "style=background-color:gray;cursor:not-allowed;";//#eee
            @endphp
<table id="order_table" class="table-striped table-bordered order-column" cellspacing="0" width="100%">
    <thead>

        <tr>
                <th></th>
                @foreach ($letras as $let)
                    <th>{{$let}}</th>
                @endforeach
        </tr>
        <tr>
        <th title="Details / To Closed">Details</th>
            @foreach($display as $dis)
                @switch($dis)
                    @case('order_id')
                        <th title="Order ID">Ord. ID</th>
                    @break
                    @case('po_number')
                        <th title="PO Number">PO #</th>
                    @break
                    @case('comments')
                        <th title="Comments">Comm</th>
                    @break
                    @case('open')
                        <th title="Status">Status</th>
                    @break
                    @case('value')
                        <th title="Value">Value</th>
                    @break
                    @case('fty_produc')
                        <th title="Factory Producting">Fty</th>
                    @break
                    @case('export_cos')
                        <th title="Export Co.">Exp Co.</th>
                    @break
                    @case('fama')
                        <th title="FAMA">Fama</th> 
                    @break
                    @case('prod_style')
                        <th title="Main Product Style(S)">Style#</th> 
                    @break
                    @case('prod_type')
                        <th title="Product Type">Prod Typ</th>
                    @break
                    @case('licenses')
                    <th title="Licenses">Lic</th>
                    @break
                    @case('customers')
                    <th title="Customer">Cust</th>
                    @break
                    @case('customer_po_number')
                    <th title="Customer PO #">Cust PO</th>
                    @break
                    @case('date_placed')
                    <th title="Date Placed">Placed</th>
                    @break
                    @case('qty_ordered')
                    <th title="Qty Ordered">Qty</th> 
                    @break
                    @case('tickets_needed')
                    <th title="Tickets Needed?">Need Tix?</th>
                    @break
                    @case('tickets_ordered')
                    <th title="Tickets ordered?">Tix Ord?</th>
                    @break
                    @case('tickets_received')
                    <th title="Tickets Received?">Tix Recv?</th>
                    @break
                    @case('shipping_from')
                    <th title="Shipping From">Ship Fr</th>
                    @break
                    @case('shipping_to')
                    <th title="Shipping To">Ship To</th>
                    @break
                    @case('estimated_arr')
                    <th title="Estimated Arr">Est Arr</th>
                    @break
                    @case('finish_prod')
                    <th title="Finish Pro">Fin Pro</th>
                    @break
                    @case('customer_cancel')
                    <th title="Customer Cancel Date">Cust Cxl</th>
                    @break
                    @case('cbm')
                    <th title="CBM">CBM</th>
                    @break
                    @case('book_container')
                    <th title="Booking or Container #">Bk/Ctr #</th>
                    @break
                    @case('act_etd_forwarder')
                    <th title="Actual ETD from Forwarder">Actu ETD</th>
                    @break
                    @case('act_eta_forwarder')
                    <th title="Actual ETA from Forwarder">Actu ETA</th>
                    @break
                    @case('safety_test')
                    <th title="Safety Test Needed?">Test Need?</th>
                    @break
                    @case('safety_test_on')
                    <th title="Safety Test Need By">Test By</th>
                    @break
                    @case('fty_prod_safety_test')
                    <th title="Fty Sent for Testing">Sent 4 Test</th>
                    @break
                    @case('safety_test_pass')
                    <th title="Safety Test Passed?">Test Pass?</th>
                    @break
                    @case('david_ins_result')
                    <th title="David Inspection Results">Insp Reslt</th>
                    @break
                    @case('david_ins_comment')
                    <th title="David Inspection comments">Insp Cmmt</th> 
                    @break
                    @case('svn_no')
                    <th title="SVN#">SVN#</th>
                    @break
                    @case('freight_forwarder')
                    <th  title="Freight Forwarder">Forwdr</th> 
                    @break
                    @case('rec_comm_inv_pking')
                    <th title="Did we recv our Comm Inv/Pking List?">Edge PL/Inv</th>
                    @break
                    @case('rec_comm_inv_pking_home')
                    <th title="Did we recv Comm Inv/Pking List for AtHome?">Athm PL/Inv</th>
                    @break
                    @case('original_fcr')
                    <th title="Did we Recv Orig FCR?">FCR?</th>
                    @break
                    @case('ctpat_form')
                    <th title="Did we Recv CTPAT?">CTPAT?</th>
                    @break
                    @case('telex_release')
                    <th title="is B/L Telexed Released">Telex Rls</th>
                    @break
                    @case('pro_photo')
                    <th title="Did we Recv Prof Photography?">Photo</th>
                    @break
                    @case('okay_to_pay')
                    <th title="OK to Pay?">Ok Pay?</th>
                    @break
                    @case('did_we_pay')
                    <th title="Did we Pay Fty?">Paid?</th>
                    @break
                    @case('send_diann')
                    <th title="What date we sent to G&A(Diann)">G&A</th>
                    @break
                    @case('receive_whse')
                    <th title="Received in warehouse?">Whse?</th>
                    @break
                    @case('salesmen')
                    <th title="Salesman">Salesman</th>
                    @break
                    @case('files')
                    <th title="Uploads doc for this order">Files</th>
                    @break
                    @default
                        <th>no se<th>
                    @break
                @endswitch
            @endforeach         
        </tr>
    </thead>
    <tbody>
        @foreach ($orders as $order)
        <!--Licensing Conditions or FAMA Condition -->
            <tr>
             @php  $Lic_Con = $Lic_Title = null;
       
             if($order->ExCFama==0 and (strpos($order->licensesFama, '1') !== false)) {
                 $Lic_Con = true;
                 $Lic_Title="Factory unapproved to produce Disney product.";}
             @endphp
             <!-- Delivery Condition -->
             @php $del_Con = null;
             if( ($order->act_eta_forwarder == null and $order->customer_cancel != null and strtotime($order->estimated_arr) > strtotime($order->customer_cancel)) ) { $del_Con = true; $del_Title="This date is before Estimated Arr";} 
             elseif($order->act_eta_forwarder != null and $order->customer_cancel != null and strtotime($order->act_eta_forwarder) > strtotime($order->customer_cancel) ) { $del_Con = true;  $del_Title="This date is before Actual ETA";} 
             @endphp
             <!-- Safety Test Conditions -->
             @php
             $safety_Title1 = $safety_Con1 = $safety_Title2 = $safety_Con2 = null;
             $date1=date_create($order->finish_prod);
             $date2=date_create(date('Y-m-d H:i:s'));
             $diff=date_diff($date1,$date2);
             if ($order->safety_test == 1 and $order->fty_prod_safety_test == null and $diff->format('%a') <= 21){$safety_Con1=true;$safety_Title1= "'Fty Submitted Prod' not entered and Safety Test is 21 days before Finish Prod";}
             if ($order->safety_test == 1 and ($order->safety_test_pass==null or $order->safety_test_pass==0) and $diff->format('%a') <= 10){$safety_Con2=true;$safety_Title2= "Safety Test is not Passed or is 10 days before Finish Prod";}
             @endphp
             <!-- CBM Conditions -->
             @php
             $CBM_Title = $CBM_Con = null;
             if ($safety_Con1==true or $safety_Con2 == true or $Lic_Con == true){$CBM_Con=true;$CBM_Title= "FAMA or Safety conditions are not met";}
             @endphp
             <!-- Payment Conditions -->
             @php 
             $OKPay_Title = $OKPay_Con = null;
             if (\App\Http\Controllers\TriggerEventsController::validateOkayToPay($order->ipo, $order->customersNames)) {$OKPay_Title = "You can now fill this field"; $OKPay_Con=true;}
             @endphp
             <!-- SVN Conditions -->
             @php 
             $SVN_Title = 'FAMA, Safety or Payment Conditions not meta';
             @endphp
             <!-- ETD Conditions -->
             @php 
             $d1=date_create(date('Y-m-d H:i:s'));//now
             if (!isset($order->act_etd_forwarder)){
                 $d2=date_create($order->finish_prod );
                 $df=date_diff($d1,$d2);
                 $days_before_etd = $df->format('%a');//edt was this many days before today.
             }
             elseif(isset($order->act_etd_forwarder)){
                 $d2=date_create($order->act_etd_forwarder);
                 $df=date_diff($d1,$d2);
                 $days_before_etd = $df->format('%a');//edt was this many days before today.
             }
             @endphp
           <td style="text-align: center;">
                <a href="{{ url('/orders/'.$order->ipo)}}" class="btn-xs btn-primary">Details</a>
            </td>
        @foreach($display as $dis)
            
            @switch($dis)
                @case('order_id')
                    <td>{{ $order->ipo }}</td>
                    @break
                @case('po_number')
                    <td id="po_number_t_{{ $order->ipo }}" class="po_number_t_{{ $order->ipo }}" ondblclick="submit_text_fields({{ $order->ipo }}, 'po_number', 'PO Number','{{ $order->po_number }}');">{{ $order->po_number }}</td>
                    @break
                @case('comments')
                    <td id="comments_t_{{ $order->ipo }}" class="td_ellipsis" title="{{ $order->comments }}" ondblclick="submit_textarea_fields({{ $order->ipo }}, 'comments', 'Comments', '{{ $order->comments }}')" >{{ $order->comments }}</td> 
                    @break
                @case('open')
                    <td ondblclick="">@if($order->open==1) <span class=" label label-primary">OPEN</span> @elseif($order->open==0) <span class=" label label-danger">CLOSED</span> @endif</td>
                    @break
                @case('value')
                    <td id="value_t_{{ $order->ipo }}" ondblclick="submit_numeric_fields({{ $order->ipo }}, 'value', 'Value $', '{{ $order->value }}')">$ {{ number_format($order->value) }}</td>
                    @break
                @case('fty_produc')
                    <td id="fty_produc_t_{{ $order->ipo }}" @if($order->ftyName )  title="{{ $order->ftyVId }}" @endif ondblclick="submit_fty_produc_field({{ $order->ipo }}, '{{ $order->fty_produc }}', '{{ $order->ftyVId }}','{{$order->ftyName}}')" @if(!$order->ftyName ) {{ $styleA }} title="Field required" @endif>{{ $order->ftyName }}</td>
                    @break
                @case('export_cos')
                <td id="export_cos_t_{{ $order->ipo }}" ondblclick="submit_export_cos_field({{ $order->ipo }}, '{{ $order->ExCName }}', '{{ $order->ExCFama }}', '{{ $order->export_cos }}')" @if($Lic_Con) {{ $styleR }} title="{{$Lic_Title}}" @endif @if(!$order->ExCName ) {{ $styleA }} title="Field required" @endif>{{ $order->ExCName }}</td> 
                @break
                @case('fama')
                <td id="export_cos_fema_{{ $order->ipo }}">@if($order->ExCFama===1) Yes @elseif($order->ExCFama===0) No @endif</td>
                @break
                @case('prod_style')
                <td id="product_styles_t_{{ $order->ipo }}" ondblclick="submit_product_styles_field({{ $order->ipo }}, '{{ $order->prod_styleNames }}', '{{ $order->prod_styleId }}')">{{ $order->prod_styleNames }}</td>
                @break
                @case('prod_type')
                <td id="product_types_t_{{ $order->ipo }}" ondblclick="submit_product_types_field({{ $order->ipo }}, '{{ $order->prod_typesNames }}', '{{ $order->prod_typesId }}')">{{ $order->prod_typesNames }}</td>
                @break
                @case('licenses')
                <td id="licenses_t_{{ $order->ipo }}" ondblclick="submit_licenses_field({{ $order->ipo }}, '{{ $order->licensesNames }}', '{{ $order->licensesId }}')" @if(!$order->licensesNames ) {{ $styleA }} title="Field required" @endif>{{ $order->licensesNames }}</td> 
                @break
                @case('customers')
                <td id="customers_t_{{ $order->ipo }}" ondblclick="submit_customers_field({{ $order->ipo }}, '{{ $order->customersNames }}', '{{ $order->customersId }}')">{{ $order->customersNames }}</td>
                @break
                @case('customer_po_number')
                <td id="customer_po_number_t_{{ $order->ipo }}" ondblclick="submit_text_fields({{ $order->ipo }}, 'customer_po_number', 'Customer PO #', '{{ $order->customer_po_number }}');">{{ $order->customer_po_number }}</td>
                @break
                @case('date_placed')
                <td id="date_placed_t_{{ $order->ipo }}" ondblclick="submit_date_fields({{ $order->ipo }}, 'date_placed', 'Date Placed', '{{ $order->date_placed }}')">
                        @php
                            if($order->date_placed){
                            $dater=date_create($order->date_placed);
                            echo date_format($dater,"M d Y");
                            }
                        @endphp
                    </td> 
                @break
                @case('qty_ordered')
                <td id="qty_ordered_t_{{ $order->ipo }}" ondblclick="submit_numeric_fields({{ $order->ipo }}, 'qty_ordered', 'Qty Ordered', '{{ $order->qty_ordered }}')">{{ $order->qty_ordered }}</td>
                @break
                @case('tickets_needed')
                <td id="tickets_needed_t_{{ $order->ipo }}" ondblclick="submit_yes_no_fields({{ $order->ipo }}, 'tickets_needed', 'Tickets Needed?', '{{ $order->tickets_needed }}')">@if($order->tickets_needed===1) Yes @elseif($order->tickets_needed===0) No @endif</td>
                @break
                @case('tickets_ordered')
                <td id="tickets_ordered_t_{{ $order->ipo }}"  ondblclick="submit_tickets_ordered_field({{ $order->ipo }}, '{{ $order->tickets_orderedDates }}')">{{ $order->tickets_orderedDates }}</td>
                @break
                @case('tickets_received')
                <td id="tickets_received_t_{{ $order->ipo }}" ondblclick="submit_date_fields({{ $order->ipo }}, 'tickets_received', 'Tickets Received', '{{ $order->tickets_received }}')">
                        @php
                        if($order->tickets_received){
                            $dater=date_create($order->tickets_received);
                            echo date_format($dater,"M d Y");
                        }
                        @endphp
                    </td> 
                @break
                @case('shipping_from')
                <td id="shipping_from_t_{{ $order->ipo }}" ondblclick="submit_ports_field({{ $order->ipo }}, 'shipping_from', 'Shipping From', '{{ $order->port_ShipF_Name }}', '{{ $order->shipping_from }}')">{{ $order->port_ShipF_Name }}</td>
                @break
                @case('shipping_to')
                <td id="shipping_to_t_{{ $order->ipo }}" ondblclick="submit_ports_to({{ $order->ipo }}, 'shipping_to', 'Shipping To', '{{ $order->port_ShipT_Name }}', '{{ $order->shipping_to }}')">{{ $order->port_ShipT_Name }}</td>
                @break
                @case('estimated_arr')
                <td id="estimated_arr_t_{{ $order->ipo }}">
                        @php
                        if($order->estimated_arr){
                            $dater=date_create($order->estimated_arr);
                            echo date_format($dater,"M d Y");
                        }
                        @endphp
                    </td>
                @break
                @case('finish_prod')
                <td id="finish_prod_t_{{ $order->ipo }}" ondblclick="submit_date_fields({{ $order->ipo }}, 'finish_prod', 'Finish Product', '{{ $order->finish_prod }}')">
                        @php
                        if($order->finish_prod){
                            $dater=date_create($order->finish_prod);
                            echo date_format($dater,"M d Y");
                        }
                        @endphp
                    </td>
                @break
                @case('customer_cancel')
                <td id="customer_cancel_t_{{ $order->ipo }}" ondblclick="submit_date_fields({{ $order->ipo }}, 'customer_cancel', 'Customer Cancel', '{{ $order->customer_cancel }}')" @if( $del_Con )  {{ $styleA }} title="{{$del_Title}}" @endif>
                        @php
                        if($order->customer_cancel){
                            $dater=date_create($order->customer_cancel);
                            echo date_format($dater,"M d Y");
                        }
                        @endphp
                    </td>
                @break
                @case('cbm')
                <td id="cbm_t_{{ $order->ipo }}" @if( $CBM_Con ) {{ $styleA }} title="{{$CBM_Title}}" ondblclick="submit_cbm_fields({{ $order->ipo }}, 'cbm', 'CBM', '{{ $order->cbm }}', 1)" @else ondblclick="submit_cbm_fields({{ $order->ipo }}, 'cbm', 'CBM', '{{ $order->cbm }}', 0)" @endif>{{ $order->cbm }}</td>
                @break
                @case('book_container')
                <td id="book_container_t_{{ $order->ipo }}" ondblclick="submit_text_fields({{ $order->ipo }}, 'book_container', 'Booking or Container #', '{{ $order->book_container }}');">{{ $order->book_container }}</td>
                @break
                @case('act_etd_forwarder')
                <td id="act_etd_forwarder_t_{{ $order->ipo }}" ondblclick="submit_date_fields({{ $order->ipo }}, 'act_etd_forwarder', 'Actual ETD from Forwarder', '{{ $order->act_etd_forwarder }}')">
                        @php
                            if($order->act_etd_forwarder){
                                $dater=date_create($order->act_etd_forwarder);
                                echo date_format($dater,"M d Y");
                            }
                        @endphp
                    </td> 
                @break
                @case('act_eta_forwarder')
                <td id="act_eta_forwarder_t_{{ $order->ipo }}" ondblclick="submit_date_fields({{ $order->ipo }}, 'act_eta_forwarder', 'Actual ETA from Forwarder', '{{ $order->act_eta_forwarder }}')">
                        @php
                            if($order->act_eta_forwarder){
                                $dater=date_create($order->act_eta_forwarder);
                                echo date_format($dater,"M d Y");
                            }
                        @endphp
                    </td> 
                @break
                @case('safety_test')
                <td id="safety_test_t_{{ $order->ipo }}" ondblclick="submit_yes_no_fields({{ $order->ipo }}, 'safety_test', 'Safety Test Needed?', '{{ $order->safety_test }}')" @if( $safety_Con1 ) {{ $styleA }} title="{{$safety_Title1}}" @endif @if($order->safety_test ===null ) {{ $styleA }} title="Field required" @endif>@if($order->safety_test===1) Yes @elseif($order->safety_test===0) No @endif</td> 
               
                @break
                @case('safety_test_on')
                <td id="safety_test_on_t_{{ $order->ipo }}" @if($order->safety_test===0) {{ $styleY }} @else ondblclick="submit_date_fields({{ $order->ipo }}, 'safety_test_on', 'Safety Test Need On', '{{ $order->safety_test_on }}')" @endif >
                        @php
                        if($order->safety_test_on){
                            $dater=date_create($order->safety_test_on);
                            echo date_format($dater,"M d Y");
                        }
                        @endphp
                    </td> 
                @break
                @case('fty_prod_safety_test')
                <td id="fty_prod_safety_test_t_{{ $order->ipo }}"  @if($order->safety_test===0) {{ $styleY }} @else ondblclick="submit_date_fields({{ $order->ipo }}, 'fty_prod_safety_test', 'Fty Submitted Prod for Safety Test', '{{ $order->fty_prod_safety_test }}')" @endif >
                        @php
                        if($order->fty_prod_safety_test){
                            $dater=date_create($order->fty_prod_safety_test);
                            echo date_format($dater,"M d Y");
                        }
                        @endphp
                    </td> 
                @break
                @case('safety_test_pass')
                <td id="safety_test_pass_t_{{ $order->ipo }}"  @if($order->safety_test===0) {{ $styleY }} @endif ondblclick="submit_yes_no_fields({{ $order->ipo }}, 'safety_test_pass', 'Safety Test Passed?', '{{ $order->safety_test_pass }}')" @if( $safety_Con2 ) {{ $styleA }} title="{{$safety_Title2}}" @endif>@if($order->safety_test_pass===1) Yes @elseif($order->safety_test_pass===0) No @endif</td>
                @break
                @case('david_ins_result')
                <td id="david_ins_result_t_{{ $order->ipo }}" ondblclick="submit_pass_fail_fields({{ $order->ipo }}, 'david_ins_result', 'David Inspection Results', '{{ $order->david_ins_result }}')">@if($order->david_ins_result===1) Pass @elseif($order->david_ins_result===0) Fail @endif</td>
                @break
                @case('david_ins_comment')
                <td id="david_ins_comment_t_{{ $order->ipo }}" class="td_ellipsis" title="{{ $order->david_ins_comment }}" ondblclick="submit_textarea_fields({{ $order->ipo }}, 'david_ins_comment', 'David Inspection comments', '{{ $order->david_ins_comment }}')"  >{{ $order->david_ins_comment }}</td>
                @break
                @case('svn_no')
                <td id="svn_t_{{ $order->ipo }}" @if($order->svn_no==null and ($CBM_Con or !$OKPay_Con)) ondblclick="submit_svn_field({{ $order->ipo }},1)" {{ $styleA }} title="{{ $SVN_Title }}" @elseif($CBM_Con or !$OKPay_Con) {{ $styleA }} title="{{ $SVN_Title }}" @elseif ($order->svn_no==null) ondblclick="submit_svn_field({{ $order->ipo }}, null)" {{ $styleG }} @endif>{{ $order->svn_no }}</td>
                @break
                @case('freight_forwarder')
                <td id="freight_forwarders_t_{{ $order->ipo }}" ondblclick="submit_freight_forwarders_field({{ $order->ipo }}, '{{ $order->FFName }}', '{{ $order->freight_forwarder }}')">{{ $order->FFName }}</td> 
                @break
                @case('rec_comm_inv_pking')
                <td id="rec_comm_inv_pking_t_{{ $order->ipo }}" {{($order->rec_comm_inv_pking)?' ':' '. $styleR }} ondblclick="submit_date_fields({{ $order->ipo }}, 'rec_comm_inv_pking', 'Did we receive Comm Inv& Pking List for us?', '{{ $order->rec_comm_inv_pking }}')">@php
                            if($order->rec_comm_inv_pking){
                                $dater=date_create($order->rec_comm_inv_pking);
                                echo date_format($dater,"M d Y");
                            }
                        @endphp</td> 
                @break
                @case('rec_comm_inv_pking_home')
                <td id="rec_comm_inv_pking_home_t_{{ $order->ipo }}" @if ( strpos($order->customersNames, 'tHome') == false) {{ $styleY }} title="Only for AtHome" @else  {{($order->rec_comm_inv_pking_home)?' ':' '. $styleR }} ondblclick="submit_date_fields({{ $order->ipo }}, 'rec_comm_inv_pking_home', 'Did we receive Comm Inv& Pking List for AtHome?', '{{ $order->rec_comm_inv_pking_home }}')" @endif>@php
                            if($order->rec_comm_inv_pking_home){
                                $dater=date_create($order->rec_comm_inv_pking_home);
                                echo date_format($dater,"M d Y");
                            }
                        @endphp</td> 
                @break
                @case('original_fcr')
                <td id="original_fcr_t_{{ $order->ipo }}" @if ( strpos($order->customersNames, 'obby Lobby') == false and strpos($order->customersNames, 'tHome') == false) {{ $styleY }} title="Only for Hobby Lobby or AtHome" @else {{($order->original_fcr)?' ':' '. $styleR }} ondblclick="submit_date_fields({{ $order->ipo }}, 'original_fcr', 'Did we Receive Original FCR?', '{{ $order->original_fcr }}')" @endif>@php
                        if($order->original_fcr){
                            $dater=date_create($order->original_fcr);
                            echo date_format($dater,"M d Y");
                        }
                        @endphp</td> 
                @break
                @case('ctpat_form')
                    <td id="ctpat_form_t_{{ $order->ipo }}" @if ( strpos($order->customersNames, 'obby Lobby') == false) {{ $styleY }} title="Only for Hobby Lobby" @else {{($order->ctpat_form)?' ':' '. $styleR }} ondblclick="submit_date_fields({{ $order->ipo }}, 'ctpat_form', 'Did we Receive CTPAT form?', '{{ $order->ctpat_form }}')" @endif>@php
                        if($order->ctpat_form){
                            $dater=date_create($order->ctpat_form);
                            echo date_format($dater,"M d Y");
                        }
                        @endphp</td> 
                @break
                @case('telex_release')
                <td id="telex_release_t_{{ $order->ipo }}" {{($order->telex_release)?' ':' '. $styleR }} ondblclick="submit_date_fields({{ $order->ipo }}, 'telex_release', 'Did we receive Telex Release B/L?', '{{ $order->telex_release }}')">@php
                            if($order->telex_release){
                                $dater=date_create($order->telex_release);
                                echo date_format($dater,"M d Y");
                            }
                        @endphp</td>
                @break
                @case('pro_photo')
                    <td id="pro_photo_t_{{ $order->ipo }}" ondblclick="submit_yes_no_fields({{ $order->ipo }}, 'pro_photo', 'Receive Professional Photography?', '{{ $order->pro_photo }}')">@if($order->pro_photo===1) Yes @elseif($order->pro_photo===0) No @endif</td>
                @break
                @case('okay_to_pay')
                <td id="okay_to_pay_t_{{ $order->ipo }}" ondblclick="submit_date_fields({{ $order->ipo }}, 'okay_to_pay', 'OK to Pay?', '{{ $order->okay_to_pay }}')">
                        @php
                        if($order->okay_to_pay){
                            $dater=date_create($order->okay_to_pay);
                            echo date_format($dater,"M d Y");
                        }
                         @endphp
                    </td>
                @break
                @case('did_we_pay')
                <td id="did_we_pay_t_{{ $order->ipo }}" ondblclick="submit_date_fields({{ $order->ipo }}, 'did_we_pay', 'Did we Pay?', '{{ $order->did_we_pay }}')">
                        @php
                        if($order->did_we_pay){
                            $dater=date_create($order->did_we_pay);
                            echo date_format($dater,"M d Y");
                        }
                         @endphp
                    </td> 
                @break
                @case('send_diann')
                <td id="send_diann_t_{{ $order->ipo }}" ondblclick="submit_date_fields({{ $order->ipo }}, 'send_diann', 'What date we sent to Diann', '{{ $order->send_diann }}')">
                        @php
                        if($order->send_diann){
                            $dater=date_create($order->send_diann);
                            echo date_format($dater,"M d Y");
                        }
                    @endphp
                    </td> 
                @break
                @case('receive_whse')
                <td id="receive_whse_t_{{ $order->ipo }}" ondblclick="submit_yes_no_fields({{ $order->ipo }}, 'receive_whse', 'Did we receive in Whse?', '{{ $order->receive_whse }}')">@if($order->receive_whse===1) Yes @elseif($order->receive_whse===0) No @endif</td>
                @break
                @case('salesmen')
                <td id="salesmen_t_{{ $order->ipo }}" ondblclick="submit_salesmen_field({{ $order->ipo }}, '{{ $order->salesmenNames }}', '{{ $order->salesmenId }}')">{{ $order->salesmenNames }}</td>
                @break
                @case('files')
                <td id="files_t_{{ $order->ipo }}" >     <a onclick="view_files({{ $order->ipo }})">View</a> / <br/><a onclick="upload_files({{ $order->ipo }})">Upload</a></td>
                @break    
                
                @default
                @break
            @endswitch
        @endforeach
                    
       </tr>
       @endforeach
    </tbody>
  
</table>
                </div>
                @endif
    </div>
</div>
{{-- @endif --}}
<!-- spinner status div --> 
<div id='sk-spinner' class="sk-spinner sk-spinner-wave" style="display: none;">
    <div class="sk-rect1"></div>
    <div class="sk-rect2"></div>
    <div class="sk-rect3"></div>
    <div class="sk-rect4"></div>
    <div class="sk-rect5"></div>
</div>
@endsection
@push('scripts')
<script>
        $(document).on('submit','form#submit_numeric_form',function(e){ 
            e.preventDefault(); 
            submit_num_ajax(); 
        }); 
        $(document).on('submit','form#submit_svn_form',function(e){ 
            e.preventDefault(); 
            submit_svn_ajax(); 
        }); 
        $(document).on('submit','form#submit_tickets_ordered_form',function(e){ 
            e.preventDefault(); 
            submit_tickets_ordered_ajax(); 
        }); 
        $(document).on('submit','form#submit_salesmen_form',function(e){ 
            e.preventDefault(); 
            submit_salesmen_ajax(); 
        });
        $(document).on('submit','form#submit_customers_form',function(e){ 
            e.preventDefault(); 
            submit_customers_ajax(); 
        }); 
        $(document).on('submit','form#submit_product_types_form',function(e){ 
            e.preventDefault(); 
            submit_product_types_ajax(); 
        });
        $(document).on('submit','form#submit_product_styles_form',function(e){ 
            e.preventDefault(); 
            submit_product_styles_ajax(); 
        }); 
        $(document).on('submit','form#submit_licenses_form',function(e){ 
            e.preventDefault(); 
            submit_licenses_ajax(); 
        }); 
        $(document).on('submit','form#submit_ports_form',function(e){ 
            e.preventDefault(); 
            submit_ports_ajax(); 
        }); 
        $(document).on('submit','form#submit_portsto_form',function(e){ 
            e.preventDefault(); 
            submit_portsto_ajax(); 
        }); 
        $(document).on('submit','form#submit_freight_forwarders_form',function(e){ 
            e.preventDefault(); 
            submit_freight_forwarders_ajax(); 
        }); 
        $(document).on('submit','form#submit_export_cos_form',function(e){ 
            e.preventDefault(); 
            submit_export_cos_ajax(); 
        }); 
        $(document).on('submit','form#submit_fty_produc_form',function(e){ 
            e.preventDefault(); 
            submit_fty_produc_ajax(); 
        }); 
        $(document).on('submit','form#submit_cbm_form',function(e){ 
            e.preventDefault(); 
            submit_cbm_ajax(); 
        }); 
        $(document).on('submit','form#submit_pass_fail_form',function(e){ 
            e.preventDefault(); 
            submit_pass_fail_ajax(); 
        }); 
        $(document).on('submit','form#submit_yes_no_form',function(e){ 
            e.preventDefault(); 
            submit_yes_no_ajax(); 
        }); 
    
        $(document).on('submit','form#submit_date_form',function(e){ 
            e.preventDefault(); 
            submit_date_ajax(); 
        }); 
        $(document).on('submit','form#submit_text_form',function(e){ 
            e.preventDefault(); 
            submit_text_ajax(); 
        }); 
        $(document).on('submit','form#submit_textarea_form',function(e){ 
            e.preventDefault(); 
            submit_textarea_ajax(); 
        }); 
   
    
</script>
@endpush