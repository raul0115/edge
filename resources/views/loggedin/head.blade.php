<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <!-- Morris -->
    <link href="{{ asset('css/plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
    
    <link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/jQueryUI/jquery-ui.css') }}" rel="stylesheet">
    
    @if(Request::path() == 'orders/open' or Request::path() == 'orders/closed')
    <!-- Select 2-->
    <link href="{{ asset('js/plugins/dataTables/jquery.dataTables.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('js/plugins/dataTables/dataTables.bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('js/plugins/dataTables/fixedColumns.bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('js/plugins/dataTables/colReorder.bootstrap.css') }}" rel="stylesheet" />
    @endif
    <link href="{{ asset('css/plugins/select2/select2.min.css') }}" rel="stylesheet" />
    @stack('styles')
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/our.css?v=1') }}" rel="stylesheet">
    @stack('styles2')

</head>