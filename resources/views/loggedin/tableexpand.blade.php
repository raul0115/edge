@extends('loggedin.layout')

<script type="text/javascript">
function display_addnew_div(tableID){
$(tableID).toggle();
}
function display_edit_div(tableID, editID, editName){
$(tableID).toggle();
$(tableID+ "_name").val(editName);
$(tableID+ "_id").val(editID);
}
function display_edit_factory_div(tableID, editID, editName, vendor_id) {
$(tableID).toggle();
$(tableID+ "_name").val(editName);
$(tableID+ "_id").val(editID);
$(tableID+ "_vendor_id").val(vendor_id);   
}
function display_edit_ecportco_div(tableID, editID, editName, fama) {
$(tableID).toggle();
$(tableID+ "_name").val(editName);
$(tableID+ "_id").val(editID);
$(tableID+ "_fama").val(fama).change();
}
</script>
@section('content')
@if (session('message'))
<div class="alert alert-success">
    <strong>Successful Action!</strong><br><br>
    <ul>
        <li>{{ session('message') }}</li>
    </ul>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif



    <div class="row">
        <div class="col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Factories Table</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content" 
                @if ($table != 'factories') 
                style='display: none;'
                @endif >
                <div class="row">
                    <div id='table_factories_edit' class="col-sm-12" style="display:none;" >
                        <form class="form-inline" role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="factories">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_factories">
                            <input type="hidden"  id="table_factories_edit_id" name="edit_id" value="">
                            <div class="form-group"><input id="table_factories_edit_name" name='name' type="text" value="" class="form-control" ></div>
                            <div class="form-group"><input id="table_factories_edit_vendor_id" name='vendor_id' type="text" value="" class="form-control" ></div>
                            <button class="btn btn-primary pull-right" type="submit"><strong>Edit</strong></button>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_factories_addnew' class="col-sm-12" style="display:none;" >
                        <form class="form-inline" role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="factories">{{ csrf_field() }}<input type="hidden" name="action" value="add_factories">
                            <div class="form-group"><input name='name' type="text" placeholder="Enter Factory Name" class="form-control" ></div>
                            <div class="form-group"><input name='vendor_id' type="text" placeholder="Enter Vendor ID" class="form-control" ></div>
                            <button class="btn btn-primary pull-right" type="submit"><strong>Add New</strong></button>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_factories_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='factories'> 
                                <input type="search" name="search_factories" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Vendor ID</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($factories as $factory)
                            <tr>
                                <td>{{ $factory->id }}</td>
                                <td>{{ $factory->name }}</td>
                                <td>{{ $factory->vendor_id }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_factory_div('#table_factories_edit', {{ $factory->id }} , '{{ $factory->name }}', '{{ $factory->vendor_id }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($factory->active == 1)
                                    <form id="delete_factory{{ $factory->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_factory_id" value="{{ $factory->id }}"><input type="hidden" name="table" value="factories"><input type="hidden" name="action" value="delete_factories">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Delete</button>
                                    </form>
                                    @elseif ($factory->active == 0)
                                    <form id="activate_factory{{ $factory->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_factory_id" value="{{ $factory->id }}"><input type="hidden" name="table" value="factories"><input type="hidden" name="action" value="activate_factories">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $factories->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>    
            
            
    <div class="col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Export Co Table</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content" 
                @if ($table != 'export_cos') 
                style='display: none;'
                @endif
                 {{$table}}>
                <div class="row">
                    <div id='table_export_cos_edit' class="col-sm-12" style="display:none;" >
                        <form class="form-inline" role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="export_cos">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_export_cos">
                            <input type="hidden"  id="table_export_cos_edit_id" name="edit_id" value="">
                            <div class="form-group"><input id="table_export_cos_edit_name" name='name' type="text" value="" class="form-control" ></div>
                            <div class="form-group"><select id='table_export_cos_edit_fama'  name='fama' class="input-sm form-control input-s-sm inline" style="height: 34px;"><option value="1">Yes</option><option value="0">No</option></select></div>
                            <button class="btn btn-primary pull-right" type="submit"><strong>Edit</strong></button>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_export_cos_addnew' class="col-sm-12" style="display:none;" >
                        <form class="form-inline" role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="export_cos">{{ csrf_field() }}<input type="hidden" name="action" value="add_export_cos">
                            <div class="form-group"><input name='name' type="text" placeholder="Enter Export Name" class="form-control" ></div>
                            <div class="form-group"><select  name='fama' class="input-sm form-control input-s-sm inline" style="height: 34px;"><option value="1">Yes</option><option value="0">No</option></select></div>
                            <button class="btn btn-primary pull-right" type="submit"><strong>Add New</strong></button>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_export_cos_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='export_cos'> 
                                <input type="search" name="search_export_cos" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>FAMA</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($export_cos as $export_co)
                            <tr>
                                <td>{{ $export_co->id }}</td>
                                <td>{{ $export_co->name }}</td>
                                <td>@if ($export_co->fama == 1) Yes @elseif ($export_co->fama == 0) No @endif </td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_ecportco_div('#table_export_cos_edit', {{ $export_co->id }} , '{{ $export_co->name }}', {{ $export_co->fama }} );">Edit</button>
                                </td>
                                <td>
                                    @if ($export_co->active == 1)
                                    <form id="delete_export_co{{ $export_co->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_export_co_id" value="{{ $export_co->id }}"><input type="hidden" name="table" value="export_cos"><input type="hidden" name="action" value="delete_export_cos">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Delete</button>
                                    </form>
                                    @elseif ($export_co->active == 0)
                                    <form id="activate_export_co{{ $export_co->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_export_co_id" value="{{ $export_co->id }}"><input type="hidden" name="table" value="export_cos"><input type="hidden" name="action" value="activate_export_cos">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $export_cos->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>        
    
            
    </div>




<div class="row">
    
    
    
    <div class="col-lg-4">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Customers Table</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content" 
                @if ($table != 'customers') 
                style='display: none;'
                @endif
                 {{$table}}>
                <div class="row">
                    <div id='table_customers_edit' class="col-sm-12" style="display:none;" >
                        <form class="form-inline" role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="customers">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_customers">
                            <input type="hidden"  id="table_customers_edit_id" name="edit_id" value="">
                            <div class="form-group"><input id="table_customers_edit_name" name='name' type="text" value="" class="form-control" ></div>
                            <button class="btn btn-primary pull-right" type="submit"><strong>Edit</strong></button>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_customers_addnew' class="col-sm-12" style="display:none;" >
                        <form class="form-inline" role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="customers">{{ csrf_field() }}<input type="hidden" name="action" value="add_customers">
                            <div class="form-group"><input name='name' type="text" placeholder="Enter Customer Name" class="form-control" ></div>
                            <button class="btn btn-primary pull-right" type="submit"><strong>Add New</strong></button>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_customers_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='customers'> 
                                <input type="search" name="search_customers" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($customers as $customer)
                            <tr>
                                <td>{{ $customer->id }}</td>
                                <td>{{ $customer->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_div('#table_customers_edit', {{ $customer->id }} , '{{ $customer->name }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($customer->active == 1)
                                    <form id="delete_customer{{ $customer->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_customer_id" value="{{ $customer->id }}"><input type="hidden" name="table" value="customers"><input type="hidden" name="action" value="delete_customers">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Delete</button>
                                    </form>
                                    @elseif ($customer->active == 0)
                                    <form id="activate_customer{{ $customer->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_customer_id" value="{{ $customer->id }}"><input type="hidden" name="table" value="customers"><input type="hidden" name="action" value="activate_customers">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $customers->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
    
    
    
    
    <div class="col-lg-4">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Ports Table</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content" 
                @if ($table != 'ports') 
                style='display: none;'
                @endif
                 {{$table}}>
                <div class="row">
                    <div id='table_ports_edit' class="col-sm-12" style="display:none;" >
                        <form class="form-inline" role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="ports">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_ports">
                            <input type="hidden"  id="table_ports_edit_id" name="edit_id" value="">
                            <div class="form-group"><input id="table_ports_edit_name" name='name' type="text" value="" class="form-control" ></div>
                            <button class="btn btn-primary pull-right" type="submit"><strong>Edit</strong></button>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_ports_addnew' class="col-sm-12" style="display:none;" >
                        <form class="form-inline" role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="ports">{{ csrf_field() }}<input type="hidden" name="action" value="add_ports">
                            <div class="form-group"><input name='name' type="text" placeholder="Enter Port Name" class p="form-control" ></div>
                            <button class="btn btn-primary pull-right" type="submit"><strong>Add New</strong></button>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_ports_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value='ports'> 
                                <input type="search" name="search_ports" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($ports as $port)
                            <tr>
                                <td>{{ $port->id }}</td>
                                <td>{{ $port->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_div('#table_ports_edit', {{ $port->id }} , '{{ $port->name }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($port->active == 1)
                                    <form id="delete_port{{ $port->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_port_id" value="{{ $port->id }}"><input type="hidden" name="table" value="ports"><input type="hidden" name="action" value="delete_ports">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Delete</button>
                                    </form>
                                    @elseif ($port->active == 0)
                                    <form id="activate_port{{ $port->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_port_id" value="{{ $port->id }}"><input type="hidden" name="table" value="ports"><input type="hidden" name="action" value="activate_ports">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $ports->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
            
            
            
   <div class="col-lg-4">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>License Table</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content" 
                @if ($table != 'licenses') 
                style='display: none;'
                @endif
                 {{$table}}>
                <div class="row">
                    <div id='table_licenses_edit' class="col-sm-12" style="display:none;" >
                        <form class="form-inline" role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="licenses">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_licenses">
                            <input type="hidden"  id="table_licenses_edit_id" name="edit_id" value="">
                            <div class="form-group"><input id="table_licenses_edit_name" name='name' type="text" value="" class="form-control" ></div>
                            <button class="btn btn-primary pull-right" type="submit"><strong>Edit</strong></button>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_licenses_addnew' class="col-sm-12" style="display:none;" >
                        <form class="form-inline" role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="licenses">{{ csrf_field() }}<input type="hidden" name="action" value="add_licenses">
                            <div class="form-group"><input name='name' type="text" placeholder="Enter License Name" class="form-control" ></div>
                            <button class="btn btn-primary pull-right" type="submit"><strong>Add New</strong></button>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_licenses_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='licenses'> 
                                <input type="search" name="search_licenses" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($licenses as $license)
                            <tr>
                                <td>{{ $license->id }}</td>
                                <td>{{ $license->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_div('#table_licenses_edit', {{ $license->id }} , '{{ $license->name }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($license->active == 1)
                                    <form id="delete_license{{ $license->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_license_id" value="{{ $license->id }}"><input type="hidden" name="table" value="licenses"><input type="hidden" name="action" value="delete_licenses">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Delete</button>
                                    </form>
                                    @elseif ($license->active == 0)
                                    <form id="activate_license{{ $license->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_license_id" value="{{ $license->id }}"><input type="hidden" name="table" value="licenses"><input type="hidden" name="action" value="activate_licenses">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $licenses->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
    
</div>
            
            
        <div class="row">
    <div class="col-lg-4">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Product styles Table</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content" 
                @if ($table != 'product_styles') 
                style='display: none;'
                @endif
                 {{$table}}>
                <div class="row">
                    <div id='table_product_styles_edit' class="col-sm-12" style="display:none;" >
                        <form class="form-inline" role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="product_styles">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_product_styles">
                            <input type="hidden"  id="table_product_styles_edit_id" name="edit_id" value="">
                            <div class="form-group"><input id="table_product_styles_edit_name" name='name' type="text" value="" class="form-control" ></div>
                            <button class="btn btn-primary pull-right" type="submit"><strong>Edit</strong></button>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_product_styles_addnew' class="col-sm-12" style="display:none;" >
                        <form class="form-inline" role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="product_styles">{{ csrf_field() }}<input type="hidden" name="action" value="add_product_styles">
                            <div class="form-group"><input name='name' type="text" placeholder="Enter Product Style" class="form-control" ></div>
                            <button class="btn btn-primary pull-right" type="submit"><strong>Add New</strong></button>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_product_styles_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='product_styles'> 
                                <input type="search" name="search_product_styles" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($product_styles as $product_style)
                            <tr>
                                <td>{{ $product_style->id }}</td>
                                <td>{{ $product_style->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_div('#table_product_styles_edit', {{ $product_style->id }} , '{{ $product_style->name }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($product_style->active == 1)
                                    <form id="delete_product_style{{ $product_style->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_product_style_id" value="{{ $product_style->id }}"><input type="hidden" name="table" value="product_styles"><input type="hidden" name="action" value="delete_product_styles">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Delete</button>
                                    </form>
                                    @elseif ($product_style->active == 0)
                                    <form id="activate_product_style{{ $product_style->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_product_style_id" value="{{ $product_style->id }}"><input type="hidden" name="table" value="product_styles"><input type="hidden" name="action" value="activate_product_styles">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $product_styles->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
    
    
    
    
    <div class="col-lg-4">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Product Types Table</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content" 
                @if ($table != 'product_types') 
                style='display: none;'
                @endif
                 {{$table}}>
                <div class="row">
                    <div id='table_product_types_edit' class="col-sm-12" style="display:none;" >
                        <form class="form-inline" role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="product_types">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_product_types">
                            <input type="hidden"  id="table_product_types_edit_id" name="edit_id" value="">
                            <div class="form-group"><input id="table_product_types_edit_name" name='name' type="text" value="" class="form-control" ></div>
                            <button class="btn btn-primary pull-right" type="submit"><strong>Edit</strong></button>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_product_types_addnew' class="col-sm-12" style="display:none;" >
                        <form class="form-inline" role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="product_types">{{ csrf_field() }}<input type="hidden" name="action" value="add_product_types">
                            <div class="form-group"><input name='name' type="text" placeholder="Enter Product Type" class p="form-control" ></div>
                            <button class="btn btn-primary pull-right" type="submit"><strong>Add New</strong></button>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_product_types_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='product_types'> 
                                <input type="search" name="search_product_types" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($product_types as $product_type)
                            <tr>
                                <td>{{ $product_type->id }}</td>
                                <td>{{ $product_type->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_div('#table_product_types_edit', {{ $product_type->id }} , '{{ $product_type->name }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($product_type->active == 1)
                                    <form id="delete_product_type{{ $product_type->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_product_type_id" value="{{ $product_type->id }}"><input type="hidden" name="table" value="product_types"><input type="hidden" name="action" value="delete_product_types">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Delete</button>
                                    </form>
                                    @elseif ($product_type->active == 0)
                                    <form id="activate_product_type{{ $product_type->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_product_type_id" value="{{ $product_type->id }}"><input type="hidden" name="table" value="product_types"><input type="hidden" name="action" value="activate_product_types">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $product_types->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
            
            
            
   <div class="col-lg-4">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Freight Forwarder Table</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content" 
                @if ($table != 'freight_forwarders') 
                style='display: none;'
                @endif
                 {{$table}}>
                <div class="row">
                    <div id='table_freight_forwarders_edit' class="col-sm-12" style="display:none;" >
                        <form class="form-inline" role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="freight_forwarders">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_freight_forwarders">
                            <input type="hidden"  id="table_freight_forwarders_edit_id" name="edit_id" value="">
                            <div class="form-group"><input id="table_freight_forwarders_edit_name" name='name' type="text" value="" class="form-control" ></div>
                            <button class="btn btn-primary pull-right" type="submit"><strong>Edit</strong></button>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_freight_forwarders_addnew' class="col-sm-12" style="display:none;" >
                        <form class="form-inline" role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="freight_forwarders">{{ csrf_field() }}<input type="hidden" name="action" value="add_freight_forwarders">
                            <div class="form-group"><input name='name' type="text" placeholder="Enter Fre For Name" class="form-control" ></div>
                            <button class="btn btn-primary pull-right" type="submit"><strong>Add New</strong></button>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_freight_forwarders_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='freight_forwarders'>
                                <input type="search" name="search_freight_forwarders" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($freight_forwarders as $freight_forwarder)
                            <tr>
                                <td>{{ $freight_forwarder->id }}</td>
                                <td>{{ $freight_forwarder->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_div('#table_freight_forwarders_edit', {{ $freight_forwarder->id }} , '{{ $freight_forwarder->name }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($freight_forwarder->active == 1)
                                    <form id="delete_freight_forwarder{{ $freight_forwarder->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_freight_forwarder_id" value="{{ $freight_forwarder->id }}"><input type="hidden" name="table" value="freight_forwarders"><input type="hidden" name="action" value="delete_freight_forwarders">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Delete</button>
                                    </form>
                                    @elseif ($freight_forwarder->active == 0)
                                    <form id="activate_freight_forwarder{{ $freight_forwarder->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_freight_forwarder_id" value="{{ $freight_forwarder->id }}"><input type="hidden" name="table" value="freight_forwarders"><input type="hidden" name="action" value="activate_freight_forwarders">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $freight_forwarders->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
    
</div>
        
        
        
        <div class="row">
            <div class="col-lg-4">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Salesmen Table</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content" 
                @if ($table != 'salesmen') 
                style='display: none;'
                @endif
                 {{$table}}>
                <div class="row">
                    <div id='table_salesmen_edit' class="col-sm-12" style="display:none;" >
                        <form class="form-inline" role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="salesmen">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_salesmen">
                            <input type="hidden"  id="table_salesmen_edit_id" name="edit_id" value="">
                            <div class="form-group"><input id="table_salesmen_edit_name" name='name' type="text" value="" class="form-control" ></div>
                            <button class="btn btn-primary pull-right" type="submit"><strong>Edit</strong></button>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_salesmen_addnew' class="col-sm-12" style="display:none;" >
                        <form class="form-inline" role="form" method="POST" action="{{ url('/tables') }}">
                            <input type="hidden" name="table" value="salesmen">{{ csrf_field() }}<input type="hidden" name="action" value="add_salesmen">
                            <div class="form-group"><input name='name' type="text" placeholder="Enter Fre For Name" class="form-control" ></div>
                            <button class="btn btn-primary pull-right" type="submit"><strong>Add New</strong></button>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_salesmen_addnew');">Add New</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group"><form class="form-horizontal" role="form" method="get" action="{{ url('/tables') }}">
                                <input type="hidden" name="table" value='salesmen'>
                                <input type="search" name="search_salesmen" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($salesmen as $salesman)
                            <tr>
                                <td>{{ $salesman->id }}</td>
                                <td>{{ $salesman->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_div('#table_salesmen_edit', {{ $salesman->id }} , '{{ $salesman->name }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($salesman->active == 1)
                                    <form id="delete_salesman{{ $salesman->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="delete_salesman_id" value="{{ $salesman->id }}"><input type="hidden" name="table" value="salesmen"><input type="hidden" name="action" value="delete_salesmen">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Delete</button>
                                    </form>
                                    @elseif ($salesman->active == 0)
                                    <form id="activate_salesman{{ $salesman->id }}" method="POST" action="{{ url('/tables') }}">
                                        <input type="hidden" name="activate_salesman_id" value="{{ $salesman->id }}"><input type="hidden" name="table" value="salesmen"><input type="hidden" name="action" value="activate_salesmen">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $salesmen->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
            
            
            
        </div>

@endsection
