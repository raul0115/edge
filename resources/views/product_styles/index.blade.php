@extends('loggedin.layout')

@section('content')

@if (session('message'))

<div class="alert alert-success">
    <strong>Successful Action!</strong><br>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row">
<div class="col-lg-12">
       
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Product styles Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_product_styles_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{route('table.product_style.post') }}">
                            <input type="hidden" name="table" value="product_styles">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_product_styles">
                            <input type="hidden"  id="table_product_styles_edit_id" name="edit_id" value="">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_product_styles_edit_name" name='name' type="text" value="" class="form-control" >
                                </div>
                           
                            </div>
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_product_styles_addnew' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{route('table.product_style.post') }}">
                            <input type="hidden" name="table" value="product_styles">{{ csrf_field() }}<input type="hidden" name="action" value="add_product_styles">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter Product Style" class="form-control" >
                                </div>
                           
                            </div>
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New Product Style</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_product_styles_addnew');">Add New Product Style</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group pull-right"><form class="form-horizontal" role="form" method="get" action="{{route('table.product_style.index') }}">
                                <input type="hidden" name="table" value='product_styles'> 
                                <input type="search" name="search_product_styles" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($product_styles as $product_style)
                            <tr>
                                <td>{{ $product_style->id }}</td>
                                <td>{{ $product_style->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_div('#table_product_styles_edit', {{ $product_style->id }} , '{{ $product_style->name }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($product_style->active == 1)
                                    <form id="delete_product_style{{ $product_style->id }}" method="POST" action="{{route('table.product_style.post') }}">
                                        <input type="hidden" name="delete_product_style_id" value="{{ $product_style->id }}"><input type="hidden" name="table" value="product_styles"><input type="hidden" name="action" value="delete_product_styles">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($product_style->active == 0)
                                    <form id="activate_product_style{{ $product_style->id }}" method="POST" action="{{route('table.product_style.post') }}">
                                        <input type="hidden" name="activate_product_style_id" value="{{ $product_style->id }}"><input type="hidden" name="table" value="product_styles"><input type="hidden" name="action" value="activate_product_styles">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $product_styles->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/table.js') }}"></script>
@endpush