@extends('loggedin.layout')

@section('content')

@if (session('message'))

<div class="alert alert-success">
    <strong>Successful Action!</strong><br>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row">
<div class="col-lg-12">
       
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Freight Forwarder Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_freight_forwarders_edit' class="col-sm-12" style="display:none;" >
                        <form class="form" role="form" method="POST" action="{{route('table.freight_forwarder.post') }}">
                            <input type="hidden" name="table" value="freight_forwarders">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_freight_forwarders">
                            <input type="hidden"  id="table_freight_forwarders_edit_id" name="edit_id" value="">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_freight_forwarders_edit_name" name='name' type="text" value="" class="form-control" >
                                </div>
                           
                            </div>
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_freight_forwarders_addnew' class="col-sm-12" style="display:none;" >
                        <form role="form" method="POST" action="{{route('table.freight_forwarder.post') }}">
                            <input type="hidden" name="table" value="freight_forwarders">{{ csrf_field() }}<input type="hidden" name="action" value="add_freight_forwarders">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter Fre For Name" class="form-control" >
                                </div>
                           
                            </div>
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New Freight Forwarder</strong></button>
                            <div class="clearfix"></div>

                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_freight_forwarders_addnew');">Add New Freight Forwarder</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group pull-right"><form class="form-horizontal" role="form" method="get" action="{{route('table.freight_forwarder.index') }}">
                                <input type="hidden" name="table" value='freight_forwarders'>
                                <input type="search" name="search_freight_forwarders" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($freight_forwarders as $freight_forwarder)
                            <tr>
                                <td>{{ $freight_forwarder->id }}</td>
                                <td>{{ $freight_forwarder->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_div('#table_freight_forwarders_edit', {{ $freight_forwarder->id }} , '{{ $freight_forwarder->name }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($freight_forwarder->active == 1)
                                    <form id="delete_freight_forwarder{{ $freight_forwarder->id }}" method="POST" action="{{route('table.freight_forwarder.post') }}">
                                        <input type="hidden" name="delete_freight_forwarder_id" value="{{ $freight_forwarder->id }}"><input type="hidden" name="table" value="freight_forwarders"><input type="hidden" name="action" value="delete_freight_forwarders">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($freight_forwarder->active == 0)
                                    <form id="activate_freight_forwarder{{ $freight_forwarder->id }}" method="POST" action="{{route('table.freight_forwarder.post') }}">
                                        <input type="hidden" name="activate_freight_forwarder_id" value="{{ $freight_forwarder->id }}"><input type="hidden" name="table" value="freight_forwarders"><input type="hidden" name="action" value="activate_freight_forwarders">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $freight_forwarders->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>       
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/table.js') }}"></script>
@endpush