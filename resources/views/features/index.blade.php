@extends('loggedin.layout')

@section('content')

@if (session('message'))

<div class="alert alert-success">
    <strong>Successful Action!</strong><br>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row">
<div class="col-lg-12">
       
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>features Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_features_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{route('table.feature.post') }}">
                            <input type="hidden" name="table" value="features">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_features">
                            <input type="hidden"  id="table_features_edit_id" name="edit_id" value="">
                            
                             <div class="col-md-4">
                                <div class="form-group">
                                    <label>code</label>
                                    <input id="table_features_edit_code" name='code' type="text" value="" class="form-control" maxlength="1">
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_features_edit_name" name='name' type="text" value="" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_features_addnew' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{route('table.feature.post') }}">
                            <input type="hidden" name="table" value="features">{{ csrf_field() }}<input type="hidden" name="action" value="add_features">
                           
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Code</label>
                                    <input name='code' type="text" placeholder="Enter  " class="form-control" maxlength="1">
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter  Name" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New Feature</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_features_addnew');">Add New Feature</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group pull-right"><form class="form-horizontal" role="form" method="get" action="{{route('table.feature.index') }}">
                                <input type="hidden" name="table" value='features'>
                                <input type="search" name="search_features" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($features as $feature)
                            <tr>
                                <td>{{ $feature->id }}</td>
                                 <td>{{ $feature->code }}</td>
                                <td>{{ $feature->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_code_div('#table_features_edit', {{ $feature->id }} , '{{ $feature->name }}','{{$feature->code}}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($feature->active == 1)
                                    <form id="delete_feature{{ $feature->id }}" method="POST" action="{{route('table.feature.post') }}">
                                        <input type="hidden" name="delete_feature_id" value="{{ $feature->id }}"><input type="hidden" name="table" value="features"><input type="hidden" name="action" value="delete_features">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($feature->active == 0)
                                    <form id="activate_feature{{ $feature->id }}" method="POST" action="{{route('table.feature.post') }}">
                                        <input type="hidden" name="activate_feature_id" value="{{ $feature->id }}"><input type="hidden" name="table" value="features"><input type="hidden" name="action" value="activate_features">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $features->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/table.js') }}"></script>
@endpush