<div class="modal fade" id="modal_current_img" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form id="form_current_img" class="form-horizontal">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div id="div-image-principal">
                            <div class="form-group">
                                {{ Form::label('Image', null, ['class' => 'col-md-4 col-xs-5 control-label']) }}
                                <div class="col-md-8 col-xs-7">
                                    <div id="div-image">
                                        <img src="{{url($item_images->path)}}" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                    </div>
                    @if($item_images->description)
                            <div class="form-group">
                                
                                
                                {{ Form::label('designer commentary', null,  ['class' => 'col-md-4 col-xs-5 control-label']) }}
                                <div class="col-md-8 col-xs-7">
                                {{Form::textarea('designer_commentary',$item_images->description,['class' => 'form-control','rows'=>2,'disabled'=>'disabled'])}}
                                </div>
                            </div>
                            
                    @endif
                    @if($status->status->code!="S7" && $item_images->commentary)
                            <div class="form-group">
                                
                                
                                {{ Form::label('L. Coordinator commentary', null,  ['class' => 'col-md-4 col-xs-5 control-label']) }}
                                <div class="col-md-8 col-xs-7">
                                {{Form::textarea('lcoordinator_commentary',$item_images->commentary,['class' => 'form-control','rows'=>2,'disabled'=>'disabled'])}}
                                </div>
                            </div>
                            
                    @endif
                    @if(Auth::user()->hasRole('License Coordinator')  && $status->status->code=="S7")
                            <div class="form-group">
                                
                                {{ Form::label('new commentary img', null,['class' => 'col-md-4 col-xs-5 control-label']) }}
                                <div class="col-md-8 col-xs-7">
                                <a href="#" id="img_commentary">{{$item_images->commentary}}</a>
                                </div>
                            </div>
                            
                    @endif
                </div>
            </div>
        </form>
    </div>
</div>
