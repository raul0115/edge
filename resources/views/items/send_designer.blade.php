<div class="modal fade" id="modal_send_designer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
            <form id="form_send_designer" class="form-horizontal" method="POST" action='{{ url("items/{$item->id}/sendDesigner") }}'>
                    {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Send Mail to Designer</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        {{ Form::label('designer', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                        <a href="#" id="designer" data-type="select2" data-title="Select Designer" data-value="{{$users_designer_id}}"></a>
                        </div>
                    </div>
                    <div class="form-group">
                        
                        
                        {{ Form::label('commentary', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::textarea('commentary',null,['class' => 'form-control','rows'=>2])}}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button id="btn_send_designer" type="submit" class="btn btn-primary hide">Send mail</button>
                </div>
            </div>
        </form>
    </div>
  </div>