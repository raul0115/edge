@extends('loggedin.layout')

@section('content')

<div class="row">
    
        @if (session('message'))
        <div class="alert alert-success">
            <strong>Successful Action!</strong><br><br>
            <ul>
                <li>{{ session('message') }}</li>
            </ul>
        </div>
        @endif
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="alert alert-warning alert-dismissable" id="alert-val">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
        </div>
       

</div>
<div class="row">
<div class="col-sm-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>New Item</h5>
        </div>
        <div class="ibox-content">
            <form class="form-horizontal" id="item-form" method="POST" action="{{ route('item.store') }}" >
                
                {{ csrf_field() }}
                <div id="fgroup_name" class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        
                        {{ Form::label('name', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                            {{Form::text('name',null,['class' => 'form-control','placeholder' => 'Required'])}}
                        </div>
                    </div>
                
                <div id="fgroup_material" class="form-group{{ $errors->has('material_id') ? ' has-error' : '' }}">
                    {{ Form::label('Material', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                    {{ Form::select('material_id', [], old('material_id'), ['id' => 'material_id','class'=>'form-control select2','placeholder' => 'Required']) }}
                       
                    </div>
                </div>
                <div id="fgroup_mounting" class="form-group{{ $errors->has('mounting_id') ? ' has-error' : '' }}">
                    {{ Form::label('Mounting', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                    {{ Form::select('mounting_id', [], old('mounting_id'), ['id' => 'mounting_id','class'=>'form-control select2','placeholder' => 'Required']) }}
                    </div>
                </div>
                <div id="fgroup_feature" class="form-group{{ $errors->has('feature_id') ? ' has-error' : '' }}">
                    {{ Form::label('feature', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                    {{ Form::select('feature_id', [], old('feature_id'), ['id' => 'feature_id','class'=>'form-control select2','placeholder' => 'Required']) }}
                    </div>
                </div>
                <div id="fgroup_size" class="form-group{{ $errors->has('size_id') ? ' has-error' : '' }}">
                    {{ Form::label('size', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                    {{ Form::select('size_id', [], old('size_id'), ['id' => 'size_id','class'=>'form-control select2','placeholder' => 'Required']) }}
                    </div>
                </div>
                <div id="fgroup_license" class="form-group{{ $errors->has('license_id') ? ' has-error' : '' }}">
                    {{ Form::label('license', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                    {{ Form::select('license_id', [], old('license_id'), ['id' => 'license_id','class'=>'form-control select2','placeholder' => 'Required']) }}
                    </div>
                </div>
               
                <div id="fgroup_character" class="form-group{{ $errors->has('character_id') ? ' has-error' : '' }}">
                    {{ Form::label('character', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                    {{ Form::select('character_id', [], old('character_id'), ['id' => 'character_id','class'=>'form-control select2','placeholder' => 'Required']) }}
                    </div>
                </div>
                <div id="fgroup_customer" class="form-group{{ $errors->has('customer_id') ? ' has-error' : '' }}">
                    {{ Form::label('customer', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                    {{ Form::select('customer_id', [], old('customer_id'), ['id' => 'customer_id','class'=>'form-control select2','placeholder' => '']) }}
                    </div>
                </div>
                <div id="fgroup_description" class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                    
                    {{ Form::label('description', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                    <div class="col-md-6 col-sm-5 col-xs-6">
                    {{Form::textarea('description',null,['class' => 'form-control','rows'=>2])}}
                    </div>
                </div>
                <div id="fgroup_group" class="form-group{{ $errors->has('group_id') ? ' has-error' : '' }}">
                        {{ Form::label('group', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                        {{ Form::select('group_id', [],null, ['id' => 'group_id','name'=>'group_id[]','multiple'=>'multiple', 'class'=>'form-control select2','placeholder' => '']) }}
                        </div>
                </div>
            <div class="form-group">
                <div class="col-lg-offset-4 col-lg-8">
                    <button class="btn btn-primary" type="submit">New Item</button>
               
                </div>
            </div>

            </form>
        </div>
    </div>
</div>
</div>
@endsection
@push('styles2')
<link rel="stylesheet" href="{{ asset('css/plugins/select2/select2.css')}}">
<link rel="stylesheet" href="{{ asset('css/plugins/select2/select2-bootstrap.css')}}">
<link href="{{ asset('css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
<style>
.has-error .form-control {
    border:1px solid rgb(248, 148, 6) !important;
}
.has-error .select2-container{
    border:1px solid rgb(248, 148, 6) !important;
}
.has-error .control-label {
    color: rgb(103, 106, 108) !important;
}
</style>
@endpush
@push('scripts')
<script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('js/plugins/validate/jquery.validate.min.js') }}"></script>
<script>
$(document).ready(function(){
    
    var alert = $("#alert-val");
    alert.hide();


    materials=<?=json_encode($materials)?>;
    var data_materials = $.map(materials, function (obj) {
        obj.text = obj.text || obj.name; // replace name with the property used for the text
        delete obj.name;
        return obj;
    });
    //console.log('materials',materials); 
    $('#material_id').select2({       
        width: "100%",
        tags: false,
        data: data_materials
    });
    mountings=<?=json_encode($mountings)?>;
    var data_mountings = $.map(mountings, function (obj) {
        obj.text = obj.text || obj.name; // replace name with the property used for the text
        delete obj.name;
        return obj;
    });
    
    $('#mounting_id').select2({       
        width: "100%",
        tags: false,
        data: data_mountings
    });

    features=<?=json_encode($features)?>;
    var data_features = $.map(features, function (obj) {
        obj.text = obj.text || obj.name; // replace name with the property used for the text
        delete obj.name;
        return obj;
    });


     $('#feature_id').select2({       
        width: "100%",
        tags: false,
        data: data_features
    });

    sizes=<?=json_encode($sizes)?>;
    var data_sizes = $.map(sizes, function (obj) {
        obj.text = obj.text || obj.name; // replace name with the property used for the text
        delete obj.name;
        return obj;
    });


     $('#size_id').select2({       
        width: "100%",
        tags: false,
        data: data_sizes
    });
    licenses=<?=json_encode($licenses)?>;
    var data_licenses = $.map(licenses, function (obj) {
        tex=(obj.code)?obj.code+':':'';
        obj.text = tex + obj.name; // replace name with the property used for the text
        delete obj.name;
        delete obj.code;
        return obj;
    });


     $('#license_id').select2({       
        width: "100%",
        tags: false,
        data: data_licenses
    });
    function fill_character(characteres){
        
        var data_characteres = $.map(characteres, function (obj) {
        obj.text = obj.text || obj.name; // replace name with the property used for the text
        delete obj.name;
        return obj;
        });

        $('#character_id').select2({       
            width: "100%",
            tags: false,
            data: data_characteres
        });
    }
    $('#character_id').select2({       
            width: "100%"
        });
    
    
    customers=<?=json_encode($customers)?>;
    var data_customers = $.map(customers, function (obj) {
        tex=(obj.code)?obj.code+':':'';
        obj.text = tex + obj.name; // replace name with the property used for the text
        delete obj.name;
        delete obj.code;
        return obj;
    });


     $('#customer_id').select2({       
        width: "100%",
        tags: false,
        data: data_customers
    });
   
    

        function obtener_character(){
      
            var ruta = "{{url('items/obtener_character/')}}";
            $.get(ruta+'/'+$('#license_id').val(), function(data) {
                $('#character_id').empty();
                var option = $("<option></option>")
                .attr("value","")		                  
                .text('Required');
                $('#character_id').append(option);
                $.each(data, function(key, value) {
                    var option = $("<option></option>")
                          .attr("value",value.id)		                  
                          .text(value.text);
        
                    $('#character_id').append(option);
                });
                $('#character_id').val("").trigger("change");
                
            });
           
        }
        $('#license_id').change(function(e) {
            e.preventDefault();
            obtener_character();
        
        });
        
       

        $('form#item-form').validate({
            rules: {
                
                name: {
                    required: true,
                },
                material_id: {
                    required: true,
                },
                mounting_id: {
                    required: true,
                },
                feature_id: {
                    required: true,
                },
                size_id: {
                    required: true,
                },
                license_id: {
                    required: true,
                },
                character_id: {
                    required: true,
                }
            },
            highlight: function (element, errorClass, validClass) {
                var elem = $(element);
                if (elem.hasClass("select2-hidden-accessible")) {
                    $("#select2-" + elem.attr("id") + "-container").parent().addClass(errorClass); 
                } else {
                    elem.addClass(errorClass);
                }
                },    
                unhighlight: function (element, errorClass, validClass) {
                    var elem = $(element);
                    if (elem.hasClass("select2-hidden-accessible")) {
                        $("#select2-" + elem.attr("id") + "-container").parent().removeClass(errorClass);
                    } else {
                        elem.removeClass(errorClass);
                    }
                },
                errorPlacement: function(error, element) {
                var elem = $(element);
                if (elem.hasClass("select2-hidden-accessible")) {
                    element = $("#select2-" + elem.attr("id") + "-container").parent(); 
                    error.insertAfter(element);
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function(form) {
                
               call_form();
            }
        });


       function call_form(){ 
            
            var form = $('form#item-form');
            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: form.serialize(),
                success: function (data) {

                    swal({
                    title: "Item created",
                    text: data.code,
                    type: "success",
                    showConfirmButton: false,
                    footer: "<a href='{{route('item.create')}}' class='btn btn-default'>Crear nuevo item</a> <a href='{{url('items/edit')}}"+"/"+data.id+"' class='btn btn-primary'>Ir a Item</a>",
                    }).then((result) => {
                        window.location.href = 'create';
                    });

                    
                },
                error: function (data, textStatus, errorThrown) {
                    var error= data.responseJSON.errors;
                    for (var e in error){
                        var txtgeneral = $("<p class='txtgeneral'></p>").text(error[e][0]);   // Create with jQuery
                        alert.append(txtgeneral);
                    }
                    alert.show();
                },
            });
        };


        var select2_config={
		allowClear: true,
		width: "100%",
		delay: 250,
		ajax: {
			url: "{{route('item.get_groups')}}",
			dataType: 'json',
			type: "POST",
			data: function(params) {
				return {
					q: params.term,
					page: params.page || 1,
					page_limit: 30,
				};
			},
			processResults: function(data, params) {
				params.page = params.page || 1;
				return {
					results: data.groups,
					pagination: {
						more: (params.page * 30) < data.total
					}
				};
			},
			cache: true,
            headers:{
                    'X-CSRF-TOKEN':'{{csrf_token()}}'
                }
		},
		placeholder: " ",
		escapeMarkup: function (markup) { return markup; },
		minimumInputLength: 1,
		templateResult: formatRepo,
  		templateSelection: formatRepoSelection
	};

    $("#group_id").select2(select2_config);

	function formatRepo (repo) {
		if (repo.loading) {
			return repo.text;
		}
		var markup = "<div class='select2-result-repository clearfix  valign-wrapper'>";
				markup += "<div> &nbsp;&nbsp;";
					markup +=repo.text ;
				markup += "</div>";
		markup += "</div>";
		return markup;
	}

	function formatRepoSelection (repo) {
		return repo.style_id || repo.text;
	}
           
});
    
</script>



@endpush
