<div class="modal fade" id="modal_old_img" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                            <div class="ibox">
                                @if($old_images->count()>1)
                                    <div class="slick_demo_1">
                                        @foreach($old_images as $img)
                                            <div>
                                                <div class="ibox-content">
                                                    <img src="{{url($img->path)}}" class="img-responsive">
                                                    @if($img->description)
                                                    <div class="slider-commentary">
                                                        <span>Designer commentary:  </span>{{$img->description}}
                                                    </div>
                                                    @endif
                                                    @if($img->commentary)
                                                    <div class="slider-commentary">
                                                    <span>Lic. Coordinator commentary:  </span> {{$img->commentary}}
                                                    </div>
                                                    @endif
                                                    
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    <div>
                                    @foreach($old_images as $img)
                                        <div class="ibox-content">
                                            <img src="{{url($img->path)}}" class="img-responsive">
                                            @if($img->description)
                                                <div class="slider-commentary">
                                                    <span>Designer commentary:  </span>{{$img->description}}
                                                </div>
                                            @endif
                                            @if($img->commentary)
                                                <div class="slider-commentary">
                                                    <span>Lic. Coordinator commentary:  </span> {{$img->commentary}}
                                                </div>
                                            @endif
                                                    
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                  
                </div>
            </div>
    </div>
</div>
