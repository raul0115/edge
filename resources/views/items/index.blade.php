@extends('loggedin.layout')

@section('content')
<div class="row">
    
        @if (session('message'))
        <div class="alert alert-success">
            <strong>Successful Action!</strong><br><br>
            <ul>
                <li>{{ session('message') }}</li>
            </ul>
        </div>
        @endif
        @if (session('error'))
        <div class="alert alert-danger">
            <strong>Whoops!</strong> {{ session('error') }}<br>
        </div>
        @endif


</div>
<div class="row">
<div class="col-sm-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>List Item</h5>
        </div>
        <div class="ibox-content">
        <table id="item-table" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Image</th>
                        <th>Nombre</th>
                        <th>Code</th>
                        <th>Status</th>
                        <th>Designer</th>
                        <th>Material</th>
                        <th>Mounting</th>
                        <th>Feature</th>
                        <th>Size</th>
                        <th>License</th>
                        <th>Character</th>
                        <th>Customer</th>
                        
                       
                    </tr>
                    <tr id="tr-primary">
                        <th></th>
                        <th></th>
                        <th>name</th>
                        <th>code</th>
                        <th>status</th>
                        <th>designer</th>
                        <th>material</th>
                        <th>mounting</th>
                        <th>feature</th>
                        <th>size</th>
                        <th>license</th>
                        <th>character</th>
                        <th>customer</th>
                        
                       
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
              </table>
        </div>
    </div>
</div>
</div>
@endsection
@push('styles2')
<link rel="stylesheet" href="{{ asset('js/plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('plugins/magnific-popup/magnific-popup.css')}}">
<style>
.column_search{
    width:100%;
}
tr {
    cursor: pointer;
}
</style>
@endpush
@push('scripts')
<script src="{{asset('js/plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('plugins/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<script>
 $( document ).ready(function() {
 
$('#tr-primary th').each( function () {
        var title = $(this).text();
        if(title!="" && (title == 'name' || title == 'code')){
            $(this).html( '<input type="text" name='+title+' class="column_search"/>' );
        }else{
            switch(title){
                case 'status':
                    $(this).html('<select  name="status[]" multiple="multiple" class="select2"></select>');
                    var status=<?=json_encode($status_all)?>;
                    
                    var data_status = $.map(status, function (obj) {
                        obj.text = obj.text || obj.name; // replace name with the property used for the text
                        delete obj.name;
                        return obj;
                    });
                    $("select[name='status[]']").select2({       
                        width: "100%",
                        data: data_status
                    });
                    break;
                case 'material':
                    $(this).html('<select  name="material[]" multiple="multiple" class="select2"></select>');
                   
                    var materials=<?=json_encode($materials)?>;
                    var data_materials = $.map(materials, function (obj) {
                        obj.text = obj.text || obj.name; // replace name with the property used for the text
                        delete obj.name;
                        return obj;
                    });
                    $("select[name='material[]']").select2({       
                        width: "100%",
                        data: data_materials
                    });
                    break;
                case 'mounting':
                    $(this).html('<select  name="mounting[]" multiple="multiple" class="select2"></select>');
                   
                    var mountings=<?=json_encode($mountings)?>;
                    var data_mountings = $.map(mountings, function (obj) {
                        obj.text = obj.text || obj.name; // replace name with the property used for the text
                        delete obj.name;
                        return obj;
                    });
                    $("select[name='mounting[]']").select2({       
                        width: "100%",
                        data: data_mountings
                    });
                    break;
                case 'feature':
                    $(this).html('<select  name="feature[]" multiple="multiple" class="select2"></select>');
                   
                    var features=<?=json_encode($features)?>;
                    var data_features = $.map(features, function (obj) {
                        obj.text = obj.text || obj.name; // replace name with the property used for the text
                        delete obj.name;
                        return obj;
                    });
                    $("select[name='feature[]']").select2({       
                        width: "100%",
                        data: data_features
                    });
                    break;
                case 'size':
                    $(this).html('<select  name="size[]" multiple="multiple" class="select2"></select>');
                   
                    var sizes=<?=json_encode($sizes)?>;
                    var data_sizes = $.map(sizes, function (obj) {
                        obj.text = obj.text || obj.name; // replace name with the property used for the text
                        delete obj.name;
                        return obj;
                    });
                    $("select[name='size[]']").select2({       
                        width: "100%",
                        data: data_sizes
                    });
                    break;
                case 'license':
                    $(this).html('<select  name="license[]" multiple="multiple" class="select2"></select>');
                   
                    var licenses=<?=json_encode($licenses)?>;
                    var data_licenses = $.map(licenses, function (obj) {
                        obj.text = obj.text || obj.name; // replace name with the property used for the text
                        delete obj.name;
                        return obj;
                    });
                    $("select[name='license[]']").select2({       
                        width: "100%",
                        data: data_licenses
                    });
                    break;
                case 'character':
                    $(this).html('<select  name="character[]" multiple="multiple" class="select2"></select>');
                   
                    var characteres=<?=json_encode($characteres)?>;
                    var data_characteres = $.map(characteres, function (obj) {
                        obj.text = obj.text || obj.name; // replace name with the property used for the text
                        delete obj.name;
                        return obj;
                    });
                    $("select[name='character[]']").select2({       
                        width: "100%",
                        data: data_characteres
                    });
                    break;
                case 'customer':
                    $(this).html('<select  name="customer[]" multiple="multiple" class="select2"></select>');
                   
                    var customers=<?=json_encode($customers)?>;
                    var data_customers = $.map(customers, function (obj) {
                        obj.text = obj.text || obj.name; // replace name with the property used for the text
                        delete obj.name;
                        return obj;
                    });
                    $("select[name='customer[]']").select2({       
                        width: "100%",
                        data: data_customers
                    });
                    break;
                case 'designer':
                    $(this).html('<select  name="designer[]" multiple="multiple" class="select2"></select>');
                   
                    var designer=<?=json_encode($designer)?>;
                    var data_designer = $.map(designer, function (obj) {
                        obj.text = obj.text || obj.name; // replace name with the property used for the text
                        delete obj.name;
                        return obj;
                    });
                    $("select[name='designer[]']").select2({       
                        width: "100%",
                        data: data_designer
                    });
                    break;
                default: break;
            }
        }
        
    } );
    /*
$('#item-table tfoot th').each( function () {
        var title = $(this).text();
        if(title!=""){
            $(this).html( '<input type="text"/>' );
        }
    } );
    */
var iTable =    $('#item-table').DataTable( {
                orderCellsTop: true,
                processing: true,
                serverSide: true,
                "scrollX": true,
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false,
                "order": [[ 0, "desc" ]],
                "columnDefs": [
                {
                    "targets": [0],
                    "searchable": false,
                    "visible": false,
                },
                { "targets":[1],
                    "searchable": false,
                    "orderable": false, 
                    "createdCell": function(td, cellData, rowData, row, col) {
                        is_box=rowData["img"];
                        if (is_box!=null)
                            $(td).addClass('details-control');
                    }
                }
                ],
                ajax: {
                    url: "{{route('item.basic-data')}}",
                    data: function (d) {
                        d.name = $('input[name=name]').val();
                        d.code = $('input[name=code]').val();
                        d.status =  $("select[name='status[]']").val();
                        d.material =  $("select[name='material[]']").val();
                        d.mounting =  $("select[name='mounting[]']").val();
                        d.feature =  $("select[name='feature[]']").val();
                        d.size =  $("select[name='size[]']").val();
                        d.license =  $("select[name='license[]']").val();
                        d.character =  $("select[name='character[]']").val();
                        d.customer =  $("select[name='customer[]']").val();
                        d.designer =  $("select[name='designer[]']").val();
                        
                    
                    }
                },
                
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'img', name: 'img',orderable:false,searchable: false},
                    {data: 'name', name: 'items.name'},
                    {data: 'code', name: 'items.code'},
                    {data: 'status_name', name: 'status_name'},
                    {data: 'designer', name: 'designer',orderable:false},
                    {data: 'material_name', name: 'material_name'},
                    {data: 'mounting_name', name: 'mounting_name'},
                    {data: 'feature_name', name: 'feature_name'},
                    {data: 'size_name', name: 'size_name'},
                    {data: 'license_name', name: 'license_name'},
                    {data: 'character_name', name: 'character_name'},
                    {data: 'customer_name', name: 'customer_name'}
                    
                ],
                "initComplete": function( settings, json ) {
                    $('.test-popup-link').magnificPopup({
                    type: 'image'
                    // other options
                    });

                }
                
            });

        /* Apply the search
        iTable.columns().every( function () {
                var that = this;
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );   
            } );
    $( '#tr-primary'  ).on( 'keyup', ".column_search",function () {
        iTable
            .column( $(this).parent().index()+1 )
            .search( this.value )
            .draw();
    });
    */
    $( 'input[type=text]'  ).on( 'keyup',function () {
        iTable.draw();
    });
    $( 'select'  ).on( 'change',function () {
        iTable.draw();
    });
        

    $("#item-table").on('click', 'td', function() {
        var colIndex = iTable.cell(this).index().column;
        if (colIndex!=1){
            var row_index= iTable.cell(this).index().row;
            var item_id=iTable.cell(row_index, 0).data();
            window.location.href="/items/edit/"+item_id;
        }
    });
   
    });
</script>
@endpush