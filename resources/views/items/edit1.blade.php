
@extends('loggedin.layout')

@section('content')

<div class="row">
    
        @if (session('message'))
        <div class="alert alert-success">
            <strong>Successful Action!</strong><br><br>
            <ul>
                <li>{{ session('message') }}</li>
            </ul>
        </div>
        @endif
        @if (session('error'))
        <div class="alert alert-danger">
            <strong>Whoops!</strong> {{ session('error') }}<br>
        </div>
        @endif
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif


</div>
<div class="row">
<div class="col-sm-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Edit Item</h5>
        </div>
        <div class="ibox-content">
            <form class="form-horizontal" method="POST" action="{{ url('/items/update/'.$item->id) }}" >
                    
                    {{ csrf_field() }}

                    <div class="form-group">
                        
                        {{ Form::label('Status', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('status',$status->status->name,['id'=>'status_name','class' => 'form-control','disabled' => 'disabled'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        
                        {{ Form::label('Created At', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('created_at',$item->created_at->format('M d Y'),['class' => 'form-control','disabled' => 'disabled'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        
                        {{ Form::label('Created By', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('created_at',$item->user->name,['class' => 'form-control','disabled' => 'disabled'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        
                        {{ Form::label('code', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('code',$item->code,['class' => 'form-control','disabled' => 'disabled'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        
                        {{ Form::label('name', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('name',$item->name,['class' => 'form-control','disabled' => 'disabled'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        
                        {{ Form::label('material', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('material',$item->material->name,['class' => 'form-control','disabled' => 'disabled'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        
                        {{ Form::label('mounting', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('mounting',$item->mounting->name,['class' => 'form-control','disabled' => 'disabled'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        
                        {{ Form::label('feature', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('feature',$item->feature->name,['class' => 'form-control','disabled' => 'disabled'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        
                        {{ Form::label('size', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('size',$item->size->name,['class' => 'form-control','disabled' => 'disabled'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        
                        {{ Form::label('license', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('license',$item->character->license->name,['class' => 'form-control','disabled' => 'disabled'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        
                        {{ Form::label('character', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('character',$item->character->name,['class' => 'form-control','disabled' => 'disabled'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        
                        {{ Form::label('customer', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::text('customer', ($item->customer)?$item->customer->name:null ,['class' => 'form-control','disabled' => 'disabled'])}}
                        </div>
                    </div>
                    @if(Auth::user()->hasRole('License Coordinator'))
                    <div class="form-group">
                        
                        {{ Form::label('description', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                        <a href="#" id="description">{{$item->description}}</a>
                        </div>
                    </div>
                       
                    @endif
                    @if($status->commentary)
                        <div class="form-group">
                        {{ Form::label('status_commentary', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                        {{Form::textarea('status_commentary',$status->commentary,['class' => 'form-control','rows'=>2,'disabled' => 'disabled'])}}
                        </div>
                        </div>
                    @endif
                    @if($status->status->code!="S1" && $status->status->code!="S2")
                        <div class="form-group">
                            {{ Form::label('License Coordinator', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                            <div class="col-md-6 col-sm-5 col-xs-6">
                            {{Form::text('disabled_lic',$users_license_name,['class' => 'form-control','disabled' => 'disabled'])}}
                        
                            </div>
                        </div>
                    @endif
                    @if($status->status->code!="S1" && $status->status->code!="S2" )
                        <div class="form-group">
                            {{ Form::label('designer', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                            <div class="col-md-6 col-sm-5 col-xs-6">
                            {{Form::text('disabled_designer',$users_designer_name,['class' => 'form-control','disabled' => 'disabled'])}}
                        
                            </div>
                        </div>
                    @endif
                  
                    @if($can_designer && ($status->status->code=="S2" || $status->status->code=="S10"))
                    <div class="form-group">
                        {{ Form::label('license_coordinator', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                        <div class="col-md-6 col-sm-5 col-xs-6">
                        <a href="#" id="user_license" data-type="select2" data-title="Select License" data-value="{{$users_license_id}}"></a>
                        </div>
                    </div>
                    @endif
                 
                    
                   
                    @if(Auth::user()->hasRole('License Coordinator') && $status->status->code=="S1" )
                    <div class="form-group">
                            <div class="col-lg-offset-4 col-lg-8">
                                <a href='#'  data-toggle="modal" data-target="#modal_send_designer" class="btn btn-info">Send Mail to Designer</a>
                            </div>
                        </div>
                    @endif
                    <div class="form-group">
                            <div class="col-lg-offset-4 col-lg-8">
                                @if(Auth::user()->hasRole('License Coordinator') && $status->status->code=="S7" )
                                    <a href='#'  data-toggle="modal" data-target="#modal_change_status" class="btn btn-info">Change status</a>
                                @endif
                                
                                @if($item_images)
                                    <a href='#'  data-toggle="modal" data-target="#modal_current_img"  class="btn btn-info">Current Image</a>
                                @endif
                                @if($old_images->count() > 0 )
                                    <a href='#'  data-toggle="modal" data-target="#modal_old_img" id="a_modal_old_img" class="btn btn-info hide">Old Images</a>
                                @endif
                                @if($can_designer && ($status->status->code=="S2" || $status->status->code=="S10"))
                                <a href='#'  data-toggle="modal" data-target="#modal_load_image" class="btn btn-info">Add Image</a>
                                @endif
                                @if($can_designer && $item_images && $item_images->rejected==0)
                                    <a href='{{url("/items/{$item->id}/sendLicense")}}' class="btn btn-info">Send For Internal Approval</a>
                                @endif
                            </div>
                        </div>

                   
            </form>
        </div>
    </div>
</div>
</div>
@if(Auth::user()->hasRole('License Coordinator') && $status->status->code=="S1" )
    @include('items.send_designer')
@endif
@if($can_designer && ($status->status->code=="S2" || $status->status->code=="S10"))
    @include('items.load_image')
@endif

@if(Auth::user()->hasRole('License Coordinator') && $status->status->code=="S7" )
    @include('items.change_status')
@endif
@if($old_images->count() > 0 )
    @include('items.carousel')
@endif

@if($item_images)
    @include('items.current_image')
@endif

@endsection
@push('styles')
<link rel="stylesheet" href="{{ asset('css/plugins/select2/select2.css')}}">
<link rel="stylesheet" href="{{ asset('css/plugins/dropzone/dropzone.css')}}">
<link href="{{ asset('js/plugins/bootstrap3-editable/css/bootstrap-editable.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/plugins/select2/select2-bootstrap.css')}}">
<link rel="stylesheet" href="{{ asset('css/plugins/slick/slick.css')}}">
<link rel="stylesheet" href="{{ asset('css/plugins/slick/slick-theme.css')}}">
<style>
.modal-footer {
    text-align: center;
}
.has-error .form-control {
    border:1px solid rgb(248, 148, 6) !important;
}
.has-error .select2-container{
    border:1px solid rgb(248, 148, 6) !important;
}
.has-error .control-label {
    color: rgb(103, 106, 108) !important;
} 
.ibox-content img{
    max-height: 280px;
    margin: 0 auto;
}
.ibox-content {
    border:none !important;
}
.ibox-conten:focus,
.slick-track:focus,
.slick_demo_1:focus,
.ibox:focus,
.col-lg-10:focus,
.col-lg-offset-1:focus,
.slick-slide:focus{
    outline: none !important;
}
.slider-commentary{
    margin-top:25px;
    color: #000!important;
    background-color:  #eee;;
    border: 1px solid #e5e6e7;
    border-radius: 1px;
    color: inherit;
    padding: 6px 12px;
    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
    width: 100%;
    font-size: 14px;ng: 0.01em 16px;
}
.slider-commentary span{
    font-weight: bold;
    font-style: italic;
    padding-right: 10px;
}

</style>
@endpush
@push('scripts')
<script src="{{ asset('js/plugins/select2/select2.js') }}"></script>
<script src="{{asset('js/plugins/dropzone/dropzone.js')}}"></script>
<script src="{{asset('js/plugins/bootstrap3-editable/js/bootstrap-editable.min.js') }}"></script>
<script src="{{ asset('js/plugins/validate/jquery.validate.min.js') }}"></script>
<script src="{{asset('js/plugins/slick/slick.min.js')}}"></script>
<script src="{{ asset('plugins/magnific-popup/jquery.magnific-popup.min.js')}}"></script>

<script>
 $( document ).ready(function() {
    $('.slick_demo_1').slick();
if($("#a_modal_old_img").hasClass( "hide" )){
                $("#a_modal_old_img").removeClass('hide');
            }

    $(document).on('submit','form#form_send_designer',function(e){ 
      
           if(!$("#btn_send_designer").hasClass( "hide" )){
                $("#btn_send_designer").addClass('hide');
            }
        });
    $('#modal_load_image').on('hide.bs.modal',function(){
        myDropzone.removeAllFiles(true); 
        if(!$("#btn_load_image").hasClass( "hide" )){
            $("#btn_load_image").addClass('hide');
        }
        var form = $('form#form_load_image');
        form.trigger("reset");
        
    });
    $('#modal_change_status').on('hide.bs.modal',function(){
        if(!$("#btn_change_status").hasClass( "hide" )){
            $("#btn_change_status").addClass('hide');
        }
        var form = $('form#form_change_status');
        form.trigger("reset");
    });
    $('#modal_old_img').on('show.bs.modal',function(){
       $('.slick_demo_1').slick("refresh");
    
    });
   
    
    $('form#form_load_image').validate({
            rules: {
                
                img_description: {
                    required: true,
                }
            },
            highlight: function (element, errorClass, validClass) {
                var elem = $(element);
                if (elem.hasClass("select2-hidden-accessible")) {
                    $("#select2-" + elem.attr("id") + "-container").parent().addClass(errorClass); 
                } else {
                    elem.addClass(errorClass);
                }
                },    
                unhighlight: function (element, errorClass, validClass) {
                    var elem = $(element);
                    if (elem.hasClass("select2-hidden-accessible")) {
                        $("#select2-" + elem.attr("id") + "-container").parent().removeClass(errorClass);
                    } else {
                        elem.removeClass(errorClass);
                    }
                },
                errorPlacement: function(error, element) {
                var elem = $(element);
                if (elem.hasClass("select2-hidden-accessible")) {
                    element = $("#select2-" + elem.attr("id") + "-container").parent(); 
                    error.insertAfter(element);
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function(form) {
                
               call_form();
            }
        });
    function call_form(){  
        var form = $('form#form_load_image');
            @if($status->status->code=="S10" ) 
                
                $.ajax({
                    type: form.attr('method'),
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function(data)             
                    {
                        location.reload();
                        //$("#div-image").html('<img src="'+ data.url+'" class="img-responsive">');       
                    }
                });
            @endif
            myDropzone.removeAllFiles(true);
            $("#btn_load_image").addClass('hide');
            form.trigger("reset");
            $("#modal_load_image").modal('toggle');
    }
     var users_designer_id='{{$users_designer_id}}';
     if(users_designer_id!=null && users_designer_id!=''){

        if($("#btn_send_designer").hasClass( "hide" )){
            $("#btn_send_designer").removeClass('hide');
        }
     }
     var item_status= '{{$status->status->code}}';
   
    users_des=<?=json_encode($user_designer)?>;
    var source_des = $.map(users_des, function (obj) {
        obj.value = obj.id; // replace name with the property used for the text
        obj.text = obj.name;
        delete obj.name;
        delete obj.id;
        return obj;
    });
    @if($can_changed_status)
        n_status=<?=json_encode($new_status)?>;
        var source_nstatus = $.map(n_status, function (obj) {
           
            obj.value = obj.id; // replace name with the property used for the text
            obj.text = obj.name;
            delete obj.name;
            delete obj.id;
            return obj;
        });
        $('#new_status').editable({
            source:  source_nstatus,
            url: '{{url("/items/submit_edit/".$item->id)}}',
            select2: {
                width: 200,
                placeholder: 'Select status',
                allowClear: true
            },
            name:'new_status',
            pk:'{{$item->id}}',
            ajaxOptions: {
                headers:{
                    'X-CSRF-TOKEN':'{{csrf_token()}}'
                }
            },
            error: function(response, newValue) {
                return response.responseJSON.error[0];
            },
            success: function(response, newValue) {
                if($("#btn_change_status").hasClass( "hide" )){
                    $("#btn_change_status").removeClass('hide');
                }
              //  $("#status_name").val(response.success);
            //item_status= response.code;
            }
        }); 
    @endif
   $('#designer').editable({ 
        type: 'select2',
        url: '{{url("/items/submit_edit/".$item->id)}}',
        source: source_des,
        select2: {
           multiple: true,
           width: 200,
        },
        name:'designer',
        pk:'{{$item->id}}',
        ajaxOptions: {
            headers:{
            'X-CSRF-TOKEN':'{{csrf_token()}}'
        }
       },
       error: function(response, newValue) {
            return response.responseJSON.error[0];
        },
        success: function(response, newValue) {
            if($("#btn_send_designer").hasClass( "hide" )){
                $("#btn_send_designer").removeClass('hide');
            }
            
            //$("#status_name").val(response.success);
            //item_status= response.code;
        }

    });
    users_lic=<?=json_encode($user_license_coordinator)?>;
    var source_lic = $.map(users_lic, function (obj) {
        obj.value = obj.id; // replace name with the property used for the text
        obj.text = obj.name;
        delete obj.name;
        delete obj.id;
        return obj;
    });
    $('#user_license').editable({ 
        type: 'select2',
        url: '{{url("/items/submit_edit/".$item->id)}}',
        source: source_lic,
        select2: {
           multiple: true,
           width: 200,
        },
        name:'user_lic',
        pk:'{{$item->id}}',
        ajaxOptions: {
            headers:{
            'X-CSRF-TOKEN':'{{csrf_token()}}'
        }
       },
       error: function(response, newValue) {
            return response.responseJSON.error[0];
        },
        success: function(response, newValue) {
            $("#status_name").val(response.success);

           
           // item_status= response.code;
           
        }

    });
    $('#description').editable({ 
        type: 'textarea',
        url: '{{url("/items/submit_edit/".$item->id)}}',
        name:'description',
        pk:'{{$item->id}}',
        rows:5,
        ajaxOptions: {
            headers:{
            'X-CSRF-TOKEN':'{{csrf_token()}}'
        }
       },  

    });
    @if($item_images)
    $('#img_commentary').editable({ 
        type: 'textarea',
        url: '{{url("/items/submit_edit/".$item->id)}}',
        name:'img_commentary',
        pk:'{{$item_images->id}}',
        rows:5,
        ajaxOptions: {
            headers:{
            'X-CSRF-TOKEN':'{{csrf_token()}}'
        }  
        }

    });
    @endif
    @if($can_designer && ($status->status->code=="S2" || $status->status->code=="S10"))
        var myDropzone= new Dropzone('.dropzone',{
            url:"{{route('item.load_imagen',['id'=>$item->id])}}",
            acceptedFiles:'image/*',
            maxFilesize: 2,
            maxFiles: 1,
            paramName:'imagen',
            headers:{
                'X-CSRF-TOKEN':'{{csrf_token()}}'
            }
        });
        myDropzone.on('error',function(file,res){
            
            var msg = res.errors.imagen[0];
           
            $('.dz-error-message:last > span').text(msg);
        });
        myDropzone.on('success',function(file,res){
            @if($status->status->code=="S10" )
                $("#image_id").val(res.image_id);
            @endif
            if($("#btn_load_image").hasClass( "hide" )){
                $("#btn_load_image").removeClass('hide');
            }

        });
        Dropzone.autoDiscover = false;
    @endif
   

});
</script>
@endpush