
@extends('loggedin.layout')

@section('content')

<div class="row">
    
        @if (session('message'))
        <div class="alert alert-success">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <strong>Successful Action!</strong><br><br>
            <ul>
                <li>{{ session('message') }}</li>
            </ul>
        </div>
        @endif
        @if (session('error'))
        <div class="alert alert-danger">
            <strong>Whoops!</strong> {{ session('error') }}<br>
        </div>
        @endif
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif


</div>
<div class="row">
    <div class="col-md-8">
        <div class="wrapper wrapper-content animated fadeInUp">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="m-b-md">
                                <h2>{{$item->name}} / {{$item->code}}</h2>
                            </div>
                            <dl class="dl-horizontal">
                                <dt>Status:</dt> <dd><span class="label">{{$status->status->name}}</span></dd>
                            </dl>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <dl class="dl-horizontal">
                                <dt>Material:</dt> <dd>{{$item->material->name}}</dd>
                                <dt>Mounting:</dt> <dd>{{$item->mounting->name}}</dd>
                                <dt>Feature:</dt> <dd>{{$item->feature->name}}</dd>
                                <dt>Size:</dt> <dd>{{$item->size->name}}</dd>
                                <dt>License:</dt> <dd>{{$item->character->license->name}}</dd>
                                <dt>Character :</dt> <dd>{{$item->character->name}}</dd>
                                @if($item->customer)
                                 <dt>Customer:</dt> <dd>{{$item->customer->name}}</dd>
                                @endif
                            </dl>
                        </div>
                        <div class="col-md-6" id="cluster_info">
                            <dl class="dl-horizontal" >
                                <dt>Created by:</dt> <dd>{{$item->user->name}}</dd>
                                <dt>Created at:</dt> <dd>{{$item->created_at->format('M d Y')}}</dd>
                                @if($users_license_name!=null && $users_license_name!='' )
                                    <dt>Lic. Coordinator:</dt> <dd>{{$users_license_name}}</dd>
                                @endif
                                @if($users_designer_name!=null && $users_designer_name!='' )
                                    <dt>Designer:</dt> <dd>{{$users_designer_name}}</dd>
                                @endif
                                @if($can_designer && $status->status->code!="S1")
                                    <dt>License Coordinator:</dt> <dd><a href="#" id="user_license" data-type="select2" data-title="Select License" data-value="{{$users_license_id}}"></a></dd>
                                @endif
                            </dl>
                            <div class="div_accion">
                                @if($can_send_designer && ($status->status->code=="S1" || $status->status->code=="S2" ))
                                  
                                        <a href='#'  data-toggle="modal" data-target="#modal_send_designer" class="btn btn-primary btn-xs">Send Mail to Designer</a>
                                   
                                @endif
                                @if($can_changed_status && $status->status->code=="S7" )
                                     
                                        <a href='#'  data-toggle="modal" data-target="#modal_change_status" class="btn btn-primary btn-xs">Change status</a>
                                    
                                @endif
                                @if($can_designer && $item_images && $item_images->rejected==0)
                                    
                                        <a href='{{url("/items/{$item->id}/sendLicense")}}' class="btn btn-primary btn-xs">Send For Internal Approval</a>
                                   
                                @endif
                                @if($can_license)
                                    
                                <a href='{{url("/items/{$item->id}/sendForSampling")}}' class="btn btn-primary btn-xs">Send For Sampling</a>                               
                                @endif
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        @if($can_change_description)
                            <div class="col-md-12">
                                <dl class="dl-horizontal">
                            
                                    <dt>Techpack:</dt>
                                    <dd><a href="#" id="techpack">{{$item->techpack}}</a></dd>
                            
                                </dl>
                            </div>
                            <div class="col-md-12">
                                <dl class="dl-horizontal">
                            
                                    <dt>Ba:</dt>
                                    <dd><a href="#" id="ba">{{$item->ba}}</a></dd>
                            
                                </dl>
                            </div>
                            <div class="col-md-12">
                                <dl class="dl-horizontal">
                            
                                    <dt>Upc:</dt>
                                    <dd><a href="#" id="upc">{{$item->upc}}</a></dd>
                            
                                </dl>
                            </div>
                        @endif
                        <div class="col-md-12">
                            <dl class="dl-horizontal">
                            @if($can_change_description)
                                <dt>Description:</dt>
                                <dd><a href="#" id="description">{{$item->description}}</a></dd>
                            
                            @elseif($item->description!=null && $item->description!='' )
                            <dt>Description:</dt>
                                <dd><div class="well">{{$item->description}}</div></dd>
                            @endif
                            </dl>
                        </div>
                        @if($status->commentary)
                            <div class="col-md-12">
                                <dl class="dl-horizontal">
                                    <dt>Status Comentary:</dt>
                                    <dd><div class="well">{{$status->commentary}}</div></dd>
                                </dl>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-4">
        <div class="wrapper wrapper-content animated fadeInUp">
            <div class="ibox">
                <div>
                    @if($item_images)
                        <div class="ibox-content no-padding border-left-right">
                            <a class="test-popup-link" href="{{url($item_images->path)}}">
                                <img alt="image" src="{{url($item_images->path)}}" class="img-responsive">
                            </a>
                        </div>
                        <div class="ibox-content profile-content">
                            <p><span>Autor: </span>{{$item_images->user->name}}</p>
                            @if($item_images->description)
                                <h5>
                                    Designer commentary:
                                </h5>
                                <p>
                                    {{$item_images->description}}
                                </p>
                            @endif
                            @if($status->status->code!="S7" && $item_images->commentary)
                                <h5>
                                    L. Coordinator commentary:
                                </h5>
                                <p>
                                    {{$item_images->commentary}}
                                </p>
                            @endif
                            @if($can_img_commentary && $status->status->code=="S7")
                                <h5>
                                    L. Coordinator commentary:
                                </h5>
                                <p>
                                    <a href="#" id="img_commentary">{{$item_images->commentary}}</a>
                                </p>
                            @endif
                        </div>
                    @endif
                    
                    
                    <div class="ibox-content">
                        <div class="user-button">
                            <div class="row">
                                @if($can_designer && $status->status->code!="S1" && $status->status->code!="S7")
                                    <div class="col-md-6">
                                        <a href='#'  data-toggle="modal" data-target="#modal_load_image" class="btn btn-primary btn-sm btn-block">Add Image</a>
                                    </div>
                                @endif
                                @if($old_images->count() > 0 )
                                    <div class="col-md-6">
                                    <a href='#'  data-toggle="modal" data-target="#modal_old_img" id="a_modal_old_img" class="btn btn-default btn-sm btn-block">Old Images</a>
                                    </div>
                                        
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if($can_designer && $status->status->code!="S1" && $status->status->code!="S7")
    @include('items.load_image')
@endif

@if($old_images->count() > 0 )
    @include('items.carousel')
@endif
@if($can_send_designer && ($status->status->code=="S1" || $status->status->code=="S2" ))
    @include('items.send_designer')
@endif
@if($can_changed_status && $status->status->code=="S7" )
    @include('items.change_status')
@endif

@endsection
@push('styles')
<link rel="stylesheet" href="{{ asset('css/plugins/select2/select2.css')}}">
<link rel="stylesheet" href="{{ asset('css/plugins/dropzone/dropzone.css')}}">
<link href="{{ asset('js/plugins/bootstrap3-editable/css/bootstrap-editable.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/plugins/select2/select2-bootstrap.css')}}">
<link rel="stylesheet" href="{{ asset('css/plugins/slick/slick.css')}}">
<link rel="stylesheet" href="{{ asset('css/plugins/slick/slick-theme.css')}}">
<link rel="stylesheet" href="{{ asset('plugins/magnific-popup/magnific-popup.css')}}">
<style>
.modal-footer {
    text-align: center;
}
.has-error .form-control {
    border:1px solid rgb(248, 148, 6) !important;
}
.has-error .select2-container{
    border:1px solid rgb(248, 148, 6) !important;
}
.has-error .control-label {
    color: rgb(103, 106, 108) !important;
} 
.ibox-content img{
    max-height: 280px;
    margin: 0 auto;
}
.ibox-content {
    border:none !important;
}
.ibox-conten:focus,
.slick-track:focus,
.slick_demo_1:focus,
.ibox:focus,
.col-lg-10:focus,
.col-lg-offset-1:focus,
.slick-slide:focus{
    outline: none !important;
}
.slider-commentary{
    margin-top:25px;
    color: #000!important;
    background-color:  #eee;;
    border: 1px solid #e5e6e7;
    border-radius: 1px;
    color: inherit;
    padding: 6px 12px;
    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
    width: 100%;
    font-size: 14px;ng: 0.01em 16px;
}
.slider-commentary span{
    font-weight: bold;
    font-style: italic;
    padding-right: 10px;
}
.div_accion{
    width:90%;
    margin: 0 auto;
}
.div_accion > a{
    margin-bottom: 3px;
}

</style>
@endpush
@push('scripts')
<script src="{{ asset('js/plugins/select2/select2.js') }}"></script>
<script src="{{asset('js/plugins/dropzone/dropzone.js')}}"></script>
<script src="{{asset('js/plugins/bootstrap3-editable/js/bootstrap-editable.min.js') }}"></script>
<script src="{{ asset('js/plugins/validate/jquery.validate.min.js') }}"></script>
<script src="{{asset('js/plugins/slick/slick.min.js')}}"></script>
<script src="{{ asset('plugins/magnific-popup/jquery.magnific-popup.min.js')}}"></script>

<script>
    var add_img = false;
    $( document ).ready(function() {
        @if($item_images)
            $('.test-popup-link').magnificPopup({
                        type: 'image'
                        // other options
            });
        @endif

        
       
        $('.slick_demo_1').slick();
        $('#modal_old_img').on('show.bs.modal',function(){
            $('.slick_demo_1').slick("refresh");
        });
        @if($old_images->count() > 1 )
            $('#modal_old_img').on('show.bs.modal',function(){
                $('.slick_demo_1').slick("refresh");
            });
        @endif
        $('#modal_load_image').on('show.bs.modal',function(){
            add_img = false;
        });
        $('#modal_load_image').on('hide.bs.modal',function(){
            if(add_img){
                location.reload();
            }
        });
        $(document).on('submit','form#form_load_image',function(e){ 
           
            @if($status->status->code!="S10" )
                e.preventDefault();
                location.reload();
            @endif
       
        });
        @if($can_change_description)

         $('#techpack').editable({ 
                type: 'text',
                url: '{{url("/items/submit_edit/".$item->id)}}',
                name:'techpack',
                pk:'{{$item->id}}',
                ajaxOptions: {
                    headers:{
                    'X-CSRF-TOKEN':'{{csrf_token()}}'
                    }
                },  
            });
        $('#ba').editable({ 
                type: 'text',
                url: '{{url("/items/submit_edit/".$item->id)}}',
                name:'ba',
                pk:'{{$item->id}}',
                ajaxOptions: {
                    headers:{
                    'X-CSRF-TOKEN':'{{csrf_token()}}'
                    }
                },  
            });
        $('#upc').editable({ 
                type: 'text',
                url: '{{url("/items/submit_edit/".$item->id)}}',
                name:'upc',
                pk:'{{$item->id}}',
                ajaxOptions: {
                    headers:{
                    'X-CSRF-TOKEN':'{{csrf_token()}}'
                    }
                },  
            });
        
       
            $('#description').editable({ 
                type: 'textarea',
                url: '{{url("/items/submit_edit/".$item->id)}}',
                name:'description',
                pk:'{{$item->id}}',
                rows:5,
                ajaxOptions: {
                    headers:{
                    'X-CSRF-TOKEN':'{{csrf_token()}}'
                    }
                },  
            });
        @endif
        @if($item_images && $can_img_commentary  && $status->status->code=="S7")
            $('#img_commentary').editable({ 
                type: 'textarea',
                url: '{{url("/items/submit_edit/".$item->id)}}',
                name:'img_commentary',
                pk:'{{$item_images->id}}',
                rows:5,
                ajaxOptions: {
                    headers:{
                    'X-CSRF-TOKEN':'{{csrf_token()}}'
                }  
                }

            });
        @endif
        @if($can_send_designer && ($status->status->code=="S1" || $status->status->code=="S2" ))
            @if($users_designer_name!='' && $users_designer_name!=null)
                if($("#btn_send_designer").hasClass( "hide" )){
                    $("#btn_send_designer").removeClass('hide');
                }
            @endif
            users_des=<?=json_encode($user_designer)?>;
            var source_des = $.map(users_des, function (obj) {
                obj.value = obj.id; // replace name with the property used for the text
                obj.text = obj.name;
                delete obj.name;
                delete obj.id;
                return obj;
            });
            $('#designer').editable({ 
            type: 'select2',
            url: '{{url("/items/submit_edit/".$item->id)}}',
            source: source_des,
            select2: {
            multiple: true,
            width: 200,
            },
            name:'designer',
            pk:'{{$item->id}}',
            ajaxOptions: {
                headers:{
                'X-CSRF-TOKEN':'{{csrf_token()}}'
            }
            },
            error: function(response, newValue) {
                    return response.responseJSON.error[0];
                },
                success: function(response, newValue) {
                    if($("#btn_send_designer").hasClass( "hide" )){
                        $("#btn_send_designer").removeClass('hide');
                    }
                    
                    //$("#status_name").val(response.success);
                    //item_status= response.code;
                }

            });
        @endif
        @if($can_designer && $status->status->code!="S1")
            users_lic=<?=json_encode($user_license_coordinator)?>;
            var source_lic = $.map(users_lic, function (obj) {
                obj.value = obj.id; // replace name with the property used for the text
                obj.text = obj.name;
                delete obj.name;
                delete obj.id;
                return obj;
            });
            $('#user_license').editable({ 
                type: 'select2',
                url: '{{url("/items/submit_edit/".$item->id)}}',
                source: source_lic,
                select2: {
                multiple: true,
                width: 200,
                },
                name:'user_lic',
                pk:'{{$item->id}}',
                ajaxOptions: {
                    headers:{
                    'X-CSRF-TOKEN':'{{csrf_token()}}'
                }
            },
            error: function(response, newValue) {
                    return response.responseJSON.error[0];
                },
                success: function(response, newValue) {
                    $("#status_name").val(response.success);

                
                // item_status= response.code;
                
                }

            });
        @endif
        @if($can_designer && $status->status->code!="S1" && $status->status->code!="S7")
            
            var myDropzone= new Dropzone('.dropzone',{
                url:"{{route('item.load_imagen',['id'=>$item->id])}}",
                acceptedFiles:'image/*',
                maxFilesize: 2,
                maxFiles: 1,
                paramName:'imagen',
                headers:{
                    'X-CSRF-TOKEN':'{{csrf_token()}}'
                }
            });
            myDropzone.on('error',function(file,res){
                
                var msg = res.errors.imagen[0];
            
                $('.dz-error-message:last > span').text(msg);
            });
            myDropzone.on('success',function(file,res){
                @if($status->status->code=="S10" )
                    $("#image_id").val(res.image_id);
                   
                @endif
                if($("#btn_load_image").hasClass( "hide" )){
                        $("#btn_load_image").removeClass('hide');
                }
                add_img=true;

            });
            Dropzone.autoDiscover = false;

                @if($status->status->code=="S10" )
                    $('form#form_load_image').validate({
                    rules: {
                        
                        img_description: {
                            required: true,
                        }
                    },
                    highlight: function (element, errorClass, validClass) {
                        var elem = $(element);
                        if (elem.hasClass("select2-hidden-accessible")) {
                            $("#select2-" + elem.attr("id") + "-container").parent().addClass(errorClass); 
                        } else {
                            elem.addClass(errorClass);
                        }
                        },    
                        unhighlight: function (element, errorClass, validClass) {
                            var elem = $(element);
                            if (elem.hasClass("select2-hidden-accessible")) {
                                $("#select2-" + elem.attr("id") + "-container").parent().removeClass(errorClass);
                            } else {
                                elem.removeClass(errorClass);
                            }
                        },
                        errorPlacement: function(error, element) {
                        var elem = $(element);
                        if (elem.hasClass("select2-hidden-accessible")) {
                            element = $("#select2-" + elem.attr("id") + "-container").parent(); 
                            error.insertAfter(element);
                        } else {
                            error.insertAfter(element);
                        }
                    }
                    });
                @endif

        @endif

        @if($can_changed_status && $status->status->code=="S7" )
            n_status=<?=json_encode($new_status)?>;
            var source_nstatus = $.map(n_status, function (obj) {
            
                obj.value = obj.id; // replace name with the property used for the text
                obj.text = obj.name;
                delete obj.name;
                delete obj.id;
                return obj;
            });
            $('#new_status').editable({
                source:  source_nstatus,
                url: '{{url("/items/submit_edit/".$item->id)}}',
                select2: {
                    width: 200,
                    placeholder: 'Select status',
                    allowClear: true
                },
                name:'new_status',
                pk:'{{$item->id}}',
                ajaxOptions: {
                    headers:{
                        'X-CSRF-TOKEN':'{{csrf_token()}}'
                    }
                },
                error: function(response, newValue) {
                    return response.responseJSON.error[0];
                },
                success: function(response, newValue) {
                    if($("#btn_change_status").hasClass( "hide" )){
                        $("#btn_change_status").removeClass('hide');
                    }
                //  $("#status_name").val(response.success);
                //item_status= response.code;
                }
            }); 
        @endif


    });
</script>
@endpush