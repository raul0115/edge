<div class="modal fade" id="modal_load_image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
            <form id="form_load_image" class="form-horizontal" method="POST" action='{{ url("items/$item->id/imgDescription") }}'>
                    {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Load Image</h4>
                </div>
                <div class="modal-body">

                   @if($can_designer && ($status->status->code=="S2" || $status->status->code=="S10"))
                    <div id="div-drop" class="form-group">
                        {{ Form::label('New Imagen', null, ['class' => 'col-md-4 col-xs-5  control-label']) }}
                        <div class="col-md-8 col-xs-7">
                        <div class="dropzone"></div>
                        </div>
                    </div>   
                    @endif
                    @if($status->status->code=="S10" )
                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                            {{Form::text('image_id',null,['id'=>'image_id','class' => 'hide'])}}
                            
                        </div>
                        <div class="form-group">
                            
                            
                            {{ Form::label('commentary', null, ['class' => 'col-md-2 col-sm-3 col-xs-4 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 control-label']) }}
                            <div class="col-md-6 col-sm-5 col-xs-6">
                            {{Form::textarea('img_description',null,['class' => 'form-control','rows'=>2])}}
                            </div>
                        </div>
                       
                    @endif
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button id="btn_load_image" type="submit" class="btn btn-primary hide">Save</button>
                </div>
            </div>
        </form>
    </div>
  </div>