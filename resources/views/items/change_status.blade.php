<div class="modal fade" id="modal_change_status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
            <form id="form_change_status" class="form-horizontal" method="POST" action='{{ url("items/{$item->id}/changeStatusCommentary") }}'>
                    {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Change Status</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        
                        {{ Form::label('new status', null, ['class' => 'col-md-4 col-xs-5 control-label']) }}
                        <div class="col-md-8 col-xs-7">
                        <a href="#" id="new_status" data-type="select2" data-title="Select Status" data-value=""></a>
                        </div>
                    </div>
                    <div class="form-group">
                        
                        {{ Form::label('commentary', null,['class' => 'col-md-4 col-xs-5 control-label']) }}
                        <div class="col-md-8 col-xs-7">
                        {{Form::textarea('commentary',null,['class' => 'form-control','rows'=>2])}}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button  id="btn_change_status" class="btn btn-primary hide" type="submit">Change Status</button>
                </div>
            </div>
        </form>
    </div>
  </div>