<div id='files_table' leftmargin=0 rightmargin=0 border='1'>
    @php 
    $admin_files =  null;
    if(auth()->user()->hasRole('Admin')) {
    $admin_files = true;
    }
    @endphp
    @if($files->count())
    <table class="table table-bordered"  style="width: auto; height: auto; font-size: 10px;">
        <tbody>
            <tr>
                <td style="">Comm Inv, Pking List</td>
                <td style="">
                     <table>
                        @forelse ($files as $file)
                            @if($file->column_type=='rec_comm_inv_pking')
                            <tr><td class="download_td"><!--<i class="fa fa-file-o"></i>--> <span class="download_link" onclick="download_file('{{ $file->server_name }}')">{{ $file->file_name }}</span> @if($admin_files)<i onclick="del_file('{{ $file->server_name }}',{{ $order_id }})" class="fa fa-trash del_file" aria-hidden="true"></i>@endif</td></tr>
                            @endif
                        @empty
                        <!--<tr><td>No Files found</td></tr>-->
                        @endforelse
                      </table>
                </td>
            </tr>
            <tr>
                <td>Original FCR</td>
                <td style="">
                     <table>
                         @forelse ($files as $file)
                            @if($file->column_type=='original_fcr')
                                <tr><td class="download_td"><!--<i class="fa fa-file-o"></i>--> <span class="download_link" onclick="download_file('{{ $file->server_name }}')">{{ $file->file_name }}</span> @if($admin_files)<i onclick="del_file('{{ $file->server_name }}',{{ $order_id }})" class="fa fa-trash del_file" aria-hidden="true"></i>@endif</td></tr>
                            @endif
                        @empty
                        <!--<tr><td>No Files found</td></tr>-->
                        @endforelse
                      </table>
                </td>
            </tr>
            <tr>
                <td>CTPAT form</td>
                <td style="">
                     <table>
                         @forelse ($files as $file)
                            @if($file->column_type=='ctpat_form')
                                <tr><td class="download_td"><!--<i class="fa fa-file-o"></i>--> <span class="download_link" onclick="download_file('{{ $file->server_name }}')">{{ $file->file_name }}</span> @if($admin_files)<i onclick="del_file('{{ $file->server_name }}',{{ $order_id }})" class="fa fa-trash del_file" aria-hidden="true"></i>@endif</td></tr>
                            @endif
                        @empty
                        <!--<tr><td>No Files found</td></tr>-->
                        @endforelse
                      </table>
                </td>
            </tr>
            <tr>
                <td>Telex Release B/L</td>
                <td style="">
                     <table>
                         @forelse ($files as $file)
                            @if($file->column_type=='telex_release')
                                <tr><td class="download_td"><!--<i class="fa fa-file-o"></i>--> <span class="download_link" onclick="download_file('{{ $file->server_name }}')">{{ $file->file_name }}</span> @if($admin_files)<i onclick="del_file('{{ $file->server_name }}',{{ $order_id }})" class="fa fa-trash del_file" aria-hidden="true"></i>@endif</td></tr>
                            @endif
                        @empty
                        <!--<tr><td>No Files found</td></tr>-->
                        @endforelse
                      </table>
                </td>
            </tr>
        </tbody>
    </table>
    @else
    No Files found
    @endif
</div>