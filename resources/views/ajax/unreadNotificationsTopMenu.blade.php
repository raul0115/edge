@php 
    function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
    'y' => 'year',
    'm' => 'month',
    'w' => 'week',
    'd' => 'day',
    'h' => 'hour',
    'i' => 'minute',
    's' => 'second',
    );
    foreach ($string as $k => &$v) {
    if ($diff->$k) {
    $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
    } else {
    unset($string[$k]);
    }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
    } 
@endphp
@if(auth()->user()->notifications)
                        @foreach(auth()->user()->unreadNotifications()->limit(4)->get() as $notification)
                        <li>
                            <!--<a href="profile.html">-->
                                <div class="alert alert-success">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
<!--                                    <i class="fa fa-twitter fa-fw"></i> -->{{ $notification->data['message'] }}
                                    <span class="pull-right text-muted small">{{ time_elapsed_string($notification->created_at) }}</span>
                                </div>
                            <!--</a>-->
                        </li>
                        @endforeach
                    @else
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i>  You have no notifications.
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                    @endif
