@extends('loggedin.layout')

@section('content')

@if (session('message'))

<div class="alert alert-success">
    <strong>Successful Action!</strong><br>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row">
<div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Customers Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_customers_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{  route('table.customer.post') }}">
                            <input type="hidden" name="table" value="customers">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_customers">
                            <input type="hidden"  id="table_customers_edit_id" name="edit_id" value="">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>code</label>
                                    <input id="table_customers_edit_code" name='code' type="text" value="" class="form-control" maxlength="2">
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_customers_edit_name" name='name' type="text" value="" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
        
                            <button class="btn btn-primary pull-right"  style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_customers_addnew' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{  route('table.customer.post') }}">
                            <input type="hidden" name="table" value="customers">{{ csrf_field() }}<input type="hidden" name="action" value="add_customers">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Code</label>
                                    <input name='code' type="text" placeholder="Enter  " class="form-control" maxlength="1">
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter Customer Name" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
        
                            <button class="btn btn-primary pull-right"  style="margin-top:22px;" type="submit"><strong>Add New Customer</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_customers_addnew');">Add New Customer</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group pull-right"><form class="form-horizontal" role="form" method="get" action="{{  route('table.customer.index') }}">
                                <input type="hidden" name="table" value='customers'> 
                                <input type="search" name="search_customers" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($customers as $customer)
                            <tr>
                                <td>{{ $customer->id }}</td>
                                <td>{{ $customer->code }}</td>
                                <td>{{ $customer->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_code_div('#table_customers_edit', {{ $customer->id }} , '{{ $customer->name }}','{{ $customer->code }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($customer->active == 1)
                                    <form id="delete_customer{{ $customer->id }}" method="POST" action="{{  route('table.customer.post') }}">
                                        <input type="hidden" name="delete_customer_id" value="{{ $customer->id }}"><input type="hidden" name="table" value="customers"><input type="hidden" name="action" value="delete_customers">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($customer->active == 0)
                                    <form id="activate_customer{{ $customer->id }}" method="POST" action="{{  route('table.customer.post') }}">
                                        <input type="hidden" name="activate_customer_id" value="{{ $customer->id }}"><input type="hidden" name="table" value="customers"><input type="hidden" name="action" value="activate_customers">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $customers->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
    
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/table.js') }}"></script>
@endpush