@extends('loggedin.layout')

@section('content')

@if (session('message'))

<div class="alert alert-success">
    <strong>Successful Action!</strong><br>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row">
<div class="col-lg-12">
       
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>characteres Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_characteres_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{route('table.character.post') }}">
                            <input type="hidden" name="table" value="characteres">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_characteres">
                            <input type="hidden"  id="table_characteres_edit_id" name="edit_id" value="">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>code</label>
                                    <input id="table_characteres_edit_code" name='code' type="text" value="" class="form-control" maxlength="2">
                                </div>
                           
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_characteres_edit_name" name='name' type="text" value="" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>License</label>
                                    {{ Form::select('license_id', [], null, ['id' => 'table_characteres_edit_license_id','class'=>'form-control select2','placeholder' => 'Seleccione']) }}
                                </div>
                           
                            </div>
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_characteres_addnew' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{route('table.character.post') }}">
                            <input type="hidden" name="table" value="characteres">{{ csrf_field() }}<input type="hidden" name="action" value="add_characteres">
                           
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Code</label>
                                    <input name='code' type="text" placeholder="Enter  " class="form-control" maxlength="2">
                                </div>
                           
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter  Name" class="form-control" maxlength="250">
                                </div>
                           
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>License</label>
                                    {{ Form::select('license_id', [], null, ['id' => 'character_license_id','class'=>'form-control select2','placeholder' => 'Seleccione']) }}
                                </div>
                               
                            </div>
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New Character</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_characteres_addnew');">Add New Character</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group pull-right"><form class="form-horizontal" role="form" method="get" action="{{route('table.character.index') }}">
                                <input type="hidden" name="table" value='characteres'>
                                <input type="search" name="search_characteres" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Lic</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($characteres as $character)
                            <tr>
                                <td>{{ $character->id }}</td>
                                 <td>{{ $character->code }}</td>
                                <td>{{ $character->name }}</td>
                                 <td>{{ $character->license->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_character_div('#table_characteres_edit', {{ $character->id }} , '{{ $character->name }}','{{$character->code}}','{{ $character->license_id }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($character->active == 1)
                                    <form id="delete_character{{ $character->id }}" method="POST" action="{{route('table.character.post') }}">
                                        <input type="hidden" name="delete_character_id" value="{{ $character->id }}"><input type="hidden" name="table" value="characteres"><input type="hidden" name="action" value="delete_characteres">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($character->active == 0)
                                    <form id="activate_character{{ $character->id }}" method="POST" action="{{route('table.character.post') }}">
                                        <input type="hidden" name="activate_character_id" value="{{ $character->id }}"><input type="hidden" name="table" value="characteres"><input type="hidden" name="action" value="activate_characteres">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $characteres->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/table.js') }}"></script>
<script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
<script>
    

    licenses=<?=json_encode($data_licenses)?>;
    var data_licenses = $.map(licenses, function (obj) {
        obj.text = obj.text || obj.name; // replace name with the property used for the text
        delete obj.name;
        delete obj.active;
        delete obj.code;
        delete obj.fama_require;
        return obj;
    });
    //console.log('materials',materials); 
    $('#character_license_id').select2({       
        width: "100%",
        tags: false,
        data: data_licenses
    });
    $('#table_characteres_edit_license_id').select2({       
        width: "100%",
        tags: false,
        data: data_licenses
    });

</script>
@endpush