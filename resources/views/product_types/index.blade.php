@extends('loggedin.layout')

@section('content')

@if (session('message'))

<div class="alert alert-success">
    <strong>Successful Action!</strong><br>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row">
<div class="col-lg-12">
       
        
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Product Types Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_product_types_edit' class="col-sm-12" style="display:none;" >
                        <form role="form" method="POST" action="{{route('table.product_type.post') }}">
                            <input type="hidden" name="table" value="product_types">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_product_types">
                            <input type="hidden"  id="table_product_types_edit_id" name="edit_id" value="">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_product_types_edit_name" name='name' type="text" value="" class="form-control" >
                                </div>
                           
                            </div>
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Edit</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_product_types_addnew' class="col-sm-12" style="display:none;" >
                        <form role="form" method="POST" action="{{route('table.product_type.post') }}">
                            <input type="hidden" name="table" value="product_types">
                            {{ csrf_field() }}
                            <input type="hidden" name="action" value="add_product_types">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name='name' type="text" placeholder="Enter Product Type" class p="form-control" >
                                </div>
                           
                            </div>
                            
                            <button class="btn btn-primary pull-right" style="margin-top:22px;" type="submit"><strong>Add New Product Type</strong></button>
                            <div class="clearfix"></div>
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_product_types_addnew');">Add New Product Type</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group pull-right"><form class="form-horizontal" role="form" method="get" action="{{route('table.product_type.index') }}">
                                <input type="hidden" name="table" value='product_types'> 
                                <input type="search" name="search_product_types" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($product_types as $product_type)
                            <tr>
                                <td>{{ $product_type->id }}</td>
                                <td>{{ $product_type->name }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_div('#table_product_types_edit', {{ $product_type->id }} , '{{ $product_type->name }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($product_type->active == 1)
                                    <form id="delete_product_type{{ $product_type->id }}" method="POST" action="{{route('table.product_type.post') }}">
                                        <input type="hidden" name="delete_product_type_id" value="{{ $product_type->id }}"><input type="hidden" name="table" value="product_types"><input type="hidden" name="action" value="delete_product_types">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($product_type->active == 0)
                                    <form id="activate_product_type{{ $product_type->id }}" method="POST" action="{{route('table.product_type.post') }}">
                                        <input type="hidden" name="activate_product_type_id" value="{{ $product_type->id }}"><input type="hidden" name="table" value="product_types"><input type="hidden" name="action" value="activate_product_types">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $product_types->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>
           
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/table.js') }}"></script>
@endpush