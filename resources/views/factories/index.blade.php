@extends('loggedin.layout')

@section('content')

@if (session('message'))

<div class="alert alert-success">
    <strong>Successful Action!</strong><br>
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Factories Table</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div id='table_factories_edit' class="col-sm-12" style="display:none;" >
                        <form  role="form" method="POST" action="{{ route('table.factory.post')}}">
                            <input type="hidden" name="table" value="factories">{{ csrf_field() }}
                            <input type="hidden" name="action" value="edit_factories">
                            <input type="hidden"  id="table_factories_edit_id" name="edit_id" value="">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_factories_edit_name" name='name' type="text" value="" class="form-control" >
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Vendor ID</label>
                                    <input id="table_factories_edit_vendor_id" name='vendor_id' type="text" value="" class="form-control" >
                                </div>
                               
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Full Name</label>
                                        <input id="table_factories_edit_full_name" name='full_name' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Address1</label>
                                        <input id="table_factories_edit_address1" name='address1' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Address2</label>
                                        <input id="table_factories_edit_address2" name='address2' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>City</label>
                                        <input id="table_factories_edit_city" name='city' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Province</label>
                                        <input id="table_factories_edit_province" name='province' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Zip</label>
                                        <input id="table_factories_edit_zip" name='zip' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Country</label>
                                        <input id="table_factories_edit_country" name='country' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input id="table_factories_edit_phone" name='phone' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bank Name</label>
                                        <input id="table_factories_edit_bank_name" name='bank_name' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bank Address</label>
                                        <input id="table_factories_edit_bank_address" name='bank_address' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bank Swift</label>
                                        <input id="table_factories_edit_bank_swift" name='bank_swift' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Usd Bank Acct</label>
                                        <input id="table_factories_edit_usd_bank_acct" name='usd_bank_acct' type="text" value="" class="form-control" >
                                    </div>
                                   
                            </div>
                            
                            
                            <button class="btn btn-primary pull-right" type="submit"><strong>Edit</strong></button>
                                   
                            
                            <div class="clearfix"></div>
                        
                           
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div id='table_factories_addnew' class="col-sm-12" style="display:none;" >
                        <form role="form" method="POST" action="{{ route('table.factory.post') }}">
                            <input type="hidden" name="table" value="factories">{{ csrf_field() }}<input type="hidden" name="action" value="add_factories">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input id="table_factories_edit_name" name='name' type="text" value="" class="form-control" >
                                </div>
                           
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Vendor ID</label>
                                    <input name='vendor_id' type="text" placeholder="Enter Vendor ID" class="form-control" >
                                </div>
                               
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Full Name</label>
                                        <input name='full_name' type="text" placeholder="Enter Full Name" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Address1</label>
                                        <input name='address1' type="text" placeholder="Enter Address1" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Address2</label>
                                        <input name='address2' type="text" placeholder="Enter Address2" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>City</label>
                                        <input name='city' type="text" placeholder="Enter City" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Province</label>
                                        <input name='province' type="text" placeholder="Enter Province" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Zip</label>
                                        <input name='zip' type="text" placeholder="Enter Zip" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Country</label>
                                        <input name='country' type="text" placeholder="Enter Country" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input name='phone' type="text" placeholder="Enter Phone" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bank Name</label>
                                        <input name='bank_name' type="text" placeholder="Enter Bank Name" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bank Address</label>
                                        <input name='bank_address' type="text" placeholder="Enter Bank Address" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bank Swift</label>
                                        <input name='bank_swift' type="text" placeholder="Enter Bank Swift" class="form-control" >
                                    </div>
                                   
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Usd Bank Acct</label>
                                        <input name='usd_bank_acct' type="text" placeholder="Enter Usd Bank Acct" class="form-control" >
                                    </div>
                                   
                            </div>
                            
                            <button class="btn btn-primary pull-right" type="submit"><strong>Add New Factory</strong></button>
                            <div class="clearfix"></div>
                            
                        </form> 
                    </div>
                    <br>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 pull-left">
                        <div class="input-group">
                            <button type="button" class='btn btn-sm btn-primary' onclick="display_addnew_div('#table_factories_addnew');">Add New Factory</button>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <div class="input-group pull-right"><form class="form-horizontal" role="form" method="get" action="{{ route('table.factory.index')  }}">
                                <input type="hidden" name="table" value='factories'> 
                                <input type="search" name="search_factories" class="form-control input-sm" placeholder="Search Name" aria-controls="example"> </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Vendor ID</th>
                                <th>Full Name</th>
                                <th>Address1</th>
                                <th>Address2</th>
                                <th>City</th>
                                <th>Province</th>
                                <th>Zip</th>
                                <th>Country</th>
                                <th>Phone</th>
                                <th>Bank Name</th>
                                <th>BAnk Address</th>
                                <th>Bank Swift</th>
                                <th>Usd Bank Acct</th>
                                <th>Edit</th>
                                <th>De/Activate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($factories as $factory)
                            <tr>
                                <td>{{ $factory->id }}</td>
                                <td>{{ $factory->name }}</td>
                                <td>{{ $factory->vendor_id }}</td>
                                <td>{{ $factory->full_name }}</td>
                                <td>{{ $factory->address1 }}</td>
                                <td>{{ $factory->address2 }}</td>
                                <td>{{ $factory->city }}</td>
                                <td>{{ $factory->province }}</td>
                                <td>{{ $factory->zip }}</td>
                                <td>{{ $factory->country }}</td>
                                <td>{{ $factory->phone }}</td>
                                <td>{{ $factory->bank_name }}</td>
                                <td>{{ $factory->bank_address }}</td>
                                <td>{{ $factory->bank_swift }}</td>
                                <td>{{ $factory->usd_bank_acct }}</td>
                                <td>
                                <button type="button" class='btn btn-xs btn-primary' onclick="display_edit_factory_div('#table_factories_edit', {{ $factory->id }} , '{{ $factory->name }}', '{{ $factory->vendor_id }}','{{ $factory->full_name }}','{{ $factory->address1 }}','{{ $factory->address2 }}','{{ $factory->city }}','{{ $factory->province }}','{{ $factory->zip }}','{{ $factory->country }}','{{ $factory->phone }}','{{ $factory->bank_name }}','{{ $factory->bank_address }}','{{ $factory->bank_swift }}','{{ $factory->usd_bank_acct }}' );">Edit</button>
                                </td>
                                <td>
                                    @if ($factory->active == 1)
                                    <form id="delete_factory{{ $factory->id }}" method="POST" action="{{  route('table.exportco.post') }}">
                                        <input type="hidden" name="delete_factory_id" value="{{ $factory->id }}"><input type="hidden" name="table" value="factories"><input type="hidden" name="action" value="delete_factories">{{ csrf_field() }}<button type="submit" class='btn-xs btn-danger'>Deactivate</button>
                                    </form>
                                    @elseif ($factory->active == 0)
                                    <form id="activate_factory{{ $factory->id }}" method="POST" action="{{  route('table.exportco.post') }}">
                                        <input type="hidden" name="activate_factory_id" value="{{ $factory->id }}"><input type="hidden" name="table" value="factories"><input type="hidden" name="action" value="activate_factories">{{ csrf_field() }}<button type="submit" class='btn-xs btn-primary'>Reactivate</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $factories->appends(array_except(Request::query(), ''))->links() }}
            </div>
        </div>
    </div>  
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/table.js') }}"></script>
@endpush