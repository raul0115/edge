<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ItemAssigned extends Model
{
    //protected $dates = ['deleted_at'];
    protected $table = 'item_assigned';
    
    protected $fillable = [
        'item_id',
        'active',
        'user_id',
        'tipo'
    ];
    
   
    public function item()
    {
        return $this->belongsTo('App\Item');
        
    }

    public function user()
    {
        return $this->belongsTo('App\User');
        
    }
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
}
