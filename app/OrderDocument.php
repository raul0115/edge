<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDocument extends Model
{
  
    protected $table = 'order_documents';
    protected $fillable = [
        'file_name',
        'server_name',
        'created_at',
        'updated_at',
        'order_id',
        'user_id',
        'column_type'

    ];
    
}