<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
  
    protected $table = 'product_types';
    protected $fillable = [
        'name',
        'active',
    ];
    public $timestamps = false;
    public function import_pos()
    {
        return $this->belongsToMany('App\ImportPo', 'product_types_select','product_types_id','order_id');
    }
    
}