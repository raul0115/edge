<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketOrdered extends Model
{
  
    protected $table = 'tickets_ordered';
    protected $dates=['tickets_ordered'];
    protected $fillable = [
        'order_id',
        'tickets_ordered',

    ];
    public $timestamps = false;
    
        public function import_po()
        {
            return $this->belongsTo('App\ImportPo', 'id', 'order_id' );
        }
}