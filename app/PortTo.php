<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PortTo extends Model
{
  
    protected $table = 'ports_to';
    protected $fillable = [
        'port_name',
        'days_est_arrival',
    ];
    public $timestamps = false;
    
}