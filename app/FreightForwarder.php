<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FreightForwarder extends Model
{
  
    protected $table = 'freight_forwarders';
    protected $fillable = [
        'name',
        'active',
    ];
    
}