<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ItemImage extends Model
{
    //protected $dates = ['deleted_at'];
    protected $table = 'item_images';
    
    protected $fillable = [
        'name',
        'item_id',
        'description',
        'file_name',
        'path',
        'version',
        'active',
        'user_id',
        'commentary',
        'commentary_by',
        'rejected'
    ];
    
   
    public function item()
    {
        return $this->belongsTo('App\Item');
        
    }

    public function user()
    {
        return $this->belongsTo('App\User');
        
    }
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
    public function scopeRejected($query)
    {
        return $query->where('rejected', 1);
    }
}
