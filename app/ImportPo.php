<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ImportPo extends Model
{

    protected $table = 'import_po';
    protected $fillable = [
        'comments',
        'po_number',
        'open',
        'value',
        'fty_produc',
        'export_cos',
        'customer_po_number',
        'qty_ordered',
        'tickets_needed',
        'shipping_from',
        'shipping_to',
        'cbm',
        'book_container',
        'safety_test',
        'safety_test_pass',
        'david_ins_result',
        'david_ins_comment',
        'svn_no',
        'freight_forwarder',
        'pro_photo',
        'receive_whse',
        'active',
        'svn_increment'
    ];

    public $timestamps = false;

    public function fama(){
        $db_res = DB::select('select `export_cos`.`name`, `export_cos`.`id`, `export_cos`.`fama` from (import_po) 
        left join `export_cos` on `import_po`.`export_cos` = `export_cos`.`id` where `import_po`.`id`  = ?',
            [$this->id]);
        if ($db_res[0]->fama == 0) {
            return 'No';
        } elseif ($db_res[0]->fama == 1){
            return 'Yes';
        } 
        return null;
    }
    public function product_styles()
    {
        return $this->belongsToMany('App\ProductStyle', 'product_styles_select', 'order_id', 'product_styles_id');
    }
    public function product_types()
    {
        return $this->belongsToMany('App\ProductType', 'product_types_select', 'order_id', 'product_types_id');
    }
    public function licenses()
    {
        return $this->belongsToMany('App\License', 'licenses_select', 'order_id', 'licenses_id');
    }
    public function customers()
    {
        return $this->belongsToMany('App\Customer', 'customers_select', 'order_id', 'customers_id');
    }
    public function salesmen()
    {
        return $this->belongsToMany('App\Salesman', 'salesmen_select', 'order_id', 'salesmen_id');
    }
    public function import_po_date(){
        return $this->hasOne('App\ImportPoDate', 'order_po_id', 'id');
    }
    public function ticket_ordered(){
        return $this->hasOne('App\TicketOrdered', 'order_id', 'id');
    }
    public function port_from()
    {
        return $this->belongsTo('App\Port', 'shipping_from','id' );
    }
    public function port_to()
    {
        return $this->belongsTo('App\PortTo', 'shipping_to', 'id' );
    }
    public function export_c()
    {
        return $this->belongsTo('App\ExportCos', 'export_cos', 'id' );
    }
    public function open_cerrada()
    {
        return ($this->open)?"Open":"Closed";
    }
    public function input_editable($area=false)
    {
        $no=['class' => 'form-control','disabled' =>'disabled' ];
        $si=['class' => 'form-control'];
        if($area){
            $no["rows"]=2;
            $si["rows"]=2;
        }
        return ($this->open)?$si:$no;
    }
    
    
}