<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
  
    protected $table = 'status';
    protected $fillable = [
        'name',
        'active',
        'code'
    ];

    public function item_status()
    {
        return $this->hasMany('App\ItemStatus');
    }
    
}