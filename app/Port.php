<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Port extends Model
{
  
    protected $table = 'ports';
    protected $fillable = [
        'name',
        'active',
    ];
    public $timestamps = false;
    
}