<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemStatus extends Model
{
  
    protected $table = 'item_status';
    protected $fillable = [
        'user_id',
        'item_id',
        'status_id',
        'status_prev',
        'active',
        'commentary',
        'send'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function item()
    {
        return $this->belongsTo('App\Item');
    }
    public function status()
    {
        return $this->belongsTo('App\Status');
    }
    public function previous()
    {
        return $this->belongsTo('App\ItemStatus','id','status_prev');
    }
    
}