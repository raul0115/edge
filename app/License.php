<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class License extends Model
{
  
    protected $table = 'licenses';
    protected $fillable = [
        'name',
        'active',
        'fama_require',
        'code'
    ];
    public $timestamps = false;
    public function import_pos()
    {
        return $this->belongsToMany('App\ImportPo', 'licenses_select','licenses_id','order_id');
    }
    public function fama(){
        if(!is_null($this->fama_require)){
           return ($this->fama_require)?'Yes':'No';
        }
        return null;
    }

    public function scopeSifama($query)
    {
        return $query->where('fama_require', 1);
    }
    public function scopeNofama($query)
    {
        return $query->where('fama_require', 0);
    }
    public function characteres(){
        return $this->hasMany('App\Character');
    }
}