<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salesman extends Model
{
  
    protected $table = 'salesmen';
    protected $fillable = [
        'name',
        'active',
    ];
    public $timestamps = false;
    
        public function import_pos()
        {
            return $this->belongsToMany('App\ImportPo', 'salesmen_select', 'salesmen_id','order_id' );
        }
    
}