<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
  
    protected $table = 'characteres';
    protected $fillable = [
        'name',
        'code',
        'active',
        'license_id'
    ];

    public function license()
    {
        return $this->belongsTo('App\License');
    }
    
}