<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mounting extends Model
{
  
    protected $table = 'mountings';
    protected $fillable = [
        'name',
        'code',
        'active',
    ];
    
}