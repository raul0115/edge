<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ItemNotification extends Notification
{
    use Queueable;
    protected $item;
    protected $tipo;
    protected $msg;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($item,$tipo,$msg)
    {
        $this->item= $item;
        $this->tipo= $tipo;
        $this->msg= $msg;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url =  $url = url('/items/edit/'.$this->item);
        if($this->tipo="Designer"){
            return (new MailMessage)
                        ->greeting('Hello!')
                        ->line($this->msg)
                        ->action('View Item',$url);
        }
        return (new MailMessage)
                        ->greeting('Hello!')
                        ->line($this->msg)
                        ->action('View Item',$url);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
