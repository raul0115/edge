<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    
    /**
     * Block inactive users according to the following via passport
     * https://medium.com/@mshanak/solved-tutorial-laravel-5-3-disable-enable-block-user-login-web-passport-oauth-4bfb74b0c810
     * 
     * @param type $identifier
     * @return type
     */
//    public function findForPassport($identifier) {
//        return User::orWhere('email', $identifier)->where('status', 1)->first();
//    }

    public function item_assigned()
    {
        return $this->hasMany('App\ItemAssigned');
    }

    public function mi_role(){
        $role= 'Inactive';
        $mis_roles = auth()->user()->getRoleNames()->toArray(); // Returns a collection
            if($mis_roles[0]!=null){
               $role= $mis_roles[0];
            }
            return $role;
    }

}
