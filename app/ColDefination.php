<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ColDefination extends Model
{
  
    protected $table = 'col_definations';
    public $timestamps = false;
    protected $fillable = [
        'col_name',
        'col_description',
        'Admin',
        'Production',
        'QC',
        'Sales',
        'Factory',
        'Custom',
        'List',
        'Order'
    ];
    
}