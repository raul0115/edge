<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDisplayRight extends Model
{
  
    protected $table = 'user_display_rights';
    protected $fillable = [
        'user_id',
        'order_id',
        'comments',
        'po_number',
        'open',
        'value',
        'fty_produc',
        'export_cos',
        'prod_style',
        'prod_type',
        'licenses',
        'customers',
        'customer_po_number',
        'date_placed',
        'qty_ordered',
        'tickets_needed',
        'tickets_ordered',
        'tickets_received',
        'shipping_from',
        'shipping_to',
        'finish_prod',
        'estimated_arr',
        'customer_cancel',
        'cbm',
        'book_container',
        'act_etd_forwarder',
        'act_eta_forwarder',
        'safety_test',
        'safety_test_on',
        'fty_prod_safety_test',
        'safety_test_pass',
        'david_ins_result',
        'david_ins_comment',
        'svn_no',
        'freight_forwarder',
        'rec_comm_inv_pking',
        'rec_comm_inv_pking_home',
        'original_fcr',
        'ctpat_form',
        'telex_release',
        'pro_photo',
        'okay_to_pay',
        'did_we_pay',
        'send_diann',
        'receive_whse',
        'salesmen',
        'files'
    ];
    
}