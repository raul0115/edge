<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Item extends Model
{
    //protected $dates = ['deleted_at'];
    protected $table = 'items';
    
    protected $fillable = [
        'name',
        'code',
        'description',
        'sequence',
        'material_id',
        'mounting_id',
        'feature_id',
        'size_id',
        'character_id',
        'customer_id',
        'user_id',
        'techpack',
        'ba',
        'upc',
        'sampling'
        
    ];
    
   
    public function material()
    {
        return $this->belongsTo('App\Material');
        
    }

    public function mounting()
    {
        return $this->belongsTo('App\Mounting');
        
    }

    public function feature()
    {
        return $this->belongsTo('App\Feature');
        
    }

    public function size()
    {
        return $this->belongsTo('App\Size');
        
    }

    public function groups()
    {
        return $this->belongsToMany('App\Group')->withTimestamps();
    }
    

    public function character()
    {
        return $this->belongsTo('App\Character');
        
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer');
        
    }
    public function user()
    {
        return $this->belongsTo('App\User');
        
    }
    public function item_images()
    {
        return $this->hasMany('App\ItemImage');
    }

    public function item_status()
    {
        return $this->hasMany('App\ItemStatus');
    }
    public function item_assigned()
    {
        return $this->hasMany('App\ItemAssigned');
    }
   
   
}
