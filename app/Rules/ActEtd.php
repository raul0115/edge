<?php

namespace App\Rules;
 
use Illuminate\Contracts\Validation\Rule;
use Carbon\Carbon;

class ActEtd implements Rule
{
   /**
    * Create a new rule instance.
    *
    * @return void
    */

    
   public function __construct()
   {
       
   }

   /**
    * Determine if the validation rule passes.
    *
    * @param  string  $attribute
    * @param  mixed  $value
    * @return bool
    */
   public function passes($attribute, $value)
   {
      
            $actual3= Carbon::now()->addDays(3);
            $etd = Carbon::parse($value);
            if($actual3<$etd){
                return true;
            }
                return false;

         
   }

   /**
    * Get the validation error message.
    *
    * @return string
    */
   public function message()
   {
       return 'Actu ETD higher than 3 days. Missing date';
   }
}