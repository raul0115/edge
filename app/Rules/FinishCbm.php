<?php

namespace App\Rules;
 
use Illuminate\Contracts\Validation\Rule;
use Carbon\Carbon;

class FinishCbm implements Rule
{
   /**
    * Create a new rule instance.
    *
    * @return void
    */

    private $finish;
   public function __construct($valor=null)
   {
       $this->finish=$valor;
   }

   /**
    * Determine if the validation rule passes.
    *
    * @param  string  $attribute
    * @param  mixed  $value
    * @return bool
    */
   public function passes($attribute, $value)
   {
       if($this->finish){
            $actual7= Carbon::now()->addDays(7);;
            $finish_prod = Carbon::parse($this->finish);
            if($actual7>=$finish_prod){
                return false;
            }
                return true;

        }
        return false;   
   }

   /**
    * Get the validation error message.
    *
    * @return string
    */
   public function message()
   {
       return 'CBM delay';
   }
}