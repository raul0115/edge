<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationSchedule extends Model
{
  
    protected $table = 'notification_schedule';
    protected $fillable = [
        'method_name',
        'order_id',
        'last_time_executed',
        'reexecute_hours'

    ];
    
}