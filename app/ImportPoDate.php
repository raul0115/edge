<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportPoDate extends Model
{
    protected $table = 'import_po_dates';
    protected $dates = 
    [
        'date_placed',
        'tickets_received',
        'finish_prod',
        'estimated_arr',
        'customer_cancel',
        'act_etd_forwarder',
        'act_eta_forwarder',
        'safety_test_on',
        'fty_prod_safety_test',
        'rec_comm_inv_pking',
        'rec_comm_inv_pking_home',
        'original_fcr',
        'ctpat_form',
        'telex_release',
        'okay_to_pay',
        'did_we_pay',
        'send_diann',
        'closed'
    ];
    protected $fillable = [
        'created_at',
        'updated_at',
        'order_po_id',
        'date_placed',
        'tickets_received',
        'finish_prod',
        'estimated_arr',
        'customer_cancel',
        'act_etd_forwarder',
        'act_eta_forwarder',
        'safety_test_on',
        'fty_prod_safety_test',
        'rec_comm_inv_pking',
        'rec_comm_inv_pking_home',
        'original_fcr',
        'ctpat_form',
        'telex_release',
        'okay_to_pay',
        'did_we_pay',
        'send_diann'
    ];
    //public $timestamps = false;

    public function import_po(){
        return $this->belongsTo('App\ImportPo', 'id', 'order_po_id');
    }
    
}