<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductStyle extends Model
{
  
    protected $table = 'product_styles';
    protected $fillable = [
        'name',
        'active',
    ];
    public $timestamps = false;

    public function import_pos()
    {
        return $this->belongsToMany('App\ImportPo', 'product_styles_select', 'product_styles_id','order_id' );
    }
}