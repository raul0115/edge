<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class GeneralController extends Controller
{

    /**
     * This function sets or updates the notification schedule
     * 
     * @param string $method_name Name of the method that should execute on requested time
     * @param int $order_id
     * @param int $reexecute_hours The amount of hours it should execute next time
     */
    public static function setNotificationSchedule($method_name, $order_id, $reexecute_hours) {
        
        $now = new \DateTime();
        
        $data = DB::table('notification_schedule')
                ->where('method_name', $method_name)
                ->where('order_id', $order_id)
                ->first();
        
        if(isset($data->method_name)) {
            
            DB::table('notification_schedule')
                ->where('method_name', $method_name)
                ->where('order_id', $order_id)
                ->update(['last_time_executed' => $now]);
            
        }
        else {
            //insert
            DB::table('notification_schedule')->insert([
                [   
                    'method_name'           => $method_name,
                    'order_id'              => $order_id,
                    'last_time_executed'    => $now,
                    'reexecute_hours'       => $reexecute_hours
                ]
            ]);
        }
    }
    
    
    /**
     * Gets the PO number by order ID
     * 
     * @param int $order_id
     * @return string
     */
    public static function getPoNumberById($order_id) {
        
        $res = DB::table('import_po')->select('po_number')->where('id', $order_id)->first();
        
        if($res){
            return $res->po_number;
        }
        
    }
    
   /**
    * The a value in the options table.
    * 
    * @param string $name
    * @param string $value Can be a string or serialized data
    */
    public static function setOption($name, $value = null) {
        //check if name exist
        $data = DB::table('options')->where('option_name', $name)->first();
        
        if (isset($data->option_name)) {
            //if exist then update
            DB::table('options')
                        ->where('id', $data->id)
                        ->update(['option_value' => $value]);
        }
        
        else {
                DB::table('options')->insertGetId([
                    'option_name' => $name,
                    'option_value' => $value
                ]);
        }
    }
    
    /**
     * Get the value of option by name
     * 
     * @param string $name
     * @return string option_value or null if not available
     */
    public static function getOption($name) {
        
        $data = DB::table('options')->where('option_name', $name)->first();
        
        if ($data) {
            return $data->option_value;
        } 
    }
  
    
    
    
    public static function time_elapsed_string($datetime, $full = false) {
        $now = new \DateTime;
        $ago = new \DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full)
            $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

}
