<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\UserController;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        if (Auth::check()) 
            {
            $user = Auth::user();
            //check if user has factory role
            if ($user->hasRole('Factory') and $fty_user = UserController::getFactoryUser($user->id)) {
                
                $data = $this->getOrderDashboardDataFactoryUser($fty_user->factory_id);
            }
            elseif($user->hasRole('Sales') and $sale_user = UserController::getSalesmenUser($user->id)){
                
                $data = $this->getOrderDashboardDataSalesUser($sale_user->salesmen_id);
            }
            else {
                $data = $this->getOrderDashboardData();
            }
            
            return view('loggedin.dashboard', ['data' => $data]);
            }
        else {
            return view('welcome');
        }
    }
    
    public function welcome()
    {
        if (Auth::check()) {
            $user = Auth::user();
            //check if user has factory role
            if ($user->hasRole('Factory')and $fty_user = UserController::getFactoryUser($user->id)) {
                
                $data = $this->getOrderDashboardDataFactoryUser($fty_user->factory_id);
            }
            elseif($user->hasRole('Sales') and $sale_user = UserController::getSalesmenUser($user->id)){
                
                $data = $this->getOrderDashboardDataSalesUser($sale_user->salesmen_id);
            }
            else {
                $data = $this->getOrderDashboardData();
            }
            return view('loggedin.dashboard', ['data' => $data]);
        }
        else {
            return view('welcome');
        }
    }
    
        
    private function getOrderDashboardData(){
        
//        $data['valueTotalCount']= DB::table('import_po')->sum('value');
//        $data['cbmTotalCount']= DB::table('import_po')->sum('cbm');
//        $data['qtyOrderedTotalCount'] = DB::table('import_po')->sum('qty_ordered');
//        $data['TotalCount']= DB::table('import_po')->count();
        
        //Open count
        $data['valueOpenCount']= DB::table('import_po')->where([
            'open' => 1,
            'active' => 1
        ])->sum('value');
        $data['cbmOpenCount']= DB::table('import_po')->where([
            'open' => 1,
            'active' => 1
        ])->sum('cbm');
        $data['qtyOrderedOpenCount'] = DB::table('import_po')->where([
            'open' => 1,
            'active' => 1
        ])->sum('qty_ordered');
        $data['OpenCount']= DB::table('import_po')->where([
            'open' => 1,
            'active' => 1
        ])->count();
        
        //Closed count
//        $data['valueClosedCount']= DB::table('import_po')->where('open', 0)->sum('value');
//        $data['cbmClosedCount']= DB::table('import_po')->where('open', 0)->sum('cbm');
//        $data['qtyOrderedClosedCount'] = DB::table('import_po')->where('open', 0)->sum('qty_ordered');
//        $data['ClosedCount']= DB::table('import_po')->where('open', 0)->count();
        
        $data['cronRunTime'] = $this->lastCronRunTime();
        
        return $data;

    }
    
    private function getOrderDashboardDataFactoryUser($factory_id){
        
//        $data['valueTotalCount']= DB::table('import_po')->where('fty_produc', $factory_id)->sum('value');
//        $data['cbmTotalCount']= DB::table('import_po')->where('fty_produc', $factory_id)->sum('cbm');
//        $data['qtyOrderedTotalCount'] = DB::table('import_po')->where('fty_produc', $factory_id)->sum('qty_ordered');
//        $data['TotalCount']= DB::table('import_po')->where('fty_produc', $factory_id)->count();
        
        //Open count
        $data['valueOpenCount']= DB::table('import_po')->where('open', 1)->where('fty_produc', $factory_id)->sum('value');
        $data['cbmOpenCount']= DB::table('import_po')->where('open', 1)->where('fty_produc', $factory_id)->sum('cbm');
        $data['qtyOrderedOpenCount'] = DB::table('import_po')->where('open', 1)->where('fty_produc', $factory_id)->sum('qty_ordered');
        $data['OpenCount']= DB::table('import_po')->where('open', 1)->where('fty_produc', $factory_id)->count();
        
        //Closed count
//        $data['valueClosedCount']= DB::table('import_po')->where('open', 0)->where('fty_produc', $factory_id)->sum('value');
//        $data['cbmClosedCount']= DB::table('import_po')->where('open', 0)->where('fty_produc', $factory_id)->sum('cbm');
//        $data['qtyOrderedClosedCount'] = DB::table('import_po')->where('open', 0)->where('fty_produc', $factory_id)->sum('qty_ordered');
//        $data['ClosedCount']= DB::table('import_po')->where('open', 0)->where('fty_produc', $factory_id)->count();
        
        $data['cronRunTime'] = null;
        
        return $data;

    }
    
    
    
    private function getOrderDashboardDataSalesUser($sales_id){
        
         
        //Open count
        $data['valueOpenCount']= DB::table('import_po')->where('open', 1)
                ->leftJoin('salesmen_select','import_po.id', '=', 'salesmen_select.order_id')
                ->leftJoin('salesmen','salesmen.id', '=', 'salesmen_select.salesmen_id')
                ->where('salesmen.id', $sales_id)->sum('value');
        
        $data['cbmOpenCount']= DB::table('import_po')->where('open', 1)
                ->leftJoin('salesmen_select','import_po.id', '=', 'salesmen_select.order_id')
                ->leftJoin('salesmen','salesmen.id', '=', 'salesmen_select.salesmen_id')
                ->where('salesmen.id', $sales_id)->sum('cbm');
        
        $data['qtyOrderedOpenCount'] = DB::table('import_po')->where('open', 1)
                ->leftJoin('salesmen_select','import_po.id', '=', 'salesmen_select.order_id')
                ->leftJoin('salesmen','salesmen.id', '=', 'salesmen_select.salesmen_id')
                ->where('salesmen.id', $sales_id)->sum('qty_ordered');
        
        $data['OpenCount']= DB::table('import_po')->where('open', 1)
                ->leftJoin('salesmen_select','import_po.id', '=', 'salesmen_select.order_id')
                ->leftJoin('salesmen','salesmen.id', '=', 'salesmen_select.salesmen_id')
                ->where('salesmen.id', $sales_id)->count();
        
        //Closed count
        $data['cronRunTime'] = null;
        
        return $data;

    }
    
    private function lastCronRunTime(){
        $lastruntime = \App\Http\Controllers\GeneralController::getOption('cronRunTime');
        if (!null) {
            return \App\Http\Controllers\GeneralController::time_elapsed_string($lastruntime);
        }
    }
    
    
}
