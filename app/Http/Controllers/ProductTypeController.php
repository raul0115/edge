<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DB;
use Log;
use Illuminate\Validation\Rule;

class ProductTypeController extends Controller {

    /**
     * Construct method makes sure that all pages here are accessed by 
     * registered users by using the auth middleware
     */
    public function __construct() {
        $this->middleware('auth');
        if (Auth::check()) {
            
        } else {
            return redirect('login');
        }
    }

    /**
     * Function to display table management page
     * 
     * @param Request $request
     * @return view
     */
    public function index(Request $request) {
        $user = Auth::user();
        
        if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
            
            return redirect('/home')->withErrors("Only Admin can access these settings.");
            
        }

      
        $product_types = $this->product_typesTable($request);
       
        $vista=view('product_types.index', 
        [
           
            'product_types' => $product_types,
            
            
        ]);
        if ($request->isMethod('post')) {
            $request->session()->flash('message', 'successful!');
        
        }
        return $vista;
    }
    
   
    
    /**
     * This function manages CRUD for product_types Table
     * 
     * @param object $request
     * @return types
     */
    private function product_typesTable($request){
        
        if(isset($request)) {
            
        //check if request has search product_types
            
            if ($request->search_product_types) {
                
                return $product_types = DB::table('product_types')->select('*')->where('name', 'like', "%$request->search_product_types%")->orderBy('id', 'desc')->paginate(10, ['*'], 'product_types');
            }
        
        //Edit
            if ($request->table == 'product_types' and $request->action  == 'edit_product_types') {
                
                $this->validate($request, [
                'name'          => 'required|string',
                'edit_id'       =>  'exists:product_types,id',
                ]);
                
                DB::table('product_types')->where('id', $request->edit_id)->update(['name' => $request->name]);

            }
        
        //Add
            if ($request->table == 'product_types' and $request->action  == 'add_product_types') {
            
                $this->validate($request, [
                    'name'          => 'required|string|unique:product_types,name',
                    'edit_id'       => 'exists:product_types,id',
                ]);
                
                DB::table('product_types')->insert(['name' => $request->name]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'product_types' and $request->action  == 'delete_product_types') {
            
                $this->validate($request, [
                    'delete_product_type_id'       => 'exists:product_types,id',
                ]);
                
                DB::table('product_types')->where('id', $request->delete_product_type_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'product_types' and $request->action  == 'activate_product_types') {
            
                $this->validate($request, [
                    'activate_product_type_id'       => 'exists:product_types,id',
                ]);
                
                DB::table('product_types')->where('id', $request->activate_product_type_id)->update(['active' => 1]);
         
            }
            
            
        }
        
        return $product_types = DB::table('product_types')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'product_types');
    }
    
    
    
}
