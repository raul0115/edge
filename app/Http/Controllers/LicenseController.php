<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\License;
use Carbon\Carbon;
use DB;
use Log;
use Illuminate\Validation\Rule;

class LicenseController extends Controller {

    /**
     * Construct method makes sure that all pages here are accessed by 
     * registered users by using the auth middleware
     */
    public function __construct() {
        $this->middleware('auth');
        if (Auth::check()) {
            
        } else {
            return redirect('login');
        }
    }

    /**
     * Function to display table management page
     * 
     * @param Request $request
     * @return view
     */
    public function index(Request $request) {
        $user = Auth::user();
        
        if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
            
            return redirect('/home')->withErrors("Only Admin can access these settings.");
            
        }

        $licenses = $this->licensesTable($request);
       
        $vista=view('licenses.index', 
        [
           
            'licenses' => $licenses,
            
            
        ]);
        if ($request->isMethod('post')) {
            $request->session()->flash('message', 'successful!');
        
        }
        return $vista;
    }
    
   
    
    /**
     * This function manages CRUD for licenses Table
     * 
     * @param object $request
     * @return types
     */
    private function licensesTable($request){
        
        if(isset($request)) {
            
        //check if request has search licenses
            
            if ($request->search_licenses) {
                
                return $licenses = DB::table('licenses')->select('*')->where('name', 'like', "%$request->search_licenses%")->orderBy('id', 'desc')->paginate(10, ['*'], 'licenses');
            }
        
        //Edit
            if ($request->table == 'licenses' and $request->action  == 'edit_licenses') {
                
                $this->validate($request, [
                'name'          => 'required|string|max:250',
                'fama_require'   => 'nullable|integer',
                'code' => [
                    'required','string','max:1',
                    Rule::unique('licenses')->ignore($request->edit_id)
                ],
                'edit_id'       =>  'exists:licenses,id',
                ]);
                
                DB::table('licenses')->where('id', $request->edit_id)->update([
                    'name' => $request->name,
                    'code'=> $request->code,
                    'fama_require'=>$request->fama_require
                    ]);

            }
        
        //Add
            if ($request->table == 'licenses' and $request->action  == 'add_licenses') {
                
                $this->validate($request, [
                    'name'          => 'required|string|unique:licenses,name|max:250',
                    'fama_require'       => 'nullable|integer',
                    'code'   => 'required|string|max:1|unique:licenses,code',
                ]);

                
                DB::table('licenses')->insert([
                    'name' => $request->name,
                    'fama_require'=>$request->fama_require,
                    'code'=> $request->code
                    ]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'licenses' and $request->action  == 'delete_licenses') {
            
                $this->validate($request, [
                    'delete_license_id'       => 'exists:licenses,id',
                ]);
                
                DB::table('licenses')->where('id', $request->delete_license_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'licenses' and $request->action  == 'activate_licenses') {
            
                $this->validate($request, [
                    'activate_license_id'       => 'exists:licenses,id',
                ]);
                
                DB::table('licenses')->where('id', $request->activate_license_id)->update(['active' => 1]);
         
            }
            
            
        }
        return $licenses= License::orderBy('id', 'desc')->paginate(10, ['*'], 'licenses');
        //return $licenses = DB::table('licenses')->select('*')->orderBy('id', 'desc')->paginate(100, ['*'], 'licenses');
    }
    
    
    
}
