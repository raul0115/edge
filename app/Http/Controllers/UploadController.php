<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Log;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    /**
     * Construct method makes sure that all pages here are accessed by 
     * registered users by using the auth middleware
     */    
    public function __construct()
    {   
        $this->middleware('auth');
        if (!Auth::check()) {return redirect('login');}
    }
    
    /**
     * A function to test raw uploads
     * 
     * @return view
     */
    public function uploadForm()
    {
        return view('loggedin.raw_upload_form');
    }
 
    /**
     * This function is used to receive documents (files) of the order
     * 
     * @param Request $request
     * @return type
     */
    public function uploadSubmit(Request $request)      
    {
          
        $this->validate($request, [
                'order_id' => 'required|numeric|exists:import_po,id',
                'file_upload_field' => 'required'
            ]);
            $message = [
                'required'=> "when uploading a file a type file must be selected"
            ];
            $this->validate($request, [
                'column_type'    => 'required|string'
            ],$message);
        
        
//        $product = Product::create($request->all());
        
        
        foreach ($request->file_upload_field as $upload_field) {
            
            $filename = $upload_field->store('Documents');
            //var_dump($upload_field);
        
            DB::table('order_documents')->insert(
                [
                    'column_type' => $request->column_type,
                    'file_name' => $upload_field->getClientOriginalName(),
                    'server_name' => $filename,
                    'order_id' => $request->order_id,
                    'user_id' => Auth::user()->id,
                    
                ]
            );
            
        }
        return back()->with('message', 'File/s uploaded successfully');

    }
    
    /**
     * This function is used to download files
     * 
     * @param string $dir Name of the dir
     * @param string $file_name name of the Server file name
     * @return type
     */
    public function downlaodFile($dir, $file_name){
        
        $a = DB::table('order_documents')
                ->where('server_name', $dir."/".$file_name)->first();
        
        //dd($a);
        //Log::info($dir."/".$file_name);
//        $this->validate($request, 
//                [
//                    'file_name' => 'required|exists:order_documents,server_name'
//                ]);
        
        return response()->download(storage_path("app/uploads/$dir/$file_name"), "$a->file_name");
    
        
    }
    
    /**
     * This function is used to delete order documents
     * 
     * @param string $dir Name of the dir
     * @param string $file_name name of the Server file name
     * @return type
     */
    public function deleteFile($dir, $file_name) {
        
        $user = Auth::user();
            
            if ($user->hasRole('Admin')) {
                
                $del = DB::table('order_documents')->where('server_name', "$dir/$file_name")->delete();
                
                
                    Storage::delete("$dir/$file_name");
                    return response()->json([   
                        'success' => 'DELETED'
                        ]);
                
            }
    }
    
}
