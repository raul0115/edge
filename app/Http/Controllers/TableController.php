<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Factory;
use App\ExportCos;
use App\License;
use App\Feature;
use App\Material;
use App\Size;
use App\Character;
use App\PortTo;
use Carbon\Carbon;
use App\Mounting;
use DB;
use Log;
use Illuminate\Validation\Rule;

class TableController extends Controller {

    /**
     * Construct method makes sure that all pages here are accessed by 
     * registered users by using the auth middleware
     */
    public function __construct() {
        $this->middleware('auth');
        if (Auth::check()) {
            
        } else {
            return redirect('login');
        }
    }

    /**
     * Function to display table management page
     * 
     * @param Request $request
     * @return view
     */
    public function index(Request $request) {
        $user = Auth::user();
        
        if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
            
            return redirect('/home')->withErrors("Only Admin can access these settings.");
            
        }

        $customers = $this->customersTable($request);
        $export_cos = $this->export_cosTable($request);
        $factories = $this->factoriesTable($request);
        $ports = $this->portsTable($request);
        $licenses = $this->licensesTable($request);
        $product_styles = $this->product_stylesTable($request);
        $product_types = $this->product_typesTable($request);
        $freight_forwarders = $this->freight_forwardersTable($request);
        $salesmen = $this->salesmenTable($request);
        $portsto = $this->portstoTable($request);
        $materials = $this->materialsTable($request);
        $sizes = $this->sizesTable($request);
        $features = $this->featuresTable($request);
        $characteres = $this->characteresTable($request);
        $mountings = $this->mountingsTable($request);
        $data_licenses = License::where('active',1)->get();
        $vista=view('loggedin.table', 
        [
            'table' => $request->table,
            'customers' => $customers,
            'export_cos'=> $export_cos,
            'factories'=> $factories,
            'ports' => $ports,
            'licenses' => $licenses,
            'product_styles' => $product_styles,
            'product_types' => $product_types,
            'freight_forwarders' => $freight_forwarders,
            'salesmen' => $salesmen,
            'portsto' => $portsto,
            'materials' => $materials,
            'sizes' => $sizes,
            'features' => $features,
            'characteres' => $characteres,
            'mountings' => $mountings,
            'data_licenses'=>$data_licenses
            
        ]);
        if ($request->isMethod('post')) {
            $request->session()->flash('message', 'successful!');
        
        }
        return $vista;
    }
    
    /**
     * This function manages CRUD for customers table
     * 
     * @param object $request
     * @return type
     */
    private function customersTable($request){
        
        if(isset($request)) {
            
        //check if request has search customers
            
            if ($request->search_customers) {
                
                return $customers = DB::table('customers')->select('*')->where('name', 'like', "%$request->search_customers%")->orderBy('id', 'desc')->paginate(10, ['*'], 'customers');
            }
        
        //Edit
            if ($request->table == 'customers' and $request->action  == 'edit_customers') {
                
                $this->validate($request, [
                'name'          => 'required|string|max:250',
                'code' => [
                    'required','string','max:2',
                    Rule::unique('customers')->ignore($request->edit_id)
                ],
                'edit_id'       =>  'exists:customers,id',
                ]);
                
                DB::table('customers')->where('id', $request->edit_id)->update([
                        'name' => $request->name,
                        'code'=> $request->code
                        ]
                );

            }
        
        //Add
            if ($request->table == 'customers' and $request->action  == 'add_customers') {
            
                $this->validate($request, [
                    'name'          => 'required|string|unique:customers,name|max:250',
                    'code'   => 'required|string|max:2|unique:customers,code',
                    'edit_id'       => 'exists:customers,id',
                ]);
                
                DB::table('customers')->insert([
                    'name' => $request->name,
                    'code'=> $request->code
                    ]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'customers' and $request->action  == 'delete_customers') {
            
                $this->validate($request, [
                    'delete_customer_id'       => 'exists:customers,id',
                ]);
                
                DB::table('customers')->where('id', $request->delete_customer_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'customers' and $request->action  == 'activate_customers') {
            
                $this->validate($request, [
                    'activate_customer_id'       => 'exists:customers,id',
                ]);
                
                DB::table('customers')->where('id', $request->activate_customer_id)->update(['active' => 1]);
         
            }
            
            
        }
        
        return $customers = DB::table('customers')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'customers');
    }
    
    
    
    /**
     * This function manages CRUD for export_cos Table
     * 
     * @param object $request
     * @return type
     */
    private function export_cosTable($request){
        
        if(isset($request)) {
            
        //check if request has search export_cos
            
            if ($request->search_export_cos) {
                
                return $export_cos = DB::table('export_cos')->select('*')->where('name', 'like', "%$request->search_export_cos%")->orderBy('id', 'desc')->paginate(10, ['*'], 'export_cos');
            }
        
        //Edit
            if ($request->table == 'export_cos' and $request->action  == 'edit_export_cos') {
                
                $this->validate($request, [
                'name'          => 'required|string',
                'fama'          => 'required|string',
                'edit_id'       =>  'exists:export_cos,id',
                'fama_exp_date' => 'nullable|date',
                ]);
                
                $export_cos=ExportCos::find( $request->edit_id);
                $export_cos->name=$request->name;
                $export_cos->fama=$request->fama;
                $export_cos->fama_exp_date=($request->fama_exp_date)?Carbon::parse($request->fama_exp_date):null;
                $export_cos->save();
                
                /*
                
                DB::table('export_cos')->where('id', $request->edit_id)->update([
                    'name' => $request->name, 
                    'fama' => $request->fama,
                    'fama_exp_date'=>$request->fama_exp_date]);
                    */

            }
        
        //Add
            if ($request->table == 'export_cos' and $request->action  == 'add_export_cos') {
            
                $this->validate($request, [
                    'name'          => 'required|string|unique:export_cos,name',
                    'fama'          => 'required|string',
                    'edit_id'       => 'exists:export_cos,id',
                    'fama_exp_date' => 'nullable|date',
                ]);
                $data= $request->all();
                ExportCos::create($data);
                
                //DB::table('export_cos')->insert(['name' => $request->name, 'fama' => $request->fama]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'export_cos' and $request->action  == 'delete_export_cos') {
            
                $this->validate($request, [
                    'delete_export_co_id'       => 'exists:export_cos,id',
                ]);
                
                DB::table('export_cos')->where('id', $request->delete_export_co_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'export_cos' and $request->action  == 'activate_export_cos') {
            
                $this->validate($request, [
                    'activate_export_co_id'       => 'exists:export_cos,id',
                ]);
                
                DB::table('export_cos')->where('id', $request->activate_export_co_id)->update(['active' => 1]);
         
            }
            
            
        }
        
        return $export_cos = ExportCos::orderBy('id', 'desc')->paginate(10, ['*'], 'export_cos');
    }
    
    
    
    /**
     * This function manages CRUD for factories Table
     * 
     * @param object $request
     * @return types
     */
    private function factoriesTable($request){
        
        if(isset($request)) {
            
        //check if request has search $factories
            
            if ($request->search_factories) {
                
                return $factories = DB::table('factories')->select('*')->where('name', 'like', "%$request->search_factories%")->orderBy('id', 'desc')->paginate(10, ['*'], 'factories');
            }
        
        //Edit
            if ($request->table == 'factories' and $request->action  == 'edit_factories') {
                
                $this->validate($request, [
                'name'          => 'required|string',
                'vendor_id'     => 'required|string',
                'edit_id'       =>  'exists:factories,id',
                'full_name'     => 'nullable|string',
                'address1'     => 'nullable|string',
                'address2'     => 'nullable|string',
                'city'     => 'nullable|string',
                'province'     => 'nullable|string',
                'zip'     => 'nullable|string',
                'country'     => 'nullable|string',
                'phone'     => 'nullable|string',
                'bank_address'     => 'nullable|string',
                'bank_name'     => 'nullable|string',
                'bank_swift'     => 'nullable|string',
                'usd_bank_acct'     => 'nullable|string',
                ]);
                
                DB::table('factories')->where('id', $request->edit_id)->update([
                    'name' => $request->name, 
                    'vendor_id' => $request->vendor_id,
                    'full_name'     => $request->full_name,
                    'address1'     => $request->address1,
                    'address2'     => $request->address2,
                    'city'     => $request->city,
                    'province'     => $request->province,
                    'zip'     => $request->zip,
                    'country'     => $request->country,
                    'phone'     => $request->phone,
                    'bank_address'     => $request->bank_address,
                    'bank_name'     => $request->bank_name,
                    'bank_swift'     => $request->bank_swift,
                    'usd_bank_acct'     => $request->usd_bank_acct,
                    ]);

            }
        
        //Add
            if ($request->table == 'factories' and $request->action  == 'add_factories') {
            
                $this->validate($request, [
                    'name'          => 'required|string|unique:factories,name',
                    'vendor_id'     => 'required|string',
                    'edit_id'       => 'exists:factories,id',
                    'full_name'     => 'nullable|string',
                    'address1'     => 'nullable|string',
                    'address2'     => 'nullable|string',
                    'city'     => 'nullable|string',
                    'province'     => 'nullable|string',
                    'zip'     => 'nullable|string',
                    'country'     => 'nullable|string',
                    'phone'     => 'nullable|string',
                    'bank_address'     => 'nullable|string',
                    'bank_name'     => 'nullable|string',
                    'bank_swift'     => 'nullable|string',
                    'usd_bank_acct'     => 'nullable|string',
                ]);

                DB::table('factories')->insert([
                    'name' => $request->name, 
                    'vendor_id' => $request->vendor_id,
                    'full_name'     => $request->full_name,
                    'address1'     => $request->address1,
                    'address2'     => $request->address2,
                    'city'     => $request->city,
                    'province'     => $request->province,
                    'zip'     => $request->zip,
                    'country'     => $request->country,
                    'phone'     => $request->phone,
                    'bank_address'     => $request->bank_address,
                    'bank_name'     => $request->bank_name,
                    'bank_swift'     => $request->bank_swift,
                    'usd_bank_acct'     => $request->usd_bank_acct,
                    ]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'factories' and $request->action  == 'delete_factories') {
            
                $this->validate($request, [
                    'delete_factory_id'       => 'exists:factories,id',
                ]);
                
                DB::table('factories')->where('id', $request->delete_factory_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'factories' and $request->action  == 'activate_factories') {
            
                $this->validate($request, [
                    'activate_factory_id'       => 'exists:factories,id',
                ]);
                
                DB::table('factories')->where('id', $request->activate_factory_id)->update(['active' => 1]);
         
            }
            
            
        }
        
        return $factories = DB::table('factories')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'factories');
    }
    
    
    
    /**
     * This function manages CRUD for ports Table
     * 
     * @param object $request
     * @return types
     */
    private function portsTable($request){
        
        if(isset($request)) {
            
        //check if request has search ports
            
            if ($request->search_ports) {
                
                return $ports = DB::table('ports')->select('*')->where('name', 'like', "%$request->search_ports%")->orderBy('id', 'desc')->paginate(10, ['*'], 'ports');
            }
        
        //Edit
            if ($request->table == 'ports' and $request->action  == 'edit_ports') {
                
                $this->validate($request, [
                'name'          => 'required|string',
                'edit_id'       =>  'exists:ports,id',
                ]);
                
                DB::table('ports')->where('id', $request->edit_id)->update(['name' => $request->name]);

            }
        
        //Add
            if ($request->table == 'ports' and $request->action  == 'add_ports') {
            
                $this->validate($request, [
                    'name'          => 'required|string|unique:ports,name',
                    'edit_id'       => 'exists:ports,id',
                ]);
                
                DB::table('ports')->insert(['name' => $request->name]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'ports' and $request->action  == 'delete_ports') {
            
                $this->validate($request, [
                    'delete_port_id'       => 'exists:ports,id',
                ]);
                
                DB::table('ports')->where('id', $request->delete_port_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'ports' and $request->action  == 'activate_ports') {
            
                $this->validate($request, [
                    'activate_port_id'       => 'exists:ports,id',
                ]);
                
                DB::table('ports')->where('id', $request->activate_port_id)->update(['active' => 1]);
         
            }
            
            
        }
        
        return $ports = DB::table('ports')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'ports');
    }
     /**
     * This function manages CRUD for ports_to Table
     * 
     * @param object $request
     * @return types
     */
    private function portstoTable($request){
       // var_dump($request->table);
        //var_dump($request->action);
        //die();
        if(isset($request)) {
            
        //check if request has search ports
            
            if ($request->search_portsto) {
                
                return $portsto = DB::table('ports_to')->select('*')->where('port_name', 'like', "%$request->search_portsto%")->orderBy('id', 'desc')->paginate(10, ['*'], 'ports_to');
            }
        
        //Edit
            if ($request->table == 'portsto' and $request->action  == 'edit_portsto') {
                
                $this->validate($request, [
                'port_name'          => 'required|string',
                'days_est_arrival'          => 'required|integer',
                'edit_id'       =>  'exists:ports_to,id',
                ]);
                
                DB::table('ports_to')
                        ->where('id', $request->edit_id)
                        ->update([
                            'port_name' => $request->port_name,
                            'days_est_arrival' => $request->days_est_arrival
                            ]);

            }
        
        //Add
            if ($request->table == 'portsto' and $request->action  == 'add_portsto') {
                
                $this->validate($request, [
                    'port_name'          => 'required|string|unique:ports_to,port_name',
                    'days_est_arrival'          => 'required|integer',
                ]);
                
                DB::table('ports_to')->insert([
                    'port_name' => $request->port_name,
                    'days_est_arrival' => $request->days_est_arrival
                    ]);
         
            }
            /*
        //Delete OR Deactivate
            if ($request->table == 'ports' and $request->action  == 'delete_ports') {
            
                $this->validate($request, [
                    'delete_port_id'       => 'exists:ports,id',
                ]);
                
                DB::table('ports')->where('id', $request->delete_port_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'ports' and $request->action  == 'activate_ports') {
            
                $this->validate($request, [
                    'activate_port_id'       => 'exists:ports,id',
                ]);
                
                DB::table('ports')->where('id', $request->activate_port_id)->update(['active' => 1]);
         
            }
            */
            
        }
        return $portsto= PortTo::orderBy('id', 'desc')->paginate(10, ['*'], 'ports_to');
        //return $portsto = DB::table('ports_to')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'ports');
    }
   
    

    
    /**
     * This function manages CRUD for licenses Table
     * 
     * @param object $request
     * @return types
     */
    private function licensesTable($request){
        
        if(isset($request)) {
            
        //check if request has search licenses
            
            if ($request->search_licenses) {
                
                return $licenses = DB::table('licenses')->select('*')->where('name', 'like', "%$request->search_licenses%")->orderBy('id', 'desc')->paginate(10, ['*'], 'licenses');
            }
        
        //Edit
            if ($request->table == 'licenses' and $request->action  == 'edit_licenses') {
                
                $this->validate($request, [
                'name'          => 'required|string|max:250',
                'fama_require'   => 'nullable|integer',
                'code' => [
                    'required','string','max:1',
                    Rule::unique('licenses')->ignore($request->edit_id)
                ],
                'edit_id'       =>  'exists:licenses,id',
                ]);
                
                DB::table('licenses')->where('id', $request->edit_id)->update([
                    'name' => $request->name,
                    'code'=> $request->code,
                    'fama_require'=>$request->fama_require
                    ]);

            }
        
        //Add
            if ($request->table == 'licenses' and $request->action  == 'add_licenses') {
                
                $this->validate($request, [
                    'name'          => 'required|string|unique:licenses,name|max:250',
                    'fama_require'       => 'nullable|integer',
                    'code'   => 'required|string|max:1|unique:licenses,code',
                ]);

                
                DB::table('licenses')->insert([
                    'name' => $request->name,
                    'fama_require'=>$request->fama_require,
                    'code'=> $request->code
                    ]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'licenses' and $request->action  == 'delete_licenses') {
            
                $this->validate($request, [
                    'delete_license_id'       => 'exists:licenses,id',
                ]);
                
                DB::table('licenses')->where('id', $request->delete_license_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'licenses' and $request->action  == 'activate_licenses') {
            
                $this->validate($request, [
                    'activate_license_id'       => 'exists:licenses,id',
                ]);
                
                DB::table('licenses')->where('id', $request->activate_license_id)->update(['active' => 1]);
         
            }
            
            
        }
        return $licenses= License::orderBy('id', 'desc')->paginate(10, ['*'], 'licenses');
        //return $licenses = DB::table('licenses')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'licenses');
    }
    
    
    
    /**
     * This function manages CRUD for product_styles Table
     * 
     * @param object $request
     * @return types
     */
    private function product_stylesTable($request){
        
        if(isset($request)) {
            
        //check if request has search product_styles
            
            if ($request->search_product_styles) {
                
                return $product_styles = DB::table('product_styles')->select('*')->where('name', 'like', "%$request->search_product_styles%")->orderBy('id', 'desc')->paginate(10, ['*'], 'product_styles');
            }
        
        //Edit
            if ($request->table == 'product_styles' and $request->action  == 'edit_product_styles') {
                
                $this->validate($request, [
                'name'          => 'required|string',
                'edit_id'       =>  'exists:product_styles,id',
                ]);
                
                DB::table('product_styles')->where('id', $request->edit_id)->update(['name' => $request->name]);

            }
        
        //Add
            if ($request->table == 'product_styles' and $request->action  == 'add_product_styles') {
            
                $this->validate($request, [
                    'name'          => 'required|string|unique:product_styles,name',
                    'edit_id'       => 'exists:product_styles,id',
                ]);
                
                DB::table('product_styles')->insert(['name' => $request->name]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'product_styles' and $request->action  == 'delete_product_styles') {
            
                $this->validate($request, [
                    'delete_product_style_id'       => 'exists:product_styles,id',
                ]);
                
                DB::table('product_styles')->where('id', $request->delete_product_style_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'product_styles' and $request->action  == 'activate_product_styles') {
            
                $this->validate($request, [
                    'activate_product_style_id'       => 'exists:product_styles,id',
                ]);
                
                DB::table('product_styles')->where('id', $request->activate_product_style_id)->update(['active' => 1]);
         
            }
            
            
        }
        
        return $product_styles = DB::table('product_styles')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'product_styles');
    }
    
    
    
    /**
     * This function manages CRUD for product_types Table
     * 
     * @param object $request
     * @return types
     */
    private function product_typesTable($request){
        
        if(isset($request)) {
            
        //check if request has search product_types
            
            if ($request->search_product_types) {
                
                return $product_types = DB::table('product_types')->select('*')->where('name', 'like', "%$request->search_product_types%")->orderBy('id', 'desc')->paginate(10, ['*'], 'product_types');
            }
        
        //Edit
            if ($request->table == 'product_types' and $request->action  == 'edit_product_types') {
                
                $this->validate($request, [
                'name'          => 'required|string',
                'edit_id'       =>  'exists:product_types,id',
                ]);
                
                DB::table('product_types')->where('id', $request->edit_id)->update(['name' => $request->name]);

            }
        
        //Add
            if ($request->table == 'product_types' and $request->action  == 'add_product_types') {
            
                $this->validate($request, [
                    'name'          => 'required|string|unique:product_types,name',
                    'edit_id'       => 'exists:product_types,id',
                ]);
                
                DB::table('product_types')->insert(['name' => $request->name]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'product_types' and $request->action  == 'delete_product_types') {
            
                $this->validate($request, [
                    'delete_product_type_id'       => 'exists:product_types,id',
                ]);
                
                DB::table('product_types')->where('id', $request->delete_product_type_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'product_types' and $request->action  == 'activate_product_types') {
            
                $this->validate($request, [
                    'activate_product_type_id'       => 'exists:product_types,id',
                ]);
                
                DB::table('product_types')->where('id', $request->activate_product_type_id)->update(['active' => 1]);
         
            }
            
            
        }
        
        return $product_types = DB::table('product_types')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'product_types');
    }
    
    
    
    
    /**
     * This function manages CRUD for freight_forwarders Table
     * 
     * @param object $request
     * @return types
     */
    private function freight_forwardersTable($request){
        
        if(isset($request)) {
            
        //check if request has search freight_forwarders
            
            if ($request->search_freight_forwarders) {
                
                return $freight_forwarders = DB::table('freight_forwarders')->select('*')->where('name', 'like', "%$request->search_freight_forwarders%")->orderBy('id', 'desc')->paginate(10, ['*'], 'freight_forwarders');
            }
        
        //Edit
            if ($request->table == 'freight_forwarders' and $request->action  == 'edit_freight_forwarders') {
                
                $this->validate($request, [
                'name'          => 'required|string',
                'edit_id'       =>  'exists:freight_forwarders,id',
                ]);
                
                DB::table('freight_forwarders')->where('id', $request->edit_id)->update(['name' => $request->name]);

            }
        
        //Add
            if ($request->table == 'freight_forwarders' and $request->action  == 'add_freight_forwarders') {
            
                $this->validate($request, [
                    'name'          => 'required|string|unique:freight_forwarders,name',
                    'edit_id'       => 'exists:freight_forwarders,id',
                ]);
                
                DB::table('freight_forwarders')->insert(['name' => $request->name]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'freight_forwarders' and $request->action  == 'delete_freight_forwarders') {
            
                $this->validate($request, [
                    'delete_freight_forwarder_id'       => 'exists:freight_forwarders,id',
                ]);
                
                DB::table('freight_forwarders')->where('id', $request->delete_freight_forwarder_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'freight_forwarders' and $request->action  == 'activate_freight_forwarders') {
            
                $this->validate($request, [
                    'activate_freight_forwarder_id'       => 'exists:freight_forwarders,id',
                ]);
                
                DB::table('freight_forwarders')->where('id', $request->activate_freight_forwarder_id)->update(['active' => 1]);
         
            }
            
            
        }
        
        return $freight_forwarders = DB::table('freight_forwarders')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'freight_forwarders');
    }
    
    
    /**
     * This function manages CRUD for salesmen Table
     * 
     * @param object $request
     * @return types
     */
    private function salesmenTable($request){
        
        if(isset($request)) {
            
        //check if request has search salesmen
            
            if ($request->search_salesmen) {
                
                return $salesmen = DB::table('salesmen')->select('*')->where('name', 'like', "%$request->search_salesmen%")->orderBy('id', 'desc')->paginate(10, ['*'], 'salesmen');
            }
        
        //Edit
            if ($request->table == 'salesmen' and $request->action  == 'edit_salesmen') {
                
                $this->validate($request, [
                'name'          => 'required|string',
                'edit_id'       =>  'exists:salesmen,id',
                ]);
                
                DB::table('salesmen')->where('id', $request->edit_id)->update(['name' => $request->name]);

            }
        
        //Add
            if ($request->table == 'salesmen' and $request->action  == 'add_salesmen') {
            
                $this->validate($request, [
                    'name'          => 'required|string|unique:salesmen,name',
                    'edit_id'       => 'exists:salesmen,id',
                ]);
                
                DB::table('salesmen')->insert(['name' => $request->name]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'salesmen' and $request->action  == 'delete_salesmen') {
            
                $this->validate($request, [
                    'delete_salesman_id'       => 'exists:salesmen,id',
                ]);
                
                DB::table('salesmen')->where('id', $request->delete_salesman_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'salesmen' and $request->action  == 'activate_salesmen') {
            
                $this->validate($request, [
                    'activate_salesman_id'       => 'exists:salesmen,id',
                ]);
                
                DB::table('salesmen')->where('id', $request->activate_salesman_id)->update(['active' => 1]);
         
            }
            
            
        }
        
        return $salesmen = DB::table('salesmen')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'salesmen');
    }
    
     /**
     * This function manages CRUD for materials Table
     * 
     * @param object $request
     * @return types
     */
    private function materialsTable($request){
        
        if(isset($request)) {
            
        //check if request has search materials
            
            if ($request->search_materials) {
                
                return $materials = DB::table('materials')->select('*')->where('name', 'like', "%$request->search_materials%")->orderBy('id', 'desc')->paginate(10, ['*'], 'materials');
            }
        
        //Edit
            if ($request->table == 'materials' and $request->action  == 'edit_materials') {
                
                $this->validate($request, [
                'name'          => 'required|string|max:250',
                'code' => [
                    'required','string','max:1',
                    Rule::unique('materials')->ignore($request->edit_id)
                ],
                'edit_id'       =>  'exists:materials,id',
                ]);
                
                Material::where('id', $request->edit_id)->update([
                    'name' => $request->name,
                    'code'=> $request->code,
                    ]);

            }
        
        //Add
            if ($request->table == 'materials' and $request->action  == 'add_materials') {
                
                $this->validate($request, [
                    'name'          => 'required|string|unique:materials,name|max:250',
                    'code'   => 'required|string|max:1|unique:materials,code',
                ]);

                
               Material::create([
                    'name' => $request->name,
                    'code'=> $request->code
                    ]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'materials' and $request->action  == 'delete_materials') {
            
                $this->validate($request, [
                    'delete_material_id'       => 'exists:materials,id',
                ]);
                
                DB::table('materials')->where('id', $request->delete_material_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'materials' and $request->action  == 'activate_materials') {
            
                $this->validate($request, [
                    'activate_material_id'       => 'exists:materials,id',
                ]);
                
                DB::table('materials')->where('id', $request->activate_material_id)->update(['active' => 1]);
         
            }
            
            
        }
        return $materials= material::orderBy('id', 'desc')->paginate(10, ['*'], 'materials');
        //return $materials = DB::table('materials')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'materials');
    }
     /**
     * This function manages CRUD for sizes Table
     * 
     * @param object $request
     * @return types
     */
    private function sizesTable($request){
        
        if(isset($request)) {
            
        //check if request has search sizes
            
            if ($request->search_sizes) {
                
                return $sizes = DB::table('sizes')->select('*')->where('name', 'like', "%$request->search_sizes%")->orderBy('id', 'desc')->paginate(10, ['*'], 'sizes');
            }
        
        //Edit
            if ($request->table == 'sizes' and $request->action  == 'edit_sizes') {
                
                $this->validate($request, [
                'name'          => 'required|string|max:250',
                'code' => [
                    'required','string','max:2',
                    Rule::unique('sizes')->ignore($request->edit_id)
                ],
                'edit_id'       =>  'exists:sizes,id',
                ]);
                
               Size::where('id', $request->edit_id)->update([
                    'name' => $request->name,
                    'code'=> $request->code,
                    ]);

            }
        
        //Add
            if ($request->table == 'sizes' and $request->action  == 'add_sizes') {
                
                $this->validate($request, [
                    'name'          => 'required|string|unique:sizes,name|max:250',
                    'code'   => 'required|string|max:2|unique:sizes,code',
                ]);

                
                Size::create([
                    'name' => $request->name,
                    'code'=> $request->code
                    ]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'sizes' and $request->action  == 'delete_sizes') {
            
                $this->validate($request, [
                    'delete_size_id'       => 'exists:sizes,id',
                ]);
                
                DB::table('sizes')->where('id', $request->delete_size_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'sizes' and $request->action  == 'activate_sizes') {
            
                $this->validate($request, [
                    'activate_size_id'       => 'exists:sizes,id',
                ]);
                
                DB::table('sizes')->where('id', $request->activate_size_id)->update(['active' => 1]);
         
            }
            
            
        }
        return $sizes= size::orderBy('id', 'desc')->paginate(10, ['*'], 'sizes');
        //return $sizes = DB::table('sizes')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'sizes');
    }

     /**
     * This function manages CRUD for features Table
     * 
     * @param object $request
     * @return types
     */
    private function featuresTable($request){
        
        if(isset($request)) {
            
        //check if request has search features
            
            if ($request->search_features) {
                
                return $features = DB::table('features')->select('*')->where('name', 'like', "%$request->search_features%")->orderBy('id', 'desc')->paginate(10, ['*'], 'features');
            }
        
        //Edit
            if ($request->table == 'features' and $request->action  == 'edit_features') {
                
                $this->validate($request, [
                'name'          => 'required|string|max:250',
                'code' => [
                    'required','string','max:1',
                    Rule::unique('features')->ignore($request->edit_id)
                ],
                'edit_id'       =>  'exists:features,id',
                ]);
                
                Feature::where('id', $request->edit_id)->update([
                    'name' => $request->name,
                    'code'=> $request->code,
                    ]);

            }
        
        //Add
            if ($request->table == 'features' and $request->action  == 'add_features') {
                
                $this->validate($request, [
                    'name'          => 'required|string|unique:features,name|max:250',
                    'code'   => 'required|string|max:1|unique:features,code',
                ]);

                
                Feature::create([
                    'name' => $request->name,
                    'code'=> $request->code
                    ]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'features' and $request->action  == 'delete_features') {
            
                $this->validate($request, [
                    'delete_feature_id'       => 'exists:features,id',
                ]);
                
                DB::table('features')->where('id', $request->delete_feature_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'features' and $request->action  == 'activate_features') {
            
                $this->validate($request, [
                    'activate_feature_id'       => 'exists:features,id',
                ]);
                
                DB::table('features')->where('id', $request->activate_feature_id)->update(['active' => 1]);
         
            }
            
            
        }
        return $features= feature::orderBy('id', 'desc')->paginate(10, ['*'], 'features');
        //return $features = DB::table('features')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'features');
    }
    /**
     * This function manages CRUD for characteres Table
     * 
     * @param object $request
     * @return types
     */
    private function characteresTable($request){
        
        if(isset($request)) {
            
        //check if request has search characteres
            
            if ($request->search_characteres) {
                
                return $characteres = Character::where('name', 'like', "%$request->search_characteres%")->orderBy('id', 'desc')->paginate(10, ['*'], 'characteres');
            }
        
        //Edit
            if ($request->table == 'characteres' and $request->action  == 'edit_characteres') {
                
                $this->validate($request, [
                'name'          => 'required|string|max:250',
                'code' => [
                    'required','string','max:2',
                    Rule::unique('characteres')->ignore($request->edit_id)
                ],
                'license_id'       =>  'required|integer|exists:licenses,id',
                'edit_id'       =>  'exists:characteres,id',
                ]);
                
                Character::where('id', $request->edit_id)->update([
                    'name' => $request->name,
                    'code'=> $request->code,
                    'license_id'=> $request->license_id,
                    ]);

            }
        
        //Add
            if ($request->table == 'characteres' and $request->action  == 'add_characteres') {
                
                $this->validate($request, [
                    'name'          => 'required|string|unique:characteres,name|max:250',
                    'license_id'       =>  'required|integer|exists:licenses,id',
                    'code'   => 'required|string|max:2|unique:characteres,code',
                ]);

                
                Character::create([
                    'name' => $request->name,
                    'code'=> $request->code,
                    'license_id'=> $request->license_id,
                    ]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'characteres' and $request->action  == 'delete_characteres') {
            
                $this->validate($request, [
                    'delete_character_id'       => 'exists:characteres,id',
                ]);
                
                DB::table('characteres')->where('id', $request->delete_character_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'characteres' and $request->action  == 'activate_characteres') {
            
                $this->validate($request, [
                    'activate_character_id'       => 'exists:characteres,id',
                ]);
                
                DB::table('characteres')->where('id', $request->activate_character_id)->update(['active' => 1]);
         
            }
            
            
        }
        return $characteres= character::with('license')->orderBy('id', 'desc')->paginate(10, ['*'], 'characteres');
        //return $characteres = DB::table('characteres')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'characteres');
    }

    /**
     * This function manages CRUD for mountings Table
     * 
     * @param object $request
     * @return types
     */
    private function mountingsTable($request){
        
        if(isset($request)) {
            
        //check if request has search mountings
            
            if ($request->search_mountings) {
                
                return $mountings = DB::table('mountings')->select('*')->where('name', 'like', "%$request->search_mountings%")->orderBy('id', 'desc')->paginate(10, ['*'], 'mountings');
            }
        
        //Edit
            if ($request->table == 'mountings' and $request->action  == 'edit_mountings') {
                
                $this->validate($request, [
                'name'          => 'required|string|max:250',
                'code' => [
                    'required','string','max:1',
                    Rule::unique('mountings')->ignore($request->edit_id)
                ],
                'edit_id'       =>  'exists:mountings,id',
                ]);
                
                Mounting::where('id', $request->edit_id)->update([
                    'name' => $request->name,
                    'code'=> $request->code,
                    ]);

            }
        
        //Add
            if ($request->table == 'mountings' and $request->action  == 'add_mountings') {
                
                $this->validate($request, [
                    'name'          => 'required|string|unique:mountings,name|max:250',
                    'code'   => 'required|string|max:1|unique:mountings,code',
                ]);

                
                Mounting::create([
                    'name' => $request->name,
                    'code'=> $request->code,
                    ]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'mountings' and $request->action  == 'delete_mountings') {
            
                $this->validate($request, [
                    'delete_mounting_id'       => 'exists:mountings,id',
                ]);
                
                DB::table('mountings')->where('id', $request->delete_mounting_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'mountings' and $request->action  == 'activate_mountings') {
            
                $this->validate($request, [
                    'activate_mounting_id'       => 'exists:mountings,id',
                ]);
                
                DB::table('mountings')->where('id', $request->activate_mounting_id)->update(['active' => 1]);
         
            }
            
            
        }
        return $mountings= Mounting::orderBy('id', 'desc')->paginate(10, ['*'], 'mountings');
        //return $mountings = DB::table('mountings')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'mountings');
    }
    
    
}
