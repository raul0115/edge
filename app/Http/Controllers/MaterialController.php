<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Material;
use Carbon\Carbon;
use DB;
use Log;
use Illuminate\Validation\Rule;

class MaterialController extends Controller {

    /**
     * Construct method makes sure that all pages here are accessed by 
     * registered users by using the auth middleware
     */
    public function __construct() {
        $this->middleware('auth');
        if (Auth::check()) {
            
        } else {
            return redirect('login');
        }
    }

    /**
     * Function to display table management page
     * 
     * @param Request $request
     * @return view
     */
    public function index(Request $request) {
        $user = Auth::user();
        
        if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
            
            return redirect('/home')->withErrors("Only Admin can access these settings.");
            
        }

        $materials = $this->materialsTable($request);
       
        $vista=view('materials.index', 
        [
           
            'materials' => $materials,
            
        ]);
        if ($request->isMethod('post')) {
            $request->session()->flash('message', 'successful!');
        
        }
        return $vista;
    }
   
    
     /**
     * This function manages CRUD for materials Table
     * 
     * @param object $request
     * @return types
     */
    private function materialsTable($request){
        
        if(isset($request)) {
            
        //check if request has search materials
            
            if ($request->search_materials) {
                
                return $materials = DB::table('materials')->select('*')->where('name', 'like', "%$request->search_materials%")->orderBy('id', 'desc')->paginate(10, ['*'], 'materials');
            }
        
        //Edit
            if ($request->table == 'materials' and $request->action  == 'edit_materials') {
                
                $this->validate($request, [
                'name'          => 'required|string|max:250',
                'code' => [
                    'required','string','max:1',
                    Rule::unique('materials')->ignore($request->edit_id)
                ],
                'edit_id'       =>  'exists:materials,id',
                ]);
                
                Material::where('id', $request->edit_id)->update([
                    'name' => $request->name,
                    'code'=> $request->code,
                    ]);

            }
        
        //Add
            if ($request->table == 'materials' and $request->action  == 'add_materials') {
                
                $this->validate($request, [
                    'name'          => 'required|string|unique:materials,name|max:250',
                    'code'   => 'required|string|max:1|unique:materials,code',
                ]);

                
               Material::create([
                    'name' => $request->name,
                    'code'=> $request->code
                    ]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'materials' and $request->action  == 'delete_materials') {
            
                $this->validate($request, [
                    'delete_material_id'       => 'exists:materials,id',
                ]);
                
                DB::table('materials')->where('id', $request->delete_material_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'materials' and $request->action  == 'activate_materials') {
            
                $this->validate($request, [
                    'activate_material_id'       => 'exists:materials,id',
                ]);
                
                DB::table('materials')->where('id', $request->activate_material_id)->update(['active' => 1]);
         
            }
            
            
        }
        return $materials= material::orderBy('id', 'desc')->paginate(10, ['*'], 'materials');
        //return $materials = DB::table('materials')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'materials');
    }
    
}
