<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;


class MailController extends Controller
{

    //testing functions created to test mail
    
    //sending without mailable class
    public function send_mail() {
        $emails = ['waqas@localhost.com'];

        Mail::send('mail.test', [], function($message) use ($emails) {
            $message->to($emails)->subject('This is test e-mail');
        });
        var_dump(Mail:: failures());
        exit;
    }

    //working with mailable class
    public function send_mail2() {
        $emails = ['waqas@localhost.com'];
        
        Mail::to($emails)->send(new \App\Mail\TestMail());

       
    }
}