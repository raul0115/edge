<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\License;
use App\Character;
use Carbon\Carbon;
use DB;
use Log;
use Illuminate\Validation\Rule;

class CharacterController extends Controller {

    /**
     * Construct method makes sure that all pages here are accessed by 
     * registered users by using the auth middleware
     */
    public function __construct() {
        $this->middleware('auth');
        if (Auth::check()) {
            
        } else {
            return redirect('login');
        }
    }

    /**
     * Function to display table management page
     * 
     * @param Request $request
     * @return view
     */
    public function index(Request $request) {
        $user = Auth::user();
        
        if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
            
            return redirect('/home')->withErrors("Only Admin can access these settings.");
            
        }

      
        $characteres = $this->characteresTable($request);
        $data_licenses = License::where('active',1)->get();
        $vista=view('characteres.index', 
        [
           
            'characteres' => $characteres,
            'data_licenses'=>$data_licenses
            
        ]);
        
        if ($request->isMethod('post')) {
            $request->session()->flash('message', 'successful!');
        
        }
        return $vista;

    }
    
    
    /**
     * This function manages CRUD for characteres Table
     * 
     * @param object $request
     * @return types
     */
    private function characteresTable($request){
        
        if(isset($request)) {
            
        //check if request has search characteres
            
            if ($request->search_characteres) {
                
                return $characteres = Character::where('name', 'like', "%$request->search_characteres%")->orderBy('id', 'desc')->paginate(10, ['*'], 'characteres');
            }
        
        //Edit
            if ($request->table == 'characteres' and $request->action  == 'edit_characteres') {
                
                $this->validate($request, [
                'name'          => 'required|string|max:250',
                'code' => [
                    'required','string','max:2',
                    Rule::unique('characteres')->ignore($request->edit_id)
                ],
                'license_id'       =>  'required|integer|exists:licenses,id',
                'edit_id'       =>  'exists:characteres,id',
                ]);
                
                Character::where('id', $request->edit_id)->update([
                    'name' => $request->name,
                    'code'=> $request->code,
                    'license_id'=> $request->license_id,
                    ]);

            }
        
        //Add
            if ($request->table == 'characteres' and $request->action  == 'add_characteres') {
                
                $this->validate($request, [
                    'name'          => 'required|string|unique:characteres,name|max:250',
                    'license_id'       =>  'required|integer|exists:licenses,id',
                    'code'   => 'required|string|max:2|unique:characteres,code',
                ]);

                
                Character::create([
                    'name' => $request->name,
                    'code'=> $request->code,
                    'license_id'=> $request->license_id,
                    ]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'characteres' and $request->action  == 'delete_characteres') {
            
                $this->validate($request, [
                    'delete_character_id'       => 'exists:characteres,id',
                ]);
                
                DB::table('characteres')->where('id', $request->delete_character_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'characteres' and $request->action  == 'activate_characteres') {
            
                $this->validate($request, [
                    'activate_character_id'       => 'exists:characteres,id',
                ]);
                
                DB::table('characteres')->where('id', $request->activate_character_id)->update(['active' => 1]);
         
            }
            
            
        }
        return $characteres= character::with('license')->orderBy('id', 'desc')->paginate(10, ['*'], 'characteres');
        //return $characteres = DB::table('characteres')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'characteres');
    }

    
}
