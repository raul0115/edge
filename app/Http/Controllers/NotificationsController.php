<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;
use DB;
use App\User;
use App\ImportPo;
use App\Http\Controllers\TriggerEventsController;
use Illuminate\Support\Facades\Log;
use App\Notifications\EstimatedArrival;
use App\Notifications\ItemNotification;
use App\Notifications\LicensingConditions;
use App\Notifications\DeliveryConditions;
use App\Notifications\SafetyTestingConditionOne;
use App\Notifications\SafetyTestingConditionTwo;

class NotificationsController extends Controller
{
    
   /**
    * This is function to display the notification page
    * 
    * @param Request $request
    * @return view
    */
    public function notificationsPage(Request $request) {
        
        return view('loggedin.notifications', [ ]);
    }
    
    /**
     * You can use this function to updates the top menu with ajax 
     * 
     * @param Request $request
     * @return view
     */
    public function unreadNotificationsTopMenu(Request $request) {
        
        return view('ajax.notifications_top', [ ]);
    }
    
    /**
     * Sends EstimatedArrival notifications
     * 
     * @param int $order_id
     * @param string $EstimatedArrival 
     */
    public function esitimateArriavl($order_id, $EstimatedArrival) {
        
        $po_num = \App\Http\Controllers\GeneralController::getPoNumberById($order_id);
        
        $date['message']="Estimated Arrival is $EstimatedArrival for Po no $po_num";
        $date['css_class'] = 'alert-warning';
        
        
        //notifications  should go to Production, sales
        /*Prueba no enviar a production :: role('Production', 'Sales') */
        $users = User::role('Sales')->where('active', 1)->get();
        Notification::send($users, new EstimatedArrival($date));
        
        //notifications should only go to those factory users which beong to that PO
        $user_ids = $this->getFactoryUserIdForOrderId($order_id);
        foreach($user_ids as $user){
                 $userObj = User::find($user->user_id);
                 $userObj->notify(new EstimatedArrival($date));
             }
    } 
    
    /**
     * Sends Licensing Conditions notifications
     * 
     * @param int $order_id
     */
    public function LicensingConditions($order_id) {
        
        $po_num = \App\Http\Controllers\GeneralController::getPoNumberById($order_id);
        
        $date['message']="Factory unapproved to produce Disney product for PO no. $po_num";
        $date['css_class'] = 'alert-danger';
        
        //an alert should be sent to production and factory that factory is unapproved to produce Disney product.
        
        /*Prueba no enviar correos
            $users = User::role('Production')->where('active', 1)->get();
            Notification::send($users, new LicensingConditions($date));
        */

        //notifications should only go to those factory users which beong to that PO
        $user_ids = $this->getFactoryUserIdForOrderId($order_id);
        foreach($user_ids as $user){
                 $userObj = User::find($user->user_id);
                 $userObj->notify(new LicensingConditions($date));
             }
        
    }
    
    /**
     * Sends Delivery Conditions notifications
     * 
     * @param int $order_id
     * @param String $date_field The name of the Date field in string
     */
    public function DeliveryConditions($order_id, $date_field) {
        
        $po_num = \App\Http\Controllers\GeneralController::getPoNumberById($order_id);
        
        $date['message']="$date_field is before Estimated Arr for PO no. $po_num";
        $date['css_class'] = 'alert-danger';
        
        //Every 3 day
        
        //and send an email to production, sales, factory
        /*Prueba no enviar a production :: role('Production', 'Sales') */
        $users = User::role('Sales')->where('active', 1)->get();
        Notification::send($users, new DeliveryConditions($date));
        
        //notifications and email should only go to those factory users which beong to that PO
        $user_ids = $this->getFactoryUserIdForOrderId($order_id);
        foreach($user_ids as $user){
                 $userObj = User::find($user->user_id);
                 $userObj->notify(new DeliveryConditions($date));
             }
        
        \App\Http\Controllers\GeneralController::setNotificationSchedule("DeliveryConditions", $order_id, 72); 
    }
    
    /**
     * Sends Safety Testing Condition One notifications
     * 
     * @param int $order_id
     */
    public function SafetyTestingConditionOne($order_id) {
        
        $this->SafetyTestingConditionOneProduction($order_id);
        $this->SafetyTestingConditionOneFactory($order_id);
        
    }
    
    /**
     * Sends Safety Testing Condition One notifications to production only
     * 
     * @param int $order_id
     */
    public function SafetyTestingConditionOneProduction($order_id) {
        
        $po_num = \App\Http\Controllers\GeneralController::getPoNumberById($order_id);
        
        $date['message']="'Fty Submitted Prod' not entered and Safety Test is 21 days before Finish Prod for PO no. $po_num";
        $date['css_class'] = 'alert-danger';
        
        //and production every 3 days
        /*Prueba no enviar a production  
        $users = User::role('Production')->where('active', 1)->get();
        Notification::send($users, new SafetyTestingConditionOne($date));
        
        
        \App\Http\Controllers\GeneralController::setNotificationSchedule("SafetyTestingConditionOneProduction", $order_id, 72);
        */
    }
    
    
    /**
     * Sends Safety Testing Condition One notifications to factory only
     * 
     * @param int $order_id
     */
    public function SafetyTestingConditionOneFactory($order_id) {
        
        $po_num = \App\Http\Controllers\GeneralController::getPoNumberById($order_id);
        
        $date['message']="'Fty Submitted Prod' not entered and Safety Test is 21 days before Finish Prod for PO no. $po_num";
        $date['css_class'] = 'alert-danger';
        
        // send email alerts to factory every day, 
        $user_ids = $this->getFactoryUserIdForOrderId($order_id);
        foreach($user_ids as $user){
                 $userObj = User::find($user->user_id);
                 $userObj->notify(new SafetyTestingConditionOne($date));
             }
        
        \App\Http\Controllers\GeneralController::setNotificationSchedule("SafetyTestingConditionOneFactory", $order_id, 24); 
        
    }

    /**
     * Sends Safety Testing Condition Two notifications
     * 
     * @param int $order_id
     */
    public function SafetyTestingConditionTwo($order_id) {
        
        $this->SafetyTestingConditionTwoProduction($order_id);
        $this->SafetyTestingConditionTwoFactory($order_id);
        
    }
    
    /**
     * Sends Safety Testing Condition Two notifications only to Production
     * 
     * @param int $order_id
     */
    public function SafetyTestingConditionTwoProduction($order_id) {
        
        $po_num = \App\Http\Controllers\GeneralController::getPoNumberById($order_id);
        
        $date['message']="Safety Test is not Passed or is 10 days before Finish Prod for PO no. $po_num";
        $date['css_class'] = "alert-danger";
        
        //and to production every 2 days.
        /*Prueba no enviar a production
        $users = User::role('Production')->where('active', 1)->get();
        Notification::send($users, new SafetyTestingConditionTwo($date));
        
        \App\Http\Controllers\GeneralController::setNotificationSchedule("SafetyTestingConditionTwoProduction", $order_id, 48); 
        */
    }
    
    /**
     * Sends Safety Testing Condition Two notifications only to Factory
     * 
     * @param int $order_id
     */
    public function SafetyTestingConditionTwoFactory($order_id) {
        
        $po_num = \App\Http\Controllers\GeneralController::getPoNumberById($order_id);
        
        $date['message']="Safety Test is not Passed or is 10 days before Finish Prod for PO no. $po_num";
        $date['css_class'] = "alert-danger";
        
        //send email alerts to factory every day 
        $user_ids = $this->getFactoryUserIdForOrderId($order_id);
        foreach($user_ids as $user){
                 $userObj = User::find($user->user_id);
                 $userObj->notify(new SafetyTestingConditionTwo($date));
             }
             
        \App\Http\Controllers\GeneralController::setNotificationSchedule("SafetyTestingConditionTwoFactory", $order_id, 24); 
             
    }

    /**
     * marks a notification as read 
     * 
     * @param Request $request
     */
    public function setNotificationAsRead(Request $request) {
        
        //Todo
    }
    
    /**
     * Gets the active user ids of the Factory associated with that Order Id
     * 
     * @param int $order_id
     * @return type
     */
    public function getFactoryUserIdForOrderId($order_id){
        
        //from order id get factory id
        $res = DB::table('import_po')->select('fty_produc')->
                where('id', $order_id)->first();
        
        if($res){
            
            //from factory id get user Ids
            
             $factory_id = $res->fty_produc;
            
             
             //a condition to select the user ids of the factory users only   

              return DB::table('factories_users')
                    ->select('user_id')
                    ->leftJoin('users', 'users.id', '=', 'factories_users.user_id')
                    ->where('factory_id', $factory_id)
                    ->where('users.active', 1)      
                    ->get();
        }
        
    }
    
    /**
     * This function is used for ajax request to delete notifications
     * 
     * @param Request $request
     * @return type
     */
    public function delete_notifications(Request $request) {
        $messages = [
            'required' => 'Notification ID not received.',
            'notification_id.exists' => 'Invalid Notification ID.',
        ];

        $validator = Validator::make($request->all(), [
                    'notification_id' => 'required|exists:notifications,id',
                        ], $messages);
        
        if ($validator->passes()) {
            
            //todo remove notifications from here
            DB::table('notifications')->where('id', $request->notification_id)->delete();

            return response()->json([   
                'success' => 'Notification deleted.',
            ]);
        }
        return response()->json(['error' => $validator->errors()->all()]);
    }
    
    
    public function checkNotificationSchedule() {
        
        
        $notification_schedules =DB::table('notification_schedule')->get();
        $i=0;$hours=null;
         //dd( $notification_schedules);
        foreach ($notification_schedules as $schedule) {
            //check if the method mentioned should be deleted or executed
            if($this->checkMethodForDelete($schedule->method_name, $schedule->order_id)){
                //delete the row here
                DB::table('notification_schedule')
                        ->where('method_name', $schedule->method_name)
                        ->where('order_id', $schedule->order_id)
                        ->delete();
            }
            else {
                //check hours and execute
                //die('no debo elimarla');
                $hours[$i] = $this->getHoursInterval($schedule->last_time_executed);
                if ($hours[$i] >= $schedule->reexecute_hours){
                    //reexecue the function here
                    $this->reexecuteMethods($schedule->method_name, $schedule->order_id);
                }
            }
                      
                
            $i++;
            
        }
        //save the notification run time
        $this->saveNotificationTime();
        
        return view('loggedin.NotificationSchedule', ['notification_schedules' => $hours]);
              
        
    }
    
    private function saveNotificationTime(){
        $now = new \DateTime();
        \App\Http\Controllers\GeneralController::setOption('cronRunTime', $now);
    }
    
    
    
    /**
     * This method will returns true if the method is no longer true and should 
     * be deleted on the notification_schedule order_id
     * 
     * @param string $param The name of the method
     * @return boolean  True if method should be deleted. False if method needs 
     *                  to be executed
     */
    private function checkMethodForDelete($method_name, $order_id) {
        
        $TriggerEventsController = new TriggerEventsController();
        $order = ImportPo::find($order_id);
        if($order->open==0){
            return true;
        }
        if  ($method_name=='DeliveryConditions' and !$TriggerEventsController->checkDeliveryConditions($order_id))
            {
                return true;
            }
        
        if  (   ($method_name=='SafetyTestingConditionOneFactory' or $method_name=='SafetyTestingConditionOneProduction')
                and !$TriggerEventsController->checkSafetyTestingConditionOne($order_id)    )
            {
                return true;
            }
            
        if  (   ($method_name=='SafetyTestingConditionTwoFactory' or $method_name=='SafetyTestingConditionTwoProduction')
                and !$TriggerEventsController->checkSafetyTestingConditionOne($order_id)    )
            {
                return true;
            }
        
    }
    
    /**
     * This function execute the method based on name and order id
     * 
     * @param type $method_name
     * @param type $order_id
     */
    private function reexecuteMethods($method_name, $order_id) {
        
        $TriggerEventsController = new TriggerEventsController();
        
        if  ( $method_name=='DeliveryConditions' )
            {
                $TriggerEventsController->triggerDeliveryConditions($order_id);
            }
        
        if  ( $method_name=='SafetyTestingConditionOneFactory')
            {
                $this->SafetyTestingConditionOneFactory($order_id);
            }
            
        if  ( $method_name=='SafetyTestingConditionOneProduction' )
            {
                $this->SafetyTestingConditionOneProduction($order_id);
            }
            
        if  ( $method_name=='SafetyTestingConditionTwoFactory' )
            {
                $this->SafetyTestingConditionTwoFactory($order_id);
            }
        
        if  ( $method_name=='SafetyTestingConditionTwoProduction' )
            {
                $this->SafetyTestingConditionTwoProduction($order_id);
            }
    }
    
    
    /**
     * This function returns the hours interval to the present time
     * 
     * @param datetime $date A datetime from database
     * @return int Number of hours
     */
    private function getHoursInterval($date){
        
            $date1 = new \DateTime($date);
            $now = new \DateTime();
            
            $interval=$now->diff($date1);
            return ($interval->days * 24) + $interval->h;
    }
    public function designerItem($item_id,$msg='New item assigned.') {

        $users= User::whereHas('roles', function ($query2) {
                        $query2->where('name','Designer');
                    })->whereHas('item_assigned', function ($query2) use ($item_id){
                        $query2->where(['item_id'=>$item_id,'active'=>1,'tipo'=>1]);
                    })->where('active',1)->get();

        Notification::send($users, new ItemNotification($item_id,'Designer',$msg));

    } 
    public function licenseItem($item_id,$msg='New item assigned, for internal approval.') {
        $users= User::whereHas('item_assigned', function ($query2) use ($item_id){
            $query2->where(['item_id'=>$item_id,'active'=>1,'tipo'=>2]);
        })->where('active',1)->get();

        Notification::send($users, new ItemNotification($item_id,'License',$msg));

    } 
    
}
