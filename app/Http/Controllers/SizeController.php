<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Size;
use Carbon\Carbon;;
use DB;
use Log;
use Illuminate\Validation\Rule;

class SizeController extends Controller {

    /**
     * Construct method makes sure that all pages here are accessed by 
     * registered users by using the auth middleware
     */
    public function __construct() {
        $this->middleware('auth');
        if (Auth::check()) {
            
        } else {
            return redirect('login');
        }
    }

    /**
     * Function to display table management page
     * 
     * @param Request $request
     * @return view
     */
    public function index(Request $request) {
        $user = Auth::user();
        
        if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
            
            return redirect('/home')->withErrors("Only Admin can access these settings.");
            
        }

        
        $sizes = $this->sizesTable($request);
       
        $vista=view('sizes.index', 
        [
            
            'sizes' => $sizes,
           
            
        ]);
        if ($request->isMethod('post')) {
            $request->session()->flash('message', 'successful!');
        
        }
        return $vista;
    }
    
    
     /**
     * This function manages CRUD for sizes Table
     * 
     * @param object $request
     * @return types
     */
    private function sizesTable($request){
        
        if(isset($request)) {
            
        //check if request has search sizes
            
            if ($request->search_sizes) {
                
                return $sizes = DB::table('sizes')->select('*')->where('name', 'like', "%$request->search_sizes%")->orderBy('id', 'desc')->paginate(10, ['*'], 'sizes');
            }
        
        //Edit
            if ($request->table == 'sizes' and $request->action  == 'edit_sizes') {
                
                $this->validate($request, [
                'name'          => 'required|string|max:250',
                'code' => [
                    'required','string','max:2',
                    Rule::unique('sizes')->ignore($request->edit_id)
                ],
                'edit_id'       =>  'exists:sizes,id',
                ]);
                
               Size::where('id', $request->edit_id)->update([
                    'name' => $request->name,
                    'code'=> $request->code,
                    ]);

            }
        
        //Add
            if ($request->table == 'sizes' and $request->action  == 'add_sizes') {
                
                $this->validate($request, [
                    'name'          => 'required|string|unique:sizes,name|max:250',
                    'code'   => 'required|string|max:2|unique:sizes,code',
                ]);

                
                Size::create([
                    'name' => $request->name,
                    'code'=> $request->code
                    ]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'sizes' and $request->action  == 'delete_sizes') {
            
                $this->validate($request, [
                    'delete_size_id'       => 'exists:sizes,id',
                ]);
                
                DB::table('sizes')->where('id', $request->delete_size_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'sizes' and $request->action  == 'activate_sizes') {
            
                $this->validate($request, [
                    'activate_size_id'       => 'exists:sizes,id',
                ]);
                
                DB::table('sizes')->where('id', $request->activate_size_id)->update(['active' => 1]);
         
            }
            
            
        }
        return $sizes= size::orderBy('id', 'desc')->paginate(10, ['*'], 'sizes');
        //return $sizes = DB::table('sizes')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'sizes');
    }

}
