<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
use App\Mounting;
use DB;
use Log;
use Illuminate\Validation\Rule;

class MountingController extends Controller {

    /**
     * Construct method makes sure that all pages here are accessed by 
     * registered users by using the auth middleware
     */
    public function __construct() {
        $this->middleware('auth');
        if (Auth::check()) {
            
        } else {
            return redirect('login');
        }
    }

    /**
     * Function to display table management page
     * 
     * @param Request $request
     * @return view
     */
    public function index(Request $request) {
        $user = Auth::user();
        
        if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
            
            return redirect('/home')->withErrors("Only Admin can access these settings.");
            
        }

        $mountings = $this->mountingsTable($request);
        $vista=view('mounting.index', 
        [
            'mountings' => $mountings
            
        ]);
        if ($request->isMethod('post')) {
            $request->session()->flash('message', 'successful!');
        
        }
        return $vista;
    }
    
   
    /**
     * This function manages CRUD for mountings Table
     * 
     * @param object $request
     * @return types
     */
    private function mountingsTable($request){
        
        if(isset($request)) {
            
        //check if request has search mountings
            
            if ($request->search_mountings) {
                
                return $mountings = DB::table('mountings')->select('*')->where('name', 'like', "%$request->search_mountings%")->orderBy('id', 'desc')->paginate(10, ['*'], 'mountings');
            }
        
        //Edit
            if ($request->table == 'mountings' and $request->action  == 'edit_mountings') {
                
                $this->validate($request, [
                'name'          => 'required|string|max:250',
                'code' => [
                    'required','string','max:1',
                    Rule::unique('mountings')->ignore($request->edit_id)
                ],
                'edit_id'       =>  'exists:mountings,id',
                ]);
                
                Mounting::where('id', $request->edit_id)->update([
                    'name' => $request->name,
                    'code'=> $request->code,
                    ]);

            }
        
        //Add
            if ($request->table == 'mountings' and $request->action  == 'add_mountings') {
                
                $this->validate($request, [
                    'name'          => 'required|string|unique:mountings,name|max:250',
                    'code'   => 'required|string|max:1|unique:mountings,code',
                ]);

                
                Mounting::create([
                    'name' => $request->name,
                    'code'=> $request->code,
                    ]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'mountings' and $request->action  == 'delete_mountings') {
            
                $this->validate($request, [
                    'delete_mounting_id'       => 'exists:mountings,id',
                ]);
                
                DB::table('mountings')->where('id', $request->delete_mounting_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'mountings' and $request->action  == 'activate_mountings') {
            
                $this->validate($request, [
                    'activate_mounting_id'       => 'exists:mountings,id',
                ]);
                
                DB::table('mountings')->where('id', $request->activate_mounting_id)->update(['active' => 1]);
         
            }
            
            
        }
        return $mountings= Mounting::orderBy('id', 'desc')->paginate(10, ['*'], 'mountings');
        //return $mountings = DB::table('mountings')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'mountings');
    }
    
    
}
