<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
use DB;
use Log;
use Illuminate\Validation\Rule;

class SalesmenController extends Controller {

    /**
     * Construct method makes sure that all pages here are accessed by 
     * registered users by using the auth middleware
     */
    public function __construct() {
        $this->middleware('auth');
        if (Auth::check()) {
            
        } else {
            return redirect('login');
        }
    }

    /**
     * Function to display table management page
     * 
     * @param Request $request
     * @return view
     */
    public function index(Request $request) {
        $user = Auth::user();
        
        if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
            
            return redirect('/home')->withErrors("Only Admin can access these settings.");
            
        }

       
        $salesmen = $this->salesmenTable($request);
        
        $vista=view('salesmen.index', 
        [
          
            'salesmen' => $salesmen,
           
        ]);
        if ($request->isMethod('post')) {
            $request->session()->flash('message', 'successful!');
        
        }
        return $vista;
    }
    
    
    
    /**
     * This function manages CRUD for salesmen Table
     * 
     * @param object $request
     * @return types
     */
    private function salesmenTable($request){
        
        if(isset($request)) {
            
        //check if request has search salesmen
            
            if ($request->search_salesmen) {
                
                return $salesmen = DB::table('salesmen')->select('*')->where('name', 'like', "%$request->search_salesmen%")->orderBy('id', 'desc')->paginate(10, ['*'], 'salesmen');
            }
        
        //Edit
            if ($request->table == 'salesmen' and $request->action  == 'edit_salesmen') {
                
                $this->validate($request, [
                'name'          => 'required|string',
                'edit_id'       =>  'exists:salesmen,id',
                ]);
                
                DB::table('salesmen')->where('id', $request->edit_id)->update(['name' => $request->name]);

            }
        
        //Add
            if ($request->table == 'salesmen' and $request->action  == 'add_salesmen') {
            
                $this->validate($request, [
                    'name'          => 'required|string|unique:salesmen,name',
                    'edit_id'       => 'exists:salesmen,id',
                ]);
                
                DB::table('salesmen')->insert(['name' => $request->name]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'salesmen' and $request->action  == 'delete_salesmen') {
            
                $this->validate($request, [
                    'delete_salesman_id'       => 'exists:salesmen,id',
                ]);
                
                DB::table('salesmen')->where('id', $request->delete_salesman_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'salesmen' and $request->action  == 'activate_salesmen') {
            
                $this->validate($request, [
                    'activate_salesman_id'       => 'exists:salesmen,id',
                ]);
                
                DB::table('salesmen')->where('id', $request->activate_salesman_id)->update(['active' => 1]);
         
            }
            
            
        }
        
        return $salesmen = DB::table('salesmen')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'salesmen');
    }
    
    
    
    
}
