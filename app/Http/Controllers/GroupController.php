<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\User;
use App\Item;
use App\ItemImage;
use App\Group;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\NotificationsController;
use Illuminate\Validation\Rule;
use Datatables;

class GroupController extends Controller
{
    public function __construct()
    {   
        $this->middleware('auth');
        if (!Auth::check()) {return redirect('login');}
    }
    public function index() {
       
            
        return view( 'groups.index');
       
      
    }
    public function create() {
       
            
        return view( 'groups.create');
       
      
    }
    public function store(Request $request){
        if($request->has('item_id') && $request->get('item_id')[0]==null){
            $array=$request->get('item_id');
            unset($array[0]);
            $request['item_id']=$array;
        }
        $this->validate($request, [     
            'name' => 'required|string|min:3', 
            'description' => 'nullable|string', 
            'item_id' => 'array|nullable'

        ]
        );

        $input = $request->all();
        $input['code']= (string) random_int(1, 999999);
        $group= Group::create($input);
        $group->items()->sync($request->get('item_id'));
        return redirect("/groups")->with('message', 'Created success.');    
    }

    public function get_items(Request $request){
        $value = $request->get('q');
        $page_limit = $request->get( 'page_limit');
        $page = $request->get( 'page' );
        $offset =($page - 1)*$page_limit;
        $items= DB::table('items')
        ->leftJoin('item_images', function ($join) {
            $join->on('item_images.item_id', '=', 'items.id')
                 ->where('item_images.active', '=', 1);
        })
        ->where([
            ['items.active', '=', 1],
            ['items.name', 'like', "%{$value}%"]
            ])
        ->select(
            'items.id as id',
            'items.name as text',
            'item_images.path as img'
        )->groupBy('items.id');
        $total=$items->get()->count();
        $coll=$items->skip( $offset)
            ->take($page_limit)->get();
        return response()->json([   
            'incomplete_results'=>false,
            'items' =>   $coll,
            'total'=>$total
        ],200);

    }
    public function getBasicObjectData(Request $request)
    {
      
      $groups = Group::select();
      if ($request->has('name')) {
        $groups->where('groups.name', 'like', "%{$request->get('name')}%");
    }
    if ($request->has('code')) {
        $groups->where('groups.code', 'like', "%{$request->get('code')}%");
    }
    return Datatables::of($groups)
        ->addColumn('details_url', function($groups) {
            return url('groups/details-data/' . $groups->id);
        })
        ->make(true);
    }
    public function getDetailsData($id)
    {
       
        $groups= Group::find($id)->items;
        return Datatables::of($groups)
        ->addColumn('material_name', function(Item $items) {
            return $items->material->name;
        })
        ->addColumn('mounting_name', function(Item $items) {
            return $items->mounting->name;
        })
        ->addColumn('license_name', function(Item $items) {
            return $items->character->license->name;
        })
        ->addColumn('character_name', function(Item $items) {
            return $items->character->name;
        })
        ->addColumn('size_name', function(Item $items) {
            return $items->size->name;
        })
        ->addColumn('feature_name', function(Item $items) {
            return $items->feature->name;
        })->addColumn('customer_name', function(Item $items) {
            return ($items->customer)? $items->customer : '';
        })
        ->addColumn('status_name', function(Item $items) {
            $status_actual = $items->item_status()->where('active', 1)->first();
            return $status_actual->status->name;
        })
        ->addColumn('action', function (Item $items) use ($id){
            return  '<button  onclick="delete_groups_item('.$id.','.$items->id.')" type="button" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></button>';
        })
        ->addColumn('img', function ( $item) {
            $img= ItemImage::where(['item_id'=>$item->id,'active'=>1])->first();
            if($img){
            
              return ' <a class="test-popup-link" href="'. url($img->path).'">
              <img src="'. url($img->path).'" width="100" height="100"></a>';
            }  
        return null;
        })
        ->rawColumns(['img','action'])
        ->make(true);
    }

    public function delete_item(Request $request,$id_group,$id_item){
        Group::find($id_group)->items()->detach($id_item);
        return response()->json([   
            'status'=>'exitoso'
        ],200);

    }

    public function get_group_items($id)
    {
       
        $items= Group::findOrFail($id)->items;
        return $items;
        //dd($items->pluck('id','name'));
    }
    public function update(Request $request, $id)
    {
        $group = Group::findOrFail($id);
        if($request->has('item_id') && $request->get('item_id')[0]==null){
            $array=$request->get('item_id');
            unset($array[0]);
            $request['item_id']=$array;
        }
        $this->validate($request, [     
            'name' => 'required|string|min:3', 
            'description' => 'nullable|string', 
            'item_id' => 'array|nullable'

        ]
        );

        $group->name= $request->get('name');
        $group->description = $request->get('description');
        $group->save();
        $group->items()->sync($request->get('item_id'));
        return redirect("/groups")->with('message', 'Updated success.');  
    }
    
}
