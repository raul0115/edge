<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
use DB;
use Log;
use Illuminate\Validation\Rule;

class PortController extends Controller {

    /**
     * Construct method makes sure that all pages here are accessed by 
     * registered users by using the auth middleware
     */
    public function __construct() {
        $this->middleware('auth');
        if (Auth::check()) {
            
        } else {
            return redirect('login');
        }
    }

    /**
     * Function to display table management page
     * 
     * @param Request $request
     * @return view
     */
    public function index(Request $request) {
        $user = Auth::user();
        
        if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
            
            return redirect('/home')->withErrors("Only Admin can access these settings.");
            
        }

     
        $ports = $this->portsTable($request);
        
        $vista=view('ports.index', 
        [
            
            'ports' => $ports,
          
        ]);
        if ($request->isMethod('post')) {
            $request->session()->flash('message', 'successful!');
        
        }
        return $vista;
    }
 
    
    /**
     * This function manages CRUD for ports Table
     * 
     * @param object $request
     * @return types
     */
    private function portsTable($request){
        
        if(isset($request)) {
            
        //check if request has search ports
            
            if ($request->search_ports) {
                
                return $ports = DB::table('ports')->select('*')->where('name', 'like', "%$request->search_ports%")->orderBy('id', 'desc')->paginate(10, ['*'], 'ports');
            }
        
        //Edit
            if ($request->table == 'ports' and $request->action  == 'edit_ports') {
                
                $this->validate($request, [
                'name'          => 'required|string',
                'edit_id'       =>  'exists:ports,id',
                ]);
                
                DB::table('ports')->where('id', $request->edit_id)->update(['name' => $request->name]);

            }
        
        //Add
            if ($request->table == 'ports' and $request->action  == 'add_ports') {
            
                $this->validate($request, [
                    'name'          => 'required|string|unique:ports,name',
                    'edit_id'       => 'exists:ports,id',
                ]);
                
                DB::table('ports')->insert(['name' => $request->name]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'ports' and $request->action  == 'delete_ports') {
            
                $this->validate($request, [
                    'delete_port_id'       => 'exists:ports,id',
                ]);
                
                DB::table('ports')->where('id', $request->delete_port_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'ports' and $request->action  == 'activate_ports') {
            
                $this->validate($request, [
                    'activate_port_id'       => 'exists:ports,id',
                ]);
                
                DB::table('ports')->where('id', $request->activate_port_id)->update(['active' => 1]);
         
            }
            
            
        }
        
        return $ports = DB::table('ports')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'ports');
    }
   
    
}
