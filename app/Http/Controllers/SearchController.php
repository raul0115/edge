<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use Spatie\Permission\Models\Role;
//use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\UserController;
use App\Http\Controllers\OrderController;
use DB;

class SearchController extends Controller
{
    
    /**
     * Construct method makes sure that all pages here are accessed by 
     * registered users by using the auth middleware
     */    
    public function __construct()
    {   
        $this->middleware('auth');
        if (Auth::check()) {}
        else {return redirect('login');}
    }

    public function searchOrders(Request $request) {
        
        $data=$role_Id=$role_table=null;
        $userObj = Auth::user();
        
        $OrderController = new OrderController();
        $tableData = $OrderController->getAllTablesData();
        
        //Checks the user types
        if ($userObj->hasRole('Factory') and $role_user = UserController::getFactoryUser($userObj->id)) {
            $role_Id = $role_user->factory_id;
            $role_table = 'import_po.fty_produc';
        }
        elseif($userObj->hasRole('Sales') and $role_user = UserController::getSalesmenUser($userObj->id)){
            $role_Id = $role_user->salesmen_id;
            $role_table = 'salesmen_select.salesmen_id';
        }    
        
        //if text fields
        if ($request->action_type == 'search_field'){
            
            if(!$this->validateTextField($request->search_field)){
                //return $this->searchOrdersDates($userObj, $request);
                return back()->withErrors('No field selected from dropdown.');
            }
            elseif (!isset($request->search_text) or $request->search_text=="") {
                
            }
            
            $whereClause = $this->getWhereClauseForQuery($request->search_field, $request->search_text);
            $orders = $this->getOrdersByTextFields($whereClause, $role_Id, $role_table);
                
            
            return view('loggedin.search', ['orders' => $orders, 'tableData' => $tableData, 'data' => $data ]);
            
            
        }
        
        
        //if date fields selected
        elseif ($request->action_type == 'search_date'){
            
            if(!$this->validateDateField($request->search_field)){
                //return $this->searchOrdersDates($userObj, $request);
                return back()->withErrors('No date field type selected from dropdown.');
            }
                //todo validate search field
                $orders = $this->getOrdersByDates($request->start, $request->end, $request->search_field, $role_Id, $role_table);
                
            
            return view('loggedin.search', ['orders' => $orders, 'tableData' => $tableData, 'data' => $data ]);
        }
        
        
        //if y/n fields selected
        elseif ($request->action_type == 'search_yes_no'){
            
            
            if(!$this->validateYesNoField($request->search_field)){
                //return $this->searchOrdersDates($userObj, $request);
                return back()->withErrors('No Y/N field type selected from dropdown.');
            }
                //todo validate search field
                $orders = $this->getOrdersByYesNoBools($request->search_field, $request->field_value, $role_Id, $role_table);
                
            return view('loggedin.search', ['orders' => $orders, 'tableData' => $tableData, 'data' => $data ]);

            
        }
        
        return view('loggedin.search', []);
        
        
    }
    
     /**
     * Returns the order
     * 
     * @param bool $open 1 for open orders and 0 for closed orders
     * @param int $factory_id
     * @return object Database row
     */
    private function getOrdersByTextFields($whereClause, $role_Id=null, $role_table=null) {
        
        if (isset($role_Id)){
            $whereClause[$role_table] = $role_Id;
            return $this->getOrderQuery($whereClause);
        }
         else {
            return $this->getOrderQuery($whereClause);
         }
    }
    
    public function getWhereClauseForQuery($field_name, $search_text) {
        
        switch ($field_name) {
            case "po_number":
                return  [['import_po.po_number', 'like', '%' . $search_text . '%']];
            case "comments":
                return [['import_po.comments', 'like', '%' . $search_text . '%']];
            case "value":
                return [['import_po.value', 'like', '%' . $search_text . '%']];
            case "fty_produc":
                return [['factories.name', 'like', '%' . $search_text . '%']];
            case "export_cos":
                return [['export_cos.name', 'like', '%' . $search_text . '%']];
            case "prod_style":
                return [['product_styles.name', 'like', '%' . $search_text . '%']];
            case "prod_type":
                return [['product_types.name', 'like', '%' . $search_text . '%']];
            case "licenses":
                return [['licenses.name', 'like', '%' . $search_text . '%']];
            case "customers":
                return [['customers.name', 'like', '%' . $search_text . '%']];
            case "customer_po_number":
                return [['import_po.customer_po_number', 'like', '%' . $search_text . '%']];
            case "qty_ordered":
                return [['import_po.qty_ordered', 'like', '%' . $search_text . '%']];
            case "shipping_from":
                return [['port_sf.name', 'like', '%' . $search_text . '%']];
            case "shipping_to":
                return [['port_st.name', 'like', '%' . $search_text . '%']];
            case "cbm":
                return [['import_po.cbm', 'like', '%' . $search_text . '%']];
            case "book_container":
                return [['import_po.book_container', 'like', '%' . $search_text . '%']];
            case "david_ins_comment":
                return [['import_po.david_ins_comment', 'like', '%' . $search_text . '%']];
            case "svn_no":
                return [['import_po.svn_no', 'like', '%' . $search_text . '%']];
            case "freight_forwarder":
                return [['freight_forwarders.name', 'like', '%' . $search_text . '%']];
            case "salesmen":
                return [['salesmen.name', 'like', '%' . $search_text . '%']];
            default:
                return null;
        }
        
    }
    
    /**
     * This function returns true if the text field is valid.
     * 
     * @param string $search_field
     * @return boolean true if the field is valid
     */
    public function validateTextField($search_field){
        
    if(
            $search_field == 'po_number' or $search_field == 'comments'
            or $search_field =='value' or $search_field == 'fty_produc' 
            or $search_field =='export_cos' or $search_field =='prod_style' 
            or $search_field =='prod_type' or $search_field =='licenses'
            or $search_field =='customers' or $search_field =='customer_po_number'
            or $search_field =='qty_ordered' or $search_field =='shipping_from'
            or $search_field =='shipping_to' or $search_field =='cbm'
            or $search_field =='book_container' or $search_field =='david_ins_comment'
            or $search_field =='svn_no' or $search_field =='freight_forwarder'
            or $search_field =='salesmen'
        ){
        return true;
        }
        
    }
    
     /**
     * Returns the order
     * 
     * @param bool $open 1 for open orders and 0 for closed orders
     * @param int $factory_id
     * @return object Database row
     */
    private function getOrdersByDates($date_from, $date_to, $field_name, $role_Id=null, $role_table=null) {
        
        if (isset($role_Id)){
             return DB::table(DB::raw('(import_po, import_po_dates)'))
                ->select('import_po.id as ipo', 'import_po.*', 'import_po_dates.*'
                        ,'factories.id as ftyId', 'factories.name as ftyName', 'factories.vendor_id as ftyVId'
                        ,'export_cos.id as ExCId' , 'export_cos.name as ExCName', 'export_cos.fama as ExCFama'
                        ,'freight_forwarders.id as FFId', 'freight_forwarders.name as FFName'
                        ,'port_sf.id as port_ShipF_ID', 'port_sf.name as port_ShipF_Name'
                        ,'port_st.id as port_ShipT_ID', 'port_st.name as port_ShipT_Name'
                        
                        ,DB::raw("GROUP_CONCAT(distinct tickets_ordered.tickets_ordered separator ', ') as tickets_orderedDates")
                        
                        ,DB::raw("GROUP_CONCAT(distinct product_types.name separator ', ') as prod_typesNames")
                        ,DB::raw("GROUP_CONCAT(distinct product_types.id separator ',') as prod_typesId")
                        ,DB::raw("GROUP_CONCAT(distinct product_styles.name ORDER BY product_styles.id separator ', ') as prod_styleNames")
                        ,DB::raw("GROUP_CONCAT(distinct product_styles.id ORDER BY product_styles.id separator ',') as prod_styleId")
                        ,DB::raw("GROUP_CONCAT(distinct customers.name separator ', ') as customersNames")
                        ,DB::raw("GROUP_CONCAT(distinct customers.id separator ',') as customersId")
                        ,DB::raw("GROUP_CONCAT(distinct licenses.name separator ', ') as licensesNames")
                        ,DB::raw("GROUP_CONCAT(distinct licenses.id separator ',') as licensesId")
                        ,DB::raw("GROUP_CONCAT(distinct salesmen.name separator ', ') as salesmenNames")
                        ,DB::raw("GROUP_CONCAT(distinct salesmen.id separator ',') as salesmenId")

                        )
                ->leftJoin('factories','import_po.fty_produc', '=', 'factories.id')
                ->leftJoin('export_cos','import_po.export_cos', '=', 'export_cos.id')
                ->leftJoin('freight_forwarders','import_po.freight_forwarder', '=', 'freight_forwarders.id')
                ->leftJoin('ports as port_sf','import_po.shipping_from', '=', 'port_sf.id')
                ->leftJoin('ports as port_st','import_po.shipping_to', '=', 'port_st.id')
                
                ->leftJoin('tickets_ordered','import_po.id', '=', 'tickets_ordered.order_id')
                
                ->leftJoin('product_types_select','import_po.id', '=', 'product_types_select.order_id')
                ->leftJoin('product_types as product_types','product_types.id', '=', 'product_types_select.product_types_id')
                
                ->leftJoin('product_styles_select','import_po.id', '=', 'product_styles_select.order_id')
                ->leftJoin('product_styles','product_styles.id', '=', 'product_styles_select.product_styles_id')
                
                ->leftJoin('licenses_select','import_po.id', '=', 'licenses_select.order_id')
                ->leftJoin('licenses','licenses.id', '=', 'licenses_select.licenses_id')
                
                ->leftJoin('customers_select','import_po.id', '=', 'customers_select.order_id')
                ->leftJoin('customers','customers.id', '=', 'customers_select.customers_id')
                
                ->leftJoin('salesmen_select','import_po.id', '=', 'salesmen_select.order_id')
                ->leftJoin('salesmen','salesmen.id', '=', 'salesmen_select.salesmen_id')
                
                ->whereRaw('`import_po`.`id` = `import_po_dates`.`order_po_id`')
                //->where('import_po.open', $open)
                ->whereBetween($field_name, [$date_from, $date_to])
                ->where("$role_table", $role_Id)
                ->groupBy('import_po.id', 'import_po.comments', 'import_po.po_number'
                        ,'import_po.open','import_po.value','import_po.fty_produc'
                        ,'import_po.export_cos','import_po.customer_po_number','import_po.qty_ordered'
                        ,'import_po.tickets_needed','import_po.shipping_from','import_po.shipping_to','import_po.cbm'
                        ,'import_po.book_container','import_po.safety_test','import_po.safety_test_pass','import_po.david_ins_result'
                        ,'import_po.david_ins_comment','import_po.svn_no','import_po.freight_forwarder','import_po.pro_photo','import_po.receive_whse'
                        ,'import_po_dates.id','import_po_dates.created_at','import_po_dates.updated_at','import_po_dates.order_po_id'
                        ,'import_po_dates.date_placed','import_po_dates.tickets_received','import_po_dates.finish_prod','import_po_dates.estimated_arr'
                        ,'import_po_dates.customer_cancel','import_po_dates.act_etd_forwarder','import_po_dates.act_eta_forwarder','import_po_dates.safety_test_on'
                        ,'import_po_dates.fty_prod_safety_test','import_po_dates.rec_comm_inv_pking','import_po_dates.rec_comm_inv_pking_home','import_po_dates.original_fcr'
                        ,'import_po_dates.ctpat_form','import_po_dates.telex_release','import_po_dates.okay_to_pay','import_po_dates.did_we_pay','import_po_dates.send_diann'
                        ,'factories.id', 'factories.name', 'factories.vendor_id'
                        ,'export_cos.id' , 'export_cos.name', 'export_cos.fama'
                        ,'freight_forwarders.id', 'freight_forwarders.name'
                        ,'port_sf.id', 'port_sf.name'
                        ,'port_st.id', 'port_st.name'
                        )
                
                ->orderBy('ipo', 'desc')
                ->paginate(15);
        }
         else {
            return DB::table(DB::raw('(import_po, import_po_dates)'))
                ->select('import_po.id as ipo', 'import_po.*', 'import_po_dates.*'
                        ,'factories.id as ftyId', 'factories.name as ftyName', 'factories.vendor_id as ftyVId'
                        ,'export_cos.id as ExCId' , 'export_cos.name as ExCName', 'export_cos.fama as ExCFama'
                        ,'freight_forwarders.id as FFId', 'freight_forwarders.name as FFName'
                        ,'port_sf.id as port_ShipF_ID', 'port_sf.name as port_ShipF_Name'
                        ,'port_st.id as port_ShipT_ID', 'port_st.name as port_ShipT_Name'
                        
                        ,DB::raw("GROUP_CONCAT(distinct tickets_ordered.tickets_ordered separator ', ') as tickets_orderedDates")
                        
                        ,DB::raw("GROUP_CONCAT(distinct product_types.name separator ', ') as prod_typesNames")
                        ,DB::raw("GROUP_CONCAT(distinct product_types.id separator ',') as prod_typesId")
                        ,DB::raw("GROUP_CONCAT(distinct product_styles.name ORDER BY product_styles.id separator ', ') as prod_styleNames")
                        ,DB::raw("GROUP_CONCAT(distinct product_styles.id ORDER BY product_styles.id separator ',') as prod_styleId")
                        ,DB::raw("GROUP_CONCAT(distinct customers.name separator ', ') as customersNames")
                        ,DB::raw("GROUP_CONCAT(distinct customers.id separator ',') as customersId")
                        ,DB::raw("GROUP_CONCAT(distinct licenses.name separator ', ') as licensesNames")
                        ,DB::raw("GROUP_CONCAT(distinct licenses.id separator ',') as licensesId")
                        ,DB::raw("GROUP_CONCAT(distinct salesmen.name separator ', ') as salesmenNames")
                        ,DB::raw("GROUP_CONCAT(distinct salesmen.id separator ',') as salesmenId")

                        )
                ->leftJoin('factories','import_po.fty_produc', '=', 'factories.id')
                ->leftJoin('export_cos','import_po.export_cos', '=', 'export_cos.id')
                ->leftJoin('freight_forwarders','import_po.freight_forwarder', '=', 'freight_forwarders.id')
                ->leftJoin('ports as port_sf','import_po.shipping_from', '=', 'port_sf.id')
                ->leftJoin('ports as port_st','import_po.shipping_to', '=', 'port_st.id')
                
                ->leftJoin('tickets_ordered','import_po.id', '=', 'tickets_ordered.order_id')
                
                ->leftJoin('product_types_select','import_po.id', '=', 'product_types_select.order_id')
                ->leftJoin('product_types as product_types','product_types.id', '=', 'product_types_select.product_types_id')
                
                ->leftJoin('product_styles_select','import_po.id', '=', 'product_styles_select.order_id')
                ->leftJoin('product_styles','product_styles.id', '=', 'product_styles_select.product_styles_id')
                
                ->leftJoin('licenses_select','import_po.id', '=', 'licenses_select.order_id')
                ->leftJoin('licenses','licenses.id', '=', 'licenses_select.licenses_id')
                
                ->leftJoin('customers_select','import_po.id', '=', 'customers_select.order_id')
                ->leftJoin('customers','customers.id', '=', 'customers_select.customers_id')
                
                ->leftJoin('salesmen_select','import_po.id', '=', 'salesmen_select.order_id')
                ->leftJoin('salesmen','salesmen.id', '=', 'salesmen_select.salesmen_id')
                
                ->whereRaw('`import_po`.`id` = `import_po_dates`.`order_po_id`')
                ->whereBetween($field_name, [$date_from, $date_to])
                //->where('import_po.open', $open)
                ->groupBy('import_po.id', 'import_po.comments', 'import_po.po_number'
                        ,'import_po.open','import_po.value','import_po.fty_produc'
                        ,'import_po.export_cos','import_po.customer_po_number','import_po.qty_ordered'
                        ,'import_po.tickets_needed','import_po.shipping_from','import_po.shipping_to','import_po.cbm'
                        ,'import_po.book_container','import_po.safety_test','import_po.safety_test_pass','import_po.david_ins_result'
                        ,'import_po.david_ins_comment','import_po.svn_no','import_po.freight_forwarder','import_po.pro_photo','import_po.receive_whse'
                        ,'import_po_dates.id','import_po_dates.created_at','import_po_dates.updated_at','import_po_dates.order_po_id'
                        ,'import_po_dates.date_placed','import_po_dates.tickets_received','import_po_dates.finish_prod','import_po_dates.estimated_arr'
                        ,'import_po_dates.customer_cancel','import_po_dates.act_etd_forwarder','import_po_dates.act_eta_forwarder','import_po_dates.safety_test_on'
                        ,'import_po_dates.fty_prod_safety_test','import_po_dates.rec_comm_inv_pking','import_po_dates.rec_comm_inv_pking_home','import_po_dates.original_fcr'
                        ,'import_po_dates.ctpat_form','import_po_dates.telex_release','import_po_dates.okay_to_pay','import_po_dates.did_we_pay','import_po_dates.send_diann'
                        ,'factories.id', 'factories.name', 'factories.vendor_id'
                        ,'export_cos.id' , 'export_cos.name', 'export_cos.fama'
                        ,'freight_forwarders.id', 'freight_forwarders.name'
                        ,'port_sf.id', 'port_sf.name'
                        ,'port_st.id', 'port_st.name'
                        )
                
                ->orderBy('ipo', 'desc')
                ->paginate(15);
         }
    }
    
    /**
     * This function returns true if the date field is valid.
     * 
     * @param string $search_field
     * @return boolean true if the field is valid
     */
    public function validateDateField($search_field){
        
    if(
            $search_field == 'date_placed' or $search_field == 'tickets_received'
            or $search_field =='finish_prod' or $search_field == 'estimated_arr' 
            or $search_field =='customer_cancel' or $search_field =='act_etd_forwarder' 
            or $search_field =='act_eta_forwarder' or $search_field == 'safety_test_on' 
            or $search_field == 'fty_prod_safety_test' or $search_field == 'rec_comm_inv_pking' 
            or $search_field == 'rec_comm_inv_pking_home' or $search_field ==  'original_fcr'
            or $search_field =='ctpat_form' or $search_field =='telex_release' 
            or $search_field =='okay_to_pay' or $search_field == 'did_we_pay' or $search_field == 'send_diann'
        ){
        return true;
        }
        
    }
    
     /**
     * This function returns true if the date field is valid.
     * 
     * @param string $search_field
     * @return boolean true if the field is valid
     */
    public function validateYesNoField($search_field){
        
    if(
            $search_field == 'open' or $search_field == 'tickets_needed'
            or $search_field =='safety_test' or $search_field == 'safety_test_pass' 
            or $search_field =='david_ins_result' or $search_field =='pro_photo' 
            or $search_field =='receive_whse'
        ){
        return true;
        }
        
    }
        
     /**
     * Returns the order
     * 
     * @param bool $open 1 for open orders and 0 for closed orders
     * @param int $factory_id
     * @return object Database row
     */
    private function getOrdersByYesNoBools($field_name, $field_value, $role_Id=null, $role_table=null) {
        
        if (isset($role_Id)){
            $whereClause = array(
			$field_name => $field_value,
                        $role_table=>$role_Id
            );
            return $this->getOrderQuery($whereClause);
        }
         else {
            $whereClause = array(
			$field_name => $field_value
            );
            return $this->getOrderQuery($whereClause);
             
         }
    }
    
    
    public function getOrderQuery($whereClause) {
        return DB::table(DB::raw('(import_po, import_po_dates)'))
                ->select('import_po.id as ipo', 'import_po.*', 'import_po_dates.*'
                        ,'factories.id as ftyId', 'factories.name as ftyName', 'factories.vendor_id as ftyVId'
                        ,'export_cos.id as ExCId' , 'export_cos.name as ExCName', 'export_cos.fama as ExCFama'
                        ,'freight_forwarders.id as FFId', 'freight_forwarders.name as FFName'
                        ,'port_sf.id as port_ShipF_ID', 'port_sf.name as port_ShipF_Name'
                        ,'port_st.id as port_ShipT_ID', 'port_st.name as port_ShipT_Name'
                        
                        ,DB::raw("GROUP_CONCAT(distinct tickets_ordered.tickets_ordered separator ', ') as tickets_orderedDates")
                        
                        ,DB::raw("GROUP_CONCAT(distinct product_types.name separator ', ') as prod_typesNames")
                        ,DB::raw("GROUP_CONCAT(distinct product_types.id separator ',') as prod_typesId")
                        ,DB::raw("GROUP_CONCAT(distinct product_styles.name ORDER BY product_styles.id separator ', ') as prod_styleNames")
                        ,DB::raw("GROUP_CONCAT(distinct product_styles.id ORDER BY product_styles.id separator ',') as prod_styleId")
                        ,DB::raw("GROUP_CONCAT(distinct customers.name separator ', ') as customersNames")
                        ,DB::raw("GROUP_CONCAT(distinct customers.id separator ',') as customersId")
                        ,DB::raw("GROUP_CONCAT(distinct licenses.name separator ', ') as licensesNames")
                        ,DB::raw("GROUP_CONCAT(distinct licenses.id separator ',') as licensesId")
                        ,DB::raw("GROUP_CONCAT(distinct salesmen.name separator ', ') as salesmenNames")
                        ,DB::raw("GROUP_CONCAT(distinct salesmen.id separator ',') as salesmenId")

                        )
                ->leftJoin('factories','import_po.fty_produc', '=', 'factories.id')
                ->leftJoin('export_cos','import_po.export_cos', '=', 'export_cos.id')
                ->leftJoin('freight_forwarders','import_po.freight_forwarder', '=', 'freight_forwarders.id')
                ->leftJoin('ports as port_sf','import_po.shipping_from', '=', 'port_sf.id')
                ->leftJoin('ports as port_st','import_po.shipping_to', '=', 'port_st.id')
                
                ->leftJoin('tickets_ordered','import_po.id', '=', 'tickets_ordered.order_id')
                
                ->leftJoin('product_types_select','import_po.id', '=', 'product_types_select.order_id')
                ->leftJoin('product_types as product_types','product_types.id', '=', 'product_types_select.product_types_id')
                
                ->leftJoin('product_styles_select','import_po.id', '=', 'product_styles_select.order_id')
                ->leftJoin('product_styles','product_styles.id', '=', 'product_styles_select.product_styles_id')
                
                ->leftJoin('licenses_select','import_po.id', '=', 'licenses_select.order_id')
                ->leftJoin('licenses','licenses.id', '=', 'licenses_select.licenses_id')
                
                ->leftJoin('customers_select','import_po.id', '=', 'customers_select.order_id')
                ->leftJoin('customers','customers.id', '=', 'customers_select.customers_id')
                
                ->leftJoin('salesmen_select','import_po.id', '=', 'salesmen_select.order_id')
                ->leftJoin('salesmen','salesmen.id', '=', 'salesmen_select.salesmen_id')
                
                ->whereRaw('`import_po`.`id` = `import_po_dates`.`order_po_id`')
                ->where($whereClause)
                //->whereIn([])
                //->where('import_po.open', $open)
                ->groupBy('import_po.id', 'import_po.comments', 'import_po.po_number'
                        ,'import_po.open','import_po.value','import_po.fty_produc'
                        ,'import_po.export_cos','import_po.customer_po_number','import_po.qty_ordered'
                        ,'import_po.tickets_needed','import_po.shipping_from','import_po.shipping_to','import_po.cbm'
                        ,'import_po.book_container','import_po.safety_test','import_po.safety_test_pass','import_po.david_ins_result'
                        ,'import_po.david_ins_comment','import_po.svn_no','import_po.freight_forwarder','import_po.pro_photo','import_po.receive_whse'
                        ,'import_po_dates.id','import_po_dates.created_at','import_po_dates.updated_at','import_po_dates.order_po_id'
                        ,'import_po_dates.date_placed','import_po_dates.tickets_received','import_po_dates.finish_prod','import_po_dates.estimated_arr'
                        ,'import_po_dates.customer_cancel','import_po_dates.act_etd_forwarder','import_po_dates.act_eta_forwarder','import_po_dates.safety_test_on'
                        ,'import_po_dates.fty_prod_safety_test','import_po_dates.rec_comm_inv_pking','import_po_dates.rec_comm_inv_pking_home','import_po_dates.original_fcr'
                        ,'import_po_dates.ctpat_form','import_po_dates.telex_release','import_po_dates.okay_to_pay','import_po_dates.did_we_pay','import_po_dates.send_diann'
                        ,'factories.id', 'factories.name', 'factories.vendor_id'
                        ,'export_cos.id' , 'export_cos.name', 'export_cos.fama'
                        ,'freight_forwarders.id', 'freight_forwarders.name'
                        ,'port_sf.id', 'port_sf.name'
                        ,'port_st.id', 'port_st.name'
                        )
                
                ->orderBy('ipo', 'desc')
                ->paginate(15)
                ;
    }
    
}
