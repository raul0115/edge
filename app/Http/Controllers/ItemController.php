<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Material;
use App\Mounting;
use App\Feature;
use App\Size;
use App\License;
use App\Character;
use App\Customer;
use App\User;
use App\Item;
use App\Status;
use App\ItemStatus;
use App\ItemAssigned;
use App\ItemImage;
use Datatables;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\NotificationsController;
use Illuminate\Validation\Rule;

class ItemController extends Controller
{
    public function __construct()
    {   
        $this->middleware('auth');
        if (!Auth::check()) {return redirect('login');}
    }
    public function index(){
        $status_all = Status::select('name as text','id')
            ->where('active',1)->get();
        $materials = Material::select(
                DB::raw("CONCAT(code,':',name) AS text"),'id')
                ->where('active',1)->get();
        $mountings = Mounting::select(
                    DB::raw("CONCAT(code,':',name) AS text"),'id')
                    ->where('active',1)->get();      
        $features = Feature::select(
                    DB::raw("CONCAT(code,':',name) AS text"),'id')
                    ->where('active',1)->get(); 
        $sizes = Size::select(
                    DB::raw("CONCAT(code,':',name) AS text"),'id')
                    ->where('active',1)->get();
        $licenses = License::select(DB::raw("CONCAT(code,':',name) AS text"),'id')
                    ->where('active',1)->get();      
        $characteres = Character::select(
                    DB::raw("CONCAT(code,':',name) AS text"),'id')
                    ->where('active',1)->get();      
        $customers = Customer::select(DB::raw("CONCAT(code,':',name) AS text"),'id')
                    ->where('active',1)->get();  
        $designer= User::select('name as text','id')->role(['Designer','License Coordinator'])->get(); 
       
      return view('items.index',compact('status_all','materials','mountings','features','sizes','characteres','licenses','customers','designer'));
    }
    public function edit($id){

        $can_load_image= false;
        $can_designer= false;
        $can_send_designer= false;
        $can_change_description= false;
        $can_license= false;
        $can_changed_status= false;
        $user_actual = Auth::user();
        $user_actual_license = false;
        $user_actual_designer = false;
        $can_img_commentary= false;
        //check if user has factory role
        if ($user_actual->hasRole('License Coordinator') ) {
            $user_actual_license = true;
        }elseif(($user_actual->hasRole('Designer') )){
            $user_actual_designer = true;
        }
        if($user_actual_designer || $user_actual_license || $user_actual->hasRole('Admin')){
            $item = Item::findOrFail($id);
            $status= '';
            $status=  ItemStatus::where(['item_id'=>$id,'active'=>1,'send'=>1])->first();
           
           if($user_actual_designer){
                $val_designer = ItemAssigned::where(['active'=>1,'item_id'=>$id,'user_id'=>Auth::user()->id,'tipo'=>1])->first();
                if(!$val_designer){
                    return redirect('/items')->with('error', 'You dont have the permission to access.');
                }
                $can_designer= true;
                $can_load_image= true;
               
            }
            if($user_actual_license ){
                $val_designer = ItemAssigned::where(['active'=>1,'item_id'=>$id,'user_id'=>Auth::user()->id,'tipo'=>1])->first();
                if($val_designer){
                    $can_designer= true;
                    $can_load_image= true;
                }
            }
          
            if($user_actual_license && ($status->status->code=="S1" || $status->status->code=="S2" || $status->status->code=="S10") ){
                $can_license= true;
                $can_send_designer= true;
                $can_change_description=true;
            }
           
            if($user_actual_license && ($status->status->code=="S7" || $status->status->code=="S8" || $status->status->code=="S9" || $status->status->code=="S10")) {
                $arr= [];
                $arr[0]= $user_actual->id;
                $mi_license = ItemAssigned::whereIn('user_id',$arr)->where(['active'=>1,'item_id'=>$id,'tipo'=>2])->first();
                
               if($mi_license){
                $can_changed_status=true;
                $can_change_description=true;
                $can_img_commentary= true;
                $can_send_designer= true;
               }
            }
            if ($user_actual->hasRole('Admin') ) {
                $can_load_image= true;
                $can_designer= true;
                $can_license= true;
                $can_changed_status= true;
                $can_change_description=true;
                $can_img_commentary= true;
                $can_send_designer= true;
            }
           
          
        $item_images=ItemImage::where('item_id',$id)->active()->first();
        $old_images=ItemImage::where(['item_id'=>$id,'active'=>0])->get();
        $materials = Material::select(
            DB::raw("CONCAT(code,':',name) AS text"),'id')
            ->where('active',1)->get();
        $mountings = Mounting::select(
            DB::raw("CONCAT(code,':',name) AS text"),'id')
            ->where('active',1)->get();      
        $features = Feature::select(
            DB::raw("CONCAT(code,':',name) AS text"),'id')
            ->where('active',1)->get(); 
        $sizes = Size::select(
            DB::raw("CONCAT(code,':',name) AS text"),'id')
            ->where('active',1)->get();
        $licenses = License::select('code','name','id')
            ->where('active',1)->get();      
        $characteres = Character::select(
            DB::raw("CONCAT(code,':',name) AS text"),'id')
            ->where('active',1)->get();      
        $customers = Customer::select('code','name','id')
            ->where('active',1)->get();          
         
        $user_designer= User::select('name','id')->role(['Designer','License Coordinator'])->get(); 
        $user_license_coordinator= User::select('name','id')->role('License Coordinator')->get(); 
        $arr_code=['S8','S9','S10'];
        $new_status=Status::select('name', 'id')
            ->whereIn('code',$arr_code)
            ->where(['active'=>1])->get();

        $assigned_designer = ItemAssigned::select('user_id')
        ->where(['active'=>1,'item_id'=>$id,'tipo'=>1])->get(); 
        $users_designer_id="";
        $users_designer_name="";
        $i=0;
        foreach($assigned_designer as $assd){
            if($i==0){
                $users_designer_id=$users_designer_id."".$assd->user_id;
                $users_designer_name=$users_designer_name."".$assd->user->name;
            }else{
                $users_designer_id=$users_designer_id.",".$assd->user_id;
                $users_designer_name=$users_designer_name.",".$assd->user->name;
            }
            $i++;
        }
        $assigned_license = ItemAssigned::select('user_id')
        ->where(['active'=>1,'item_id'=>$id,'tipo'=>2])->get(); 
        $users_license_id="";
        $users_license_name="";
        $i=0;
        foreach($assigned_license as $assl){
            if($i==0){
                $users_license_id=$users_license_id."".$assl->user_id;
                $users_license_name=$users_license_name."".$assl->user->name;
            }else{
                $users_license_id=$users_license_id.",".$assl->user_id;
                $users_license_name=$users_license_name.",".$assl->user->name;
            }
            $i++;
        }
       
        return view( 'items.edit',compact(
            'item',
            'materials',
            'mountings',
            'features',
            'sizes',
            'licenses',
            'characteres',
            'customers',
            'item_images',
            'user_designer',
            'user_license_coordinator',
            'users_designer_id',
            'users_designer_name',
            'users_license_id',
            'users_license_name',
            'status',
            'can_designer',
            'can_license',
            'can_changed_status',
            'new_status',
            'old_images',
            'can_change_description',
            'can_img_commentary',
            'can_send_designer'
        ));
        }
        return redirect('/items')->with('error', 'You dont have the permission to access.');
      }
  

    public function edita($id){

        $can_load_image= false;
        $can_designer= false;
        $can_send_designer= false;
        $can_change_description= false;
        $can_license= false;
        $can_changed_status= false;
        $user_actual = Auth::user();
        $user_actual_license = false;
        $user_actual_designer = false;
        $can_img_commentary= false;
        //check if user has factory role
        if ($user_actual->hasRole('License Coordinator') ) {
            $user_actual_license = true;
        }elseif(($user_actual->hasRole('Designer') )){
            $user_actual_designer = true;
        }
        if($user_actual_designer || $user_actual_license || $user_actual->hasRole('Admin')){
            $item = Item::findOrFail($id);
            $status= '';
            $status=  ItemStatus::where(['item_id'=>$id,'active'=>1,'send'=>1])->first();
           
            if($user_actual_designer && $status->status->code=="S1"){
                return redirect('/items')->with('error', 'You dont have the permission to access.');
            }
            if($user_actual_designer && ($status->status->code=="S2" || $status->status->code=="S10" || $status->status->code=="S7")){
                $val_designer = ItemAssigned::where(['active'=>1,'item_id'=>$id,'user_id'=>Auth::user()->id,'tipo'=>1])->first();
                if(!$val_designer){
                    return redirect('/items')->with('error', 'You dont have the permission to access.');
                }
                $can_designer= true;
                $can_load_image= true;
               
            }
            if($user_actual_license ){
                $val_designer = ItemAssigned::where(['active'=>1,'item_id'=>$id,'user_id'=>Auth::user()->id,'tipo'=>1])->first();
                if($val_designer){
                    $can_designer= true;
                    $can_load_image= true;
                }
            }
          
            if($user_actual_license && ($status->status->code=="S1" || $status->status->code=="S2" || $status->status->code=="S10") ){
                $can_license= true;
                $can_send_designer= true;
                $can_change_description=true;
            }
           
            if($user_actual_license && ($status->status->code=="S7" || $status->status->code=="S8" || $status->status->code=="S9" || $status->status->code=="S10")) {
                $arr= [];
                $arr[0]= $user_actual->id;
                $mi_license = ItemAssigned::whereIn('user_id',$arr)->where(['active'=>1,'item_id'=>$id,'tipo'=>2])->first();
                
               if($mi_license){
                $can_changed_status=true;
                $can_change_description=true;
                $can_img_commentary= true;
                $can_send_designer= true;
               }
            }
            if ($user_actual->hasRole('Admin') ) {
                $can_load_image= true;
                $can_designer= true;
                $can_license= true;
                $can_changed_status= true;
                $can_change_description=true;
                $can_img_commentary= true;
                $can_send_designer= true;
            }
           
          
        $item_images=ItemImage::where('item_id',$id)->active()->first();
        $old_images=ItemImage::where(['item_id'=>$id,'active'=>0])->get();
        $materials = Material::select(
            DB::raw("CONCAT(code,':',name) AS text"),'id')
            ->where('active',1)->get();
        $mountings = Mounting::select(
            DB::raw("CONCAT(code,':',name) AS text"),'id')
            ->where('active',1)->get();      
        $features = Feature::select(
            DB::raw("CONCAT(code,':',name) AS text"),'id')
            ->where('active',1)->get(); 
        $sizes = Size::select(
            DB::raw("CONCAT(code,':',name) AS text"),'id')
            ->where('active',1)->get();
        $licenses = License::select('code','name','id')
            ->where('active',1)->get();      
        $characteres = Character::select(
            DB::raw("CONCAT(code,':',name) AS text"),'id')
            ->where('active',1)->get();      
        $customers = Customer::select('code','name','id')
            ->where('active',1)->get();          
         
        $user_designer= User::select('name','id')->role(['Designer','License Coordinator'])->get(); 
        $user_license_coordinator= User::select('name','id')->role('License Coordinator')->get(); 
        $arr_code=['S8','S9','S10'];
        $new_status=Status::select('name', 'id')
            ->whereIn('code',$arr_code)
            ->where(['active'=>1])->get();

        $assigned_designer = ItemAssigned::select('user_id')
        ->where(['active'=>1,'item_id'=>$id,'tipo'=>1])->get(); 
        $users_designer_id="";
        $users_designer_name="";
        $i=0;
        foreach($assigned_designer as $assd){
            if($i==0){
                $users_designer_id=$users_designer_id."".$assd->user_id;
                $users_designer_name=$users_designer_name."".$assd->user->name;
            }else{
                $users_designer_id=$users_designer_id.",".$assd->user_id;
                $users_designer_name=$users_designer_name.",".$assd->user->name;
            }
            $i++;
        }
        $assigned_license = ItemAssigned::select('user_id')
        ->where(['active'=>1,'item_id'=>$id,'tipo'=>2])->get(); 
        $users_license_id="";
        $users_license_name="";
        $i=0;
        foreach($assigned_license as $assl){
            if($i==0){
                $users_license_id=$users_license_id."".$assl->user_id;
                $users_license_name=$users_license_name."".$assl->user->name;
            }else{
                $users_license_id=$users_license_id.",".$assl->user_id;
                $users_license_name=$users_license_name.",".$assl->user->name;
            }
            $i++;
        }
       
        return view( 'items.edit',compact(
            'item',
            'materials',
            'mountings',
            'features',
            'sizes',
            'licenses',
            'characteres',
            'customers',
            'item_images',
            'user_designer',
            'user_license_coordinator',
            'users_designer_id',
            'users_designer_name',
            'users_license_id',
            'users_license_name',
            'status',
            'can_designer',
            'can_license',
            'can_changed_status',
            'new_status',
            'old_images',
            'can_change_description',
            'can_img_commentary',
            'can_send_designer'
        ));
        }
        return redirect('/items')->with('error', 'You dont have the permission to access.');
      }
  

    public function create() {
        $materials = Material::select(
            DB::raw("CONCAT(code,':',name) AS text"),'id')
            ->where('active',1)->get();
        $mountings = Mounting::select(
            DB::raw("CONCAT(code,':',name) AS text"),'id')
            ->where('active',1)->get();      
        $features = Feature::select(
            DB::raw("CONCAT(code,':',name) AS text"),'id')
            ->where('active',1)->get(); 
        $sizes = Size::select(
            DB::raw("CONCAT(code,':',name) AS text"),'id')
            ->where('active',1)->get();
        $licenses = License::select('code','name','id')
            ->where('active',1)->get();      
        $characteres = Character::select(
            DB::raw("CONCAT(code,':',name) AS text"),'id')
            ->where('active',1)->get();      
        $customers = Customer::select('code','name','id')
            ->where('active',1)->get();          
            
        return view( 'items.create',compact(
            'materials',
            'mountings',
            'features',
            'sizes',
            'licenses',
            'characteres',
            'customers'
        ));
       
      
    }
    public function store(Request $request){
        if($request->has('group_id') && $request->get('group_id')[0]==null){
            $array=$request->get('group_id');
            unset($array[0]);
            $request['group_id']=$array;
        }
        $this->validate($request, [     
            'name' => 'required|string|min:3', 
            'description' => 'nullable|string', 
            'material_id' => 'required|integer|exists:materials,id', 
            'mounting_id' => 'required|integer|exists:mountings,id',
            'feature_id' => 'required|integer|exists:features,id', 
            'size_id' => 'required|integer|exists:sizes,id', 
            'license_id' => 'required|integer|exists:licenses,id',
            'character_id' => 'required|integer|exists:characteres,id',
            'customer_id' => 'nullable|integer|exists:customers,id',
            'group_id' =>  'array|nullable'

        ]
        );
        $material = Material:: findOrFail($request->material_id);
        $mounting = Mounting:: findOrFail($request->mounting_id);
        $feature = Feature:: findOrFail($request->feature_id);
        $size = Size:: findOrFail($request->size_id);
        $character = Character:: findOrFail($request->character_id);

        $last = Item::orderby('created_at','DESC')->first();
        $letras = collect( range("A","Z"));
      
        $sequence='';
        if(is_null($last)){
            $sequence= "A";
        }elseif( $letras->search($last->sequence) >= 0 ){
            $index=$letras->search($last->sequence);
            if($index==25){
                $sequence=1;
            }else{
                $sequence = $letras->toArray()[$index+1];
            }
        }else{
            $num= (int) $last->sequence;
            $sequence = $num+1;
        }
        $code= $material->code . $mounting->code . $feature->code . $size->code . $sequence . $character->code;
        if($request->customer_id!=null && $request->customer_id!=''){
            $customer = Customer:: findOrFail($request->customer_id);
            $code = $code . '-' . $customer->code;
        }
        $campos = $request->all();
        $campos['sequence']=$sequence;
        $campos['code']=$code;
        $campos['user_id'] = Auth::user()->id;
        $status= Status::where(['code'=>'S1','active'=>1])->first();
        $item = Item::create($campos);
        $item->groups()->sync($request->get('group_id'));
        $item_status= ItemStatus::create([
            'item_id'=>$item->id,
            'status_id'=>$status->id,
            'user_id'=>Auth::user()->id,
            'active'=>1,
            'send'=>1
        ]);
        $itemAssigned = ItemAssigned::create([
                'item_id'=>$item->id,
                'active'=>1,
                'user_id'=>Auth::user()->id,
                'tipo'=>2

        ]);
        
        return $item;
    }
    public function obtener_character(Request $request,$id=null)
    {
        if($id==null){
            return  []; 
        }
        return  Character::select(
            DB::raw("CONCAT(code,':',name) AS text"),'id')
            ->where(['active'=>1,'license_id'=>$id])->get();  
    }


    public function load_imagen(Request $request,$id){
        $this->validate($request,[
            'imagen'=>'required|image|max:10240'
        ]);
        $imagen = $request->file('imagen');
        $name= $imagen->getClientOriginalName();
        $imagenStore = $request->file('imagen')->store('public');
        $path = Storage::url("public/$imagenStore");
        $version =1;
        $img_anterior= ItemImage::where(['item_id'=>$id,'active'=>1])->first();
        if($img_anterior){
            if($img_anterior->rejected==1){
                $version = $img_anterior->version + 1;
            }else{
                $version = $img_anterior->version;
            }
            $img_anterior->active=0;
            $img_anterior->save();
        }
        $item_image = ItemImage::create([
            'item_id'=>$id,
            'name'=>$name,
            'file_name'=>$imagenStore,
            'version'=>$version,
            'path'=>$path,
            'user_id'=>Auth::user()->id
        ]);
        return response()->json([
            'image_id' => $item_image->id
        ], 200);


    }


    public function getBasicObjectDatasss(Request $request)
    {
        $status=false;
        $status_text="";
        $query="";
        /*
        if($request->columns[3]['search']['value']!=null){
             $status_text=$request->columns[3]['search']['value'];
             $status=true;
             $query = Item::whereHas('item_status', function ($query2){
                 $query2->where('active', 1);
             })->whereHas('item_status.status', function ($query2) use ($status_text) {
                $query2->where('name', 'like', "%{$status_text}%");
            })->with(['material','mounting','feature','size','character.license','character','customer','item_status'=>function($query3){
                 $query3->where('active', 1);
             },'item_status.status'=>function ($query2) use ($status_text) {
                $query2->where('name', 'like', "%{$status_text}%");
            },'item_images'=>function ($query2) {
                $query2->where('active', 1);
            }])->select('items.*')->groupBy('items.id');
         }else{
             $query = Item::whereHas('item_status', function ($query2){
                 $query2->where('active', 1);
                 $query2->first();
             })->with(['material','mounting','feature','size','character.license','character','customer','item_status'=>function($query3){
                 $query3->where('active', 1);
             },'item_status.status','item_images'=>function ($query2) {
                $query2->where('active', 1);
                $query2->first();
            }])->select('items.*')->groupBy('items.id');
         }*/
         $query = Item::whereHas('item_status', function ($query2){
            $query2->where('active', 1);
        });
        if ($request->has('name')) {
            $query->where('items.name', 'like', "%{$request->get('name')}%");
        }
        if ($request->has('code')) {
            $query->where('items.code', 'like', "%{$request->get('code')}%");
        }
        
        $query->with(['material','mounting','feature','size','character.license','character','customer',
        'item_status'=>function ($query2) use ($request){
            $query2->where('active',1);
            if ($request->has('status') && $request->get('status')!=null) {
                $query2->whereIn('status_id', [1,2]);
            }
           
        },'item_status.status']);
        $query->selectRaw('distinct items.*');
        return Datatables::eloquent($query)
        ->editColumn('customer.name', function (Item $item) {
            return ($item->customer)?$item->customer->name:'';
        })
       ->addColumn('status', function (Item $item) {
            return $item->item_status[0]->status->name;
        })
        ->addColumn('img', function (Item $item) {
              $img= ItemImage::where(['item_id'=>$item->id,'active'=>1])->first();
              if($img){
               
                return ' 
                <a onclick="view_files('.$img->id.')">view image</a>';
              }  
          return null;
        })
        ->rawColumns(['img'])
        ->make(true);
    }
    public function getBasicObjectData(Request $request)
    {
      //  dd($request->get('assigned'));
        $items= DB::table('items')
            ->join('materials', 'materials.id', '=', 'items.material_id')
            ->join('mountings', 'mountings.id', '=', 'items.mounting_id')
            ->join('features', 'features.id', '=', 'items.feature_id')
            ->join('sizes', 'sizes.id', '=', 'items.size_id')
            ->join('characteres', 'characteres.id', '=', 'items.character_id')
            ->leftJoin('customers', 'customers.id', '=', 'items.customer_id')
            ->join('licenses', 'licenses.id', '=', 'characteres.license_id')
            
            ->join('item_status', function ($join) {
                $join->on('item_status.item_id', '=', 'items.id')
                     ->where('item_status.active', '=', 1)
                     ->where('item_status.send', '=', 1);
            })
            
            ->join('status', 'status.id', '=', 'item_status.status_id');
            
            if ($request->has('name')) {
                $items->where('items.name', 'like', "%{$request->get('name')}%");
            }

            if ($request->has('code')) {
                $items->where('items.code', 'like', "%{$request->get('code')}%");
            }
            if ($request->has('status') && $request->get('status')!=null) {
                $items->whereIn('status_id', $request->get('status'));
            }
            if ($request->has('designer') && $request->get('designer')!=null) {
                $items->join('item_assigned', function ($join) use ($request){
                    $join->on('item_assigned.item_id', '=', 'items.id')
                     ->where('item_assigned.active', '=', 1)
                     ->whereIn('item_assigned.user_id', $request->get('designer'));
                });
            }else{
                
                $items->leftJoin('item_assigned', function ($join) {
                    $join->on('item_assigned.item_id', '=', 'items.id')
                     ->where('item_assigned.active', '=', 1);
                });
            }
            if ($request->has('material') && $request->get('material')!=null) {
                $items->whereIn('items.material_id', $request->get('material'));
            }
            if ($request->has('mounting') && $request->get('mounting')!=null) {
                $items->whereIn('items.mounting_id', $request->get('mounting'));
            }
            if ($request->has('feature') && $request->get('feature')!=null) {
                $items->whereIn('items.feature_id', $request->get('feature'));
            }
            if ($request->has('size') && $request->get('size')!=null) {
                $items->whereIn('items.size_id', $request->get('size'));
            }
            if ($request->has('license') && $request->get('license')!=null) {
                $items->whereIn('licenses.id', $request->get('license'));
            }
            if ($request->has('character') && $request->get('character')!=null) {
                $items->whereIn('items.character_id', $request->get('character'));
            }
            if ($request->has('customer') && $request->get('customer')!=null) {
                $items->whereIn('items.customer_id', $request->get('customer'));
            }
            
            
            $items->select('items.*',
             'item_status.status_id as status_id',
             'status.name as status_name',
             
              'materials.name as material_name',
              'materials.code as material_code',
              
              
              'mountings.name as mounting_name',
              'mountings.code as mounting_code',
              
              'features.name as feature_name',
              'features.code as feature_code',
              
              'sizes.name as size_name',
              'sizes.code as size_code',
              
              'characteres.name as character_name',
              'characteres.code as character_code',
              
              'customers.name as customer_name',
              'customers.code as customer_code',
              
              'licenses.name as license_name',
              'licenses.id as license_id',
              'licenses.code as license_code',
             DB::raw("GROUP_CONCAT(distinct item_assigned.user_id separator ',') as item_assigned_id")

            );
            $items->groupBy('items.id');
        return Datatables::of($items)
        ->addColumn('designer', function ($item) {
            $assigned = ItemAssigned::where(['active'=>1,'item_id'=>$item->id])->with('user')->get();
            return $assigned->map(function(ItemAssigned $assig) {
                return $assig->user->name;
            })->implode(',');
        })
        ->addColumn('img', function ( $item) {
              $img= ItemImage::where(['item_id'=>$item->id,'active'=>1])->first();
              if($img){
              
                return ' <a class="test-popup-link" href="'. url($img->path).'">
                <img src="'. url($img->path).'" width="100" height="100"></a>';
              }  
          return null;
        })
        ->rawColumns(['img','assigned'])
        ->make(true);
    }

    public function submit_edit(Request $request,$id) {
        
        $messages = [   
            'string'    => 'Invalid data.',
            'array'    => 'Invalid users.',
            'required'  => 'This field is required.',
            'in'      => 'The :attribute must be one of the following types: :values'
        ];
        if($request->name!="designer" && $request->name!="user_lic" && $request->name!="new_status"){
            $validator = Validator::make($request->all(), [
            'name'   => 'required|string',
            'value'  => 'string'
            ], $messages);
        }elseif($request->name=="new_status"){
            $arr_code=['S8','S9','S10'];
            $n_status =Status::select('name','id')
            ->whereIn('code',$arr_code)
            ->where(['active'=>1])->get();
            $arr_status_name=$n_status->pluck('name');
            $arr_status_id=$n_status->pluck('id')->toArray();
            
            $st_name="";
            $i=0;
            foreach( $arr_status_name as $arr1){
                if($i==0){
                    $st_name= $st_name."".$arr1;
                }else{
                    $st_name=  $st_name.",".$arr1;
                }
                $i++;
            }
            $messages = [   
                'string'    => 'Invalid data.',
                'array'    => 'Invalid users.',
                'required'  => 'This field is required.',
                'in'      => 'The :attribute must be one of the following types: '.$st_name
            ];
           
           
            $validator = Validator::make($request->all(), [
                'name'   => 'required|string',
                'value'  => [
                    'required',
                    'integer',
                    Rule::in($arr_status_id),
                ],
                ], $messages);
        }else{
            $validator = Validator::make($request->all(), [
                'name'   => 'required|string',
                'value'  => 'required|array'
                ], $messages);
        }
        
        if ($validator->passes()) {
           
            if($request->name=="description" || $request->name=="techpack" || $request->name=="ba" || $request->name=="upc"){
                $item= Item::find($id);
                $item->{$request->name} =$request->value;
                $item->save();
                return response()->json([   
                    'success'=>'Updated records.',
                                'newValue' =>   $item->{$request->name}],200);
            }elseif($request->name=="img_commentary"){
                $item_image= ItemImage::find($request->pk);
                $item_image->commentary= $request->value;
                $item_image->commentary_by = Auth::user()->id;
                $item_image->save();
                return response()->json([   
                    'success'=>'Updated records.',
                                'newValue' =>   $item_image->commentary],200);

            }elseif($request->name=="new_status"){
                

                $status_actual=ItemStatus::where([
                    'item_id'=>$id,
                    'active'=> 1,
                    'send'=>1
                    ])->first();

                    $status= Status::findOrFail($request->value);
                  

                $item_status= ItemStatus::create([
                        'item_id' => $id,
                        'status_id'=>$status->id,
                        'user_id'=>Auth::user()->id,
                        'active'=>1,
                        'status_prev'=> $status_actual->id,
                        'send'=>0
                    ]);
                    if($status->code=="S10"){
                        $item_images=ItemImage::where('item_id',$id)->active()->first();
                        $item_images->rejected=1;
                        $item_images->save();
                    }
                  
                return response()->json([   
                        'success'=>$status->name,'code'=>$status->code],200);


            }else{
                $tipo=1;
                if($request->name!="designer"){
                    $tipo=2;
                }

                ItemAssigned::where(['active'=> 1,'item_id'=>$id,'tipo'=>$tipo])
                    ->where('item_id', $id)
                    ->update(['active' => 0]);
                    foreach($request->value as $arr){
                        $campos = [];
                        $campos['item_id']=$id;
                        $campos['active']=1;
                        $campos['user_id'] = $arr;
                        $campos['tipo'] = $tipo;
                        $itemAssigned = ItemAssigned::create($campos);
                    }
                
                return response()->json([   
                    'success'=>'sucess'],200);
                
            }      
                
          
			
        }   
                                                                                                                       
    	return response()->json(['error'=>$validator->errors()->all()],500);
        }
        public function change_status_commentary (Request $request,$id){
            $status_actual=ItemStatus::where([
                'item_id'=>$id,
                'active'=> 1,
                'send'=>1
            ])->first();
            $status_new=ItemStatus::where([
                'item_id'=>$id,
                'active'=> 1,
                'status_prev'=>$status_actual->id,
            ])->first();
            $status_actual->active=0;
            $status_actual->save();
            $status_new->commentary= ($request->get('commentary'))? $request->get('commentary') : null;
            $status_new->send=1;
            $status_new->save();
            $notification_controller = new NotificationsController();
            $notification_controller->designerItem($id,'Item assigned, change status.');
            return redirect("/items/edit/{$id}")->with('message', 'Change status successfull.');    

        }
        
        public function img_description (Request $request,$id){
            $img= ItemImage::findOrFail($request->image_id);
            $img->description= $request->img_description;
            $img->save();
            return redirect("/items/edit/{$id}")->with('message', 'Load image successfull.');
        }
        
        public function view_img($id){
            $img= ItemImage::findOrFail($id);
            return url($img->path);
        }
        public function send_designer(Request $request,$id){
            $item = Item::findOrFail($id);
            $user_actual = Auth::user();
            $val_designer = ItemAssigned::where(['active'=>1,'item_id'=>$id,'tipo'=>1])->first();
            if(!$val_designer){
                return redirect("/items/edit/{$id}")->with('error', 'You must add an Designer.');
            }
            if(!$user_actual->hasRole(['License Coordinator','Admin'])){
                return redirect("/items/edit/{$id}")->with('error', 'You dont have the permission to access.');
            }
            $code="S2";
            $status_actual=ItemStatus::where([
                    'item_id'=>$id,
                    'active'=> 1
            ])->first();
            $status_actual->active=0;
            $status_actual->save();
            $status= Status::where(['code'=>$code,'active'=>1])->first();
            $item_status= ItemStatus::create([
                    'item_id' => $id,
                    'status_id'=>$status->id,
                    'user_id'=>Auth::user()->id,
                    'active'=>1,
                    'status_prev'=> $status_actual->id,
                    'commentary'=> ($request->get('commentary'))? $request->get('commentary') : null,
                    'send'=>1
            ]);
            $notification_controller = new NotificationsController();
            $notification_controller->designerItem($id);
                
            return redirect("/items/edit/{$id}")->with('message', 'Send mail successfull.');    
           
        }
        public function send_license($id){
            $item = Item::findOrFail($id);
            $user_actual = Auth::user();
            $val_license = ItemAssigned::where(['active'=>1,'item_id'=>$id,'tipo'=>2])->first();
            if(!$val_license){
                return redirect("/items/edit/{$id}")->with('error', 'You must add an License Coordinator.');
            }
            $val_designer = ItemAssigned::where(['active'=>1,'item_id'=>$id,'user_id'=> $user_actual->id,'tipo'=>1])->first();
            if(!$user_actual->hasRole(['Admin']) && !$val_designer){
                    return redirect("/items/edit/{$id}")->with('error', 'You dont have the permission to access.');
            }
            $code="S7";
            $status_actual=ItemStatus::where([
                'item_id'=>$id,
                'active'=> 1
                ])->first();
           

            if($status_actual->status->code!="S1"){
                $item_images=ItemImage::where(['item_id'=>$id,'rejected'=>0])->active()->first();
                if(!$item_images){
                    return redirect("/items/edit/{$id}")->with('error', 'You must add an image.');
                }
                $status= Status::where(['code'=>$code,'active'=>1])->first();
                
                $status_actual->active=0;
                $status_actual->save();
                $item_status= ItemStatus::create(
                    ['item_id' => $id,
                    'status_id'=>$status->id,
                    'user_id'=>$user_actual->id,
                    'active'=>1,
                    'status_prev'=> $status_actual->id,
                    'send'=>1
                ]);
                
                $notification_controller = new NotificationsController();
                $notification_controller->licenseItem($id);
                return redirect("/items/edit/{$id}")->with('message', 'Send mail successfull.');    
            }
            return redirect("/items/edit/{$id}")->with('message', 'Existe una peticion de este tipo en proceso');
        }
        public function get_groups(Request $request){
            $value = $request->get('q');
            $page_limit = $request->get( 'page_limit');
            $page = $request->get( 'page' );
            $offset =($page - 1)*$page_limit;
            $groups= DB::table('groups')
            ->where([
                ['groups.name', 'like', "%{$value}%"]
                ])
            ->select(
                'groups.id as id',
                'groups.name as text'
            )->groupBy('groups.id');
            $total=$groups->get()->count();
            $coll=$groups->skip( $offset)
                ->take($page_limit)->get();
            return response()->json([   
                'incomplete_results'=>false,
                'groups' =>   $coll,
                'total'=>$total
            ],200);
    
        }

    public function send_for_sampling($id){

        $item = Item::findOrFail($id);
        $item->sampling = 1;
        $item->save();
        return redirect("/items/edit/{$id}")->with('message', 'Send for sampling successfull.');    
    }
}
