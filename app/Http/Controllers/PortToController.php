<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\PortTo;
use Carbon\Carbon;
use DB;
use Log;
use Illuminate\Validation\Rule;

class PortToController extends Controller {

    /**
     * Construct method makes sure that all pages here are accessed by 
     * registered users by using the auth middleware
     */
    public function __construct() {
        $this->middleware('auth');
        if (Auth::check()) {
            
        } else {
            return redirect('login');
        }
    }

    /**
     * Function to display table management page
     * 
     * @param Request $request
     * @return view
     */
    public function index(Request $request) {
        $user = Auth::user();
        
        if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
            
            return redirect('/home')->withErrors("Only Admin can access these settings.");
            
        }

      
        $portsto = $this->portstoTable($request);
       
        $vista=view('portsto.index', 
        [
            
            'portsto' => $portsto,
           
            
        ]);
        if ($request->isMethod('post')) {
            $request->session()->flash('message', 'successful!');
        
        }
        return $vista;
    }
    
   
     /**
     * This function manages CRUD for ports_to Table
     * 
     * @param object $request
     * @return types
     */
    private function portstoTable($request){
       // var_dump($request->table);
        //var_dump($request->action);
        //die();
        if(isset($request)) {
            
        //check if request has search ports
            
            if ($request->search_portsto) {
                
                return $portsto = DB::table('ports_to')->select('*')->where('port_name', 'like', "%$request->search_portsto%")->orderBy('id', 'desc')->paginate(10, ['*'], 'ports_to');
            }
        
        //Edit
            if ($request->table == 'portsto' and $request->action  == 'edit_portsto') {
                
                $this->validate($request, [
                'port_name'          => 'required|string',
                'days_est_arrival'          => 'required|integer',
                'edit_id'       =>  'exists:ports_to,id',
                ]);
                
                DB::table('ports_to')
                        ->where('id', $request->edit_id)
                        ->update([
                            'port_name' => $request->port_name,
                            'days_est_arrival' => $request->days_est_arrival
                            ]);

            }
        
        //Add
            if ($request->table == 'portsto' and $request->action  == 'add_portsto') {
                
                $this->validate($request, [
                    'port_name'          => 'required|string|unique:ports_to,port_name',
                    'days_est_arrival'          => 'required|integer',
                ]);
                
                DB::table('ports_to')->insert([
                    'port_name' => $request->port_name,
                    'days_est_arrival' => $request->days_est_arrival
                    ]);
         
            }
            /*
        //Delete OR Deactivate
            if ($request->table == 'ports' and $request->action  == 'delete_ports') {
            
                $this->validate($request, [
                    'delete_port_id'       => 'exists:ports,id',
                ]);
                
                DB::table('ports')->where('id', $request->delete_port_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'ports' and $request->action  == 'activate_ports') {
            
                $this->validate($request, [
                    'activate_port_id'       => 'exists:ports,id',
                ]);
                
                DB::table('ports')->where('id', $request->activate_port_id)->update(['active' => 1]);
         
            }
            */
            
        }
        return $portsto= PortTo::orderBy('id', 'desc')->paginate(10, ['*'], 'ports_to');
        //return $portsto = DB::table('ports_to')->select('*')->orderBy('id', 'desc')->paginate(100, ['*'], 'ports');
    }
   
    
    
}
