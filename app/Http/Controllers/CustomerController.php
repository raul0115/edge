<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Customer;
use Carbon\Carbon;
use DB;
use Log;
use Illuminate\Validation\Rule;

class CustomerController extends Controller {

    /**
     * Construct method makes sure that all pages here are accessed by 
     * registered users by using the auth middleware
     */
    public function __construct() {
        $this->middleware('auth');
        if (Auth::check()) {
            
        } else {
            return redirect('login');
        }
    }

    /**
     * Function to display table management page
     * 
     * @param Request $request
     * @return view
     */
    public function index(Request $request) {
        $user = Auth::user();
        
        if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
            
            return redirect('/home')->withErrors("Only Admin can access these settings.");
            
        }

        $customers = $this->customersTable($request);
        
        $vista=view('customers.index', 
        [
            
            'customers' => $customers,
            
            
        ]);
        if ($request->isMethod('post')) {
            $request->session()->flash('message', 'successful!');
        
        }
        return $vista;
    }
    
    /**
     * This function manages CRUD for customers table
     * 
     * @param object $request
     * @return type
     */
    private function customersTable($request){
        
        if(isset($request)) {
            
        //check if request has search customers
            
            if ($request->search_customers) {
                
                return $customers = DB::table('customers')->select('*')->where('name', 'like', "%$request->search_customers%")->orderBy('id', 'desc')->paginate(10, ['*'], 'customers');
            }
        
        //Edit
            if ($request->table == 'customers' and $request->action  == 'edit_customers') {
                
                $this->validate($request, [
                'name'          => 'required|string|max:250',
                'code' => [
                    'required','string','max:2',
                    Rule::unique('customers')->ignore($request->edit_id)
                ],
                'edit_id'       =>  'exists:customers,id',
                ]);
                
                DB::table('customers')->where('id', $request->edit_id)->update([
                        'name' => $request->name,
                        'code'=> $request->code
                        ]
                );

            }
        
        //Add
            if ($request->table == 'customers' and $request->action  == 'add_customers') {
            
                $this->validate($request, [
                    'name'          => 'required|string|unique:customers,name|max:250',
                    'code'   => 'required|string|max:2|unique:customers,code',
                    'edit_id'       => 'exists:customers,id',
                ]);
                
                DB::table('customers')->insert([
                    'name' => $request->name,
                    'code'=> $request->code
                    ]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'customers' and $request->action  == 'delete_customers') {
            
                $this->validate($request, [
                    'delete_customer_id'       => 'exists:customers,id',
                ]);
                
                DB::table('customers')->where('id', $request->delete_customer_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'customers' and $request->action  == 'activate_customers') {
            
                $this->validate($request, [
                    'activate_customer_id'       => 'exists:customers,id',
                ]);
                
                DB::table('customers')->where('id', $request->activate_customer_id)->update(['active' => 1]);
         
            }
            
            
        }
        
        return $customers = DB::table('customers')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'customers');
    }
    
    
    
}
