<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;

use DB;
use Log;
use Illuminate\Validation\Rule;

class ProductStyleController extends Controller {

    /**
     * Construct method makes sure that all pages here are accessed by 
     * registered users by using the auth middleware
     */
    public function __construct() {
        $this->middleware('auth');
        if (Auth::check()) {
            
        } else {
            return redirect('login');
        }
    }

    /**
     * Function to display table management page
     * 
     * @param Request $request
     * @return view
     */
    public function index(Request $request) {
        $user = Auth::user();
        
        if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
            
            return redirect('/home')->withErrors("Only Admin can access these settings.");
            
        }

       
        $product_styles = $this->product_stylesTable($request);
      
        $vista=view('product_styles.index', 
        [
           
            'product_styles' => $product_styles
            
        ]);
        if ($request->isMethod('post')) {
            $request->session()->flash('message', 'successful!');
        
        }
        return $vista;
    }
    
   
    
    /**
     * This function manages CRUD for product_styles Table
     * 
     * @param object $request
     * @return types
     */
    private function product_stylesTable($request){
        
        if(isset($request)) {
            
        //check if request has search product_styles
            
            if ($request->search_product_styles) {
                
                return $product_styles = DB::table('product_styles')->select('*')->where('name', 'like', "%$request->search_product_styles%")->orderBy('id', 'desc')->paginate(10, ['*'], 'product_styles');
            }
        
        //Edit
            if ($request->table == 'product_styles' and $request->action  == 'edit_product_styles') {
                
                $this->validate($request, [
                'name'          => 'required|string',
                'edit_id'       =>  'exists:product_styles,id',
                ]);
                
                DB::table('product_styles')->where('id', $request->edit_id)->update(['name' => $request->name]);

            }
        
        //Add
            if ($request->table == 'product_styles' and $request->action  == 'add_product_styles') {
            
                $this->validate($request, [
                    'name'          => 'required|string|unique:product_styles,name',
                    'edit_id'       => 'exists:product_styles,id',
                ]);
                
                DB::table('product_styles')->insert(['name' => $request->name]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'product_styles' and $request->action  == 'delete_product_styles') {
            
                $this->validate($request, [
                    'delete_product_style_id'       => 'exists:product_styles,id',
                ]);
                
                DB::table('product_styles')->where('id', $request->delete_product_style_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'product_styles' and $request->action  == 'activate_product_styles') {
            
                $this->validate($request, [
                    'activate_product_style_id'       => 'exists:product_styles,id',
                ]);
                
                DB::table('product_styles')->where('id', $request->activate_product_style_id)->update(['active' => 1]);
         
            }
            
            
        }
        
        return $product_styles = DB::table('product_styles')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'product_styles');
    }
    
    
}
