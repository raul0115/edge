<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Auth;
use DB;
use App\ImportPo;
use App\Http\Controllers\NotificationsController;
class TriggerEventsController extends Controller
{
    /**
     * This function trigger shipping to computation and sends notification if necessary
     * 
     * @param int $order_id
     * @param string $shipping_to LA or NJ
     */
    public function triggerShippingTo($order_id, $shipping_to) {
        
        $import_po_dates = DB::table('import_po_dates')->where('order_po_id', $order_id)->first();
        
        if ($import_po_dates->finish_prod != null)
        {
            $EstimatedArrival = $this->calculateEstimatedArrival($shipping_to, $import_po_dates->finish_prod );
            DB::table('import_po_dates')
                        ->where('order_po_id', $order_id)
                        ->update(['estimated_arr' => $EstimatedArrival]);
            
            $notification_controller = new NotificationsController();
            $notification_controller->esitimateArriavl($order_id, $EstimatedArrival);
        }
        
        
    }
    
    /**
     * Gets the LA hours
     * 
     * @return int
     */
    private function getLA(){
        
        $LA = \App\Http\Controllers\GeneralController::getOption('LA');
        
        if ($LA !=null and is_numeric($LA)) {
            return $LA;
        }
        else {
            return 1;
        }
    }
    
    /**
     * Gets the NJ hours
     * 
     * @return int
     */
    private function getNJ(){
        
        $NJ = \App\Http\Controllers\GeneralController::getOption('NJ');
        
        if ($NJ !=null and is_numeric($NJ)) {
            return $NJ;
        }
        else {
            return 2;
        }
    }
    
    /**
     * The function calculates EstimatedArrival
     * 
     * @param String $shipping_to LA or NJ
     * @param date $finish_prod
     * @return date new date for estimated arrival field
     */
    private function calculateEstimatedArrival($shipping_to, $finish_prod) {
        
            if ($shipping_to == "LA"){
                $LA = $this->getLA();
                //YYYY-MM-DD mysql formate in php is date("Y-m-d")
                //EstimatedArrival =FinishProd+24
                return date("Y-m-d", strtotime("$finish_prod + $LA days"));
            }
            elseif ($shipping_to == "NJ"){
                $NJ = $this->getNJ();
                //EstimatedArrival=FinishProd+38
                return date("Y-m-d", strtotime("$finish_prod + $NJ days"));
            }
    }
    
    
    /**
     * This function trigger License Conditions sends notification if necessary
     * 
     * @param int $order_id
     */
    public function triggerLicenseConditions($order_id) {
        
        $db_res = $this->checkLicenseConditions($order_id);
        
        if ($db_res) {
            $notification_controller = new NotificationsController();
            $notification_controller->LicensingConditions($order_id);
        }
    }
    
    /**
     * This is also called FAMA Conditions
     * 
     * If FAMA is 0 and one of the license is Marvel 
     * 
     * @param int $order_id
     * @return type
     */
    public function checkLicenseConditions($order_id){
        $registros = ImportPo::where('id',$order_id)
                    ->whereHas('export_c', function ($query) {
                        $query->where('fama', 0);
                    })
                    ->whereHas('licenses', function ($query) {
                        $query->where('fama_require', 1);
                    })->first();
       if($registros!=null){
            return true;
       }
       return false;
       /*
        return DB::select("SELECT `licenses`.`name`, `export_cos`.`fama` FROM `import_po`
        left join licenses_select on `import_po`.`id` = `licenses_select`.`order_id`
        left join licenses on `licenses`.`id` = `licenses_select`.`licenses_id`

        left join export_cos on `import_po`.`export_cos` = `export_cos`.`id`

        where `licenses`.`name` = 'Marvel' 
        and `export_cos`.`fama` = 0
        and `import_po`.`id` = ?", [$order_id]);*/
    }

    
    /**
     * This function trigger Delivery Conditions sends notification if necessary
     * 
     * @param int $order_id
     */
    public function triggerDeliveryConditions ($order_id) {
        $db_res1 = $this->checkDeliveryConditionsEstimatedArr($order_id);
        
        if ($db_res1 != null) {
            //then highlight it in red, and send an email to sales, factory, and production every 3 days 
            $notification_controller = new NotificationsController();
            $notification_controller->DeliveryConditions($order_id, "Customer Cancel");
        }
        else {
            $db_res2 = $this->checkDeliveryConditionsActualETA($order_id);
            if ($db_res2 != null) {
                //then highlight it in red, and send an email to sales, factory, and production every 3 days 
                $notification_controller = new NotificationsController();
                $notification_controller->DeliveryConditions($order_id, "Actual ETA");
            }
        }
    }
    
    /**
     * Checks if any of the both delivery condition is true
     * 
     * @param int $order_id
     * @return boolean Return true if any of the two conditions are valid on an order
     */
    public function checkDeliveryConditions($order_id) {
        
        $db_res1 = $this->checkDeliveryConditionsEstimatedArr($order_id);
        
        if ($db_res1 != null) {
            return true;
        }
        else {
            $db_res2 = $this->checkDeliveryConditionsActualETA($order_id);
            if ($db_res2 != null) {
                return true;
            }
        }
        
        
    }
    
    
    /**
     *  Checks Delivery Conditions
     * 
     *  Checks if CustomerCancel is before EstimatedArr 
     * 
     * @param int $order_id
     * @return type
     */
    public function checkDeliveryConditionsEstimatedArr($order_id) {
        
        return DB::select("select import_po_dates.customer_cancel, import_po_dates.estimated_arr from
        import_po_dates
        where DATE(import_po_dates.estimated_arr)  >  DATE(import_po_dates.customer_cancel) 
        and import_po_dates.act_eta_forwarder is null
        and import_po_dates.order_po_id = ?", [$order_id]);

    }
     
     
    /**
     * Delivery condition based on actual ETA (Estimated Time of Arival)
     * If CustomerCancel is before ActualETA
     * 
     * @param int $order_id
     */
    public function checkDeliveryConditionsActualETA ($order_id) {
        return DB::select("select import_po_dates.customer_cancel, import_po_dates.estimated_arr from
        import_po_dates
        where DATE(import_po_dates.act_eta_forwarder)  >  DATE(import_po_dates.customer_cancel) 
        and import_po_dates.order_po_id = ?", [$order_id]);
    }
    
    /**
     * Checks and triggers both safety conditions
     * 
     * @param int $order_id
     */
    public function triggerSafetyTestingConditions($order_id){
        $this->triggerSafetyTestingConditionOne($order_id);
        $this->triggerSafetyTestingConditionTwo($order_id);
    }
    
    /**
     * Check and triggers safety condition one along with notification
     * 
     * @param int $order_id
     */
    public function triggerSafetyTestingConditionOne($order_id) {
        
        $db_res = $this->checkSafetyTestingConditionOne($order_id);
        
        if ($db_res != null) {
            //then highlight ‘SafetyTestNeeded’ in red and send email alerts to factory every day, and production every 3 days
            $notification_controller = new NotificationsController();
            $notification_controller->SafetyTestingConditionOne($order_id);
        }
        
    }
    
    /**
     * Check safety condition one
     * 
     * @param int $order_id
     * @return type Returns false if the condition do not meet or returns the 
     *              row if condition is true
     */
    public function checkSafetyTestingConditionOne($order_id) {
        $now = date('Y-m-d H:i:s');
            return DB::select("select DATEDIFF(`import_po_dates`.`finish_prod`,'$now') as dayDiff
            from
            (import_po, import_po_dates)
            where 
            `import_po`.`id` = `import_po_dates`.`order_po_id`

            #SafetyTestNeeded is marked as Yes 
            and `import_po`.`safety_test` = 1 

            #no date has been entered in FtySubmittedProd 
            and `import_po_dates`.`fty_prod_safety_test` IS NULL 

            #safety_test_on is 21 days before FinishProd 
            and DATEDIFF(`import_po_dates`.`finish_prod`,'$now') <= 21
            and import_po.id =  ?", [$order_id]);
    }
    
    /**
     * Check and triggers safety condition two along with notification
     * 
     * @param int $order_id
     */
    public function triggerSafetyTestingConditionTwo($order_id) {
        
        $db_res = $this->checkSafetyTestingConditionTwo($order_id);
        
        if ($db_res != null) {
            //hightlight ‘SafetyTestPassed’ in red and send email alerts to factory every day and to production every 2 days.
            $notification_controller = new NotificationsController();
            $notification_controller->SafetyTestingConditionTwo($order_id);
        }
        
    }
    
    
    /**
     * Check safety condition one
     * 
     * @param int $order_id
     * @return type Returns false if the condition do not meet or returns the 
     *              row if condition is true
     */
    public function checkSafetyTestingConditionTwo($order_id) {
        $now = date('Y-m-d H:i:s');
        return DB::select("select DATEDIFF(`import_po_dates`.`finish_prod`,'$now') as dayDiff
        from
        (import_po, import_po_dates)
        where 
        `import_po`.`id` = `import_po_dates`.`order_po_id`
        
        #SafetyTestNeeded is marked as Yes 
        and `import_po`.`safety_test` = 1 
        
        #SafetyTestPassed is not marked as ‘Yes’,
        and (`import_po`.`safety_test_pass` = 0 or `import_po`.`safety_test_pass` IS NULL)
        
        #safety_test_on is 10 days before FinishProd 
        and DATEDIFF(`import_po_dates`.`finish_prod`,'$now') <= 10
        
        and import_po.id = ?", [$order_id]);
    }
    
    /**
     * Can be used to check CBM condition
     * 
     * @param type $order_id
     * @return boolean
     */
    public function triggerCBMCondition($order_id) {
        
        $LC = $this->checkLicenseConditions($order_id);
        $STC1 = $this->checkSafetyTestingConditionOne($order_id);
        $STC2 = $this->checkSafetyTestingConditionTwo($order_id);
        
        // If they enter that, but the safety test conditions are not met, or if the FAMA conditions are not met
        if (!$LC or !$STC1 or !$STC2){
            // It accepts the number, but the number is highlighted in red and they get a warning.
            return false;
        }
        else {
            return true;
        }
        
         
        
        
    }
    
    /**
     * check Payment Condition
     * 
     * @param int $order_id
     * @param String $customer AtHome or Hobby Lobby
     */
    public static function checkPaymentCondition($order_id, $customer=null) {
        
         
        /** The following fields are need for AtHome 
         *  AI rec_comm_inv_pking_home is only for AtHome, not needed for others
         *  AJ original_fcr is either AtHome OR Hobby Lobby, not needed for others
            rec_comm_inv_pking_home
            original_fcr
            telex_release
            pro_photo
        **/
        if ($customer == 'AtHome') {
            $db_res2 = DB::select("SELECT `import_po`.`id` FROM `import_po`, `import_po_dates` WHERE 
            `import_po`.`id` = `import_po_dates`.`order_po_id` and
            `import_po`.`safety_test_pass` = 1 and
            `import_po`.`david_ins_result` = 1 and
            `import_po_dates`.`rec_comm_inv_pking` is not null and
            `import_po_dates`.`rec_comm_inv_pking_home` is not null and
            `import_po_dates`.`original_fcr` is not null and
            `import_po_dates`.`telex_release` is not null and
            `import_po`.`pro_photo` =1 and
            `import_po`.`id` = ?", [$order_id]);
            
            if ($db_res2) {
                return true;
            }
            else {
                return false;
            }
        }
        
       /** 
            The following fields are need for Hobby Lobby
         
            AK ctpat_form is only for Hobby Lobby, not needed for others
            AJ original_fcr is either AtHome OR Hobby Lobby, not needed for others
            telex_release
            pro_photo
        **/  
      
        if ($customer == 'Hobby Lobby') {
            $db_res3 = DB::select("SELECT `import_po`.`id` FROM `import_po`, `import_po_dates` WHERE 
            `import_po`.`id` = `import_po_dates`.`order_po_id` and
            `import_po`.`safety_test_pass` = 1 and
            `import_po`.`david_ins_result` = 1 and
            `import_po_dates`.`rec_comm_inv_pking` is not null and
            `import_po_dates`.`ctpat_form` is not null and
            `import_po_dates`.`original_fcr` is not null and
            `import_po_dates`.`telex_release` is not null and
            `import_po`.`pro_photo` =1 and
            `import_po`.`id` = ?", [$order_id]);
            
            if ($db_res3) {
                return true;
            }
            else {
                return false;
            }
        }
        
        
        /** The following fields are need for all other types of customers 
         //safety_test_pass should be 1
         //david_ins_result should be 1
            telex_release should not be null
            pro_photo should be 1
        **/
        if ($customer != 'AtHome' or $customer != 'Hobby Lobby') {
            $db_res = DB::select("SELECT `import_po`.`id` FROM `import_po`, `import_po_dates` WHERE 
            `import_po`.`id` = `import_po_dates`.`order_po_id` and
            `import_po`.`safety_test_pass` = 1 and
            `import_po`.`david_ins_result` = 1 and
            `import_po_dates`.`rec_comm_inv_pking` is not null and
            `import_po_dates`.`telex_release` is not null and
            `import_po`.`pro_photo` = 1 and
            `import_po`.`id` = ?", [$order_id]);
            
            if ($db_res) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    
    /**
     * Validate Okay to Pay
     * 
     * @param int $order_id
     * @param string $customers A complete customers string which can contain AtHome or Hobby Lobby or both
     * @return boolean
     */
    public static function validateOkayToPay($order_id, $customers) {
        //if triggerPaymentCondition is true 
       if (strpos($customers, 'AtHome') !== false){
            $AtHome = self::checkPaymentCondition($order_id, 'AtHome');
            if(!$AtHome) {return false;}
       }
       if (strpos($customers, 'Hobby Lobby') !== false){
            $hobbyLobby = self::checkPaymentCondition($order_id, 'Hobby Lobby');
            if(!$hobbyLobby) {return false;}
       }
       if (strpos($customers, 'Hobby Lobby') == false or strpos($customers, 'AtHome') == false){
           $others = self::checkPaymentCondition($order_id, null);
           if(!$others) {return false;}
       }
       return true;
       
    }
    
    /**
     * Check if we can add Did we Pay
     * 
     * @param int $order_id
     */
    public function validateDidWePay($order_id) {
        
        //OkayToPay is 1
        $db_res = DB::select("SELECT `import_po_dates`.`id`  WHERE 
            `import_po_dates`.`okay_to_pay` is not null and
            `import_po_dates`.`order_po_id` = ?", [$order_id]);
    }
    
    /**
     * Validates if the order should be closed
     * 
     * @param type $order_id
     * @return boolean
     */
    public function validateOrderCloseCondition($order_id) {
        /*
            receive_whse
            did_we_pay
        */

        $db_res = DB::select("SELECT `import_po`.`id` FROM `import_po`, `import_po_dates` WHERE 
            `import_po`.`id` = `import_po_dates`.`order_po_id` and
           `import_po`.`receive_whse`= 1 and 
           `import_po_dates`.`okay_to_pay` is not null and 
           `import_po_dates`.`did_we_pay` is not null and
           `import_po`.`id` = ?", [$order_id]);
        
        if ($db_res) {
                return true;
            }
            
    }
    
    
    public function tiggerSVNCondition($order_id) {
        
    }
    
}
