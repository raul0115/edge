<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Feature;
use Carbon\Carbon;
use DB;
use Log;
use Illuminate\Validation\Rule;

class FeatureController extends Controller {

    /**
     * Construct method makes sure that all pages here are accessed by 
     * registered users by using the auth middleware
     */
    public function __construct() {
        $this->middleware('auth');
        if (Auth::check()) {
            
        } else {
            return redirect('login');
        }
    }

    /**
     * Function to display table management page
     * 
     * @param Request $request
     * @return view
     */
    public function index(Request $request) {
        $user = Auth::user();
        
        if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
            
            return redirect('/home')->withErrors("Only Admin can access these settings.");
            
        }

      
        $features = $this->featuresTable($request);
        
        $vista=view('features.index', 
        [
            
            'features' => $features
            
        ]);
        if ($request->isMethod('post')) {
            $request->session()->flash('message', 'successful!');
        
        }
        return $vista;
    }
    
  
     /**
     * This function manages CRUD for features Table
     * 
     * @param object $request
     * @return types
     */
    private function featuresTable($request){
        
        if(isset($request)) {
            
        //check if request has search features
            
            if ($request->search_features) {
                
                return $features = DB::table('features')->select('*')->where('name', 'like', "%$request->search_features%")->orderBy('id', 'desc')->paginate(10, ['*'], 'features');
            }
        
        //Edit
            if ($request->table == 'features' and $request->action  == 'edit_features') {
                
                $this->validate($request, [
                'name'          => 'required|string|max:250',
                'code' => [
                    'required','string','max:1',
                    Rule::unique('features')->ignore($request->edit_id)
                ],
                'edit_id'       =>  'exists:features,id',
                ]);
                
                Feature::where('id', $request->edit_id)->update([
                    'name' => $request->name,
                    'code'=> $request->code,
                    ]);

            }
        
        //Add
            if ($request->table == 'features' and $request->action  == 'add_features') {
                
                $this->validate($request, [
                    'name'          => 'required|string|unique:features,name|max:250',
                    'code'   => 'required|string|max:1|unique:features,code',
                ]);

                
                Feature::create([
                    'name' => $request->name,
                    'code'=> $request->code
                    ]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'features' and $request->action  == 'delete_features') {
            
                $this->validate($request, [
                    'delete_feature_id'       => 'exists:features,id',
                ]);
                
                DB::table('features')->where('id', $request->delete_feature_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'features' and $request->action  == 'activate_features') {
            
                $this->validate($request, [
                    'activate_feature_id'       => 'exists:features,id',
                ]);
                
                DB::table('features')->where('id', $request->activate_feature_id)->update(['active' => 1]);
         
            }
            
            
        }
        return $features= feature::orderBy('id', 'desc')->paginate(10, ['*'], 'features');
        //return $features = DB::table('features')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'features');
    }
  
    
}
