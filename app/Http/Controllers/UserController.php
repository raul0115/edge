<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use Spatie\Permission\Models\Role;
//use Spatie\Permission\Models\Permission;
use App\User;
use DB;
use App\ColDefination;
use App\Factory;
use Log;
use Hash;
use Spatie\Permission\Models\Role;

use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    
        /**
         * Construct method makes sure that all pages here are accessed by 
         * registered users by using the auth middleware
         */    
	public function __construct()
	{   
            $this->middleware('auth');
            if (Auth::check()) {}
            else {return redirect('login');}
        }
    
        /*
         * This is the users page 
         * 
         * 
         * @param int $id this is the user ID
         * 
         * @return Response
         */
	public function users(Request $request, $id=null)
	{
            //verify that page is accessed by admin
            $user = Auth::user();
            
            if ($user->hasRole('Admin')) {
                
                $users = DB::table('users')->select('users.*', 'roles.name as RoleName')->leftJoin('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')->leftJoin('roles', 'model_has_roles.role_id', '=', 'roles.id')->orderBy('users.id', 'desc')->paginate(10);
                $roles = Role::all()->pluck('name','name');
                $searchOrders = null;
                
                $userEmail = $request->input('userEmail');//userName is the search "user name"
                
                if ($userEmail){
                    
                    $usersSearc = DB::table('users')->select('users.*', 'roles.name as RoleName')->leftJoin('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')->leftJoin('roles', 'model_has_roles.role_id', '=', 'roles.id')->where('users.email', 'like', "%$userEmail%")->orderBy('users.id', 'desc')->paginate(10);
                    
                    if(!$usersSearc->total())
                        {$searchOrders='not found';}
                    else {$users = $usersSearc;}//return serach result if users are found
                
                    }
                //getting permission status of the user
                $editIdfields = $this->getUserFieldsById($id);
                return view('loggedin.usermanagement', ['roles'=>$roles,'users' => $users, 'searchOrders' => $searchOrders, 'editID' => $id, 'col_definations' => $editIdfields  ]);
              
                }
                //if not admin redirect
            else {return redirect('/home')->withErrors('You dont have the permission to access users permission page.');}
        
        }
        
        /**
         * This is the user settings page
         * 
         * @param Request $request
         * @param int $id
         * @return type
         */
        public function userSettings(Request $request, $id=null) {
            $data =null;
            $user = Auth::user();
            if ($user->hasRole('Admin')) {
            
                $editIdfields = $this->getUserFieldsById($id);

                //todo check if id has role factory
                $userObj = User::find($id);
                $su_role= 'Inactive';
                $sus_roles =  $userObj->getRoleNames()->toArray(); // Returns a collection
                    if($sus_roles[0]!=null){
                       $su_role= $sus_roles[0];
                    }
                if ($userObj->hasRole('Factory')) {
                    $data['factories'] = DB::select("select * from factories where active = 1");
                    $data['factory_ID'] = $this->getFactoryUser($id);
                }
                elseif ($userObj->hasRole('Sales')){
                    $data['salesmen'] = DB::select("select * from salesmen where active = 1");
                    $data['salesmen_ID'] = $this->getSalesmenUser($id);
                }

                return view('loggedin.user_settings', [ 'userObj' => $userObj, 'su_role' => $su_role,'col_definations' => $editIdfields, 'tableData' => $data  ]);
        
            }
        }
        
        /**
         * This is the function that assign the factory user to a factory 
         *  
         * @param Request $request
         * @return type
         */
        public function assignFactorySubmit(Request $request) {
            //validate if the user_id has the role factory
            
            $user = Auth::user();
            if ($user->hasRole('Admin')) {
            
                $this->validate($request, [
                        'user_id'               => 'exists:users,id',
                        'sub_fty_produc_value'  => 'exists:factories,id'
                    ]);
                
                $this->setFactoryUser($request->user_id, $request->sub_fty_produc_value);
                
                return redirect("/usermanagement/userSettings/$request->user_id")->with('message', 'User Updated');
                
            }
        }
        
        /**
         * This is the function that assign the salesmen user to a Saleman 
         *  
         * @param Request $request
         * @return type
         */
        public function assignSalesmenSubmit(Request $request) {
            //validate if the user_id has the role factory
            
            $user = Auth::user();
            if ($user->hasRole('Admin')) {
            
                $this->validate($request, [
                        'user_id'               => 'exists:users,id',
                        'sub_salesmen_value'  => 'exists:salesmen,id'
                    ]);
                
                $this->setSalesmenUser($request->user_id, $request->sub_salesmen_value);
                
                return redirect("/usermanagement/userSettings/$request->user_id")->with('message', 'User Updated');
                
            }
        }
        
        /**
         * This function gets the user id and factory id and assign the user to that factory
         * 
         * @param int $user_id
         * @param int $factory_id
         */
        public function setFactoryUser($user_id, $factory_id) {

        $user = $this->getFactoryUser($user_id);

        //insert if not exist
            if ($user == null) {
                DB::table('factories_users')->insertGetId([
                    'user_id' => $user_id,
                    'factory_id' => $factory_id
                ]);
                
            }
            else {
                //update if alredy exist
                DB::table('factories_users')
                        ->where('user_id', $user_id)
                        ->update(['factory_id' => $factory_id]);
            }
        }
        
        /**
         * This function gets the user id and factory id and assign the user to that factory
         * 
         * @param int $user_id
         * @param int $factory_id
         */
        public function setSalesmenUser($user_id, $salesmen_id) {

        $user = $this->getSalesmenUser($user_id);

        //insert if not exist
            if ($user == null) {
                DB::table('salesmen_users')->insertGetId([
                    'user_id' => $user_id,
                    'salesmen_id' => $salesmen_id
                ]);
                
            }
            else {
                //update if alredy exist
                DB::table('salesmen_users')
                        ->where('user_id', $user_id)
                        ->update(['salesmen_id' => $salesmen_id]);
            }
        }

        /**
         * This function return the factories_users row by user id
         * 
         * @param int $user_id
         * @return type
         */
        public static function getFactoryUser($user_id) {
            
            //return null if not exist
            return DB::table('factories_users')->where('user_id', $user_id)->first();
        }
        
        /**
         * This function return the factories_users row by user id
         * 
         * @param int $user_id
         * @return type
         */
        public static function getSalesmenUser($user_id) {
            
            //return null if not exist
            return DB::table('salesmen_users')->where('user_id', $user_id)->first();
        }
        
        /**
         * Updates the user role
         * 
         * @param Request $request
         * @return null redirects
         */
        public function updateUsersRole(Request $request) {
            
            $user = Auth::user();  
            if ($user->hasRole('Admin')) {//verifing that the user accessing this page is admin 
                
                //continue with the update user role request.
                $userID = $request->input('userID');
                $userRole = $request->input('userRole');
                $userObj=    User::find($userID);
                $userObj->syncRoles([$userRole]);
                
                
                return redirect('/usermanagement/users')->with('message', 'Role updated successfully');
            }
        
        }
        
        /**
         * This is the delete user request page.
         * 
         * @param Request $request
         * @return type
         */
        public function checkDeleteRequest(Request $request) {

        $user = Auth::user();
        if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
            return redirect('/home')->withErrors("Only Admin can edit these settings.");
        }
        
        $this->validate($request, [
                    'DeleteuserID'       => 'exists:users,id',
                ]);

        //these line handel delete request of user
           // $result = $this->deleteUserById($request->DeleteuserID);
            $result = $this->deactivateUserById($request->DeleteuserID);
        
            if ($result) {
                return redirect('/usermanagement/users')->with('message', 'User successfully deactivated');
            } else {
                return redirect('/usermanagement/users')->withErrors("An error occured! You may be trying to deactivate your own user.");
            }
        }
        
        /**
         * This is the delete user request page.
         * 
         * @param Request $request
         * @return type
         */
        public function activateUsers(Request $request) {

        $user = Auth::user();
        if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
            return redirect('/home')->withErrors("Only Admin can edit these settings.");
        }
        
        $this->validate($request, [
                    'ActivateuserID'       => 'exists:users,id',
                ]);

            $result = $this->activateUserById($request->ActivateuserID);
        
            if ($result) {
                return redirect('/usermanagement/users')->with('message', 'User successfully activated');
            } else {
                return redirect('/usermanagement/users')->withErrors("An error occured! You may be trying to activate your own user.");
            }
        }
        

        /**
         * This function deletes a user by ID
         * 
         * @param int $id
         * @return boolean return true id user is deleted successfully
         */
        private function deleteUserById ($id) {
            
            $user = Auth::user();  // This is the user who is deleting the other user
            if ($id == $user->id) {
                return false;
            }
            
            $userObj = User::find($id);   
            $userObj->delete();
            return true;
            
            //todo unset all roles if possible
            
        }
        
        /**
         * This function deactivates a user by ID
         * 
         * @param int $id
         * @return boolean return true id user is deactivated successfully
         */
        private function deactivateUserById ($id) {
            
            $user = Auth::user();  // This is the user who is deleting the other user
            if ($id == $user->id) {
                return false;
            }
            
            
            DB::table('users')->where('id', $id)->update(['active' => 0]);
            return true;
            
        }
        
         /**
         * This function activates a user by ID
         * 
         * @param int $id
         * @return boolean return true id user is deactivated successfully
         */
        private function activateUserById ($id) {
            
            $user = Auth::user();  // This is the user who is deleting the other user
            if ($id == $user->id) {
                return false;
            }
            
            
            DB::table('users')->where('id', $id)->update(['active' => 1]);
            return true;
            
        }
        
        /**
         * This function assigns a new role to user
         * 
         * @param int $userRole role id
         * @param int $userID user id
         * @return boolean
         */
        private function giveRole ($userRole, $userID) {
            
            $userObj=    User::find($userID);
            
            $r = DB::select('SELECT `users`.*, `roles`.`name` as RoleName FROM `users` LEFT JOIN `model_has_roles` ON (`model_has_roles`.`model_id` = `users`.`id`) LEFT JOIN `roles`  ON (`model_has_roles`.`role_id` = `roles`.`id`) where (`roles`.`name` = "Admin") limit 2;');
            $user = Auth::user();
            
            if (count($r) == 1 and $userID==$user->id) {// only one admin, and changing his own role
                return false;
            }
                if ($userRole==7){//make him designer
                    $role = 'Designer';
                    $userObj->syncRoles(['Designer']);
                }
                if ($userRole==6){//make him license
                    $role = 'License';
                    $userObj->syncRoles(['License']);
                }
                if ($userRole==5){//make him license_coordinator
                    $role = 'License_coordinator';
                    $userObj->syncRoles(['License Coordinator']);
                }

                if ($userRole==4){//make him admin
                    $role = 'Admin';
                    $userObj->syncRoles(['Admin']);
                }
                elseif($userRole==3){
                    $role = 'Factory';
                    $userObj->syncRoles(['Factory']);
                }
                elseif($userRole==2){
                    $role = 'Sales';
                    $userObj->syncRoles(['Sales']);
                }
                elseif($userRole==1){
                    $role = 'QC';
                    $userObj->syncRoles(['QC']);
                }
                elseif($userRole==0){
                    $role = 'Production';
                    $userObj->syncRoles(['Production']);
                }
                $this->userNewRights($userID, $role);
                //run the userNewRights function here
                
                if($userRole!=3){//always delete the factory user id if user is no longer factory
                    DB::table('factories_users')->where('user_id', $userID)->delete();
                }
                
            return true;
        }
        
        /**
         * If the role of the user changes this function resets the user rights
         * according to new role 
         * 
         * @param int $user_id
         * @param string $role role name that exist on col_definations
         */
        private function userNewRights ($user_id, $role) {
            
            //check if user id exist in the userlevel_rights and user_display_rights
            $this->createUserTablesifNotExist($user_id);
            
            //set the users all values to 0
            $this->removeRightsFromUserTables($user_id);
            
            //get the new rights from col_definatoins and assign to this user
            $this->assignNewUserRights($user_id, $role);
            
            //get the new rights from col_definatoins and assign to this user personal display
            $this->assignNewUserDisplayRights($user_id, $role);
        }
        
        /**
         * Assigns the new role to the user on its userlevel_rights table
         * 
         * @param int $user_id user id
         * @param string $role role name that exist on col_definations
         */
        private function assignNewUserRights($user_id, $role) {
            $col_def = DB::select("select * from col_definations where $role = 1");
            //$i =0;
            foreach ($col_def as $key => $value) {
                //foreach columns this user has access to
                DB::table('userlevel_rights')->where('user_id', $user_id)->update([$value->col_name => 1]);
                
                //$i++;  
            }
        }

        /**
         * Assigns the new role to the user on its user_display_rights table
         * 
         * @param int $user_id user id
         * @param string $role role name that exist on col_definations
         */
        private function assignNewUserDisplayRights($user_id, $role) {
            $col_def = DB::select("select * from col_definations where $role = 1");
            //$i =0;
            foreach ($col_def as $key => $value) {
                //foreach columns this user has access to
                DB::table('user_display_rights')->where('user_id', $user_id)->update([$value->col_name => 1]);
                
                //$i++;  
            }
        }

        /**
         * This function removes sets all the fields to zero for userlevel_rights and user_display_rights
         * 
         * @param type $user_id
         */
        private function removeRightsFromUserTables($user_id){
            
            DB::table('userlevel_rights')
            ->where('user_id', $user_id)
            ->update([
                'order_id' => 0,
                'comments' => 0,
                'po_number' => 0,
                'open' => 0,
                'value' => 0,
                'fty_produc' => 0,
                'export_cos' => 0,
                'prod_style' => 0,
                'prod_type' => 0,
                'licenses' => 0,
                'customers' => 0,
                'customer_po_number' => 0,
                'date_placed' => 0,
                'qty_ordered' => 0,
                'tickets_needed' => 0,
                'tickets_ordered' => 0,
                'tickets_received' => 0,
                'shipping_from' => 0,
                'shipping_to' => 0,
                'finish_prod' => 0,
                'estimated_arr' => 0,
                'customer_cancel' => 0,
                'cbm' => 0,
                'book_container' => 0,
                'act_etd_forwarder' => 0,
                'act_eta_forwarder' => 0,
                'safety_test' => 0,
                'safety_test_on' => 0,
                'fty_prod_safety_test' => 0,
                'safety_test_pass' => 0,
                'david_ins_result' => 0,
                'david_ins_comment' => 0,
                'svn_no' => 0,
                'freight_forwarder' => 0,
                'rec_comm_inv_pking' => 0,
                'rec_comm_inv_pking_home' => 0,
                'original_fcr' => 0,
                'ctpat_form' => 0,
                'telex_release' => 0, 
                'pro_photo' => 0,
                'okay_to_pay' => 0,
                'did_we_pay' => 0,
                'send_diann' => 0,
                'receive_whse' => 0,
                'salesmen' => 0,
                'files' => 0
                ]);
            
            
            DB::table('user_display_rights')
            ->where('user_id', $user_id)
            ->update([
                'order_id' => 0,
                'comments' => 0,
                'po_number' => 0,
                'open' => 0,
                'value' => 0,
                'fty_produc' => 0,
                'export_cos' => 0,
                'prod_style' => 0,
                'prod_type' => 0,
                'licenses' => 0,
                'customers' => 0,
                'customer_po_number' => 0,
                'date_placed' => 0,
                'qty_ordered' => 0,
                'tickets_needed' => 0,
                'tickets_ordered' => 0,
                'tickets_received' => 0,
                'shipping_from' => 0,
                'shipping_to' => 0,
                'finish_prod' => 0,
                'estimated_arr' => 0,
                'customer_cancel' => 0,
                'cbm' => 0,
                'book_container' => 0,
                'act_etd_forwarder' => 0,
                'act_eta_forwarder' => 0,
                'safety_test' => 0,
                'safety_test_on' => 0,
                'fty_prod_safety_test' => 0,
                'safety_test_pass' => 0,
                'david_ins_result' => 0,
                'david_ins_comment' => 0,
                'svn_no' => 0,
                'freight_forwarder' => 0,
                'rec_comm_inv_pking' => 0,
                'rec_comm_inv_pking_home' => 0,
                'original_fcr' => 0,
                'ctpat_form' => 0,
                'telex_release' => 0, 
                'pro_photo' => 0,
                'okay_to_pay' => 0,
                'did_we_pay' => 0,
                'send_diann' => 0,
                'receive_whse' => 0,
                'salesmen' => 0,
                'files' => 0
                ]);
            
        }


        /**
        * This function create the tables for new users who are just assigned new roles
        * 
        * @param int $user_id user id
        */
        private function createUserTablesifNotExist($user_id){
            
            
            //checks if the user_id exist in the userlevel_rights table
            $validate_userlevel_rights = Validator::make( ['user_id' => $user_id], 
                              ['user_id' => "required|exists:userlevel_rights,user_id"]);
                    
            //if it does not exist then create one
            if ($validate_userlevel_rights->fails()) {
                            DB::table('userlevel_rights')->insertGetId([
                                'user_id' => $user_id,
                             ]);
            }
            
            //checks if the user_id exist in the user_display_rights table
            $user_display_rights = Validator::make( ['user_id' => $user_id], 
                              ['user_id' => "required|exists:user_display_rights,user_id"]);
                    
            //if it does not exist then create one
            if ($user_display_rights->fails()) {
                            DB::table('user_display_rights')->insertGetId([
                                'user_id' => $user_id,
                             ]);
            }
            
        }
        
        /**
         * This function displays the group management page
         * 
         * @return type
         */
        public function groupManagemenet() {
            $user = Auth::user();  
            if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
                return redirect('/home')->withErrors("Only Admin can access these settings.");
            }
            
            $col_def = DB::select('select * from col_definations');
            return view('loggedin.group', ['col_definations' => $col_def]);
        }

        /**
         * Saves the settings from group management page
         * 
         * @param Request $request
         * @return type
         */
        public function postGroupManagemenet(Request $request) {
            
            $user = Auth::user();  
            if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
                return redirect('/home')->withErrors("Only Admin can edit these settings.");
            }
            
            $input = $request->all();
            if($input['group_type']=='Production') {
                foreach ($input as $key => $value) {
                    DB::update('update col_definations set Production = ? where col_name = ?', [$value, $key]);
                }
            }
            if($input['group_type']=='QC') {
                foreach ($input as $key => $value) {
                    DB::update('update col_definations set QC = ? where col_name = ?', [$value, $key]);
                }
            }
            if($input['group_type']=='Sales') {
                foreach ($input as $key => $value) {
                    DB::update('update col_definations set Sales = ? where col_name = ?', [$value, $key]);
                }
            }
            if($input['group_type']=='Factory') {
                foreach ($input as $key => $value) {
                    DB::update('update col_definations set Factory = ? where col_name = ?', [$value, $key]);
                }
            }
            if($input['group_type']=='List') {
               //dd($input);
                foreach ($input as $key => $value) {
                $findme   = '-order';

                    if($key!="_token" && $key!="group_type" && strpos($key, $findme)===false ){
                        
                        DB::update('update col_definations set List = ? where col_name = ?', [$value, $key]);
                    }elseif($key!="_token" && $key!="group_type"){
                        if($value<1){
                            $value = 1;
                        }   
                        $key = explode($findme, $key)[0];
                        $valor= ColDefination::where('col_name', $key)
                        ->update(['Order' => (int)$value]);
                    }
                }
                return back()->with('message', "updated sucessfully");
            }
            
            return redirect('/usermanagement/group')->with('message', "{$input['group_type']} Group settings updated successfully");
        }
        
        
        private function getUserFieldsById($user_id) {
            //get user role
            $userObj = User::find($user_id);
            if($userObj){
             $roleNames = $userObj->getRoleNames();//returns an object/collection of roles
             $array = $roleNames->toArray();//convert the obj to array
            $admin_exist = array_search('Admin', $array);
                if ($array and !$admin_exist) { 
                    //suppose $array[0] is 'Sales', following query we get all the sales data from col_def
                    return $this->getUserRightsArray($array[0], $user_id);
                }
                else {
                    
                }
            }
        }
        
        /**
         * Returns the array according to the role and the user field rights 
         * 
         * @param string $roleName The name of the role such as Sales
         * @param int $user_id
         * @return array or null if col_definations's role has no rights
         */
        private function getUserRightsArray($roleName, $user_id){
            $convert= strtolower($roleName);
            $roleName= str_replace(" ", "_", $convert);
            $roleName= ucfirst($roleName);
           
            $col_def = DB::select("select * from col_definations where $roleName = 1");
            
            $res = DB::select("select * from userlevel_rights where user_id = $user_id" );
                    
                    if (!isset($res[0])) {//if user donot exist
                        $i =0;
                        foreach ($col_def as $key => $value) {//foreach columns this user has access to
                                $col_def[$i]->role_current = 0;//return everything to zero
                        $i++;  
                        }
                    }
                    
                    else {//if user exist
                        $j =0;
                        foreach ($col_def as $key => $value) {
                            $col_name = $col_def[$j]->col_name;//names of the columns this user has access to
                                $col_def[$j]->role_current = $res[0]->$col_name; //setting property role_current to userrights table value
                        $j++;  
                        }
                    }
                    
            return $col_def;
            
        }
        
        /**
         * 
         * @return type
         */
        public function displayManagemenet() {
            $col_def = null;
            $userObj = Auth::user();
            $user_id = $userObj->id;//getting the logged in user ID
            
            if($userObj){
                
            $roleNames = $userObj->getRoleNames();//returns an object/collection of roles
            $array = $roleNames->toArray();//convert the obj to array
             //search array to see if he is admin
                if ($array) { 
                    //suppose $array[0] is 'Sales', following query we get all the sales data from col_def
                    $col_def = $this->getUserDisplayRightsArray($array[0], $user_id);
                }
            }
            
            return view('loggedin.display', ['col_definations' => $col_def]);
        }
        
        /**
         * Returns the array according to the role and the user display rights
         * 
         * @param string $roleName
         * @param int $user_id
         * @return array or null if col_definations's role has no rights
         */
        private function getUserDisplayRightsArray($roleName, $user_id){
            $convert= strtolower($roleName);
            $roleName= str_replace(" ", "_", $convert);

            $col_def = DB::select("select * from col_definations where $roleName = 1");
            
            $res = DB::select("select * from user_display_rights where user_id = $user_id" );
                    
                    if (!isset($res[0])) {//if user donot exist
                        $i =0;
                        foreach ($col_def as $key => $value) {//foreach columns this user has access to
                                $col_def[$i]->role_current = 0;//return everything to zero
                        $i++;  
                        }
                    }
                    
                    else {//if user exist
                        $j =0;
                        foreach ($col_def as $key => $value) {
                            $col_name = $col_def[$j]->col_name;//names of the columns this user has access to
                                $col_def[$j]->role_current = $res[0]->$col_name; //setting property role_current to userrights table value
                        $j++;  
                        }
                    }
                    
            return $col_def;
            
        }
        
        /**
         * This function is used to submit form/create users 
         * 
         * @param Request $request
         * @return type
         */
        public function createUser(Request $request) {
            
           
            $this->validate($request, [
                'username'    => 'required|string',
                'email'    => 'required|email|unique:users',
                'userRole'    => 'required',
                'password' => 'required|string',
            ]);

            
            $user = Auth::user();  
            if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
                return redirect('/home')->withErrors("Only Admin can create new users.");
            }
            $factory_id=0;
            if($request->userRole=="Factory"){
                $this->validate($request, [
                    'factory_id'    => 'required|exists:factories,id'
                ]);
                $factory_id=$request->factory_id;
            }
            $newUserId = DB::table('users')->insertGetId([
                'name' => $request->username,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);
            if ($request->userRole !=null or $request->userRole != "") {
                
                $userObj=    User::find($newUserId);
                $userObj->assignRole($request->userRole);
                $this->userNewRights($newUserId, $request->userRole);
                if($factory_id>0){
                    $this->setFactoryUser($newUserId, $factory_id);
                }
            }
            
            
            return redirect('/usermanagement/users')->with('message', 'User successfully created');
        }
        
        /**
         * Test function to create random users
         * 
         * @param Request $request
         * @return type
         */
        public function createUser2(Request $request) {
            
            //validate name
            //validate email
            //password
            //validate role
            
            $user = Auth::user();  
            if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
                return redirect('/home')->withErrors("Only Admin can create new users.");
            }
            
            DB::table('users')->insert([
                'name' => str_random(10),
                'email' => str_random(10).'@gmail.com',
                'password' => bcrypt('123456'),
            ]);
        }
        
        
//        private function validateUserName($request) {
//            
//            $this->validate($request, [
//                'username'    => 'required|string',
//                'email'    => 'required|email|unique:users',
//                'password' => 'required|string',
//            ]);
//            
//        }
        
        
        public function userFieldPermissionSubmit(Request $request){
            $user = Auth::user();  
            if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
                return redirect('/home')->withErrors("Only Admin can edit these settings.");
            }
            
                //todo need to test if the user id validation is happening
            $this->validate($request, [
                'user_id'    =>  'exists:users,id',
            ]);
            
            //do a select query to check if the record exist
            $res = DB::select('select user_id from userlevel_rights where user_id = ?', [$request->user_id]);
            
            //if do not exist insert user id
            if (!isset($res[0])) {
                DB::table('userlevel_rights')->insert(
                    ['user_id' => $request->user_id]
                );
            }
            
            //then update rows one by one
            $input =  (array) $request->all();// convert all input to array 
            
            unset($input['user_id']);
            unset($input['_token']);
            
            foreach ($input as $key => $value) {
                DB::update("update userlevel_rights set $key = ? where user_id = ?", [$value, $request->user_id]);
            }
            
            return redirect('/usermanagement/users')->with('message', "User settings updated successfully");
       
        }
        
        public function userDisplayPermissionSubmit(Request $request){
            $user = Auth::user();

            
            //do a select query to check if the record exist
            $res = DB::select('select user_id from user_display_rights where user_id = ?', [$user->id]);

            //if do not exist insert user id
            if (!isset($res[0])) {
                DB::table('user_display_rights')->insert(
                    ['user_id' =>  $user->id]
                );
            }
            
            //then update rows one by one
            $input =  (array) $request->all();// convert all input to array 
            
            unset($input['user_id']);
            unset($input['_token']);
            
            foreach ($input as $key => $value) {
                DB::update("update user_display_rights set $key = ? where user_id = ?", [$value, $user->id]);
            }
            
            return redirect('/usermanagement/display')->with('message', "User settings updated successfully");
       
        }
        
        /**
         * Profile Management page
         * 
         * @return type
         */
        public function profileManagemenet() {
            
            $user = User::find(Auth::user()->id);    
            
            return view('loggedin.profile', ['user' => $user ]);
            
        }
        
        /**
         * Edit Profile page
         * 
         * @return type
         */
        public function editprofile()
        {
            $user = User::find(Auth::user()->id);    
            return view('loggedin.editprofile',["user" => $user]);
        }
        
        /**
         * Edit user name function
         * 
         * @param Request $input
         * @return type
         */
        public function saveeditprofile(Request $input){
            $this->validate($input, [
            'name' => 'required|max:255',
            'password' => 'nullable|min:6',
            'id'=>  'required|integer'        
                ]);
            $id=Auth::user()->id;
            $usermanager=false;
            if($input["id"]!=$id){
                if (Auth::user()->hasRole('Admin')) {
                    $usermanager=true;
                    $id=$input["id"];
                }                
            }
            $user = User::find($id);
            $user->name = $input["name"];
            if (Auth::user()->hasRole('Admin')) {
                if($input->password){
                    $user->password = bcrypt($input['password']);
                }
            }
            $user->save();
            if($usermanager){
                return redirect('/usermanagement/users')->with('message', "Username successfully changed");
            }
            
            return redirect('/usermanagement/profile')->with('message', "Username successfully changed");
        }
        
        public function resetPassword() {
            
            return view('loggedin.edit_password');
        }
        
        /**
         * Function that updates new password
         * 
         * @param Request $request
         * @return type
         */
        public function submitNewPassword(Request $request) {
                    
            $this->validate( $request,[
            'password' => 'required',
            'newpassword' => 'required|min:6|confirmed|different:password'
            ]);
                    
            if (!Hash::check($request->password, Auth::user()->password)){
                return redirect('/edit-password')->withErrors(array('old_password' => 'Old password is incorrect.'));
            }

            $user = Auth::user();
            $user->password = bcrypt($request->input('newpassword'));
            $user->save();
        
        
            return redirect('/usermanagement/profile')->with('message', "Password successfully reset");
        
        }

        public function factories(Request $request)
        {
            $factories= Factory::where('active',1)->get()->pluck("name","id");
            return $factories;
        }

        public function newUpdateUsers(Request $request,$id=null) {
           
            $user = Auth::user();
            if ($user->hasRole('Admin')) {
                $user = User::find($id); 
                if($user) 
                    return view('loggedin.editprofile',["user" => $user]);
                return redirect('/usermanagement/users');
            }
            return back();
        }

        
}
