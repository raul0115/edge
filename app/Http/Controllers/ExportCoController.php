<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ExportCos;
use Carbon\Carbon;
use DB;
use Log;
use Illuminate\Validation\Rule;

class ExportCoController extends Controller {

    /**
     * Construct method makes sure that all pages here are accessed by 
     * registered users by using the auth middleware
     */
    public function __construct() {
        $this->middleware('auth');
        if (Auth::check()) {
            
        } else {
            return redirect('login');
        }
    }

    /**
     * Function to display table management page
     * 
     * @param Request $request
     * @return view
     */
    public function index(Request $request) {
        $user = Auth::user();
        
        if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
            
            return redirect('/home')->withErrors("Only Admin can access these settings.");
            
        }

      
        $export_cos = $this->export_cosTable($request);
      
       
        $vista=view('export_cos.index', 
        [
            'export_cos'=> $export_cos
           
        ]);
        
        if ($request->isMethod('post')) {
            $request->session()->flash('message', 'successful!');
        
        }
        return $vista;
    }
   
    
    
    /**
     * This function manages CRUD for export_cos Table
     * 
     * @param object $request
     * @return type
     */
    private function export_cosTable($request){
        
        if(isset($request)) {
            
        //check if request has search export_cos
            
            if ($request->search_export_cos) {
                
                return $export_cos = DB::table('export_cos')->select('*')->where('name', 'like', "%$request->search_export_cos%")->orderBy('id', 'desc')->paginate(10, ['*'], 'export_cos');
            }
        
        //Edit
            if ($request->table == 'export_cos' and $request->action  == 'edit_export_cos') {
                
                $this->validate($request, [
                'name'          => 'required|string',
                'fama'          => 'required|string',
                'edit_id'       =>  'exists:export_cos,id',
                'fama_exp_date' => 'nullable|date',
                ]);
                
                $export_cos=ExportCos::find( $request->edit_id);
                $export_cos->name=$request->name;
                $export_cos->fama=$request->fama;
                $export_cos->fama_exp_date=($request->fama_exp_date)?Carbon::parse($request->fama_exp_date):null;
                $export_cos->save();
                
                /*
                
                DB::table('export_cos')->where('id', $request->edit_id)->update([
                    'name' => $request->name, 
                    'fama' => $request->fama,
                    'fama_exp_date'=>$request->fama_exp_date]);
                    */

            }
        
        //Add
            if ($request->table == 'export_cos' and $request->action  == 'add_export_cos') {
              
                $this->validate($request, [
                    'name'          => 'required|string|unique:export_cos,name',
                    'fama'          => 'required|string',
                    'edit_id'       => 'exists:export_cos,id',
                    'fama_exp_date' => 'nullable|date',
                ]);
                $data= $request->all();
                $data['fama_exp_date']= Carbon::parse($data['fama_exp_date']);
                ExportCos::create($data);
                
                //DB::table('export_cos')->insert(['name' => $request->name, 'fama' => $request->fama]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'export_cos' and $request->action  == 'delete_export_cos') {
            
                $this->validate($request, [
                    'delete_export_co_id'       => 'exists:export_cos,id',
                ]);
                
                DB::table('export_cos')->where('id', $request->delete_export_co_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'export_cos' and $request->action  == 'activate_export_cos') {
            
                $this->validate($request, [
                    'activate_export_co_id'       => 'exists:export_cos,id',
                ]);
                
                DB::table('export_cos')->where('id', $request->activate_export_co_id)->update(['active' => 1]);
         
            }
            
            
        }
        
        return $export_cos = ExportCos::orderBy('id', 'desc')->paginate(10, ['*'], 'export_cos');
    }
    
    
}
