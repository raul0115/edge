<?php
/*OrderController*/
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use Spatie\Permission\Models\Role;
//use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;
use App\User;
use DB;
use Log;
use App\ImportPo;
use App\ImportPoDate;
use App\ExportCos;
use App\License;
use App\PortTo;
use App\Option;
use Carbon\Carbon;
use App\Customer;
use Illuminate\Validation\Rule;
use App\Rules\FinishCbm;
use App\Rules\ActEtd;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TriggerEventsController;

class OrderController extends Controller
{
    
    /**
     * Construct method makes sure that all pages here are accessed by 
     * registered users by using the auth middleware
     */    
    public function __construct()
    {   
        $this->middleware('auth');
        if (!Auth::check()) {return redirect('login');}
    }
      
    /**
     * This function is responsible for showing open orders page.
     * 
     * @param Request $request
     * @return View
     */
    public function showOpenOrders(Request $request) {

        $role_Id = $role_table = null;
        $tableData = $this->getAllTablesData();
        $user = Auth::user();
        //check if user has factory role
        if ($user->hasRole('Factory') and $role_user = UserController::getFactoryUser($user->id)) {
            $role_Id = $role_user->factory_id;
            $role_table = 'import_po.fty_produc';
        }
        elseif($user->hasRole('Sales') and $role_user = UserController::getSalesmenUser($user->id)){
            $role_Id = $role_user->salesmen_id;
            $role_table = 'salesmen_select.salesmen_id';
        }
        
//        if ($request->action == 'serach')
//        {
//            $searchOrders = $this->searchOrders($request);
//        }

        $orders = $this->getOrders(1, $role_Id, $role_table);
        $page= \App\Http\Controllers\GeneralController::getOption('openOrderPageLimit');
        return view('loggedin.openordes', ['orders' => $orders, 'page'=>$page,'tableData' => $tableData ]);
    }
    
    /**
     * This function is responsible for showing closed orders page.
     * 
     * @param Request $request
     * @return View
     */
    public function showClosedOrders(Request $request) {
        $role_Id = $role_table = null;
        $user = Auth::user();
        //check if user has factory role
        if ($user->hasRole('Factory') and $role_user = UserController::getFactoryUser($user->id)) {
            $role_Id = $role_user->factory_id;
            $role_table = 'import_po.fty_produc';
        }
        elseif($user->hasRole('Sales') and $role_user = UserController::getSalesmenUser($user->id)){
            $role_Id = $role_user->salesmen_id;
            $role_table = 'salesmen_select.salesmen_id';
        }
        $orders = $this->getOrders(0, $role_Id, $role_table);
        $page= \App\Http\Controllers\GeneralController::getOption('openOrderPageLimit');
        return view('loggedin.closedorders', ['orders' => $orders, 'page'=>$page]);
    }
    
    /**
     * Returns the order
     * 
     * @param bool $open 1 for open orders and 0 for closed orders
     * @param int $factory_id
     * @return object Database row
     */
    private function getOrders($open, $role_Id=null, $role_table=null) {
        if ($open == 1){
            $pageLimit = $this->getOpenOrderPageLimit();
        }
        else {
            $pageLimit = $this->getClosedOrderPageLimit();
        };
        if (isset($role_Id)){
             return DB::table(DB::raw('(import_po, import_po_dates)'))
                ->select('import_po.id as ipo', 'import_po.*', 'import_po_dates.*'
                        ,'factories.id as ftyId', 'factories.name as ftyName', 'factories.vendor_id as ftyVId'
                        ,'export_cos.id as ExCId' , 'export_cos.name as ExCName', 'export_cos.fama as ExCFama'
                        ,'freight_forwarders.id as FFId', 'freight_forwarders.name as FFName'
                        ,'port_sf.id as port_ShipF_ID', 'port_sf.name as port_ShipF_Name'
                        ,'port_st.id as port_ShipT_ID', 'port_st.port_name as port_ShipT_Name'
                        
                        ,DB::raw("GROUP_CONCAT(distinct tickets_ordered.tickets_ordered separator ', ') as tickets_orderedDates")
                        
                        ,DB::raw("GROUP_CONCAT(distinct product_types.name separator ', ') as prod_typesNames")
                        ,DB::raw("GROUP_CONCAT(distinct product_types.id separator ',') as prod_typesId")
                        ,DB::raw("GROUP_CONCAT(distinct product_styles.name ORDER BY product_styles.id separator ', ') as prod_styleNames")
                        ,DB::raw("GROUP_CONCAT(distinct product_styles.id ORDER BY product_styles.id separator ',') as prod_styleId")
                        ,DB::raw("GROUP_CONCAT(distinct customers.name separator ', ') as customersNames")
                        ,DB::raw("GROUP_CONCAT(distinct customers.id separator ',') as customersId")
                        ,DB::raw("GROUP_CONCAT(distinct licenses.name separator ', ') as licensesNames")
                        ,DB::raw("GROUP_CONCAT(distinct licenses.id separator ',') as licensesId")
                        ,DB::raw("GROUP_CONCAT(distinct salesmen.name separator ', ') as salesmenNames")
                        ,DB::raw("GROUP_CONCAT(distinct salesmen.id separator ',') as salesmenId")
                        ,DB::raw("GROUP_CONCAT(distinct licenses.fama_require separator ', ') as licensesFama")
                        )
                ->leftJoin('factories','import_po.fty_produc', '=', 'factories.id')
                ->leftJoin('export_cos','import_po.export_cos', '=', 'export_cos.id')
                ->leftJoin('freight_forwarders','import_po.freight_forwarder', '=', 'freight_forwarders.id')
                ->leftJoin('ports as port_sf','import_po.shipping_from', '=', 'port_sf.id')
                ->leftJoin('ports_to as port_st','import_po.shipping_to', '=', 'port_st.id')
                
                ->leftJoin('tickets_ordered','import_po.id', '=', 'tickets_ordered.order_id')
                
                ->leftJoin('product_types_select','import_po.id', '=', 'product_types_select.order_id')
                ->leftJoin('product_types as product_types','product_types.id', '=', 'product_types_select.product_types_id')
                
                ->leftJoin('product_styles_select','import_po.id', '=', 'product_styles_select.order_id')
                ->leftJoin('product_styles','product_styles.id', '=', 'product_styles_select.product_styles_id')
                
                ->leftJoin('licenses_select','import_po.id', '=', 'licenses_select.order_id')
                ->leftJoin('licenses','licenses.id', '=', 'licenses_select.licenses_id')
                
                ->leftJoin('customers_select','import_po.id', '=', 'customers_select.order_id')
                ->leftJoin('customers','customers.id', '=', 'customers_select.customers_id')
                
                ->leftJoin('salesmen_select','import_po.id', '=', 'salesmen_select.order_id')
                ->leftJoin('salesmen','salesmen.id', '=', 'salesmen_select.salesmen_id')
                
                ->whereRaw('`import_po`.`id` = `import_po_dates`.`order_po_id`')
                ->where('import_po.active', 1)
                ->where('import_po.open', $open)
                ->where("$role_table", $role_Id)
                ->groupBy('import_po.id','import_po.comments', 'import_po.po_number'
                        ,'import_po.open','import_po.value','import_po.fty_produc'
                        ,'import_po.export_cos','import_po.customer_po_number','import_po.qty_ordered'
                        ,'import_po.tickets_needed','import_po.shipping_from','import_po.shipping_to','import_po.cbm'
                        ,'import_po.book_container','import_po.safety_test','import_po.safety_test_pass','import_po.david_ins_result'
                        ,'import_po.david_ins_comment','import_po.svn_no','import_po.freight_forwarder','import_po.pro_photo','import_po.receive_whse'
                        ,'import_po_dates.id','import_po_dates.created_at','import_po_dates.updated_at','import_po_dates.order_po_id'
                        ,'import_po_dates.date_placed','import_po_dates.tickets_received','import_po_dates.finish_prod','import_po_dates.estimated_arr'
                        ,'import_po_dates.customer_cancel','import_po_dates.act_etd_forwarder','import_po_dates.act_eta_forwarder','import_po_dates.safety_test_on'
                        ,'import_po_dates.fty_prod_safety_test','import_po_dates.rec_comm_inv_pking','import_po_dates.rec_comm_inv_pking_home','import_po_dates.original_fcr'
                        ,'import_po_dates.ctpat_form','import_po_dates.telex_release','import_po_dates.okay_to_pay','import_po_dates.did_we_pay','import_po_dates.send_diann'
                        ,'factories.id', 'factories.name', 'factories.vendor_id'
                        ,'export_cos.id' , 'export_cos.name', 'export_cos.fama'
                        ,'freight_forwarders.id', 'freight_forwarders.name'
                        ,'port_sf.id', 'port_sf.name'
                        ,'port_st.id', 'port_st.port_name',
                        'import_po_dates.closed'
                        )
                ->orderBy('import_po_dates.closed', 'desc')
                ->orderBy('ipo', 'desc')
                ->get();
        }
         else {
            return DB::table(DB::raw('(import_po, import_po_dates)'))
                ->select('import_po.id as ipo', 'import_po.*', 'import_po_dates.*'
                        ,'factories.id as ftyId', 'factories.name as ftyName', 'factories.vendor_id as ftyVId'
                        ,'export_cos.id as ExCId' , 'export_cos.name as ExCName', 'export_cos.fama as ExCFama'
                        ,'freight_forwarders.id as FFId', 'freight_forwarders.name as FFName'
                        ,'port_sf.id as port_ShipF_ID', 'port_sf.name as port_ShipF_Name'
                        ,'port_st.id as port_ShipT_ID', 'port_st.port_name as port_ShipT_Name'
                        
                        ,DB::raw("GROUP_CONCAT(distinct tickets_ordered.tickets_ordered separator ', ') as tickets_orderedDates")
                        
                        ,DB::raw("GROUP_CONCAT(distinct product_types.name separator ', ') as prod_typesNames")
                        ,DB::raw("GROUP_CONCAT(distinct product_types.id separator ',') as prod_typesId")
                        ,DB::raw("GROUP_CONCAT(distinct product_styles.name ORDER BY product_styles.id separator ', ') as prod_styleNames")
                        ,DB::raw("GROUP_CONCAT(distinct product_styles.id ORDER BY product_styles.id separator ',') as prod_styleId")
                        ,DB::raw("GROUP_CONCAT(distinct customers.name separator ', ') as customersNames")
                        ,DB::raw("GROUP_CONCAT(distinct customers.id separator ',') as customersId")
                        ,DB::raw("GROUP_CONCAT(distinct licenses.name separator ', ') as licensesNames")
                        ,DB::raw("GROUP_CONCAT(distinct licenses.id separator ',') as licensesId")
                        ,DB::raw("GROUP_CONCAT(distinct salesmen.name separator ', ') as salesmenNames")
                        ,DB::raw("GROUP_CONCAT(distinct salesmen.id separator ',') as salesmenId")
                        ,DB::raw("GROUP_CONCAT(distinct licenses.fama_require separator ', ') as licensesFama")

                        )
                ->leftJoin('factories','import_po.fty_produc', '=', 'factories.id')
                ->leftJoin('export_cos','import_po.export_cos', '=', 'export_cos.id')
                ->leftJoin('freight_forwarders','import_po.freight_forwarder', '=', 'freight_forwarders.id')
                ->leftJoin('ports as port_sf','import_po.shipping_from', '=', 'port_sf.id')
                ->leftJoin('ports_to as port_st','import_po.shipping_to', '=', 'port_st.id')
                
                ->leftJoin('tickets_ordered','import_po.id', '=', 'tickets_ordered.order_id')
                
                ->leftJoin('product_types_select','import_po.id', '=', 'product_types_select.order_id')
                ->leftJoin('product_types as product_types','product_types.id', '=', 'product_types_select.product_types_id')
                
                ->leftJoin('product_styles_select','import_po.id', '=', 'product_styles_select.order_id')
                ->leftJoin('product_styles','product_styles.id', '=', 'product_styles_select.product_styles_id')
                
                ->leftJoin('licenses_select','import_po.id', '=', 'licenses_select.order_id')
                ->leftJoin('licenses','licenses.id', '=', 'licenses_select.licenses_id')
                
                ->leftJoin('customers_select','import_po.id', '=', 'customers_select.order_id')
                ->leftJoin('customers','customers.id', '=', 'customers_select.customers_id')
                
                ->leftJoin('salesmen_select','import_po.id', '=', 'salesmen_select.order_id')
                ->leftJoin('salesmen','salesmen.id', '=', 'salesmen_select.salesmen_id')
                
                ->whereRaw('`import_po`.`id` = `import_po_dates`.`order_po_id`')
                ->whereRaw('`import_po`.`active` = 1')
                ->where('import_po.open', $open)
                ->groupBy('import_po.id','import_po.comments', 'import_po.po_number'
                        ,'import_po.open','import_po.value','import_po.fty_produc'
                        ,'import_po.export_cos','import_po.customer_po_number','import_po.qty_ordered'
                        ,'import_po.tickets_needed','import_po.shipping_from','import_po.shipping_to','import_po.cbm'
                        ,'import_po.book_container','import_po.safety_test','import_po.safety_test_pass','import_po.david_ins_result'
                        ,'import_po.david_ins_comment','import_po.svn_no','import_po.freight_forwarder','import_po.pro_photo','import_po.receive_whse'
                        ,'import_po_dates.id','import_po_dates.created_at','import_po_dates.updated_at','import_po_dates.order_po_id'
                        ,'import_po_dates.date_placed','import_po_dates.tickets_received','import_po_dates.finish_prod','import_po_dates.estimated_arr'
                        ,'import_po_dates.customer_cancel','import_po_dates.act_etd_forwarder','import_po_dates.act_eta_forwarder','import_po_dates.safety_test_on'
                        ,'import_po_dates.fty_prod_safety_test','import_po_dates.rec_comm_inv_pking','import_po_dates.rec_comm_inv_pking_home','import_po_dates.original_fcr'
                        ,'import_po_dates.ctpat_form','import_po_dates.telex_release','import_po_dates.okay_to_pay','import_po_dates.did_we_pay','import_po_dates.send_diann'
                        ,'factories.id', 'factories.name', 'factories.vendor_id'
                        ,'export_cos.id' , 'export_cos.name', 'export_cos.fama'
                        ,'freight_forwarders.id', 'freight_forwarders.name'
                        ,'port_sf.id', 'port_sf.name'
                        ,'port_st.id', 'port_st.port_name',
                        'import_po_dates.closed'
                        )
                ->orderBy('import_po_dates.closed', 'desc')
                ->orderBy('ipo', 'desc')
                ->get();
         }
    }
    
    
    
    /**
     * This function returns the data from all secondary tables on table
     * managemnt page.
     * 
     * @return array
     */
    public function getAllTablesData() {
        $data['product_styles'] = DB::select("select * from product_styles where active = 1");
        $data['product_types'] = DB::select("select * from product_types where active = 1");
        $data['freight_forwarders'] = DB::select("select * from freight_forwarders where active = 1");
        $data['ports'] = DB::select("select * from ports where active = 1");
        $data['customers'] = DB::select("select * from customers where active = 1");
        $data['factories'] = DB::select("select * from factories where active = 1");
        $data['export_cos'] = DB::select("select * from export_cos where active = 1");
        $data['licenses'] = DB::select("select * from licenses where active = 1");
        $data['salesmen'] = DB::select("select * from salesmen where active = 1");
        $data['ports_to'] = DB::select("select * from ports_to");
        return $data;
    }


    /**
     * This function creates a new order.
     * 
     * @param Request $request
     * @return null The page redirects after this function
     */
    public function startNewOrder(Request $request) {
        $request->validate([
            'new_orde_po' => 'required|unique:import_po,po_number|max:7'
        ]);
        
        $id = DB::table('import_po')->insertGetId(
            ['po_number' => $request->new_orde_po]
        );
        DB::table('import_po_dates')->insert(
            ['order_po_id' => $id]
        );
        
        return redirect('/orders/open')->with('message', 'New Import PO Successfully Added');
    }
    
    
    /**
     * Order settings page
     * 
     * @param Request $request
     * @return type
     */
    public function showOrdersSettings(Request $request) {
        
        $user = Auth::user();
            
            if ($user->hasRole('Admin')) {
                $safety_test_on =Option::where('option_name','days_safety_test_on')->first();
                $data['safety_test_on']= ($safety_test_on) ? $safety_test_on->option_value: null;
                $data['open'] = \App\Http\Controllers\GeneralController::getOption('openOrderPageLimit');
                $data['closed'] = \App\Http\Controllers\GeneralController::getOption('closedOrderPageLimit');

                $col_def = DB::select('select * from col_definations');
                return view( 'loggedin.order_settings', [ 'data' => $data ,'col_definations' => $col_def] );
            }
            else{
                return redirect('/home')->withErrors('You dont have the permission to access users permission page.');
            }
    }
   
     /**
     * days safety_test_on are submitted through this function
     * 
     * @param Request $request
     * @return type
     */
    public function submitDaysSafetyTestOn(Request $request) {
        
        $user = Auth::user();

        if ($user->hasRole('Admin')) {
            $this->validate($request, [
                'option_value' => 'required|integer'
            ]);
            $option = Option::firstOrNew(
                [
                'option_name' => 'days_safety_test_on'
                ]
            );
            $option->option_value=$request->option_value;
            $option->save();

            return redirect('/orders/settings')->with('message', 'Values updated successfully');
        }
    }
    /**
     * Number of orders to be displayed on open and cosed order pages for pagination
     * 
     * @param Request $request
     * @return type
     */
    public function submitOrderDisplayLimits(Request $request) {
        
        $user = Auth::user();

        if ($user->hasRole('Admin')) {
            $this->validate($request, [
                'open' => 'numeric|nullable',
                'closed' => 'numeric|nullable'
            ]);
            \App\Http\Controllers\GeneralController::setOption('openOrderPageLimit', $request->open);
            \App\Http\Controllers\GeneralController::setOption('closedOrderPageLimit', $request->closed);

            return redirect('/orders/settings')->with('message', 'Values updated successfully');
        }
    }
    
    /**
     * Gets Open Order Page Limit for pagination
     * 
     * @return int
     */
    private function getOpenOrderPageLimit(){
        
        $open = \App\Http\Controllers\GeneralController::getOption('openOrderPageLimit');
        
        if ($open !=null and is_numeric($open)) {
            return $open;
        }
        else {
            return 10;
        }
    }
    
    /**
     * Gets closed order page limit for pagination
     * 
     * @return int
     */
    private function getClosedOrderPageLimit(){
        
        $closed = \App\Http\Controllers\GeneralController::getOption('closedOrderPageLimit');
        
        if ($closed !=null and is_numeric($closed)) {
            return $closed;
        }
        else {
            return 10;
        }
    }
    
    /**
     * Submit Textarea Fields with this function
     * 
     * @param Request $request
     * @return json
     */
    public function submit_textarea_fields(Request $request) {
        
        $messages = [   
                        'string'    => 'Invalid string.',
                        'required'  => 'This field is required.',
                        'sub_textarea_field_value.max' => 'A value longer than 90 characters received',
                        'sub_textarea_order_id.numeric'  => 'Order ID must be numeric',
                        'sub_textarea_order_id.exists'  => 'Invalid Order ID.'
                    ];
        
        $validator = Validator::make($request->all(), [
            'sub_textarea_field_name'   => 'required|string',
            'sub_textarea_field_value'  => 'max:90',
            'sub_textarea_order_id'     => 'required|numeric|exists:import_po,id',
        ], $messages);
//
        if ($validator->passes()) {
            $trimed = str_replace(array("\n","\r","\t", "\0", "\x0B", "\"", "'"), '', $request->sub_textarea_field_value);
            //update the row here
            DB::table('import_po')
            ->where('id', $request->sub_textarea_order_id)
            ->update([$request->sub_textarea_field_name => $trimed]);
            
            $sub_textarea_field_name = $request->sub_textarea_field_name;
            $db_res = DB::table('import_po')->select($request->sub_textarea_field_name)->where('id', $request->sub_textarea_order_id)->first();
            
			return response()->json([   'success'=>'Added new records.',
                                                    'newValue' => $db_res->$sub_textarea_field_name,
                                                    'newValueRowId'=> $request->sub_textarea_field_name .'_t_'. $request->sub_textarea_order_id
                        ]);
        }                                                                                                                                   
    	return response()->json(['error'=>$validator->errors()->all()]);

    }
    
    /**
     * Submit Text Fields with this function
     * 
     * @param Request $request
     * @return json
     */
    public function submit_text_fields(Request $request) {
        $messages = [   
                        'string'    => 'Invalid string.',
                        'required'  => 'This field is required.',
                        'sub_txt_field_value.max' => 'A value longer than 25 characters received',
                        'sub_txt_order_id.numeric'  => 'Order ID must be numeric',
                        'sub_txt_order_id.exists'  => 'Invalid Order ID.'
                    ];
        
        $validator = Validator::make($request->all(), [
            'sub_txt_field_name'   => 'required|string',
            'sub_txt_field_value'  => 'max:25',
            'sub_txt_order_id'     => 'required|numeric|exists:import_po,id',
        ], $messages);
        if ($validator->passes()) {
            if($request->sub_txt_field_name=="po_number"){
                $validator = Validator::make($request->all(), [
                    'sub_txt_field_value'  => 'required'
                ], $messages);

            }
           
            if ($validator->passes()) {
                //update the row here
                //$req = $request->all();
                DB::table('import_po')
                ->where('id', $request->sub_txt_order_id)
                ->update([$request->sub_txt_field_name => $request->sub_txt_field_value]);
                
                $sub_txt_field_name = $request->sub_txt_field_name;
                $db_res = DB::table('import_po')->select($request->sub_txt_field_name)->where('id', $request->sub_txt_order_id)->first();
                
                return response()->json([   'success'=>'Added new records.',
                                                        'newValue' => $db_res->$sub_txt_field_name,
                                                        'newValueRowId'=> $request->sub_txt_field_name .'_t_'. $request->sub_txt_order_id
                                                        //'request' => $req
                            ]);
            }
        }      

    	return response()->json(['error'=>$validator->errors()->all()]);
    }
    
    
    /**
     * Submit Date fields with this function
     * 
     * @param Request $request
     * @return json
     */
    public function submit_date_fields(Request $request) {
        $messages = [   'string'    => 'Invalid string.',
                        'required'  => 'This field is required.',
                        'sub_date_field_value.date' => 'Incorrect date format received',
                        'sub_date_field_value.date_format' => 'Incorrect date format received',
                        'sub_date_order_id.numeric'  => 'Order ID must be numeric',
                        'sub_date_order_id.exists'  => 'Invalid Order ID.'
                    ];
        
        $validator = Validator::make($request->all(), [
            'sub_date_field_name'   => 'required|string',
            'sub_date_field_value'  => 'nullable|date',//date if date_format not suitabe|date_format:yyyy-mm-dd
            'sub_date_order_id'     => 'required|numeric|exists:import_po,id',
        ], $messages);
//

        if ($validator->passes()) {
            if($request->sub_date_field_name=="act_etd_forwarder"){
                $validator = Validator::make($request->all(), [
                    'sub_date_field_value'  => [new ActEtd()]
                ]);
                if (!$validator->passes()) {
                    return response()->json(['error'=>$validator->errors()->all()]);
                }

            }
            $validator = Validator::make($request->all(), [
                'sub_date_field_name'   => 'required|string',
                'sub_date_field_value'  => 'nullable|date',//date if date_format not suitabe|date_format:yyyy-mm-dd
                'sub_date_order_id'     => 'required|numeric|exists:import_po,id',
            ], $messages);


            if($request->sub_date_field_name=="date_placed" || $request->sub_date_field_name=="safety_test_on" || $request->sub_date_field_name=="finish_prod"){
                $validator = Validator::make($request->all(), [
                    'sub_date_field_value'  => 'required'
                ], $messages);

            }
            if ($validator->passes()) {
            
                //update the row here
                //$req = $request->all();
                DB::table('import_po_dates')
                ->where('order_po_id', $request->sub_date_order_id)
                ->update([$request->sub_date_field_name => $request->sub_date_field_value]);
                
                $sub_date_field_name = $request->sub_date_field_name;
                $db_res = DB::table('import_po_dates')->select($request->sub_date_field_name)->where('order_po_id', $request->sub_date_order_id)->first();
                
                if ($request->sub_date_field_name == 'finish_prod') { 
                    $this->populateEstimatedArrOnEditingFinishProd($request->sub_date_order_id);   
                }
                
                if ($request->sub_date_field_name == 'finish_prod') {     
                    //safety condition on editing finish_prod
                    $TriggerEvents = new TriggerEventsController();
                    $TriggerEvents->triggerSafetyTestingConditions($request->sub_date_order_id);
                }
                
                if (    $request->sub_date_field_name == 'customer_cancel' or $request->sub_date_field_name == 'estimated_arr' or
                        $request->sub_date_field_name == 'act_eta_forwarder' ) { 
                    $TriggerEvents = new TriggerEventsController();
                    $TriggerEvents->triggerDeliveryConditions($request->sub_date_order_id);
                }
                
                //on did_we_pay field check if we need to close this order
                if (    $request->sub_date_field_name == 'did_we_pay' ) { 
                    
                    $TriggerEvents = new TriggerEventsController();
                    if ($TriggerEvents->validateOrderCloseCondition($request->sub_date_order_id)) {
                        
                        $this->closeOrder($request->sub_date_order_id);
                    }
                    
                }
                
                
                    return response()->json([   'success'=>'Added new records.',
                                                'newValue' => $db_res->$sub_date_field_name,
                                                'newValueRowId'=> $request->sub_date_field_name .'_t_'. $request->sub_txt_order_id
                                                //'request' => $req
                    ]);
            }
        }                                                                                                                                   
    	return response()->json(['error'=>$validator->errors()->all()]);
    }
    
    
    private function closeOrder($order_id){
        $order = ImportPo::findOrFail($order_id);
        $order->open= 0;
        $order->import_po_date()->update([
            'closed'=>Carbon::now()
        ]);
        $order->save();

       /* DB::table('import_po')
            ->where('id', $order_id)
            ->update(['open' => 0]);*/
        
    }
    
    /**
     * This function runs the logic for editing Finish Prod
     * 
     * @param int $order_id
     */
    private function populateEstimatedArrOnEditingFinishProd($order_id){
        $db_res = DB::select("select `ports`.`name`, `ports`.`id` 
                from (import_po) 
                left join `ports` on `import_po`.`shipping_to` = `ports`.`id` where `import_po`.`id`  = ?",
                [$order_id]);
        
            if (isset($db_res[0]->name)) {
                $TriggerEvents = new TriggerEventsController();
                $TriggerEvents->triggerShippingTo($order_id, $db_res[0]->name);
            }
    }
    
    /**
     * Submit Boolean fields (Yes/No) with this function
     * 
     * @param Request $request
     * @return json
     */
    public function submit_yes_no_fields(Request $request) {
        
        $messages = [   'string'    => 'Invalid string.',
                        'required'  => 'This field is required.',
                        'sub_yes_no_order_id.numeric'  => 'Order ID must be numeric',
                        'sub_yes_no_order_id.exists'  => 'Invalid Order ID.'
                    ];
        
        $validator = Validator::make($request->all(), [
            'sub_yes_no_name'     => 'required|string',
            'sub_yes_no_value'          => 'nullable|boolean',
            'sub_yes_no_order_id'       => 'required|numeric|exists:import_po,id',
        ], $messages);
//
        if ($validator->passes()) {
            if($request->sub_yes_no_name=="safety_test"){
                $validator = Validator::make($request->all(), [
                    'sub_yes_no_value'   => 'required'
                ], $messages);

            }

            if ($validator->passes()) {
            
                //update the row here
                //$req = $request->all();
                DB::table('import_po')
                ->where('id', $request->sub_yes_no_order_id)
                ->update([$request->sub_yes_no_name => $request->sub_yes_no_value]);
                
                $sub_yes_no_name = $request->sub_yes_no_name;
                $db_res = DB::table('import_po')->select($request->sub_yes_no_name)->where('id', $request->sub_yes_no_order_id)->first();
                //if ($db_res->$sub_yes_no_name == 1){$newValue = "Yes";}elseif($db_res->$sub_yes_no_name == 0){$newValue = "No";}else {$newValue = null;}
                
                if ($request->sub_yes_no_name == 'safety_test' or $request->sub_yes_no_name == 'safety_test_pass') { 
                    $TriggerEvents = new TriggerEventsController();
                    $TriggerEvents->triggerSafetyTestingConditions($request->sub_yes_no_order_id);
                }
                
                //on receive_whse field check if we need to close this order
                if (    $request->sub_yes_no_name == 'receive_whse' ) { 
                    
                    $TriggerEvents = new TriggerEventsController();
                    if ($TriggerEvents->validateOrderCloseCondition($request->sub_yes_no_order_id)) {
                        
                            $this->closeOrder($request->sub_yes_no_order_id);
                    }
                }
                
                return response()->json([   'success'=>'Added new records.',
                                                        'newValue' => $db_res->$sub_yes_no_name,
                                                        'newValueRowId'=> $request->sub_yes_no_name .'_t_'. $request->sub_yes_no_order_id
                                                        //'request' => $req
                            ]);
            }
        }                                                                                                                                   
    	return response()->json(['error'=>$validator->errors()->all()]);
        
    }
    
    
    
    /**
     * Submit Boolean fields (PASS/FAIL) with this function
     * 
     * @param Request $request
     * @return json
     */
    public function submit_pass_fail_fields(Request $request) {
        
        $messages = [   'string'    => 'Invalid string.',
                        'required'  => 'This field is required.',
                        'sub_pass_fail_value.date' => 'Incorrect date format received',
                        'sub_pass_fail_value.date_format' => 'Incorrect date format received',
                        'sub_pass_fail_order_id.numeric'  => 'Order ID must be numeric',
                        'sub_pass_fail_order_id.exists'  => 'Invalid Order ID.'
                    ];
        
        $validator = Validator::make($request->all(), [
            'sub_pass_fail_name'     => 'required|string',
            'sub_pass_fail_value'    => 'nullable|boolean',
            'sub_pass_fail_order_id' => 'required|numeric|exists:import_po,id',
        ], $messages);
//
        if ($validator->passes()) {
            $user = Auth::user();
            if($request->sub_pass_fail_name == 'david_ins_result' and !($user->hasRole('QC') or $user->hasRole('Admin'))) 
                
            {
                return response()->json(['error'=>'This field can only be updated by Admin or QC user']);
            }
            
            
            //update the row here\
            DB::table('import_po')
            ->where('id', $request->sub_pass_fail_order_id)
            ->update([$request->sub_pass_fail_name => $request->sub_pass_fail_value]);
            
            $sub_yes_no_name = $request->sub_pass_fail_name;
            $db_res = DB::table('import_po')->select($request->sub_pass_fail_name)->where('id', $request->sub_pass_fail_order_id)->first();
			return response()->json([   'success'=>'Added new records.',
                                                    'newValue' => $db_res->$sub_yes_no_name,
                                                    'newValueRowId'=> $request->sub_pass_fail_name .'_t_'. $request->sub_pass_fail_order_id
                                                    //'request' => $req
                        ]);
        }                                                                                                                                   
    	return response()->json(['error'=>$validator->errors()->all()]);
        
    }
    
    /**
     * Submit Numeric fields with this function
     * 
     * @param Request $request
     * @return json
     */
    public function submit_numeric_fields(Request $request) {
        $messages = [   'string'    => 'Invalid string.',
                        'required'  => 'This field is required.',
                        'sub_num_order_id.numeric'  => 'Order ID must be numeric',
                        'sub_num_order_id.exists'  => 'Invalid Order ID.'
                    ];
        
        $validator = Validator::make($request->all(), [
            'sub_num_name'   => 'required',
            'sub_num_value'  => 'nullable|numeric',//todo add max 10 validatio
            'sub_num_order_id'     => 'required|numeric|exists:import_po,id',
        ], $messages);
//
        if ($validator->passes()) {
            
            //update the row here
            //$req = $request->all();
            DB::table('import_po')
            ->where('id', $request->sub_num_order_id)
            ->update([$request->sub_num_name => $request->sub_num_value]);
            
            $sub_num_name = $request->sub_num_name;
            $db_res = DB::table('import_po')->select($request->sub_num_name)->where('id', $request->sub_num_order_id)->first();
            
            $newValueMoney = '$ '.number_format($db_res->$sub_num_name);
            
			return response()->json([   'success'=>'Added new records.',
                                                    'newValue' => $db_res->$sub_num_name,
                                                    'newValueMoney' => $newValueMoney,
                                                    'newValueRowId'=> $request->sub_num_name .'_t_'. $request->sub_num_order_id
                                                    //'request' => $req
                        ]);
        }                                                                                                                                   
    	return response()->json(['error'=>$validator->errors()->all()]);

    }
    
    
    public function submit_cbm_field(Request $request){
        $messages = [   'string'    => 'Invalid string.',
                        'required'  => 'This field is required.',
                        'sub_cbm_order_id.numeric'  => 'Order ID must be numeric',
                        'sub_cbm_order_id.exists'  => 'Invalid Order ID.'
                    ];
        
        $validator = Validator::make($request->all(), [
            'sub_cbm_name'   => 'required',
            'sub_cbm_value'  => 'nullable|numeric',//todo add max 10 validatio
            'sub_cbm_order_id'     => 'required|numeric|exists:import_po,id',
        ], $messages);
//
        if ($validator->passes()) {
            
            //update the row here
            //$req = $request->all();
            DB::table('import_po')
            ->where('id', $request->sub_cbm_order_id)
            ->update([$request->sub_cbm_name => $request->sub_cbm_value]);
            
            $sub_num_name = $request->sub_cbm_name;
            $db_res = DB::table('import_po')->select($request->sub_cbm_name)->where('id', $request->sub_cbm_order_id)->first();
            
            
			return response()->json([   'success'=>'Added new records.',
                                                    'newValue' => $db_res->$sub_num_name,
                                                    'newValueRowId'=> $request->sub_cbm_name .'_t_'. $request->sub_cbm_order_id
                                                    //'request' => $req
                        ]);
        }                                                                                                                                   
    	return response()->json(['error'=>$validator->errors()->all()]);
    }
    
    
    /**
     * Submit fty_produc_field with this function
     * 
     * @param Request $request
     * @return json
     */
    public function submit_fty_produc_field(Request $request) {
        $messages = [   'string'    => 'Invalid string.',
                        'required'  => 'This field is required.',
                        'sub_fty_produc_order_id.numeric'  => 'Order ID must be numeric',
                        'sub_fty_produc_order_id.exists'  => 'Invalid Order ID.'
                    ];
        
        $validator = Validator::make($request->all(), [
            'sub_fty_produc_value'  => 'required|numeric',//todo add max 10 validatio
            'sub_fty_produc_order_id'     => 'required|numeric|exists:import_po,id',
        ], $messages);
        
        
        if ($validator->passes()) {
            
            //update the row here
            //$req = $request->all();
            

            DB::table('import_po')
            ->where('id', $request->sub_fty_produc_order_id)
            ->update(['fty_produc' => $request->sub_fty_produc_value]);
            
            $db_res = DB::select('select `factories`.`name`, `factories`.`id`,`factories`.`vendor_id`
            from (import_po) 
            left join `factories` on `import_po`.`fty_produc` = `factories`.`id`  
            where `import_po`.`id` = ?', [$request->sub_fty_produc_order_id]);
            
            
			return response()->json([   'success'=>'Added new records.',
                                                    'newValue' => $db_res[0]->name,
                                                    'newId' => $db_res[0]->id,
                                                    'VID' => $db_res[0]->vendor_id
                        ]);
        }                                                                                                                                   
    	return response()->json(['error'=>$validator->errors()->all()]);
        
    }
    
    /**
     * Submit export_cos (export company) with this function
     * 
     * @param Request $request
     * @return json
     */
    public function submit_export_cos_field(Request $request) {
        
        $messages = [   'string'    => 'Invalid string.',
                        'required'  => 'This field is required.',
                        'sub_export_cos_order_id.numeric'  => 'Order ID must be numeric',
                        'sub_export_cos_order_id.exists'  => 'Invalid Order ID.'
                    ];
        
        $validator = Validator::make($request->all(), [
            'sub_export_cos_value'  => 'required|numeric',//todo add max 10 validatio
            'sub_export_cos_order_id'     => 'required|numeric|exists:import_po,id',
        ], $messages);
        
        
        if ($validator->passes()) {
            
            //update the row here
            //$req = $request->all();
            

            DB::table('import_po')
            ->where('id', $request->sub_export_cos_order_id)
            ->update(['export_cos' => $request->sub_export_cos_value]);
            
            $db_res = DB::select('select `export_cos`.`name`, `export_cos`.`id`, `export_cos`.`fama` from (import_po) 
                left join `export_cos` on `import_po`.`export_cos` = `export_cos`.`id` where `import_po`.`id`  = ?',
                    [$request->sub_export_cos_order_id]);
            
            if ($db_res[0]->fama == 0) {
                $fama = 'No';
            } elseif ($db_res[0]->fama == 1){
                $fama = 'Yes';
            } 
            
            
            $TriggerEvents = new TriggerEventsController();
            $TriggerEvents->triggerLicenseConditions($request->sub_export_cos_order_id);
            
			return response()->json([   'success'=>'Added new records.',
                                                    'newValue' => $db_res[0]->name,
                                                    'newId' => $db_res[0]->id,
                                                    'fama' => $fama
                        ]);
        }                                                                                                                                   
    	return response()->json(['error'=>$validator->errors()->all()]);
    }
    
    
    /**
     * Submit ports field (Shipping to and shipping from) with this function
     * 
     * @param Request $request
     * @return json
     */
    public function submit_ports_field(Request $request){
      
        $messages = [   'string'    => 'Invalid string.',
                'required'  => 'This field is required.',
                'sub_ports_order_id.numeric'  => 'Order ID must be numeric',
                'sub_ports_order_id.exists'  => 'Invalid Order ID.'
            ];
        
        $validator = Validator::make($request->all(), [
            'sub_ports_name'           => 'required|string',
            'sub_ports_value'          => 'nullable|string',
            'sub_ports_order_id'       => 'required|numeric|exists:import_po,id',
        ], $messages);
        
        
        if ($validator->passes()) {
            
            DB::table('import_po')
            ->where('id', $request->sub_ports_order_id)
            ->update([$request->sub_ports_name => $request->sub_ports_value]);
            
            $db_res = DB::select("select `ports`.`name`, `ports`.`id` 
                from (import_po) 
                left join `ports` on `import_po`.`$request->sub_ports_name` = `ports`.`id` where `import_po`.`id`  = ?",
                    [$request->sub_ports_order_id]);
            
            return response()->json([   'success'=>'Added new records.',
                                        'newValue' => $db_res[0]->name,
                                        'newId' => $db_res[0]->id,
            ]);
  
        }
        
    	return response()->json(['error'=>$validator->errors()->all()]);
        
    }
    /**
     * Submit ports field (Shipping to and shipping from) with this function
     * 
     * @param Request $request
     * @return json
     */
    public function submit_portsto_field(Request $request){
        $messages = [   'string'    => 'Invalid string.',
                'required'  => 'This field is required.',
                'sub_ports_order_id.numeric'  => 'Order ID must be numeric',
                'sub_ports_order_id.exists'  => 'Invalid Order ID.'
            ];
        
        $validator = Validator::make($request->all(), [
            'sub_ports_name'           => 'required|string',
            'sub_portsto_value'          => 'required|string',
            'sub_ports_order_id'       => 'required|numeric|exists:import_po,id',
        ], $messages);
        
        
        if ($validator->passes()) {
            $importPo = ImportPo::find($request->sub_ports_order_id);
            $importPo->shipping_to= $request->sub_portsto_value;
            //$portsto= PortTo::find($request->shipping_to);
            $importPo->save();
            $importPo->import_po_date()->update([
                'estimated_arr'=>Carbon::parse($importPo->import_po_date->finish_prod)->addDay($importPo->port_to->days_est_arrival)
            ]);
            
            
       
            return response()->json([   'success'=>'Added new records.',
                                        'newValue' => $importPo->port_to->port_name,
                                        'newId' => $importPo->shipping_to,
            ]);
  
        }
        
    	return response()->json(['error'=>$validator->errors()->all()]);
        
    }
    
    /**
     * Submit freight_forwarders with this function
     * 
     * @param Request $request
     * @return json
     */
    public function submit_freight_forwarders_field(Request $request){
        $messages = [
                'string'    => 'Invalid string.',
                'required'  => 'A value not received.',
                'sub_freight_forwarders_order_id.numeric'    => 'Order ID must be numeric',
                'sub_freight_forwarders_order_id.exists'     => 'Invalid Order ID.'
            ];
        
        $validator = Validator::make($request->all(), [
            'sub_freight_forwarders_value'          => 'nullable|string',
            'sub_freight_forwarders_order_id'       => 'required|numeric|exists:import_po,id',
        ], $messages);
        
        
        if ($validator->passes()) {
            
            DB::table('import_po')
            ->where('id', $request->sub_freight_forwarders_order_id)
            ->update(['freight_forwarder' => $request->sub_freight_forwarders_value]);
            
            $db_res = DB::select("select `freight_forwarders`.`name`, `freight_forwarders`.`id` 
                from (import_po) 
                left join `freight_forwarders` on `import_po`.`freight_forwarder` = `freight_forwarders`.`id` where `import_po`.`id`  = ?",
                    [$request->sub_freight_forwarders_order_id]);
            
            
			return response()->json([   'success'=>'Added new records.',
                                                    'newValue' => $db_res[0]->name,
                                                    'newId' => $db_res[0]->id,
                        ]);
        }                                                                                                                                   
    	return response()->json(['error'=>$validator->errors()->all()]);
        
    }
    
    /**
     * Submit product_styles with this function
     * 
     * @param Request $request
     * @return json
     */
    public function submit_product_styles_field(Request $request) {
        
        $messages = [
                'string'    => 'Invalid string.',
                'required'  => 'A value not received.',
                'sub_product_styles_order_id.numeric'   => 'Order ID must be numeric',
                'sub_product_styles_order_id.exists'    => 'Invalid Order ID.',
                'sub_product_styles_value.exists'       => 'Invalid Product Style submitted.'
            ];

        $validator = Validator::make($request->all(), [
            'sub_product_styles_value'      => 'array|required',
            'sub_product_styles_value.*'    => 'required|numeric|exists:product_styles,id',
            'sub_product_styles_order_id'   => 'required|numeric|exists:import_po,id',
        ], $messages);

        if ($validator->passes()) {

        //delete previous values
        DB::table('product_styles_select')->where('order_id', '=', $request->sub_product_styles_order_id)->delete();
            
            foreach ($request->sub_product_styles_value as $value){
                
                if($value !== null) {
                    DB::table('product_styles_select')->insert(
                        ['product_styles_id' => $value, 'order_id' => $request->sub_product_styles_order_id]
                    );
                }
            }
            
            $db_res = DB::select("select GROUP_CONCAT(product_styles.`name` separator ', ') as product_stylesNames,
                GROUP_CONCAT(product_styles.`id` separator ',') as product_stylesIds
                from (import_po)
                left join `product_styles_select` on `import_po`.`id` = `product_styles_select`.`order_id` left join `product_styles` 
                on `product_styles`.`id` = `product_styles_select`.`product_styles_id` where `import_po`.`id` = ?",
                    [$request->sub_product_styles_order_id]);
            
			return response()->json([   'success'=>'Added new records.',
                                                    'newValue' => $db_res[0]->product_stylesNames,
                                                    'newIds' => $db_res[0]->product_stylesIds,
                        ]);
        }                                                                                                                                   
    	return response()->json(['error'=>$validator->errors()->all()]);
    }
    
    /**
     * Submit product_types with this function
     * 
     * @param Request $request
     * @return json
     */
    public function submit_product_types_field(Request $request) {

        $messages = [
            'string' => 'Invalid string.',
            'required' => 'A value not received.',
            'sub_product_types_order_id.numeric' => 'Order ID must be numeric',
            'sub_product_types_order_id.exists' => 'Invalid Order ID.',
            'sub_product_types_value.exists' => 'Invalid Product Type submitted.'
        ];

        $validator = Validator::make($request->all(), [
                    'sub_product_types_value'      => 'array|required',
                    'sub_product_types_value.*' => 'required|numeric|exists:product_types,id',
                    'sub_product_types_order_id' => 'required|numeric|exists:import_po,id',
                        ], $messages);

        if ($validator->passes()) {

            //delete previous values
            DB::table('product_types_select')->where('order_id', '=', $request->sub_product_types_order_id)->delete();

            foreach ($request->sub_product_types_value as $value) {

                if ($value !== null) {
                    DB::table('product_types_select')->insert(
                            ['product_types_id' => $value, 'order_id' => $request->sub_product_types_order_id]
                    );
                }
            }

            $db_res = DB::select("select GROUP_CONCAT(product_types.`name` separator ', ') as producttypesNames, 
                GROUP_CONCAT(product_types.`id` separator ',') as producttypesIds
                from import_po
                left join `product_types_select` on `import_po`.`id` = `product_types_select`.`order_id` 
                left join `product_types` on `product_types`.`id` = `product_types_select`.`product_types_id` 
                where `import_po`.`id` = ?", [$request->sub_product_types_order_id]);

            return response()->json(['success' => 'Added new records.',
                        'newValue' => $db_res[0]->producttypesNames,
                        'newIds' => $db_res[0]->producttypesIds
            ]);
        }
        return response()->json(['error' => $validator->errors()->all()]);
    }
    
    /**
     * Submit license with this function
     * 
     * @param Request $request
     * @return json
     */
    public function submit_licenses_field(Request $request) {
         $messages = [
            'string' => 'Invalid string.',
            'required' => 'A value not received.',
            'sub_licenses_order_id.numeric' => 'Order ID must be numeric',
            'sub_licenses_order_id.exists' => 'Invalid Order ID.',
            'sub_licenses_value.exists' => 'Invalid License submitted.'
        ];

        $validator = Validator::make($request->all(), [
                    'sub_licenses_value'    => 'array|required',
                    'sub_licenses_value.*'  => 'required|numeric|exists:licenses,id',
                    'sub_licenses_order_id' => 'required|numeric|exists:import_po,id',
                    ], $messages);

        if ($validator->passes()) {

            //delete previous values
            DB::table('licenses_select')->where('order_id', '=', $request->sub_licenses_order_id)->delete();

            foreach ($request->sub_licenses_value as $value) {

                if ($value !== null) {
                    DB::table('licenses_select')->insert(
                            ['licenses_id' => $value, 'order_id' => $request->sub_licenses_order_id]
                    );
                }
            }

            $db_res = DB::select("select GROUP_CONCAT(licenses.`name` separator ', ') as licensesNames,
                GROUP_CONCAT(licenses.`id` separator ',') as licensesIds
                from (import_po)
                left join `licenses_select` on `import_po`.`id` = `licenses_select`.`order_id` 
                left join `licenses` on `licenses`.`id` = `licenses_select`.`licenses_id` where `import_po`.`id` = ?", [$request->sub_licenses_order_id]);

            //trigger notification 
            $TriggerEvents = new TriggerEventsController();
            $TriggerEvents->triggerLicenseConditions($request->sub_licenses_order_id);
            
            return response()->json(['success' => 'Added new records.',
                        'newValue'  =>  $db_res[0]->licensesNames,
                        'newIds'    =>  $db_res[0]->licensesIds
            ]);
        }
        return response()->json(['error' => $validator->errors()->all()]);
    }
    
    /**
     * Submit customers with this function
     * 
     * @param Request $request
     * @return json
     */
    public function submit_customers_field(Request $request) {
        $messages = [
            'string' => 'Invalid string.',
            'required' => 'A value not received.',
            'sub_customers_order_id.numeric' => 'Order ID must be numeric',
            'sub_customers_order_id.exists' => 'Invalid Order ID.',
            'sub_customers_value.exists' => 'Invalid License submitted.'
        ];

        $validator = Validator::make($request->all(), [
                    'sub_customers_value' => 'array|required',
                    'sub_customers_value.*' => 'required|numeric|exists:customers,id',
                    'sub_customers_order_id' => 'required|numeric|exists:import_po,id',
                        ], $messages);

        if ($validator->passes()) {

            //delete previous values
            DB::table('customers_select')->where('order_id', '=', $request->sub_customers_order_id)->delete();

            foreach ($request->sub_customers_value as $value) {

                if ($value !== null) {
                    DB::table('customers_select')->insert(
                            ['customers_id' => $value, 'order_id' => $request->sub_customers_order_id]
                    );
                }
            }

            $db_res = DB::select("select GROUP_CONCAT(customers.`name` separator ', ') as customersNames,
                GROUP_CONCAT(customers.`id` separator ',') as customersIds 
                from (import_po)
                left join `customers_select` on `import_po`.`id` = `customers_select`.`order_id` 
                left join `customers` on `customers`.`id` = `customers_select`.`customers_id` where `import_po`.`id` = ?", [$request->sub_customers_order_id]);

            return response()->json(['success' => 'Added new records.',
                        'newValue' => $db_res[0]->customersNames,
                        'newIds' => $db_res[0]->customersIds
            ]);
        }
        return response()->json(['error' => $validator->errors()->all()]);
    }
    
    /**
     * Submit salesmen with this function
     * 
     * @param Request $request
     * @return json
     */
    public function submit_salesmen_field(Request $request) {
        
        $messages = [
            'string' => 'Invalid string.',
            'required' => 'A value not received.',
            'sub_salesmen_order_id.numeric' => 'Order ID must be numeric',
            'sub_salesmen_order_id.exists' => 'Invalid Order ID.',
            'sub_salesmen_value.exists' => 'Invalid Salesmen submitted.'
        ];

        $validator = Validator::make($request->all(), [
                    'sub_salesmen_value'    => 'array|required',
                    'sub_salesmen_value.*'  => 'required|numeric|exists:salesmen,id',
                    'sub_salesmen_order_id' => 'required|numeric|exists:import_po,id',
                        ], $messages);

        if ($validator->passes()) {

            //delete previous values
            DB::table('salesmen_select')->where('order_id', '=', $request->sub_salesmen_order_id)->delete();

            foreach ($request->sub_salesmen_value as $value) {

                if ($value !== null) {
                    DB::table('salesmen_select')->insert(
                            ['salesmen_id' => $value, 'order_id' => $request->sub_salesmen_order_id]
                    );
                }
            }

            $db_res = DB::select("select GROUP_CONCAT(salesmen.`name` separator ', ') as salesmenNames,
                GROUP_CONCAT(salesmen.`id` separator ',') as salesmenIds 
                from (import_po)
                left join `salesmen_select` on `import_po`.`id` = `salesmen_select`.`order_id` 
                left join `salesmen` on `salesmen`.`id` = `salesmen_select`.`salesmen_id` where `import_po`.`id` = ?", [$request->sub_salesmen_order_id]);

            return response()->json(['success' => 'Added new records.',
                        'newValue' => $db_res[0]->salesmenNames,
                        'newIds' => $db_res[0]->salesmenIds
            ]);
        }
        return response()->json(['error' => $validator->errors()->all()]);
    }
    
    /**
     * Submit tickets_ordered with this function
     * 
     * @param Request $request
     * @return json
     */
    public function submit_tickets_ordered_field(Request $request) {
        $messages = [
            'string' => 'Invalid string.',
            'required' => 'A value not received.',
            'submit_tickets_ordered_order_id.numeric' => 'Order ID must be numeric',
            'submit_tickets_ordered_order_id.exists' => 'Invalid Order ID. Check if tickets needed is Yes',
            'submit_tickets_ordered_value.exists' => 'Invalid Salesmen submitted.'
        ];

        $validator = Validator::make($request->all(), [
                    'submit_tickets_ordered_value' => 'required|date',
                    'submit_tickets_ordered_order_id' => 'required|numeric|exists:import_po,id,tickets_needed,1',
                        ], $messages);

        if ($validator->passes()) {

            
                    DB::table('tickets_ordered')->insert(
                            [   
                                'tickets_ordered' => $request->submit_tickets_ordered_value, 
                                'order_id' => $request->submit_tickets_ordered_order_id
                            ]
                    );

            $db_res = DB::select("select GROUP_CONCAT(tickets_ordered.`tickets_ordered` separator ', ') as tickets_orderedDates from (import_po)
                left join `tickets_ordered` on `import_po`.`id` = `tickets_ordered`.`order_id` 
                where `import_po`.`id` = ?", [$request->submit_tickets_ordered_order_id]);

            return response()->json([   'success' => 'Added new records.',
                                        'newValue' => $db_res[0]->tickets_orderedDates,
            ]);
        }
        return response()->json(['error' => $validator->errors()->all()]);
    }
    
    /**
     * Submit SVN field with this function
     * 
     * @param Request $request
     * @return json
     */
    public function submit_svn_field(Request $request) {
        $messages = [
            'required' => 'A value not received.',
            'submit_svn_order_id.numeric' => 'Order ID must be numeric',
            'submit_svn_order_id.exists' => 'Invalid Order ID. Check if tickets needed is Yes',
        ];

        $validator = Validator::make($request->all(), [
                    'submit_svn_order_id' => 'required|numeric|exists:import_po,id',
                        ], $messages);

        if ($validator->passes()) {

            $count= DB::table('import_po')
            ->whereNotNull('svn_increment')
            ->count();
            $count++;
            
            DB::table('import_po')
            ->where('id', $request->submit_svn_order_id)
            ->update(['svn_increment' => $count]);

            DB::table('import_po')
            ->where('id', $request->submit_svn_order_id)
            ->update(['svn_no' => 'SVN'.$count]); 

            return response()->json([   'success' => 'Added new records.',
                                        'newValue' => 'SVN'.$count,
            ]);
        }
        return response()->json(['error' => $validator->errors()->all()]);
    }
    

    /**
     * This function is used for ajax request to view files of that order ID
     * 
     * @param Request $request
     * @return view
     */
    public function viewFiles(Request $request){
        
        $messages = [
            'order_id.required' => 'A value not received.',
            'order_id.numeric' => 'Order ID must be numeric',
            'order_id.exists' => 'Invalid Order ID',
        ];
        
        $validator = Validator::make($request->all(), 
                [
                    'order_id' => 'required|numeric|exists:import_po,id'  
                ],
                $messages);
        
        if ($validator->passes()) {
            
            //todo get all files
            $files = DB::table('order_documents')->where('order_id', $request->order_id)->get();
            
            
            return view('ajax.view_files', ['files' => $files, 'order_id' => $request->order_id ]);
            
        }
        return response()->json(['error' => $validator->errors()->all()]);
        
    }

    public function editOrders($id) {
        
       
         
                $importPo= ImportPo::findOrFail($id);

            if ($importPo->active) {
                

                $db_res = DB::select('select `export_cos`.`name`, `export_cos`.`id`, `export_cos`.`fama` from (import_po) 
                left join `export_cos` on `import_po`.`export_cos` = `export_cos`.`id` where `import_po`.`id`  = ?',
                    [$id]);
                $tableData = $this->getAllTablesData();
                $display = \App\Http\Controllers\PermissionController::checkUserDispalyPermissions();
                return view( 'loggedin.order_edit',compact('importPo','tableData','display') );
            }else{
                return redirect('/orders/open')->withErrors('Not exists the order.');
            }
    }
    public function updateOrders(Request $request, $id)
    {
            
            $order = ImportPo::findOrFail($id);
            if ($order->active) {
            $modExportCos=false;
            $modPort=false;
            $changed_did_we_pay = false;
            $changed_finish_prod =false;
            $changed_safety_test =false;
            $changed_customer_cancel =false;
            $changed_act_eta_forwarder =false;
            $changed_licenses =false;

            if(isset($request->value)){
                $request['value']=  str_replace(',', '',$request->value);
                
            }
            if($order->export_cos!=$request->export_cos){
                $modExportCos=true;
                
            }
          

            if($order->shipping_to!=$request->shipping_to || $order->import_po_date->finish_prod!=Carbon::parse($request->finish_prod) ){
                $modPort=true;
            }
            
            $finish_prod = $request->finish_prod;
            if($finish_prod){
                if($request->safety_test && $request->safety_test==1){
                $dias_safety =Option::where('option_name','days_safety_test_on')->first();

                $dias_s=($dias_safety)? $dias_safety->option_value :20;
                $request['safety_test_on']=Carbon::parse($finish_prod)->subDay($dias_s)->format('M d Y');
                }else{
                    $request['safety_test_on']="";
                }
                if($order->import_po_date->finish_prod!=Carbon::parse($request->finish_prod)){
                    $changed_finish_prod = true;
                    $modPort = true;
                }
            
            }
            if($request->did_we_pay && $order->import_po_date->did_we_pay!=$request->did_we_pay){
                $changed_did_we_pay = true;
            }
            if($request->did_we_pay && $order->import_po_date->did_we_pay!=Carbon::parse($request->did_we_pay)){
                $changed_did_we_pay = true;
            }
            if(($request->safety_test && $order->safety_test!=$request->safety_test) || ($request->safety_test_pass && $order->safety_test_pass!=$request->safety_test_pass) ){
                $changed_safety_test =true;
            }
            if($request->get('licenses') && $order->licenses()->get()->pluck('id')->toArray() != $request->get('licenses')){
                $changed_licenses =true;
            }
            if($request->customer_cancel && $order->import_po_date->customer_cancel!=Carbon::parse($request->customer_cancel) ){
              
                $changed_customer_cancel =true;
            }
            if($request->act_eta_forwarder && $order->import_po_date->act_eta_forwarder!=Carbon::parse($request->act_eta_forwarder) ){
               
                $changed_act_eta_forwarder =true;
            }

            $this->validate($request, [     
               'po_number' => 'required|string|unique:import_po,po_number,' . $id, 
               'comments' => 'nullable|string', 
               'book_container' => 'nullable|string', 
               'customer_po_number' => 'nullable|string', 
               'david_ins_comment' => 'nullable|string',
               'value'=>'nullable|numeric',
               'fty_produc'=>'required|integer',
               'export_cos'=>'required|integer',
               'qty_ordered'=>'nullable|numeric',
               'product_styles'=>'array|required',
               'product_types'=>'array|required',
               'licenses'=>'array|required',
               'customers'=>'array|nullable',
               'salesmen'=>'array|nullable',
               'date_placed'=>'required|date',
               'tickets_received'=>'nullable|date',
               'finish_prod'=>'required|date',
               'estimated_arr'=>'nullable|date',
               'customer_cancel'=>'nullable|date',
               'act_etd_forwarder'=>'nullable|date',
               'act_eta_forwarder'=>'nullable|date',
               'safety_test_on'=>'nullable|date',
               'fty_prod_safety_test'=>'nullable|date',
               'rec_comm_inv_pking'=>'nullable|date',
               'rec_comm_inv_pking_home'=>'nullable|date',
               'original_fcr'=>'nullable|date',
               'ctpat_form'=>'nullable|date',
               'telex_release'=>'nullable|date',
               'okay_to_pay'=>'nullable|date',
               'did_we_pay'=>'nullable|date',
               'send_diann'=>'nullable|date',
               'tickets_needed'=>'nullable|integer',
               'tickets_ordered'=>'nullable|date',
               'shipping_from'=>'nullable|integer',
               'shipping_to'=>'required|integer',
               'cbm'=>'nullable|integer',
               'safety_test'=>'required|integer',
               'safety_test_pass'=>'nullable|integer',
               'david_ins_result'=>'nullable|integer',
               'freight_forwarder'=>'nullable|integer',
               'pro_photo'=>'nullable|integer',
               'receive_whse'=>'nullable|integer',
               
            ]);
           
            if(isset( $request->product_styles[0])){
    
                $this->validate($request, [     
                    'product_styles.*'    => 'required|numeric|exists:product_styles,id',
                 ]);
            }
            if(isset( $request->product_types[0])){
                
                            $this->validate($request, [     
                                'product_types.*'    => 'required|numeric|exists:product_types,id',
                             ]);
            }
            if(isset( $request["licenses"])){
                
                            $this->validate($request, [     
                                'licenses.*'    => 'required|numeric|exists:licenses,id',
                             ]);
            }
            if(isset( $request->customers[0])){
                
                            $this->validate($request, [     
                                'customers.*'    => 'required|numeric|exists:customers,id',
                             ]);
            }
            if(isset( $request->salesmen[0])){
                
                            $this->validate($request, [     
                                'salesmen.*'    => 'required|numeric|exists:salesmen,id',
                             ]);
            }
            if(isset($request->tickets_needed) && $request->tickets_needed==0){
               
                if(isset($order->ticket_ordered->id)){
                    $order->ticket_ordered()->delete();
                }              
            }elseif(isset($request->tickets_needed) && $request->tickets_needed==1){
                if(isset($request->tickets_ordered)){
                    if(isset($order->ticket_ordered->id)){
                        $order->ticket_ordered()->update([
                            'tickets_ordered'=>Carbon::parse($request->tickets_ordered)
                        ]);
                    }else{
                        $order->ticket_ordered()->create([
                            'order_id'=>$id,
                            'tickets_ordered'=>Carbon::parse($request->tickets_ordered)
                        ]);
                    }
                }else{
                    if(isset($order->ticket_ordered->id)){
                        $order->ticket_ordered()->delete();   
                    }
                }                    
            }
            if(!isset($order->svn_no)){
                $db_res1 = DB::select("SELECT `svn_no`, `id` FROM `import_po`
                where `svn_no` is not null
                order by svn_no desc");
                            
           
           
           
                            if (isset($db_res1[0]->svn_no)) {
                                $val = str_replace("SVN","",$db_res1[0]->svn_no);
                                $count = $val + 1;
                } else {$count=1;}
                
                $request["svn_no"]='SVN'.$count; 
                
            }else{
                unset($request["svn_no"]);
            }
            $input = $request->all();
            $order->update($input);

            $order->product_styles()->sync($request->get('product_styles'));
            $order->product_types()->sync($request->get('product_types'));
            
            $order->licenses()->sync($request->get('licenses'));
            $order->customers()->sync($request->get('customers'));
            $order->salesmen()->sync($request->get('salesmen'));
           
            $portsto= PortTo::find($request->shipping_to);
          
            $order->import_po_date()->update([
                'date_placed'=>($request->date_placed)?Carbon::parse($request->date_placed):null,
                'tickets_received'=>($request->tickets_received)?Carbon::parse($request->tickets_received):null,
                'finish_prod'=>($request->finish_prod)?Carbon::parse($request->finish_prod):null,
                'estimated_arr'=>($modPort)?Carbon::parse($request->finish_prod)->addDay($portsto->days_est_arrival):$order->import_po_date->estimated_arr,
                'customer_cancel'=>($request->customer_cancel)?Carbon::parse($request->customer_cancel):null,
                'act_etd_forwarder'=>($request->act_etd_forwarder)?Carbon::parse($request->act_etd_forwarder):null,
                'act_eta_forwarder'=>($request->act_eta_forwarder)?Carbon::parse($request->act_eta_forwarder):null,
                'safety_test_on'=>($request->safety_test_on)?Carbon::parse($request->safety_test_on):null,
                'fty_prod_safety_test'=>($request->fty_prod_safety_test)?Carbon::parse($request->fty_prod_safety_test):null,
                'rec_comm_inv_pking'=>($request->rec_comm_inv_pking)?Carbon::parse($request->rec_comm_inv_pking):null,
                'rec_comm_inv_pking_home'=>($request->rec_comm_inv_pking_home)?Carbon::parse($request->rec_comm_inv_pking_home):null,
                'original_fcr'=>($request->original_fcr)?Carbon::parse($request->original_fcr):null,
                'ctpat_form'=>($request->ctpat_form)?Carbon::parse($request->ctpat_form):null,
                'telex_release'=>($request->telex_release)?Carbon::parse($request->telex_release):null,
                'okay_to_pay'=>($request->okay_to_pay)?Carbon::parse($request->okay_to_pay):null,
                'did_we_pay'=>($request->did_we_pay)?Carbon::parse($request->did_we_pay):null,
                'send_diann'=>($request->send_diann)?Carbon::parse($request->send_diann):null
            ]);
            $order->save();
            
            $TriggerEvents = new TriggerEventsController();
            if( $changed_licenses){
               // se envia a production factories
                $TriggerEvents->triggerLicenseConditions($id);
            }
            if($modPort || $changed_customer_cancel ||  $changed_act_eta_forwarder){
                //Tarda
                // se envia a sales production factories
                $TriggerEvents->triggerDeliveryConditions($id);
            }
           
            
            if ($modPort)
            {
                
                $notification_controller = new NotificationsController();
                  //Se envia a production sales factories
                $notification_controller->esitimateArriavl($order->id,$order->import_po_date->estimated_arr);
                
            }
            if($changed_finish_prod || $changed_safety_test){
                //envia a production
                $TriggerEvents->triggerSafetyTestingConditions($id);
            }
             
            
                if ($changed_did_we_pay && $TriggerEvents->validateOrderCloseCondition($id)) {
                   
                    $this->closeOrder($id);
                }
                
            
            return back()->with('message', "P.O #{$order->po_number} updated successfully");
        }else{
            return back()->withErrors('Not exists the order.');
        }
    }
    public function delete_order(Request $request,$id){

        $user = Auth::user();
        
        if ($user->hasRole('Admin')) {
            $order = ImportPo::findOrFail($id);
            $order->active= 0;
            $order->save();

            if($order->open==1){
                return redirect('/orders/open')->with('message', "P.O #{$order->po_number} deleted successfully");
            }else{
                return redirect('/orders/closed')->with('message', "P.O #{$order->po_number} deleted successfully");
            }
           
        }
        return redirect('/home')->withErrors('You dont have the permission to access users permission page.');
    }
    public function move_to_closed(Request $request,$id){
        
                $user = Auth::user();
                
                if(Auth::user()->hasRole(['Admin','Production'])){
                    $order = ImportPo::findOrFail($id);
                    $order->open= 0;
                    $order->import_po_date()->update([
                        'closed'=>Carbon::now()
                    ]);
                    $order->save();
                    return redirect('/orders/open')->with('message', "P.O #{$order->po_number} moved to closed orders successfully");
                }
                return redirect('/home')->withErrors('You dont have the permission to access users permission page.');
    }
    public function move_to_open(Request $request,$id){
        
                $user = Auth::user();
                
                if ($user->hasRole('Admin')) {
                    $order = ImportPo::findOrFail($id);
                    $order->open= 1;
                    $order->save();
                    return redirect('/orders/open')->with('message', "P.O #{$order->po_number} moved to open orders successfully");
                }
                return redirect('/home')->withErrors('You dont have the permission to access users permission page.');
    }

    public function createOrder() {
        if(Auth::user()->hasRole(['Admin','Production'])){
            $tableData = $this->getAllTablesData();
            return view( 'loggedin.order_create',compact('tableData'));
        }
    
        return redirect('/home')->withErrors('You dont have the permission to access users permission page.');
       

    }
    public function storeOrder(Request $request)
    {
        if(Auth::user()->hasRole(['Admin','Production'])){
            if(isset($request->value)){
            $request['value']=  str_replace(',', '',$request->value);
            
            }
            $finish_prod = $request->finish_prod;
            if($finish_prod){
                if($request->safety_test && $request->safety_test==1){
                $dias_safety =Option::where('option_name','days_safety_test_on')->first();
                $dias_s=($dias_safety)? $dias_safety->option_value :20;
                $request['safety_test_on']=Carbon::parse($finish_prod)->subDay($dias_s)->format('M d Y');
                }else{
                    $request['safety_test_on']="";
                }
            }

            $this->validate($request, [     
               'po_number' => 'required|string|unique:import_po', 
               'comments' => 'nullable|string', 
               'value'=>'nullable|numeric',
               'fty_produc'=>'required|integer',
               'export_cos'=>'required|integer',
               'qty_ordered'=>'nullable|numeric',
               'product_styles'=>'array|required',
               'product_types'=>'array|required',
               'licenses'=>'array|required',
               'customers'=>'array|nullable',
               'salesmen'=>'array|nullable',
               'date_placed'=>'required|date',
               'finish_prod'=>'required|date',
              // 'estimated_arr'=>'required|date',
               'customer_cancel'=>'nullable|date',
               'safety_test_on'=>'nullable|date',
               'tickets_needed'=>'nullable|integer',
               'tickets_ordered'=>'nullable|date',
               'shipping_from'=>'nullable|integer',
               'shipping_to'=>'required|integer',
               'safety_test'=>'required|integer',
               'pro_photo'=>'nullable|integer',
            ]);

            /*$export_cos=ExportCos::find($request->export_cos);
            if(!$export_cos->fama){
                $famas=License::Sifama()->get()->pluck('id')->toArray();
             
                $message = [
                    'not_in'=> "The :attribute require fama"
                ];
                $this->validate($request,[
                    'licenses.*'=>[Rule::notIn($famas)]
                ],$message);
            }*/
            $order=ImportPo::create([
                'po_number' => $request->po_number
            ]);
            $order_date= new ImportPoDate();
            $order_date->order_po_id=$order->id;
            $order_date->save();
           
            $input = $request->all();
            
            $order->update($input);

            $order->product_styles()->sync($request->get('product_styles'));
            $order->product_types()->sync($request->get('product_types'));
            $order->licenses()->sync($request->get('licenses'));
            $order->customers()->sync($request->get('customers'));
            $order->salesmen()->sync($request->get('salesmen'));
            $portsto= PortTo::find($request->shipping_to);
           
            $order->import_po_date()->update([
                'date_placed'=>($request->date_placed)?Carbon::parse($request->date_placed):null,
                'estimated_arr'=>Carbon::parse($request->finish_prod)->addDay($portsto->days_est_arrival),
                'customer_cancel'=>($request->customer_cancel)?Carbon::parse($request->customer_cancel):null,
                'safety_test_on'=>($request->safety_test_on)?Carbon::parse($request->safety_test_on):null,
                'finish_prod'=>($request->finish_prod)?Carbon::parse($request->finish_prod):null
                  ]);
           

                  $order->save();
                  
                  $TriggerEvents = new TriggerEventsController();
                  //Este trigger compara si fama es requiere o no y envia correos
                  $TriggerEvents->triggerLicenseConditions($order->id);
                  $TriggerEvents->triggerDeliveryConditions($order->id);
                  //$this->populateEstimatedArrOnEditingFinishProd($order->id);
                  
                  if ($order->import_po_date->finish_prod != null)
                  {
                  
                    
                      $notification_controller = new NotificationsController();
                      $notification_controller->esitimateArriavl($order->id,$order->import_po_date->estimated_arr);
                  }
                  
                  $TriggerEvents->triggerSafetyTestingConditions($order->id); 
                  
                    /*  if ($TriggerEvents->validateOrderCloseCondition($order->id)) {
                          
                          $this->closeOrder($order->id);
                      }*/
                    
                return redirect('/orders/open')->with('message', "P.O #{$order->po_number} created successfully");
            }
            
                return redirect('/home')->withErrors('You dont have the permission to access users permission page.');
            }
    
    public function obtener_fama(Request $request,$id)
    {
        $export=ExportCos::find($id);
        
        return ($export->fama)?'Yes':'No';
    }
    public function obtener_arrival(Request $request)
    {
        $port_to = $request->query('port_to');
        $finish_prod = $request->query('finish_prod');
        if($port_to && $finish_prod){
            $portsto= PortTo::find($port_to);
            $fecha=Carbon::parse($finish_prod)->addDay($portsto->days_est_arrival)->format('M d Y');
                return $fecha;
        }
        return null;
    }
    public function obtener_test_on(Request $request)
    {
        $finish_prod = $request->query('finish_prod');
        if($finish_prod){
            $dias_safety =Option::where('option_name','days_safety_test_on')->first();
            $dias_s=($dias_safety)? $dias_safety->option_value :20;
        
            $fecha=Carbon::parse($finish_prod)->subDay($dias_s)->format('M d Y');
                return $fecha;
        }
        return null;
    }

    public function validarLicencias(Request $request)
    {
        if($request->export_cos){
            $export_cos=ExportCos::find($request->export_cos);
            if(!$export_cos->fama){
                $famas=License::Sifama()->get()->pluck('id')->toArray();
            
                $message = [
                    'not_in'=> "The licenses require fama"
                ];
            
                $validator = Validator::make($request->all(), [
                    'licenses.*'=>[Rule::notIn($famas)]
                ],$message);
        
                if (!$validator->passes()) {
        
                    return response()->json(['error'=>$validator->errors()->all()]);
                }   
            }
        }

        return response()->json(['success'=>'OK.']);
    }
    public function validarCBM(Request $request)
    {

        

        if($request->finish_prod){
            $validator = Validator::make($request->all(), [
                'cbm'=>['integer',new FinishCbm($request["finish_prod"])]
            ]);
    
            if (!$validator->passes()) {
                return response()->json(['error'=>$validator->errors()->all()]);
            }      
        }
        return response()->json(['success'=>'OK.']);
    }
    public function validarETD(Request $request)
    {



       
            $validator = Validator::make($request->all(), [
                'act_etd_forwarder'=>[new ActEtd()],
                //'rec_comm_inv_pking' =>'required',
                //'rec_comm_inv_pking_home' =>'required',
                //'org_lacey' =>'required',
                //'ctpat_form' =>'required',
                //'telex_release' =>'required'
            ]);
    
            if (!$validator->passes()) {
                return response()->json(['error'=>$validator->errors()->all()]);
            }      
        
        return response()->json(['success'=>'OK.']);
    }

    public function validar_customer_cancel(Request $request)
    {
        $port_to = $request->query('port_to');
        $finish_prod = $request->query('finish_prod');
        $customer_cancel = $request->query('customer_cancel');
        if($port_to && $finish_prod && $customer_cancel){
            $portsto= PortTo::find($port_to);
            $fecha=Carbon::parse($finish_prod)->addDay($portsto->days_est_arrival+10);
            $customer_cancel = Carbon::parse($customer_cancel);

            if($customer_cancel>$fecha){
                return 1;
            }
            
        }
        return 0;
    }
    public function validarCustomers(Request $request)
    {

        
        $cah= false;
        $chl= false;
        if($request->customers){
            $atHome= Customer::where('code','AH')->first();
            $hobbyLobby= Customer::where('code','HL')->first();
            $coll= collect($request->customers);
            if($coll->contains($atHome->id)){
                $cah= true;
            }
            if($coll->contains($hobbyLobby->id)){
                $chl= true;
            }
                 
        }
        if($cah && !$chl){
            return response()->json(['success'=>1]);
        }elseif(!$cah && $chl){
            return response()->json(['success'=>2]);
        }elseif($cah && $chl){
            return response()->json(['success'=>3]);
        }
        return response()->json(['success'=>0]);
    }
   
}
