<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
use DB;
use Log;
use Illuminate\Validation\Rule;

class FreightForwarderController extends Controller {

    /**
     * Construct method makes sure that all pages here are accessed by 
     * registered users by using the auth middleware
     */
    public function __construct() {
        $this->middleware('auth');
        if (Auth::check()) {
            
        } else {
            return redirect('login');
        }
    }

    /**
     * Function to display table management page
     * 
     * @param Request $request
     * @return view
     */
    public function index(Request $request) {
        $user = Auth::user();
        
        if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
            
            return redirect('/home')->withErrors("Only Admin can access these settings.");
            
        }

      
        $freight_forwarders = $this->freight_forwardersTable($request);
     
        $vista=view('freight_forwarders.index', 
        [
            'freight_forwarders' => $freight_forwarders,
           
            
        ]);
        if ($request->isMethod('post')) {
            $request->session()->flash('message', 'successful!');
        
        }
        return $vista;
    }
    
    
    /**
     * This function manages CRUD for freight_forwarders Table
     * 
     * @param object $request
     * @return types
     */
    private function freight_forwardersTable($request){
        
        if(isset($request)) {
            
        //check if request has search freight_forwarders
            
            if ($request->search_freight_forwarders) {
                
                return $freight_forwarders = DB::table('freight_forwarders')->select('*')->where('name', 'like', "%$request->search_freight_forwarders%")->orderBy('id', 'desc')->paginate(10, ['*'], 'freight_forwarders');
            }
        
        //Edit
            if ($request->table == 'freight_forwarders' and $request->action  == 'edit_freight_forwarders') {
                
                $this->validate($request, [
                'name'          => 'required|string',
                'edit_id'       =>  'exists:freight_forwarders,id',
                ]);
                
                DB::table('freight_forwarders')->where('id', $request->edit_id)->update(['name' => $request->name]);

            }
        
        //Add
            if ($request->table == 'freight_forwarders' and $request->action  == 'add_freight_forwarders') {
            
                $this->validate($request, [
                    'name'          => 'required|string|unique:freight_forwarders,name',
                    'edit_id'       => 'exists:freight_forwarders,id',
                ]);
                
                DB::table('freight_forwarders')->insert(['name' => $request->name]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'freight_forwarders' and $request->action  == 'delete_freight_forwarders') {
            
                $this->validate($request, [
                    'delete_freight_forwarder_id'       => 'exists:freight_forwarders,id',
                ]);
                
                DB::table('freight_forwarders')->where('id', $request->delete_freight_forwarder_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'freight_forwarders' and $request->action  == 'activate_freight_forwarders') {
            
                $this->validate($request, [
                    'activate_freight_forwarder_id'       => 'exists:freight_forwarders,id',
                ]);
                
                DB::table('freight_forwarders')->where('id', $request->activate_freight_forwarder_id)->update(['active' => 1]);
         
            }
            
            
        }
        
        return $freight_forwarders = DB::table('freight_forwarders')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'freight_forwarders');
    }
    
    
}
