<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Factory;
use Carbon\Carbon;
use DB;
use Log;
use Illuminate\Validation\Rule;

class FactoryController extends Controller {

    /**
     * Construct method makes sure that all pages here are accessed by 
     * registered users by using the auth middleware
     */
    public function __construct() {
        $this->middleware('auth');
        if (Auth::check()) {
            
        } else {
            return redirect('login');
        }
    }

    /**
     * Function to display table management page
     * 
     * @param Request $request
     * @return view
     */
    public function index(Request $request) {
        $user = Auth::user();
        
        if (!$user->hasRole('Admin')) {//verifing that the user accessing this page is admin     
            
            return redirect('/home')->withErrors("Only Admin can access these settings.");
            
        }

        $factories = $this->factoriesTable($request);
      
       
        $vista=view('factories.index', 
        ['factories'=> $factories]);
        
        if ($request->isMethod('post')) {
            $request->session()->flash('message', 'successful!');
        
        }
        return $vista;
    }
    

    
    /**
     * This function manages CRUD for factories Table
     * 
     * @param object $request
     * @return types
     */
    private function factoriesTable($request){
        
        if(isset($request)) {
            
        //check if request has search $factories
            
            if ($request->search_factories) {
                
                return $factories = DB::table('factories')->select('*')->where('name', 'like', "%$request->search_factories%")->orderBy('id', 'desc')->paginate(10, ['*'], 'factories');
            }
        
        //Edit
            if ($request->table == 'factories' and $request->action  == 'edit_factories') {
                
                $this->validate($request, [
                'name'          => 'required|string',
                'vendor_id'     => 'required|string',
                'edit_id'       =>  'exists:factories,id',
                'full_name'     => 'nullable|string',
                'address1'     => 'nullable|string',
                'address2'     => 'nullable|string',
                'city'     => 'nullable|string',
                'province'     => 'nullable|string',
                'zip'     => 'nullable|string',
                'country'     => 'nullable|string',
                'phone'     => 'nullable|string',
                'bank_address'     => 'nullable|string',
                'bank_name'     => 'nullable|string',
                'bank_swift'     => 'nullable|string',
                'usd_bank_acct'     => 'nullable|string',
                ]);
                
                DB::table('factories')->where('id', $request->edit_id)->update([
                    'name' => $request->name, 
                    'vendor_id' => $request->vendor_id,
                    'full_name'     => $request->full_name,
                    'address1'     => $request->address1,
                    'address2'     => $request->address2,
                    'city'     => $request->city,
                    'province'     => $request->province,
                    'zip'     => $request->zip,
                    'country'     => $request->country,
                    'phone'     => $request->phone,
                    'bank_address'     => $request->bank_address,
                    'bank_name'     => $request->bank_name,
                    'bank_swift'     => $request->bank_swift,
                    'usd_bank_acct'     => $request->usd_bank_acct,
                    ]);

            }
        
        //Add
            if ($request->table == 'factories' and $request->action  == 'add_factories') {
            
                $this->validate($request, [
                    'name'          => 'required|string|unique:factories,name',
                    'vendor_id'     => 'required|string',
                    'edit_id'       => 'exists:factories,id',
                    'full_name'     => 'nullable|string',
                    'address1'     => 'nullable|string',
                    'address2'     => 'nullable|string',
                    'city'     => 'nullable|string',
                    'province'     => 'nullable|string',
                    'zip'     => 'nullable|string',
                    'country'     => 'nullable|string',
                    'phone'     => 'nullable|string',
                    'bank_address'     => 'nullable|string',
                    'bank_name'     => 'nullable|string',
                    'bank_swift'     => 'nullable|string',
                    'usd_bank_acct'     => 'nullable|string',
                ]);

                DB::table('factories')->insert([
                    'name' => $request->name, 
                    'vendor_id' => $request->vendor_id,
                    'full_name'     => $request->full_name,
                    'address1'     => $request->address1,
                    'address2'     => $request->address2,
                    'city'     => $request->city,
                    'province'     => $request->province,
                    'zip'     => $request->zip,
                    'country'     => $request->country,
                    'phone'     => $request->phone,
                    'bank_address'     => $request->bank_address,
                    'bank_name'     => $request->bank_name,
                    'bank_swift'     => $request->bank_swift,
                    'usd_bank_acct'     => $request->usd_bank_acct,
                    ]);
         
            }
            
        //Delete OR Deactivate
            if ($request->table == 'factories' and $request->action  == 'delete_factories') {
            
                $this->validate($request, [
                    'delete_factory_id'       => 'exists:factories,id',
                ]);
                
                DB::table('factories')->where('id', $request->delete_factory_id)->update(['active' => 0]);
         
            }
            
        //ReActivate
            if ($request->table == 'factories' and $request->action  == 'activate_factories') {
            
                $this->validate($request, [
                    'activate_factory_id'       => 'exists:factories,id',
                ]);
                
                DB::table('factories')->where('id', $request->activate_factory_id)->update(['active' => 1]);
         
            }
            
            
        }
        
        return $factories = DB::table('factories')->select('*')->orderBy('id', 'desc')->paginate(10, ['*'], 'factories');
    }
    
    
    
    
}
