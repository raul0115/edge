<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

    
/**
 * In the current system we have one role per user, hence the following 
 * functions can work.
 * 
 * Best if the no of rows on col_definations table match 
 * no of columns of userlevel_rights table
 * 
 * Since, col_definations rows (col_name) would be columns in userlevel_rights 
 * table
 * 
 */
class PermissionController extends Controller
{
    
    /**
     * IMPORTANT: In the current system we have one role per user. Hence 
     * col_definations table rows should match no of columns of userlevel_rights
     * table
     * 
     * @return type
     */
    public static function checkUserEditPermissions(){
        
        
        $user = Auth::user();
        
        $roles = $user->getRoleNames();
        $userRole = $roles[0]; //the role name e.g. Admin or QC etc.
        
        $data = self::getEditPermissionsArray($user->id, $userRole);
        
        return $data;
        
        //return view('loggedin.test', ['data' => $data]);
    }
    
    /**
     * Get the array for editing rights for the user
     * 
     * @param int $user_id
     * @param string $role E.g. QC, Admin, Production etc.
     * 
     * @return array
     */
    private static function getEditPermissionsArray($user_id, $role){
        $convert= strtolower($role);
        $role= str_replace(" ", "_", $convert);
        $role= ucfirst($role);
        $data=[];
        
        //fetch the necessary tables
        $col_definations    = DB::table('col_definations')->get();
        $userlevel_rights   = DB::table('userlevel_rights')->where('user_id', $user_id)->get();
        
        foreach ($col_definations as $col_defination) {
            
            // The name of the current column in the loop
            $col_name = $col_defination->col_name; 
            
            //user role level value for that field e.g. $col_defination->QC = 1 for a given $col_defination->col_name
            $roleLevelValue = $col_defination->$role;
            
            //this checks if userlevel_rights table has the column called $col_name
            if(isset($userlevel_rights[0]->$col_name)){
                
                //user field level value 1 or 0
                $userLevelValue = $userlevel_rights[0]->$col_name;
            
                if($roleLevelValue==1 and $userLevelValue==1){
                    $data[$col_name] = 1;//User have the permission
                }
                else {
                    $data[$col_name] = 0;//User donot have the permission
                }
            }
            
        }
        
        return $data;
    }
    
    
    /**
     * IMPORTANT: In the current system we have one role per user. Hence 
     * col_definations table rows should match no of columns of userlevel_rights
     * and user_display_rights table
     * 
     * @return type
     */
    public static function checkUserDispalyPermissions(){
        
        
        $user = Auth::user();
        
        $roles = $user->getRoleNames();
        $userRole = $roles[0]; //the role name e.g. Admin or QC etc.
        
        $data = self::getDispalyPermissionsArray($user->id, $userRole);
        
        return $data;
        //return view('loggedin.test', ['data' => $data]);
    }
    public static function checkUserDispalyPermissionsList(){
        
        
        $user = Auth::user();
        
        $roles = $user->getRoleNames();
        $userRole = $roles[0]; //the role name e.g. Admin or QC etc.
        
        $data = self::getDispalyPermissionsArrayList($user->id, $userRole);
        
        return $data;
        //return view('loggedin.test', ['data' => $data]);
    }
    
    
    
    /**
     * Get the array for editing rights for the user
     * 
     * @param int $user_id
     * @param string $role E.g. QC, Admin, Production etc.
     * 
     * @return array
     */
    private static function getDispalyPermissionsArray($user_id, $role){
        
        $data=[];
        
        //fetch the necessary tables
        $col_definations    = DB::table('col_definations')->get();
        $userlevel_rights   = DB::table('userlevel_rights')->where('user_id', $user_id)->get();
        $user_display_rights   = DB::table('user_display_rights')->where('user_id', $user_id)->get();
        
        foreach ($col_definations as $col_defination) {
            
            // The name of the current column in the loop
            $col_name = $col_defination->col_name; 
            
            //user role level value for that field e.g. $col_defination->QC = 1 for a given $col_defination->col_name
            $roleLevelValue = $col_defination->$role;
            
            //this checks if userlevel_rights table has the column called $col_name
            if(isset($userlevel_rights[0]->$col_name) and isset($user_display_rights[0]->$col_name)){
                
                //user field level value 1 or 0
                $userLevelValue = $userlevel_rights[0]->$col_name;
                 $displayLevelValue = $user_display_rights[0]->$col_name;
            
                if($roleLevelValue==1 and $userLevelValue==1 and $displayLevelValue==1){
                    $data[$col_name] = 1;//User have the permission
                }
                else {
                    $data[$col_name] = 0;//User donot have the permission
                }
            }
            
        }
        
        return $data;
    }
     /**
     * Get the array for editing rights for the user
     * 
     * @param int $user_id
     * @param string $role E.g. QC, Admin, Production etc.
     * 
     * @return array
     */
    private static function getDispalyPermissionsArrayList($user_id, $role){
        $convert= strtolower($role);
        $role= str_replace(" ", "_", $convert);
        $role= ucfirst($role);
        $data=[];
      
        //fetch the necessary tables
        $col_definations    = DB::table('col_definations')
                            ->where('List',1)->orderBy('Order','asc')->get();
        $userlevel_rights   = DB::table('userlevel_rights')->where('user_id', $user_id)->get();
        $user_display_rights   = DB::table('user_display_rights')->where('user_id', $user_id)->get();
        $cant=0;
        foreach ($col_definations as $col_defination) {
            $cant = $cant+1;
            // The name of the current column in the loop
            $col_name = $col_defination->col_name; 
            
            //user role level value for that field e.g. $col_defination->QC = 1 for a given $col_defination->col_name
            $roleLevelValue = $col_defination->$role;
           
            //this checks if userlevel_rights table has the column called $col_name
            if(isset($userlevel_rights[0]->$col_name) and isset($user_display_rights[0]->$col_name)){
                
                //user field level value 1 or 0
                $userLevelValue = $userlevel_rights[0]->$col_name;
                 $displayLevelValue = $user_display_rights[0]->$col_name;
            
                if($roleLevelValue==1 and $userLevelValue==1 and $displayLevelValue==1){
                    $data[$cant] = $col_name;//User have the permission
                    if($col_name=="export_cos"){
                        $cant = $cant+1;
                        $data[$cant] = 'fama';
                    }
                }
                
            }
            
        }
        
        return $data;
    }
    public static function getDisplayLetras($cantidad){
        
        $data=[];
        for ($i = 1; $i <= $cantidad; $i++) {
            $valor="";
            switch($i){
                case 1:
                    $valor="A";
                    break;
                case 2:
                    $valor="B";
                    break;
                case 3:
                    $valor="C";
                    break;
                case 4:
                    $valor="D";
                    break;
                case 5:
                    $valor="E";
                    break;
                case 6:
                    $valor="F";
                break;
                case 7:
                    $valor="G";
                break;
                case 8:
                    $valor="H";
                break;
                case 9:
                    $valor="I";
                break;
                case 10:
                    $valor="J";
                break;
                case 11:
                    $valor="K";
                break;
                case 12:
                    $valor="L";
                break;
                case 13:
                    $valor="M";
                break;
                case 14:
                    $valor="N";
                break;
                case 15:
                    $valor="O";
                break;
                case 16:
                    $valor="P";
                break;
                case 17:
                    $valor="Q";
                break;
                case 18:
                    $valor="R";
                break;
                case 19:
                    $valor="S";
                break;
                case 20:
                    $valor="T";
                break;
                case 21:
                    $valor="U";
                break;
                case 22:
                    $valor="V";
                break;
                case 23:
                    $valor="W";
                break;
                case 24:
                    $valor="X";
                break;
                case 25:
                    $valor="Y";
                break;
                case 26:
                    $valor="Z";
                break;
                case 27:
                    $valor="AA";
                break;
                case 28:
                    $valor="AB";
                break;
                case 29:
                    $valor="AC";
                break;
                case 30:
                    $valor="AD";
                break;
                case 31:
                    $valor="AE";
                break;
                case 32:
                    $valor="AF";
                break;
                case 33:
                    $valor="AG";
                break;
                case 34:
                    $valor="AH";
                break;
                case 35:
                    $valor="AI";
                break;
                case 36:
                    $valor="AJ";
                break;
                case 37:
                    $valor="AK";
                break;
                case 38:
                    $valor="AL";
                break;
                case 39:
                    $valor="AM";
                break;
                case 40:
                    $valor="AN";
                break;
                case 41:
                    $valor="AO";
                break;
                case 42:
                    $valor="AP";
                break;
                case 43:
                    $valor="AQ";
                break;
                case 44:
                    $valor="AR";
                break;
                case 45:
                    $valor="AS";
                break;
                case 46:
                    $valor="AT";
                break;
                case 47:
                    $valor="AU";
                break;
                default: break;
            }
           $data[$i]=$valor; 
        }
        return $data;
    }
}
