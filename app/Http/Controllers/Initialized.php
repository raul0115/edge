<?php
namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Auth;
/**
 * Description of initialized
 *
 * @author Ultimate 1
 */
class Initialized extends Controller {
    
    /**
     * This function is currently used for installing a new website, creates 
     * admin roles for the first user 
     *  
     * @return view
     */
    public function index()
	{
            $this->createAdminRoles();
            $user = Auth::user();
             return view('layouts/app', ['roles' => $user ]);
	}
        
     /**
     * This function create the role and permissions if they do not exist
     * This is the install function for a new website, or the first user
     */
    private function createAdminRoles(){
         $r1 = DB::select('select `name` from `roles` where name = "Admin"');
         if(!$r1) {
                    Role::create(['name' => 'Production']);
                    Role::create(['name' => 'QC']);
                    Role::create(['name' => 'Sales']);
            $role = Role::create(['name' => 'Factory']);
                    Role::create(['name' => 'Admin']);
            
         }
         $user = Auth::user();
         $user->assignRole('Admin');
    }
}
