<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ExportCos extends Model
{
    
    public $timestamps = false;
    protected $table = 'export_cos';
    protected $dates = 
    [
        'fama_exp_date'
    ];
    protected $fillable = [
        'fama',
        'name',
        'active',
        'fama_exp_date'
    ];
    
}