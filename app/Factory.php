<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factory extends Model
{
  
    protected $table = 'factories';
    protected $fillable = [
        'name',
        'vendedor_id',
        'full_name',
        'address1',
        'address2',
        'city',
        'province',
        'zip',
        'country',
        'phone',
        'bank_name',
        'bank_adrress',
        'bank_swift',
        'usd_bank_acct',
    ];
    
}