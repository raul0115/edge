<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
  
    protected $table = 'customers';
    protected $fillable = [
        'name',
        'code',
        'active',
    ];
    public $timestamps = false;
    public function import_pos()
    {
        return $this->belongsToMany('App\ImportPo', 'customers_select','customers_id','order_id');
    }
    
}